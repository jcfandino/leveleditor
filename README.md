# M8 - Level Editor 
[![Build status](https://gitlab.com/jcfandino/leveleditor/badges/master/pipeline.svg)](https://gitlab.com/jcfandino/leveleditor/commits/master)

Simple level editor for jME3 based on Doom and Duke Nukem 3D editors workflow.

Requires java 17+

**Download latest version from** [Releases](https://gitlab.com/jcfandino/leveleditor/-/releases)

**Run**: ```java -jar app/target/m8-level-editor.jar```

**Development**

Build: ```./mvnw clean verify```

Run single test: ```./mvnw test -Dsuites='com.stovokor.editor.spec.DrawSectorSpec'```

