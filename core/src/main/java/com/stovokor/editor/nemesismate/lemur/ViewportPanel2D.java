/*
BSD 3-Clause License

Copyright (c) 2016, NemesisMate
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.stovokor.editor.nemesismate.lemur;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.simsilica.lemur.style.ElementId;

/**
 *
 * @author xelun
 */
public class ViewportPanel2D extends ViewportPanel {
    protected Node rootNode = new Node("Absolute Node");

    public ViewportPanel2D(AppStateManager stateManager, ElementId elementid, String style) {
        super(stateManager, elementid, style);
        viewPortNode.attachChild(rootNode);
        autoZoom = false;
    }

    public ViewportPanel2D(ElementId elementid, String style) {
        super(null, elementid, style);
    }

    @Override
    public void setCam(Camera cam) {
        super.setCam(cam);

        
        this.cam.setLocation(Vector3f.ZERO);
        this.cam.setParallelProjection(true);
    }


    @Override
    protected void updatePerspective(Vector3f size) { }

    @Override
    protected void setViewPortSize(Vector3f size) {
        if(viewport == null) {
            return;
        }

        super.setViewPortSize(size);

        //TODO: Wouldn't it work if instead of the rootNode, the viewportNode was used? (avoiding instanceof checking on recalculate real size on ViewportPanel)
        Camera camera = viewport.getCamera();
        rootNode.setLocalTranslation(camera.getFrustumLeft(), camera.getFrustumTop(), -10f);
        rootNode.setLocalScale((camera.getFrustumRight() * 2) / size.x, (camera.getFrustumTop() * 2) / size.y, 1);
    }

    @Override
    public Node getViewportNode() {
        return rootNode;
    }
}