/*
BSD 3-Clause License

Copyright (c) 2016, NemesisMate
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.stovokor.editor.nemesismate.lemur;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import org.slf4j.LoggerFactory;

public abstract class ScenegraphListenerControl extends AbstractControl {

    protected ScenegraphAutoRemoverChecker checker;
    protected Spatial rootParent;

    public abstract void onAttached();
    public abstract void onDetached();

    public final void detach() {
        // Get sure that the checker is removed (eg: externally called)
        removeChecker();
        onDetached();

        checker = null;
    }

    public boolean isAttached() {
        return checker != null;
    }

    public Node getRootNode() {
        return checker != null ? (Node) checker.getSpatial() : null;
    }

    @Override
    protected void controlUpdate(float tpf) {
        if(checker != null) {
            checker.check();
            return;
        }


        rootParent = SpatialUtil.getRootFor(spatial);

        if (rootParent != null) {
//            ViewportPanel.this.rootNode = rootParent;
            checker = createChecker();
            rootParent.addControl(checker);
            onAttached();
        }
    }

    protected ScenegraphAutoRemoverChecker createChecker() {
        return new ScenegraphAutoRemoverChecker();
    }

    @Override
    public final void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);

        // If the control is removed because it is unwanted
        if(this.spatial == null && checker != null) {
            removeChecker();
        }
    }

    private void removeChecker() {
        // To avoid the warning
        checker.flagged = true;

        Spatial checkerSpatial = checker.getSpatial();
        if(checkerSpatial != null) {
            checkerSpatial.removeControl(checker);
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }

    public class ScenegraphAutoRemoverChecker extends AbstractControl {

        boolean flagged;

        public void check() {
            flagged = false;
        }

//            public abstract void onClose();

        @Override
        public void setSpatial(Spatial spatial) {
            if(spatial == null) {
                if(!flagged) {
                    LoggerFactory.getLogger(this.getClass()).warn("Shouldn't be removing this control manually!");
//                    this.spatial.addControl(this); // Re-add??, or just better let the developer see the problem.
                }
                // Not closing here because of this way the developer sees if there is something wrong.
//                    close();
            }

            super.setSpatial(spatial);
        }

        @Override
        protected void controlUpdate(float tpf) {
            if(flagged) {
                detach();
            } else {
                flagged = true;
            }
        }

        public final void detach() {
            ScenegraphListenerControl.this.detach();

            // Actually the SpatialAutoManager.detach() already does this removal
//            spatial.removeControl(this);
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }
    }
}