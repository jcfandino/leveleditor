package com.stovokor.editor.builder

import com.stovokor.editor.model.Point
import com.stovokor.editor.model.PolygonBuilder
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.service.SectorService

object SectorBuilder {
  def start(p: Point) = new SectorBuilder(PolygonBuilder.start(p), List())
  def start(p: Point, id: Long) =
    new SectorBuilder(PolygonBuilder.start(p), List(id))
}

class SectorBuilder(
    val polygonBuilder: PolygonBuilder,
    val neighbours: List[Long]
) {

  def first = polygonBuilder.first
  def last = polygonBuilder.last
  def size = polygonBuilder.size
  def lines = polygonBuilder.lines
  def points = polygonBuilder.points

  def add(id: Long) = new SectorBuilder(polygonBuilder, neighbours ++ List(id))
  def add(p: Point) = new SectorBuilder(polygonBuilder.add(p), neighbours)

  def sectorRepo = SectorRepository()
  def borderRepo = BorderRepository()
  def sectorService = SectorService()

  /** do first and last points touch the same sector? */
  def isLoop() = {
    if (size > 1) {
      def closed = size > 2 && first == last
      def sectorsFirst = sectorRepo.findByPoint(first).map(_._1)
      def sectorsLast = sectorRepo.findByPoint(last).map(_._1)
      def isHole = sectorService.isCuttingHole(points)
      closed || (!sectorsFirst.intersect(sectorsLast).isEmpty && !isHole)
    } else false
  }

  def removeLast: Option[SectorBuilder] =
    if (size < 2) None
    else
      Some(
        new SectorBuilder(
          new PolygonBuilder(polygonBuilder.points.init),
          neighbours
        )
      )

}
