package com.stovokor.editor.input

import com.simsilica.lemur.input.InputMapper
import com.simsilica.lemur.input.FunctionId
import com.jme3.input.KeyInput
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.Axis
import com.stovokor.util.LemurExtensions._
import com.simsilica.lemur.input.Button

object InputFunctionsMapper {

  def initialize(inputMapper: InputMapper): Unit = {
    inputMapper.mapKey(InputFunction.moveX, KeyInput.KEY_LEFT, state = false)
    inputMapper.mapKey(InputFunction.moveX, KeyInput.KEY_A, state = false)
    inputMapper.mapKey(InputFunction.moveX, KeyInput.KEY_RIGHT)
    inputMapper.mapKey(InputFunction.moveX, KeyInput.KEY_D)
    inputMapper.mapKey(InputFunction.moveY, KeyInput.KEY_UP)
    inputMapper.mapKey(InputFunction.moveY, KeyInput.KEY_W)
    inputMapper.mapKey(InputFunction.moveY, KeyInput.KEY_DOWN, state = false)
    inputMapper.mapKey(InputFunction.moveY, KeyInput.KEY_S, state = false)
    inputMapper.mapKey(InputFunction.moveZ, KeyInput.KEY_PGDN, state = false)
    inputMapper.mapKey(
      InputFunction.moveZ,
      KeyInput.KEY_RBRACKET,
      state = false
    )
    inputMapper.mapKey(InputFunction.moveZ, KeyInput.KEY_PGUP)
    inputMapper.mapKey(InputFunction.moveZ, KeyInput.KEY_LBRACKET)

    inputMapper.map(InputFunction.mouseX, Axis.MOUSE_X)
    inputMapper.map(InputFunction.mouseY, Axis.MOUSE_Y)
    inputMapper.mapWheel(InputFunction.mouseWheel)
    inputMapper.mapWheel(InputFunction.mouseWheelShift, shift = true)

    inputMapper.map(InputFunction.clickKey, Button.MOUSE_BUTTON1)

    inputMapper.mapKey(InputFunction.cancel, KeyInput.KEY_ESCAPE)
    inputMapper.mapKey(InputFunction.revert, KeyInput.KEY_BACK)
    inputMapper.mapKey(InputFunction.resizeGridBigger, KeyInput.KEY_G)
    inputMapper.mapKey(
      InputFunction.resizeGridSmaller,
      KeyInput.KEY_G,
      shift = true
    )
    inputMapper.mapKey(InputFunction.snapToGrid, KeyInput.KEY_G, ctrl = true)
    inputMapper.mapKey(InputFunction.switchViewMode, KeyInput.KEY_TAB)
    inputMapper.mapKey(InputFunction.settings, KeyInput.KEY_F12)
    inputMapper.mapKey(InputFunction.insertMode, KeyInput.KEY_I)
    inputMapper.mapKey(InputFunction.clickKey, KeyInput.KEY_SPACE)
    inputMapper.mapKey(InputFunction.selectPoints, KeyInput.KEY_1)
    inputMapper.mapKey(InputFunction.selectLines, KeyInput.KEY_2)
    inputMapper.mapKey(InputFunction.selectSectors, KeyInput.KEY_3)
    inputMapper.mapKey(InputFunction.split, KeyInput.KEY_PERIOD, ctrl = true)
    inputMapper.mapKey(InputFunction.settings, KeyInput.KEY_F12)
    inputMapper.mapKey(InputFunction.exit, KeyInput.KEY_Q, ctrl = true)
    inputMapper.mapKey(InputFunction.help, KeyInput.KEY_F1)
    inputMapper.mapKey(InputFunction.test1, KeyInput.KEY_F7)
    inputMapper.mapKey(InputFunction.test2, KeyInput.KEY_F2)
    inputMapper.mapKey(InputFunction.test3, KeyInput.KEY_F3)
    inputMapper.mapKey(InputFunction.test4, KeyInput.KEY_F4)
    inputMapper.mapKey(InputFunction.toggleStats, KeyInput.KEY_F11)

    inputMapper.mapKey(InputFunction.newFile, KeyInput.KEY_N, ctrl = true)
    inputMapper.mapKey(InputFunction.open, KeyInput.KEY_O, ctrl = true)
    inputMapper.mapKey(InputFunction.open, KeyInput.KEY_F9)
    inputMapper.mapKey(InputFunction.save, KeyInput.KEY_S, ctrl = true)
    inputMapper.mapKey(InputFunction.save, KeyInput.KEY_F5)
    inputMapper.mapKey(
      InputFunction.saveAs,
      KeyInput.KEY_S,
      ctrl = true,
      shift = true
    )
    inputMapper.mapKey(InputFunction.export, KeyInput.KEY_X, ctrl = true)

    // sector height
    inputMapper.mapKey(InputFunction.editHeight, KeyInput.KEY_P)
    inputMapper.mapKey(InputFunction.editHeight, KeyInput.KEY_O, state = false)
    inputMapper.mapKey(
      InputFunction.editHeightSlow,
      KeyInput.KEY_P,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editHeightSlow,
      KeyInput.KEY_O,
      shift = true,
      state = false
    )

    // texture offset
    inputMapper.mapKey(
      InputFunction.editTextureOffsetX,
      KeyInput.KEY_L,
      state = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetX,
      KeyInput.KEY_H,
      state = false
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetY,
      KeyInput.KEY_K,
      state = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetY,
      KeyInput.KEY_J,
      state = false
    )
    inputMapper.mapWheel(
      InputFunction.editTextureOffsetX,
      shift = true,
      ctrl = true
    )
    inputMapper.mapWheel(InputFunction.editTextureOffsetY, ctrl = true)
    // texture offset slow
    inputMapper.mapKey(
      InputFunction.editTextureOffsetXSlow,
      KeyInput.KEY_L,
      state = true,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetXSlow,
      KeyInput.KEY_H,
      state = false,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetYSlow,
      KeyInput.KEY_K,
      state = true,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureOffsetYSlow,
      KeyInput.KEY_J,
      state = false,
      shift = true
    )

    // texture scale
    inputMapper.mapKey(
      InputFunction.editTextureScaleX,
      KeyInput.KEY_L,
      state = true,
      ctrl = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleX,
      KeyInput.KEY_H,
      state = false,
      ctrl = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleY,
      KeyInput.KEY_K,
      state = true,
      ctrl = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleY,
      KeyInput.KEY_J,
      state = false,
      ctrl = true
    )
    inputMapper.mapWheel(
      InputFunction.editTextureScaleX,
      shift = true,
      alt = true
    )
    inputMapper.mapWheel(InputFunction.editTextureScaleY, alt = true)
    // texture scale slow
    inputMapper.mapKey(
      InputFunction.editTextureScaleXSlow,
      KeyInput.KEY_L,
      state = true,
      ctrl = true,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleXSlow,
      KeyInput.KEY_H,
      state = false,
      ctrl = true,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleYSlow,
      KeyInput.KEY_K,
      state = true,
      ctrl = true,
      shift = true
    )
    inputMapper.mapKey(
      InputFunction.editTextureScaleYSlow,
      KeyInput.KEY_J,
      state = false,
      ctrl = true,
      shift = true
    )

    // slopes
    inputMapper.mapKey(
      InputFunction.editSlopeAngleX,
      KeyInput.KEY_T,
      state = true
    )
    inputMapper.mapKey(
      InputFunction.editSlopeAngleX,
      KeyInput.KEY_R,
      state = false
    )
    inputMapper.mapKey(
      InputFunction.editSlopeAngleY,
      KeyInput.KEY_T,
      shift = true,
      state = true
    )
    inputMapper.mapKey(
      InputFunction.editSlopeAngleY,
      KeyInput.KEY_R,
      shift = true,
      state = false
    )
    inputMapper.mapKey(
      InputFunction.editSlopeAngleY,
      KeyInput.KEY_T,
      shift = true,
      state = true
    )
    inputMapper.mapKey(
      InputFunction.editSlopeAngleY,
      KeyInput.KEY_R,
      shift = true,
      state = false
    )
    inputMapper.mapKey(
      InputFunction.changeSlopeStretch,
      KeyInput.KEY_Y,
      state = true
    )

    // Surface edit window
    inputMapper.mapKey(InputFunction.editSurface, KeyInput.KEY_E, state = true)

    // Sector edit window
    inputMapper.mapKey(
      InputFunction.editSector,
      KeyInput.KEY_E,
      shift = true,
      state = true
    )

    inputMapper.mapKey(InputFunction.changeMaterial, KeyInput.KEY_M)
  }
}

object InputFunction {

  val camera = "camera"
  val moveX = new FunctionId(camera, "camX")
  val moveY = new FunctionId(camera, "camY")
  val moveZ = new FunctionId(camera, "camZ")

  val mouse = "mouse"
  val mouseX = new FunctionId(mouse, "mouseX")
  val mouseY = new FunctionId(mouse, "mouseY")
  val mouseWheel = new FunctionId(mouse, "mouseWheel")
  val mouseWheelShift = new FunctionId(mouse, "mouseWheelShift")

  val files = "files"
  val newFile = new FunctionId(files, "newFile")
  val open = new FunctionId(files, "open")
  val save = new FunctionId(files, "save")
  val saveAs = new FunctionId(files, "saveAs")
  val `export` = new FunctionId(files, "export")
  val exit = new FunctionId(files, "exit")

  val general = "general"
  val cancel = new FunctionId(general, "cancel")
  val revert = new FunctionId(general, "revert")
  val snapToGrid = new FunctionId(general, "snapToGrid")
  val resizeGridBigger = new FunctionId(general, "resizeGrid+")
  val resizeGridSmaller = new FunctionId(general, "resizeGrid-")
  val switchViewMode = new FunctionId(general, "switchViewMode")
  val settings = new FunctionId(general, "settings")
  val insertMode = new FunctionId(general, "insertMode")
  val clickKey = new FunctionId(general, "clickKey")
  val selectPoints = new FunctionId(general, "selectPoints")
  val selectLines = new FunctionId(general, "selectLines")
  val selectSectors = new FunctionId(general, "selectSectors")
  val split = new FunctionId(general, "split")
  val help = new FunctionId(general, "help")
  val test1 = new FunctionId(general, "test1")
  val test2 = new FunctionId(general, "test2")
  val test3 = new FunctionId(general, "test3")
  val test4 = new FunctionId(general, "test4")
  val toggleStats = new FunctionId(general, "toggleStats")

  val edit3d = "edit3d"
  val editHeight = new FunctionId(edit3d, "editHeight")
  val editHeightSlow = new FunctionId(edit3d, "editHeightSlow")
  val editTextureOffsetX = new FunctionId(edit3d, "editTextureOffsetX")
  val editTextureOffsetY = new FunctionId(edit3d, "editTextureOffsetY")
  val editTextureScaleX = new FunctionId(edit3d, "editTextureScaleX")
  val editTextureScaleY = new FunctionId(edit3d, "editTextureScaleY")
  val editTextureOffsetXSlow = new FunctionId(edit3d, "editTextureOffsetXSlow")
  val editTextureOffsetYSlow = new FunctionId(edit3d, "editTextureOffsetYSlow")
  val editTextureScaleXSlow = new FunctionId(edit3d, "editTextureScaleXSlow")
  val editTextureScaleYSlow = new FunctionId(edit3d, "editTextureScaleYSlow")
  val editSlopeAngleX = new FunctionId(edit3d, "editSlopeAngleX")
  val editSlopeAngleY = new FunctionId(edit3d, "editSlopeAngleY")
  val changeSlopeStretch = new FunctionId(edit3d, "changeSlopeStretch")
  val editSurface = new FunctionId(edit3d, "editSurface")
  val editSector = new FunctionId(edit3d, "editSector")

  val changeMaterial = new FunctionId(edit3d, "changeMaterial")

}
