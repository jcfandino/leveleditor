package com.stovokor.editor.model

import java.util.Objects
import com.jme3.math.Vector2f

object Point {

  private def round(n: Float) =
    "%.3f".format(n).toFloat + 0.0f // because -0.0 exists

  def apply(x: Float, y: Float) = new Point(round(x), round(y))
  def apply(x: Double, y: Double): Point = Point(x.toFloat, y.toFloat)
}

case class Point private (x: Float, y: Float) extends Ordered[Point] {

  def distance(other: Point) =
    Math.sqrt(Math.pow(other.x - x, 2) + Math.pow(other.y - y, 2)).toFloat
  def move(dx: Float, dy: Float) = Point(x + dx, y + dy)

  override lazy val hashCode =
    Objects.hash(x.asInstanceOf[Object], y.asInstanceOf[Object])

  def vector(end: Point) = new Vector2f(end.x - x, end.y - y)

  override def compare(that: Point): Int = {
    var xcomp = x compare that.x
    if (xcomp == 0) y compare that.y else xcomp
  }

}
