package com.stovokor.editor.model.repository
import org.json4s.JsonAST.JValue

trait DataExtension {
  def fetch: JValue
  def load(data: JValue): Unit
  def clear: Unit
}

object ExtensionRepository {
  val instance = new ExtensionRepository
  def apply() = instance
}

class ExtensionRepository {
  var extensions: Map[String, DataExtension] = Map()

  def register(key: String, extension: DataExtension): Unit = {
    extensions = extensions.updated(key, extension)
  }

  def get = extensions.map({ case (key, extension) => (key, extension.fetch) })

  def load(key: String, data: JValue): Unit = {
    extensions.get(key).foreach(_.load(data))
  }

  def clear(): Unit = {
    extensions.foreach({ case (key, repo) => repo.clear })
  }
}
