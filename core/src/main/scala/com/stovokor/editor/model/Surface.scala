package com.stovokor.editor.model

import java.util.Objects
import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import com.jme3.math.Quaternion

object Surface {
  def apply(height: Float, slope: Slope, texture: SurfaceTexture) =
    new Surface(height, slope, texture)
  def apply(height: Float, texture: SurfaceTexture) =
    new Surface(height, Slope.flat, texture)
  def apply(height: Float) =
    new Surface(height, Slope.flat, SurfaceTexture(0, 1f))
  def empty = Surface(0f)
}

case class Surface(height: Float, slope: Slope, texture: SurfaceTexture) {

  def move(d: Float) = updateHeight(height + d)
  def updateTexture(t: SurfaceTexture) = Surface(height, slope, t)
  def updateHeight(h: Float) = Surface(h, slope, texture)
  def updateSlope(s: Slope) = Surface(height, s, texture)

  override lazy val hashCode =
    Objects.hash(height.asInstanceOf[Object], slope, texture)

  def heightOnPoint(p: Point, center: Point): Float = {
    val n = normal(Vector3f.UNIT_Z) // Z up
    -(n.x * (p.x - center.x) + n.y * (p.y - center.y) - height) / n.z
  }

  def minHeight(ps: List[Point], center: Point): Float = {
    ps.map(heightOnPoint(_, center)).min
  }

  def normal(up: Vector3f) =
    new Quaternion().fromAngles(slope.angleX, slope.angleY, 0) mult up
}

sealed trait StretchMode
case object StretchMode {
  private val stretchModes = List(Normal, FollowFloor, FollowCeiling)
  def next(mode: StretchMode) = {
    stretchModes((stretchModes.indexOf(mode) + 1) % stretchModes.size)
  }

  case object Normal extends StretchMode
  case object FollowFloor extends StretchMode
  case object FollowCeiling extends StretchMode
}

object Slope {
  def flat = Slope(0, 0)
}

case class Slope(angleX: Float, angleY: Float) {
  def tiltX(delta: Float) = Slope(bound(angleX + delta), angleY)
  def tiltY(delta: Float) = Slope(angleX, bound(angleY + delta))

  val maxAngle = 89f * FastMath.DEG_TO_RAD
  // TODO make fixed steps
  private def bound(angle: Float) =
    Math.min(maxAngle, Math.max(-maxAngle, angle))

  override lazy val hashCode =
    Objects.hash(angleX.asInstanceOf[Object], angleY.asInstanceOf[Object])

}

object SurfaceTexture {
  def apply(
      index: Long = 0L,
      texScaleX: Float = 1f,
      texScaleY: Float = 1f,
      texOffsetX: Float = 0f,
      texOffsetY: Float = 0f,
      flags: SurfaceTextureFlags = SurfaceTextureFlags()
  ) =
    new SurfaceTexture(
      index,
      texScaleX,
      texScaleY,
      texOffsetX,
      texOffsetY,
      flags
    )

  def apply(index: Long, texScale: Float) =
    new SurfaceTexture(index, texScale, texScale, 0f, 0f, SurfaceTextureFlags())

}

case class SurfaceTexture(
    index: Long,
    texScaleX: Float,
    texScaleY: Float,
    texOffsetX: Float,
    texOffsetY: Float,
    flags: SurfaceTextureFlags
) {

  def updateFlags(f: SurfaceTextureFlags) =
    SurfaceTexture(index, texScaleX, texScaleY, texOffsetX, texOffsetY, f)

  override lazy val hashCode = Objects.hash(
    index.asInstanceOf[Object],
    texScaleX.asInstanceOf[Object],
    texScaleY.asInstanceOf[Object],
    texOffsetX.asInstanceOf[Object],
    texOffsetY.asInstanceOf[Object],
    flags
  )

  def uvx(x: Float) = x / texScaleX + (texOffsetX / texScaleX)
  def uvy(y: Float) = y / texScaleY + (texOffsetY / texScaleY)

  def move(dx: Float, dy: Float) = // TODO wrap values (1.1 => 0.1)
    SurfaceTexture(
      index,
      texScaleX,
      texScaleY,
      texOffsetX + dx,
      texOffsetY + dy,
      flags
    )

  def resetOffset =
    SurfaceTexture(index, texScaleX, texScaleY, 0, 0, flags)

  def scale(dx: Float, dy: Float) = SurfaceTexture(
    index,
    Math.max(0.01f, texScaleX + dx),
    Math.max(0.01f, texScaleY + dy),
    texOffsetX,
    texOffsetY,
    flags
  )

  def resetScale =
    SurfaceTexture(index, 1, 1, texOffsetX, texOffsetY, flags)

  def updateIndex(idx: Long) =
    SurfaceTexture(idx, texScaleX, texScaleY, texOffsetX, texOffsetY, flags)

}

object SurfaceTextureFlags {
  def apply() = new SurfaceTextureFlags(true)
}
case class SurfaceTextureFlags(inLightmap: Boolean) {
  def updateInLightmap(b: Boolean) = SurfaceTextureFlags(b)
  override lazy val hashCode = Objects.hashCode(inLightmap.toString)
}
