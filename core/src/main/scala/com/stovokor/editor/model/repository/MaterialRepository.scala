package com.stovokor.editor.model.repository

import com.stovokor.editor.model.SurfaceMaterial
import com.stovokor.editor.model.NullMaterial
import com.stovokor.editor.model.MissingMaterial
import java.util.concurrent.atomic.AtomicLong

object MaterialRepository {
  val instance = new MaterialRepository()
  def apply() = instance
}

class MaterialRepository {

  var materials: Map[Long, SurfaceMaterial] =
    Map().withDefaultValue(MissingMaterial)
  var density: Float = 1f

  var idGenerator = new AtomicLong(0)

  def add(material: SurfaceMaterial) = {
    if (!contains(material)) {
      val id = idGenerator.getAndIncrement
      materials = materials.updated(id, material)
      id
    } else {
      materials.find({ case (_, m) => m == material }).get._1
    }
  }

  def update(index: Long, material: SurfaceMaterial) = {
    materials = materials.updated(index, material)
    if (idGenerator.get <= index) idGenerator.set(index + 1)
    material
  }

  def remove(index: Long) =
    update(index, NullMaterial)

  def removeAll: Unit = {
    idGenerator.set(0)
    materials = Map().withDefaultValue(MissingMaterial)
  }

  def get(index: Long) = materials(index)

  def contains(material: SurfaceMaterial) =
    materials.values.toSet.contains(material)

  def updateDensity(d: Float): Unit = {
    density = d
  }
}
