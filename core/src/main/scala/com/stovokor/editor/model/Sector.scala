package com.stovokor.editor.model

import java.util.Objects
import com.stovokor.editor.model.StretchMode.Normal
import com.stovokor.editor.algo.Triangulator

object Sector {

  def apply(
      polygon: Polygon,
      floor: Surface,
      ceiling: Surface,
      openWalls: List[Wall],
      wallSurface: SurfaceTexture
  ) =
    new Sector(
      polygon,
      floor,
      ceiling,
      openWalls,
      defaultClosedWalls(polygon, openWalls, wallSurface)
    )

  def apply(
      polygon: Polygon,
      floor: Surface,
      ceiling: Surface,
      openWalls: List[Wall]
  ) =
    new Sector(
      polygon,
      floor,
      ceiling,
      openWalls,
      defaultClosedWalls(polygon, openWalls)
    )

  def apply(
      polygon: Polygon,
      floor: Surface,
      ceiling: Surface,
      openWalls: List[Wall],
      closedWalls: List[Wall]
  ) =
    new Sector(polygon, floor, ceiling, openWalls, closedWalls)

  def defaultClosedWalls(
      polygon: Polygon,
      openWalls: List[Wall],
      texture: SurfaceTexture = SurfaceTexture(0, 1f)
  ) =
    polygon.edges
      .map(l => Wall(l, texture))
      .filterNot(openWalls.contains)
}

case class Sector(
    polygon: Polygon,
    floor: Surface,
    ceiling: Surface,
    openWalls: List[Wall],
    closedWalls: List[Wall],
    holes: Set[Polygon] = Set(),
    platforms: List[Platform] = List()
) {

  override lazy val hashCode =
    Objects.hash(polygon, floor, ceiling, openWalls, closedWalls, holes)

  def updatedPolygon(updated: Polygon) =
    Sector(updated, floor, ceiling, openWalls, closedWalls, holes, platforms)
  def updatedFloor(updated: Surface) =
    Sector(polygon, updated, ceiling, openWalls, closedWalls, holes, platforms)
  def updatedCeiling(updated: Surface) =
    Sector(polygon, floor, updated, openWalls, closedWalls, holes, platforms)

  def cutHole(hole: Polygon, withWalls: Boolean = true) = {

    def wallsByLine(wallsCollection: List[Wall]) = {
      wallsCollection
        .foldLeft(Map[Line, Wall]())((map, wall) =>
          map
            .updated(wall.line, wall)
            .updated(wall.line.reverse, wall)
        )
    }
    // merges new hole with previous ones
    val updatedHoles = appendHole(hole).toSet

    var (updatedClosedWalls, updatedOpenWalls) = if (withWalls) {
      // collect pre existing walls to reuse them later
      val preClosedWallsByLine = wallsByLine(closedWalls)
      val preOpenWallsByLine = wallsByLine(openWalls)
      // get those that are not in holes, but in subsectors external lines are not walls!
      val externalWalls = polygon.edges.flatMap(preClosedWallsByLine.get(_))
      //      val newClosedWalls = externalWalls ++ updatedHoles
      //        .flatMap(wallsForHole(_)) // generate the walls, check if they predated the new hole (then use them)
      //        .filter(w => hole.lines.contains(w.line) || !preOpenWalls.contains(w.line) )
      //        .map(w => if (preClosedWalls.contains(w.line)) preClosedWalls(w.line) else w) // TODO can fail sometimes!
      //        .toList
      val newClosedWalls = closedWalls ++ wallsForHole(hole)
      val newOpenWalls = openWalls.filter(w =>
        newClosedWalls.find(cw => cw.line == w.line).isEmpty
      )
      (newClosedWalls, newOpenWalls)
    } else {
      val preWalls = wallsByLine(openWalls)
      // TODO is this even necessary?
      val newWalls = openWalls ++ wallsForHole(hole)
        .filter(w => !preWalls.contains(w.line))
      (closedWalls, newWalls)
    }
    Sector(
      polygon,
      floor,
      ceiling,
      updatedOpenWalls,
      updatedClosedWalls,
      updatedHoles
    )
  }

  // create the solid walls around a hole
  private def wallsForHole(hole: Polygon) =
    (hole.vertices ++ List(hole.vertices.head)).reverse
      .sliding(2)
      .map(l => Line(l(0), l(1)))
      .map(l => Wall(l, SurfaceTexture()))

  private def appendHole(hole: Polygon) = {
    // TODO not sure about this..
    val mergeEnabled = false
    if (mergeEnabled) { // disabled merging of holes for now
      def merge(list: List[Polygon], poly: Polygon) = {
        val result = poly.merge(list.head)
        if (result.size == 1) // merge successful
          result.toList ++ list.tail
        else list ++ List(poly)
      }
      holes.foldLeft(List(hole))(merge)
    } else {
      holes ++ Set(hole)
    }
  }
  def updatedOpenWalls(updated: List[Wall]) = Sector(
    polygon,
    floor,
    ceiling,
    updated,
    closedWalls.filterNot(updated.contains),
    holes,
    platforms
  )

  def updatedClosedWalls(updated: List[Wall]) = Sector(
    polygon,
    floor,
    ceiling,
    openWalls.filterNot(updated.contains),
    updated,
    holes,
    platforms
  )

  def updatedOpenWall(idx: Int, updated: Wall) = Sector(
    polygon,
    floor,
    ceiling,
    openWalls.updated(idx, updated),
    closedWalls.filterNot(updated.equals),
    holes,
    platforms
  )

  def updatedClosedWall(idx: Int, updated: Wall) = Sector(
    polygon,
    floor,
    ceiling,
    openWalls.filterNot(updated.equals),
    closedWalls.updated(idx, updated),
    holes,
    platforms
  )

  // calling this without updating the walls will make them out of sync
  def updatedHoles(updated: Set[Polygon]) =
    Sector(polygon, floor, ceiling, openWalls, closedWalls, updated, platforms)

  def removedHole(old: Polygon) = {
    val oldLines = old.edges.toSet
    val newHoles = holes - old
    // need to update the open walls (assuming hole is open)
    val newWalls = openWalls.filterNot(w => oldLines.contains(w.line))
    this.updatedHoles(newHoles).updatedOpenWalls(newWalls)
  }

  def updatedPlatforms(updated: List[Platform]) =
    Sector(polygon, floor, ceiling, openWalls, closedWalls, holes, updated)

  def moveSinglePoint(point: Point, dx: Float, dy: Float) = {
    def updateWalls(walls: List[Wall]) = {
      walls
        .map(wall => {
          val line = wall.line
          if (line.a == point) wall.updateLine(Line(point.move(dx, dy), line.b))
          else if (line.b == point)
            wall.updateLine(Line(line.a, point.move(dx, dy)))
          else wall
        })
        .filterNot(_.line.length == 0f)
    }
    def updatePlatforms() = platforms.map(p => {
      val updatedSides = p.sides.map(side => {
        val line = side.line
        if (line.a == point) side.updateLine(Line(point.move(dx, dy), line.b))
        else if (line.b == point)
          side.updateLine(Line(line.a, point.move(dx, dy)))
        else side
      })
      p.updatedSides(updatedSides)
    })
    def updateHoles() = {
      holes.map(h => h.changePoint(point, point.move(dx, dy)))
    }
    def cleanupHoles(p: Sector) = {
      p.holes.foldLeft(p)((s, h) => if (h.isDegenerate) s.removedHole(h) else s)
    }
    val updatedSector =
      updatedPolygon(polygon.changePoint(point, point.move(dx, dy)))
        .updatedHoles(updateHoles())
        .updatedOpenWalls(updateWalls(openWalls))
        .updatedClosedWalls(updateWalls(closedWalls))
        .updatedPlatforms(updatePlatforms())
    cleanupHoles(updatedSector)
  }

  def openWall(line: Line) = {
    val wall = closedWalls.find(_.line.alike(line))
    if (wall.isDefined) {
      updatedClosedWalls(closedWalls.filterNot(_ == wall.get))
        .updatedOpenWalls(openWalls ++ List(wall.get))
    } else this
  }

  def closeWall(line: Line) = {
    val wall = openWalls.find(_.line.alike(line))
    if (wall.isDefined) {
      updatedOpenWalls(openWalls.filterNot(_ == wall.get))
        .updatedClosedWalls(closedWalls ++ List(wall.get))
    } else this
  }

  def wallTexture(query: List[Line]) = (closedWalls ++ openWalls)
    .find(w => query.contains(w.line.reverse) || query.contains(w.line))
    .map(_.texture)
    .orElse(Some(SurfaceTexture(0, 1f)))
    .get

  def replaceLine(line: Line, lines: List[Line]) = {
    def updateSide[S <: Side[S]](side: S): List[S] = {
      if (side.line == line)
        lines.map(side.updateLine)
      else if (side.line.reverse == line) // If we got it reversed
        lines.reverse.map(_.reverse).map(side.updateLine)
      else
        List(side)
    }
    def updateWalls(walls: List[Wall]) = walls.flatMap(updateSide)
    val newPolygon = polygon.addLines(line, lines)
    val newPlatforms = platforms
      .map(platform =>
        platform.updatedSides(platform.sides.flatMap(updateSide))
      )
    val newHoles = holes.map(_.addLines(line, lines))

    updatedPolygon(newPolygon)
      .updatedOpenWalls(updateWalls(openWalls))
      .updatedClosedWalls(updateWalls(closedWalls))
      .updatedPlatforms(newPlatforms)
      .updatedHoles(newHoles)
  }

  def updatedPlatform(idx: Int, updated: Platform) = updatedPlatforms(
    platforms.zipWithIndex
      .map({ case (p, i) => if (idx == i) updated else p })
  )

  def addPlatform() = {
    def texture(line: Line) =
      (openWalls ++ closedWalls)
        .find(wall => wall.line == line)
        .map(_.texture)
        .orElse(Some(SurfaceTexture()))
        .get
    val base = platforms
      .foldLeft(floor.height)((h, p) => h.max(p.top.height)) +
      0.5f.min((ceiling.height - floor.height) / 2f)
    val lines = (openWalls ++ closedWalls).map(_.line)
    val platform = Platform(
      ceiling.updateHeight(base + 0.2f),
      floor.updateHeight(base),
      lines.map(line => PlatformSide(line, texture(line)))
    )
    updatedPlatforms(platforms ++ List(platform))
  }

  def removePlatform(idx: Int) = updatedPlatforms(
    platforms.zipWithIndex
      .filter({ case (_, i) => i != idx })
      .map({ case (p, _) => p })
  )

  def inside(point: Point) = {
    polygon.inside(point) && (holes.forall(p => !p.inside(point)) ||
      holes.flatMap(_.vertices).contains(point))
  }

  def insidePolygon(point: Point) = {
    polygon.inside(point)
  }

  def triangulate = Triangulator.triangulate(polygon, holes)

  def floorHeightOn(p: Point) = floor.heightOnPoint(p, polygon.center)
  def ceilingHeightOn(p: Point) = ceiling.heightOnPoint(p, polygon.center)

  def minFloorHeight(ps: List[Point] = polygon.vertices) =
    floor.minHeight(ps, polygon.center)

  def platformTopHeightOn(idx: Int, p: Point) =
    platforms(idx).topHeightOn(p, polygon)
  def platformBottomHeightOn(idx: Int, p: Point) =
    platforms(idx).bottomHeightOn(p, polygon)

  // holes surrounded by closed walls
  def openHoles = {
    val holesWalls = closedWalls.map(_.line.reverse).toSet
    holes.filter(hole => hole.edges.forall(holesWalls.contains))
  }
}

trait Side[S <: Side[S]] {
  def line: Line
  def texture: SurfaceTexture

  def updateTexture(t: SurfaceTexture): S
  def updateLine(updated: Line): S
}

object Wall {
  def apply(line: Line, texture: SurfaceTexture) =
    new Wall(line, texture, Normal)
  def apply(line: Line, texture: SurfaceTexture, stretchMode: StretchMode) =
    new Wall(line, texture, stretchMode)
}

case class Wall(line: Line, texture: SurfaceTexture, stretchMode: StretchMode)
    extends Side[Wall] {

  def updateTexture(t: SurfaceTexture) = Wall(line, t, stretchMode)
  def updateLine(updated: Line) = Wall(updated, texture, stretchMode)

  // The mode in the Wall is ignored in open walls (mode is in BorderStrip).
  def updateStretchMode(mode: StretchMode) = Wall(line, texture, mode)
  def rotateStretchMode = updateStretchMode(StretchMode.next(stretchMode))

  override lazy val hashCode = Objects.hash(line, texture, stretchMode)
}
