package com.stovokor.editor.model

import java.util.Objects
import com.jme3.asset.AssetManager
import com.jme3.material.MatParamTexture
import com.stovokor.editor.gui.GuiFactory
import com.jme3.asset.AssetNotFoundException
import com.jme3.asset.AssetLoadException

trait SurfaceMaterial {
  def name: String
}

case class SimpleMaterial(path: String) extends SurfaceMaterial {
  override lazy val hashCode = Objects.hash(path)
  override def name = path
}

case class MatDefMaterial(path: String) extends SurfaceMaterial {
  override lazy val hashCode = Objects.hash(path)
  override def name = path
}

object MissingMaterial extends SurfaceMaterial {
  def name = "missing material"
}

object NullMaterial extends SurfaceMaterial {
  def name = "null material"
}

case class SurfaceMaterialThumbnail(path: String, image: String)

object SurfaceMaterialThumbnail {
  def load(material: SurfaceMaterial)(implicit
      assetManager: AssetManager
  ): SurfaceMaterialThumbnail = material match {
    case SimpleMaterial(path) => SurfaceMaterialThumbnail(path, path)
    case MatDefMaterial(path) =>
      loadMatDef(path)
        .map(_.getParam("DiffuseMap"))
        .filter(_.isInstanceOf[MatParamTexture])
        .map(_.asInstanceOf[MatParamTexture])
        .map(_.getTextureValue.getName)
        .map(SurfaceMaterialThumbnail(path, _))
        .orElse(
          Some(
            SurfaceMaterialThumbnail(
              path,
              GuiFactory.iconBasePath + "no-preview.png"
            )
          )
        )
        .get
    case _ =>
      SurfaceMaterialThumbnail(
        "Textures/Missing.png",
        GuiFactory.iconBasePath + "no-preview.png"
      )
  }

  private def loadMatDef(path: String)(implicit assetManager: AssetManager) = {
    try {
      Some(assetManager.loadMaterial(path))
    } catch {
      case e: AssetLoadException     => None
      case e: AssetNotFoundException => None
    }
  }
}

object SurfaceMaterialOrdering extends Ordering[SurfaceMaterial] {
  def str(m: SurfaceMaterial) = m match {
    case SimpleMaterial(path) => "0:" + path
    case MatDefMaterial(path) => "1:" + path
    case _                    => "9"
  }
  def compare(m1: SurfaceMaterial, m2: SurfaceMaterial) =
    str(m1).compare(str(m2))
}
