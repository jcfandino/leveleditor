package com.stovokor.editor.model

import com.stovokor.editor.model.StretchMode.Normal
import java.util.Objects

object Platform {
  def apply(top: Surface, bottom: Surface, sides: List[PlatformSide]) =
    new Platform(top, bottom, sides, PlatformFlags(true))

  def apply(
      top: Surface,
      bottom: Surface,
      sides: List[PlatformSide],
      flags: PlatformFlags
  ) =
    new Platform(top, bottom, sides, flags)
}

case class Platform(
    top: Surface,
    bottom: Surface,
    sides: List[PlatformSide],
    flags: PlatformFlags
) {

  // TODO validate that they not cross
  def updatedTop(updated: Surface) = Platform(updated, bottom, sides, flags)
  def updatedBottom(updated: Surface) = Platform(top, updated, sides, flags)
  def updatedSides(updated: List[PlatformSide]) =
    Platform(top, bottom, updated, flags)
  def updatedFlags(updated: PlatformFlags) =
    Platform(top, bottom, sides, updated)

  def updatedSide(idx: Int, updated: PlatformSide) =
    updatedSides(
      sides.zipWithIndex
        .map({ case (s, i) => if (i == idx) updated else s })
    )

  def isAllInLightmap =
    top.texture.flags.inLightmap &&
      bottom.texture.flags.inLightmap &&
      sides.map(_.texture.flags).forall(_.inLightmap)

  def updateAllInLightmap(b: Boolean) =
    Platform(
      top.updateTexture(
        top.texture.updateFlags(top.texture.flags.updateInLightmap(b))
      ),
      bottom.updateTexture(
        bottom.texture.updateFlags(bottom.texture.flags.updateInLightmap(b))
      ),
      sides.map(side =>
        side.updateTexture(
          side.texture.updateFlags(side.texture.flags.updateInLightmap(b))
        )
      ),
      flags
    )

  def topHeightOn(p: Point, polygon: Polygon) =
    top.heightOnPoint(p, polygon.center)
  def bottomHeightOn(p: Point, polygon: Polygon) =
    bottom.heightOnPoint(p, polygon.center)

}

object PlatformSide {
  def apply(line: Line, texture: SurfaceTexture) =
    new PlatformSide(line, texture, Normal)
  def apply(line: Line, texture: SurfaceTexture, stretchMode: StretchMode) =
    new PlatformSide(line, texture, stretchMode)
}

case class PlatformSide(
    line: Line,
    texture: SurfaceTexture,
    stretchMode: StretchMode
) extends Side[PlatformSide] {

  def updateLine(updated: Line) = PlatformSide(updated, texture, stretchMode)
  def updateTexture(updated: SurfaceTexture) =
    PlatformSide(line, updated, stretchMode)

  // The mode in the PlatformSide is ignored in open walls (mode is in BorderStrip).
  def updateStretchMode(mode: StretchMode) = PlatformSide(line, texture, mode)
  def rotateStretchMode = updateStretchMode(StretchMode.next(stretchMode))

  override lazy val hashCode = Objects.hash(line, texture, stretchMode)
}

object PlatformFlags {
  val defaultBatchId = "base"
  def apply(inheritHoles: Boolean, batchId: String = defaultBatchId) =
    new PlatformFlags(inheritHoles, batchId)
}
case class PlatformFlags(inheritHoles: Boolean, batchId: String) {
  def updateInheritHoles(b: Boolean) = PlatformFlags(b, batchId)
  def updateBatchId(s: String) = PlatformFlags(
    inheritHoles,
    if (s.trim.isEmpty) PlatformFlags.defaultBatchId else s
  )

  def isDetached = batchId != PlatformFlags.defaultBatchId

  override lazy val hashCode = Objects.hash(inheritHoles.toString(), batchId)
}
