package com.stovokor.editor.model

import earcut4j.Earcut
import scala.jdk.CollectionConverters._
import com.jme3.math.FastMath
import com.jme3.math.Vector2f
import java.util.Objects
import com.stovokor.editor.algo.FindFaces
import com.stovokor.editor.algo.Triangulator

object Polygon {
  def apply(vertices: List[Point]) = {
    new Polygon(PointsSorter.sort(vertices))
  }

}

private object PointsSorter {

  private def findPathsBetween(ps: List[Point], p1: Point, p2: Point) = {
    val i1 = ps.indexOf(p1)
    val i2 = ps.indexOf(p2)
    val path1 =
      if (i1 < i2) ps.slice(i1, i2 + 1)
      else ps.slice(i1, ps.size) ++ ps.slice(0, i2 + 1)
    val path2 = (if (i1 > i2) ps.slice(i2, i1 + 1)
                 else ps.slice(i2, ps.size) ++ ps.slice(0, i1 + 1)).reverse
    (path1, path2)
  }

  private def findPathsBetweenSorted(
      ps: List[Point],
      p1: Point,
      p2: Point
  ): (List[Point], List[Point]) =
    findPathsBetween(ps, p1, p2)

  private def isClockwise(ps: List[Point]) = {
    val limY = ps.map(_.y).min
    // area of the rect+triang between negative limY and line a to b.
    def areaBelow(a: Point, b: Point) =
      0.5 * (b.x - a.x) * (b.y + a.y - 2 * limY)
    val sorted = ps.sortBy(_.x)
    val leftmost = sorted.head
    val rightmost = sorted.last
    val (path1, path2) = findPathsBetween(ps, leftmost, rightmost)
    val area1 = path1.sliding(2).map(ab => areaBelow(ab(0), ab(1))).sum
    val area2 = path2.sliding(2).map(ab => areaBelow(ab(0), ab(1))).sum
    val diff = area1 - area2
    diff > 0
  }

  def sort(vertices: List[Point]): List[Point] = {
    // find the clockwise orientation
    val oriented =
      if (isClockwise(vertices)) vertices
      else vertices.reverse
    // make the first point be the smallest in natural order
    val (_, idx) = oriented.zipWithIndex.sortBy(_._1).head
    val (a, b) = oriented.splitAt(idx).swap
    a ++ b
  }

  def sort(v1: Point, v2: Point, v3: Point): (Point, Point, Point) = {
    val sorted = sort(List(v1, v2, v3))
    (sorted(0), sorted(1), sorted(2))
  }

}

case class Polygon protected (vertices: List[Point]) {

  override lazy val hashCode = Objects.hash(vertices)

  override def equals(other: Any) =
    other.isInstanceOf[Polygon] &&
      hashCode == other.hashCode &&
      vertices == other.asInstanceOf[Polygon].vertices

  lazy val edges = {
    ((vertices zip vertices.tail) map ((p) => Line(p._1, p._2))) ++ List(
      Line(vertices.last, vertices.head)
    )
  }

  def changePoint(from: Point, to: Point): Polygon = {
    val idx = vertices.indexOf(from)
    if (from == to || idx == -1) {
      this
    } else if (vertices.contains(to)) {
      Polygon(vertices.filterNot(_ == from))
    } else {
      Polygon(vertices.updated(idx, to))
    }
  }

  def addLines(between: Line, lines: List[Line]): Polygon = {
    val idxa = vertices.indexOf(between.a)
    val idxb = vertices.indexOf(between.b)
    if (idxa != -1 && idxb != -1) {
      val (idx1, idx2) = (idxa min idxb, idxa max idxb)
      val last = idx1 == 0 && idx2 == vertices.size - 1
      val newPoints = // this is logic is weird, could be simpler?
        if (idxa < idxb && !last || idxa > idxb && last)
          lines.drop(1).map(_.a)
        else
          lines.map(_.reverse).reverse.drop(1).map(_.a)
      if (last)
        Polygon(vertices ++ newPoints)
      else
        Polygon(
          (vertices.slice(0, idx1 + 1) ++
            newPoints) ++
            vertices.slice(idx2, vertices.size)
        )
    } else this
  }

  /** Find edges belonging to both polygons */
  def borderWith(other: Polygon): List[Line] = {
    val otherLines = other.edges.toSet
    edges.filter(l => otherLines.contains(l) || otherLines.contains(l.reverse))
  }

  // if the point is right in the border, the result in undetermined!
  def inside(point: Point): Boolean = {
    // https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
    edges
      .map(line =>
        (line.a.y > point.y) != (line.b.y > point.y) &&
          (point.x < (line.b.x - line.a.x) * (point.y - line.a.y) /
            (line.b.y - line.a.y) + line.a.x)
      )
      .count(identity) % 2 == 1
  }

  // when all the points of the other polygon are inside this polygon
  def inside(other: Polygon): Boolean = {
    other.vertices.forall(inside)
  }

  def innerPoints(ps: List[Point]) =
    if (ps.length < 3) List() else ps.slice(1, ps.length - 1)
  def cutInside(cut: List[Point]) =
    cut.size == 2 || innerPoints(cut).find(inside).isDefined

  def sharedLines(other: Polygon): List[Line] = {
    val otherLines = (other.edges.flatMap(_.andReverse)).toSet
    edges.filter(otherLines.contains)
  }

  // merge with another polygon if they share a line
  def merge(other: Polygon): Set[Polygon] = {
    val border = borderWith(other)
    if (border.isEmpty) {
      Set(this, other)
    } else {
      val faces = FindFaces(other.edges ++ edges)
      faces.outer
    }
  }

  def isDegenerate = vertices.size < 3

  lazy val boundBox: BoundBox = {
    val firstPoint = vertices.head
    vertices.foldLeft(BoundBox(firstPoint, firstPoint))((box, point) => {
      BoundBox(
        Point(box.from.x.min(point.x), box.from.y.min(point.y)),
        Point(box.to.x.max(point.x), box.to.y.max(point.y))
      )
    })
  }

  lazy val center: Point = {
    val count = vertices.size
    val sum = vertices
      .foldLeft(Point(0f, 0f))((sum, point) =>
        Point(sum.x + point.x, sum.y + point.y)
      )
    Point(sum.x / count, sum.y / count)
  }

  def area: Float = Triangulator.triangulate(this).map(_.area).sum
}

object Triangle {
  def apply(v1: Point, v2: Point, v3: Point) = {
    val (s1, s2, s3) = PointsSorter.sort(v1, v2, v3)
    new Triangle(s1, s2, s3)
  }
}

class Triangle private (val v1: Point, val v2: Point, val v3: Point)
    extends Polygon(List(v1, v2, v3)) {

  def reverse = new Triangle(v3, v2, v1)

  override def area = v1.vector(v2).cross(v1.vector(v3)).z.abs / 2f
}
