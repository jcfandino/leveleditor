package com.stovokor.editor.model
import org.json4s.JsonAST.JValue

case class MapFile(
    version: Int,
    sectors: Map[Long, Sector],
    borders: Map[Long, Border],
    materials: Map[Long, SurfaceMaterial],
    textureDensity: Option[Float],
    extensions: Map[String, JValue]
) {}
