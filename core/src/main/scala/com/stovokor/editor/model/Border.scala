package com.stovokor.editor.model

import java.util.Objects
import com.stovokor.editor.model.StretchMode.Normal

object Border {
  def apply(
      sectorA: Long,
      sectorB: Long,
      line: Line,
      surfaceFloor: BorderStrip,
      surfaceCeiling: BorderStrip,
      surfaceMiddle: BorderStrip = BorderStrip.empty
  ) = new Border(
    sectorA,
    sectorB,
    line,
    surfaceFloor,
    surfaceCeiling,
    surfaceMiddle
  )
}

/** A border (aka portal) between two sectors
  */
case class Border(
    sectorA: Long,
    sectorB: Long,
    line: Line,
    surfaceFloor: BorderStrip,
    surfaceCeiling: BorderStrip,
    surfaceMiddle: BorderStrip
) {

  override lazy val hashCode = Objects.hash(
    sectorA.asInstanceOf[Object],
    sectorB.asInstanceOf[Object],
    line,
    surfaceFloor,
    surfaceCeiling,
    surfaceMiddle
  )

  def updateAny(
      sectorAUpdated: Long = sectorA,
      sectorBUpdated: Long = sectorB,
      lineUpdated: Line = line,
      surfaceFloorUpdated: BorderStrip = surfaceFloor,
      surfaceCeilingUpdated: BorderStrip = surfaceCeiling,
      surfaceMiddleUpdated: BorderStrip = surfaceMiddle
  ) = Border(
    sectorAUpdated,
    sectorBUpdated,
    lineUpdated,
    surfaceFloorUpdated,
    surfaceCeilingUpdated,
    surfaceMiddleUpdated
  )

  def updateSectors(updatedA: Long, updatedB: Long) =
    updateAny(sectorAUpdated = updatedA, sectorBUpdated = updatedB)
  def updateLine(updated: Line) = updateAny(lineUpdated = updated)
  def updateSurfaceFloor(updated: BorderStrip) =
    updateAny(surfaceFloorUpdated = updated)
  def updateSurfaceCeiling(updated: BorderStrip) =
    updateAny(surfaceCeilingUpdated = updated)
  def updateSurfaceMiddle(updated: BorderStrip) =
    updateAny(surfaceMiddleUpdated = updated)

}

object BorderStrip {
  def apply(texture: SurfaceTexture) = new BorderStrip(texture, Normal)
  def apply(texture: SurfaceTexture, stretchMode: StretchMode) =
    new BorderStrip(texture, stretchMode)
  def empty = BorderStrip(SurfaceTexture(0, 1))
}

case class BorderStrip(texture: SurfaceTexture, stretchMode: StretchMode) {

  def updateTexture(t: SurfaceTexture) = BorderStrip(t, stretchMode)
  def updateStretchMode(m: StretchMode) = BorderStrip(texture, m)
  def rotateStretchMode = updateStretchMode(StretchMode.next(stretchMode))

  override lazy val hashCode = Objects.hash(texture, stretchMode)
}
