package com.stovokor.editor.model

object Settings {
  val defaultDir = System.getProperty("user.dir")
  def apply() = new Settings(Map(), 1f)
}

case class Settings(paths: Map[String, String], defaultTextureDensity: Float) {

  def assetsBasePath = paths.getOrElse("assetsBase", Settings.defaultDir)
  def lastProjectPath = paths.getOrElse("lastProject", Settings.defaultDir)
  def lastExportPath = paths.getOrElse("lastExport", Settings.defaultDir)

  def updateAssetBasePath(path: String) =
    Settings(paths.updated("assetsBase", path), defaultTextureDensity)
  def updateLastProjectPath(path: String) =
    Settings(paths.updated("lastProject", path), defaultTextureDensity)
  def updateLastExportPath(path: String) =
    Settings(paths.updated("lastExport", path), defaultTextureDensity)
  def updateDefaultTextureDensity(d: Float) = Settings(paths, d)

}
