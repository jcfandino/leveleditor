package com.stovokor.editor.model

import com.jme3.export.Savable
import com.jme3.export.JmeImporter
import com.jme3.export.JmeExporter
import com.jme3.scene.UserData

object Target {

  case class Floor(sectorId: Long) extends SectorTarget {
    val name = "floor"
  }
  case class Ceiling(sectorId: Long) extends SectorTarget {
    val name = "ceiling"
  }
  case class Wall(sectorId: Long, idx: Int) extends SectorTarget {
    val name = s"wall-$idx"
  }
  case class BorderHi(sectorId: Long, idx: Int) extends SectorTarget {
    val name = s"borderHi-$idx"
  }
  case class BorderLow(sectorId: Long, idx: Int) extends SectorTarget {
    val name = s"borderLow-$idx"
  }
  case class PlatformTop(sectorId: Long, idx: Int)
      extends SectorTarget
      with PlatformTarget {
    val name = s"platform-$idx-top"
  }
  case class PlatformBottom(sectorId: Long, idx: Int)
      extends SectorTarget
      with PlatformTarget {
    val name = s"platform-$idx-bottom"
  }
  case class PlatformSide(sectorId: Long, idx: Int, sideIdx: Int)
      extends SectorTarget
      with PlatformTarget {
    val name = s"platform-$idx-side-$sideIdx"
  }
  case class DecalTarget(decalId: Long) extends Target {
    val name = s"decal-$decalId"
  }
}

trait Target {
  def name: String
}

trait SectorTarget extends Target {
  def sectorId: Long
}

trait PlatformTarget extends Target {
  def idx: Int
}

// to attach to geometries
object SavableTarget {

  val floor = "floor".r
  val ceiling = "ceiling".r
  val wall = raw"wall-(\d+)".r
  val borderHi = raw"borderHi-(\d+)".r
  val borderLow = raw"borderLow-(\d+)".r
  val platformTop = raw"platform-(\d+)-top".r
  val platformBottom = raw"platform-(\d+)-bottom".r
  val platformSide = raw"platform-(\d+)-side-(\d+)".r

  def apply(target: Target) =
    new UserData(UserData.getObjectType(""), target.name)
  def apply(sectorId: Long, data: String) = data match {
    case floor()             => Target.Floor(sectorId)
    case ceiling()           => Target.Ceiling(sectorId)
    case wall(idx)           => Target.Wall(sectorId, idx.toInt)
    case borderHi(idx)       => Target.BorderHi(sectorId, idx.toInt)
    case borderLow(idx)      => Target.BorderLow(sectorId, idx.toInt)
    case platformTop(idx)    => Target.PlatformTop(sectorId, idx.toInt)
    case platformBottom(idx) => Target.PlatformBottom(sectorId, idx.toInt)
    case platformSide(pidx, sidx) =>
      Target.PlatformSide(sectorId, pidx.toInt, sidx.toInt)
    case str => throw new IllegalArgumentException(s"Cannot match $str")
  }
}
