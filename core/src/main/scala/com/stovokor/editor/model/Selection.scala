package com.stovokor.editor.model

import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.decal.Decal
import com.stovokor.editor.decal.DecalRepository

object Selection {
  def apply(target: Target): Selection = {
    def sector =
      SectorRepository().get(target.asInstanceOf[SectorTarget].sectorId)
    target match {
      case Target.Floor(_)     => new FloorSurfaceSelection(sector.floor)
      case Target.Ceiling(_)   => new CeilingSurfaceSelection(sector.ceiling)
      case Target.Wall(_, idx) => new WallSelection(sector.closedWalls(idx))
      case Target.BorderLow(sectorId, idx) =>
        BorderRepository()
          .find((sector.openWalls(idx)).line)
          .find({ tuple => tuple._2.sectorA == sectorId })
          .map({ tuple => new FloorStripSelection(tuple._2.surfaceFloor) })
          .get
      case Target.BorderHi(sectorId, idx) =>
        BorderRepository()
          .find((sector.openWalls(idx)).line)
          .find({ tuple => tuple._2.sectorA == sectorId })
          .map({ tuple =>
            new CeilingBorderStripSelection(tuple._2.surfaceCeiling)
          })
          .get
      case Target.PlatformBottom(sectorId, idx) =>
        new PlatformBottom(sector.platforms(idx).bottom)
      case Target.PlatformTop(sectorId, idx) =>
        new PlatformTop(sector.platforms(idx).top)
      case Target.PlatformSide(sectorId, pIdx, sIdx) =>
        new PlatformSideSel(sector.platforms(pIdx).sides(sIdx))
      case Target.DecalTarget(decalId) =>
        new DecalSelection(DecalRepository.get(decalId))
      case _ => new FloorSurfaceSelection(Surface.empty)
    }
  }

}
trait Selection {
  def texture: SurfaceTexture
  def describe: String
  def stretchMode: Option[StretchMode]
}
class FloorSurfaceSelection(surface: Surface) extends Selection {
  def texture = surface.texture
  def describe = "Floor"
  def stretchMode = None
}
class CeilingSurfaceSelection(surface: Surface) extends Selection {
  def texture = surface.texture
  def describe = "Ceiling"
  def stretchMode = None
}
class WallSelection(wall: Wall) extends Selection {
  def texture = wall.texture
  def describe = "Wall"
  def stretchMode = Some(wall.stretchMode)
}
class FloorStripSelection(strip: BorderStrip) extends Selection {
  def texture = strip.texture
  def describe = "Low border"
  def stretchMode = Some(strip.stretchMode)
}
class CeilingBorderStripSelection(strip: BorderStrip) extends Selection {
  def texture = strip.texture
  def describe = "Top border"
  def stretchMode = Some(strip.stretchMode)
}
class PlatformTop(surface: Surface) extends Selection {
  def texture = surface.texture
  def describe = "Platform Top"
  def stretchMode = None
}
class PlatformBottom(surface: Surface) extends Selection {
  def texture = surface.texture
  def describe = "Platform Bottom"
  def stretchMode = None
}
class PlatformSideSel(side: PlatformSide) extends Selection {
  def texture = side.texture
  def describe = "Platform Side"
  def stretchMode = Some(side.stretchMode)
}
class DecalSelection(decal: Decal) extends Selection {
  def texture = decal.texture
  def describe = "Decal"
  def stretchMode = None
}
