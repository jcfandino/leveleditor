package com.stovokor.editor.lighting.baking

import java.awt.image.BufferedImage
import java.io.File
import java.nio.FloatBuffer

import org.sunflow.SunflowAPI
import org.sunflow.core.camera.PinholeLens
import org.sunflow.core.display.FileDisplay
import org.sunflow.core.light.DirectionalSpotlight
import org.sunflow.core.light.SunSkyLight
import org.sunflow.core.primitive.TriangleMesh
import org.sunflow.core.shader.DiffuseShader
import org.sunflow.image.Color
import org.sunflow.math.Matrix4
import org.sunflow.math.Point3
import org.sunflow.math.Vector3

import com.jme3.math.Vector2f
import com.jme3.scene.BatchNode
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.VertexBuffer
import com.jme3.util.BufferUtils
import com.stovokor.util.JmeExtensions._

import javax.imageio.ImageIO
import com.stovokor.editor.lighting.Light
import com.stovokor.editor.lighting.PointLight
import com.stovokor.editor.lighting.DirectionalLight
import com.stovokor.editor.lighting.AmbientLight
import com.stovokor.editor.lighting.SpotLight
import com.stovokor.util.CoordinateSystem
import com.jme3.math.Vector3f
import com.jme3.math.ColorRGBA

class LightmapBaker(filePath: String, settings: BakeSettings) {

  def bake(
      node: Node,
      lights: Set[Light],
      extras: Set[Spatial] = Set()
  ): Unit = {
    println(s"Baking $settings")
    val builder = new LightmapGeometryBuilder
    builder.add(node)
    val geometry = builder.build

    val scene = new SunflowScene
    scene.init(settings)

    scene.addGeometry(geometry)

    if (!extras.isEmpty) {
      val extraGeomBuilder = new LightmapGeometryBuilder
      for (extra <- extras) {
        println(s"Adding extra object ${extra.getName}")
        extraGeomBuilder.add(extra)
      }
      scene.addGeometry(extraGeomBuilder.build)
    }

    def mapPoint(position: com.stovokor.editor.lighting.Vector) = {
      val jpos = CoordinateSystem.toJme(position.toVector3f)
      new Point3(jpos.x, jpos.y, jpos.z)
    }
    def mapVec(position: com.stovokor.editor.lighting.Vector) = {
      val jpos = CoordinateSystem.toJme(position.toVector3f)
      new Vector3(jpos.x, jpos.y, jpos.z)
    }
    def mapCol(color: com.stovokor.editor.lighting.Color) = {
      val factor = settings.colorFactor
      new Color(factor * color.r, factor * color.g, factor * color.b)
    }
    lights.foreach({
      case PointLight(_, _, col, pos) =>
        scene.addPointLight(mapCol(col), mapPoint(pos))
      case SpotLight(_, _, col, pos, dir) =>
        scene.addSpotLight(mapCol(col), mapPoint(pos), mapVec(dir))
      case _ => println(s"Light not supported for baking")
    })
    scene.render
  }

  class SunflowScene {
    val sunflow = new CompatibleSunflowAPI

    var names: Map[Geometry, String] = Map.empty

    def init(settings: BakeSettings): Unit = {
      // Bake lightmap settings
      sunflow.parameter("baking.instance", "geometry_0.instance")
      sunflow.parameter("baking.viewdep", false)
      sunflow.options(SunflowAPI.DEFAULT_OPTIONS)
      // Image settings
      sunflow.parameter("resolutionX", settings.width);
      sunflow.parameter("resolutionY", settings.height);

      sunflow.parameter("aa.min", settings.aaMin)
      sunflow.parameter("aa.max", settings.aaMax)

      sunflow.parameter("samples", settings.samples)
      sunflow.parameter("contrast", settings.contrast)
      sunflow.parameter("filter", settings.filter)
      sunflow.parameter("jitter", settings.filter)
      sunflow.parameter("caustics", "none")

      sunflow.options(SunflowAPI.DEFAULT_OPTIONS)

      // Shader diffuse
      sunflow.parameter("diffuse", new Color(1f, 1f, 1f))
      sunflow.shader("default", new DiffuseShader)

      // Shader ambient occlusion
      if (settings.amboccSamples > 0) {
        sunflow.parameter("bright", new Color(1f))
        sunflow.parameter("dark", new Color(0f))
        sunflow.parameter("samples", settings.amboccSamples)
        sunflow.parameter("maxdist", settings.amboccMaxDist)
        sunflow.shader("ambocc", "ambient_occlusion");
      }

      // Camera
      val eye = new Point3(362f, 100f, 26f)
      val target = new Point3(361f, 96f, 27f)
      val up = new Vector3(0, 1f, 0f)
      sunflow.parameter("fov", 500f)
      sunflow.parameter(
        "aspect",
        settings.width.toFloat / settings.height.toFloat
      )
      sunflow.parameter("transform", Matrix4.lookAt(eye, target, up))

      val camName = sunflow.getUniqueName("camera")
      sunflow.camera(camName, new PinholeLens)

      sunflow.parameter("camera", camName);
      sunflow.options(SunflowAPI.DEFAULT_OPTIONS);
    }

    def render: Unit = {
      val display = new FileDisplay(filePath)
      val opt = SunflowAPI.DEFAULT_OPTIONS
      sunflow.render(opt, display)
    }

    def addGeometry(geometry: Geometry): Unit = {
      val name = "geometry_" + names.size
      val triangles = new Array[Int](geometry.getMesh().getIndexBuffer().size())

      for (i <- 0 to triangles.length - 1) {
        triangles(i) = geometry.getMesh().getIndexBuffer().get(i)
      }

      val points =
        BufferUtils.getFloatArray(
          geometry.getMesh().getFloatBuffer(VertexBuffer.Type.Position)
        )
      val normals =
        BufferUtils.getFloatArray(
          geometry.getMesh().getFloatBuffer(VertexBuffer.Type.Normal)
        )
      val uvs =
        BufferUtils.getFloatArray(
          geometry.getMesh().getFloatBuffer(VertexBuffer.Type.TexCoord2)
        )

      val matrix = new Array[Float](16)

      geometry.getWorldMatrix().get(matrix, true)
      val transform = new Matrix4(matrix, true)

      sunflow.parameter("shader", "default")
      sunflow.parameter("triangles", triangles)
      sunflow.parameter("points", "point", "vertex", points)
      sunflow.parameter("normals", "vector", "vertex", normals)
      sunflow.parameter("uvs", "texcoord", "vertex", uvs)
      sunflow.geometry(name, new TriangleMesh())

      sunflow.parameter("shaders", "default")
      sunflow.parameter("transform", transform)
      sunflow.instance(name + ".instance", name)

      names = names.updated(geometry, name)
    }

    /*
     * Documentation about lights can be found here:<br>
     * https://web.archive.org/web/20081211075958/http://sfwiki.geneome.net/index.php5?title=Lights#searchInput
     */
    def addPointLight(color: Color, pos: Point3): Unit = {
      sunflow.parameter("center", pos)
      sunflow.parameter("power", color)
      val lightName = sunflow.getUniqueName("pointLight")
      sunflow.light(lightName, "point")
    }

    def addSpotLight(color: Color, pos: Point3, dir: Vector3): Unit = {
      // this isn't quite what I expect.
      // all rays are parallel, not at an agle it seems
      // it renders an infinite cylinder of light.
      sunflow.parameter("source", pos)
      sunflow.parameter("dir", dir);
      sunflow.parameter("radius", 2f) // TODO config this
      sunflow.parameter("radiance", color)
      val lightName = sunflow.getUniqueName("dirlight")
      sunflow.light(lightName, "directional")
    }

  }
}

class DebugLightmapBaker(filePath: String, settings: BakeSettings)
    extends LightmapBaker(filePath, settings) {

  val width = 8000
  val height = 8000
  val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

  // TODO broken, fix if needed, or remove.
  def bake2(node: Node, lights: Set[Light], extras: Set[Spatial]): Unit = {

    // draw objects
    node.breadthFirst(s => {
      if (s.isInstanceOf[Geometry])
        drawGeometry(s.asInstanceOf[Geometry])
    })

    ImageIO.write(img, "png", new File(filePath))
  }

  def drawGeometry(geom: Geometry): Unit = {
    val buf = geom.getMesh().getBuffer(VertexBuffer.Type.TexCoord2).getData

    val (v1, v2, v3, v4) =
      (new Vector2f, new Vector2f, new Vector2f, new Vector2f)

    BufferUtils.populateFromBuffer(v1, buf.asInstanceOf[FloatBuffer], 0)
    BufferUtils.populateFromBuffer(v2, buf.asInstanceOf[FloatBuffer], 1)
    BufferUtils.populateFromBuffer(v3, buf.asInstanceOf[FloatBuffer], 2)
    BufferUtils.populateFromBuffer(v4, buf.asInstanceOf[FloatBuffer], 3)

    val minx = (v1.x * width).toInt
    val maxx = (v4.x * width).toInt - 1
    val miny = (v1.y * height).toInt
    val maxy = (v4.y * height).toInt - 1

    println(s"Drawing square ($minx,$miny)-($maxx,$maxy)")
    for (i <- minx to maxx) {
      draw(i, miny, Color.RED)
      draw(i, miny + 1, Color.RED)
      draw(i, maxy, Color.RED)
      draw(i, maxy - 2, Color.RED)
      draw(
        i,
        miny + (((i - minx).toFloat / (maxx - minx)) * (maxy - miny)).toInt,
        Color.RED
      )
      draw(
        i,
        maxy - (((i - minx).toFloat / (maxx - minx)) * (maxy - miny)).toInt,
        Color.RED
      )
    }
    for (i <- miny to maxy) {
      draw(minx, i, Color.RED)
      draw(minx + 1, i, Color.RED)
      draw(maxx, i, Color.RED)
      draw(maxx - 1, i, Color.RED)
      draw(
        minx + (((i - miny).toFloat / (maxy - miny)) * (maxx - minx)).toInt,
        i,
        Color.RED
      )
      draw(
        maxx - (((i - miny).toFloat / (maxy - miny)) * (maxx - minx)).toInt,
        i,
        Color.RED
      )
    }
  }

  def draw(x: Int, y: Int, c: Color): Unit = {
    img.setRGB(x, img.getHeight() - y - 1, c.toRGB())
  }
}
