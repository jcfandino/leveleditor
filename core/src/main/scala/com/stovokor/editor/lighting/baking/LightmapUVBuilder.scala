package com.stovokor.editor.lighting.baking

import com.jme3.scene.Geometry
import com.jme3.math.Vector2f
import com.jme3.scene.VertexBuffer
import com.jme3.util.BufferUtils
import java.lang.Math.min
import java.lang.Math.max
import com.jme3.scene.Spatial
import com.jme3.scene.Node
import scala.jdk.CollectionConverters._
import java.nio.FloatBuffer
import com.jme3.scene.Mesh
import com.stovokor.editor.util.math.RectanglePacker
import com.stovokor.editor.util.math.RectanglePacker.Rectangle
import com.jme3.math.Vector3f
import com.stovokor.editor.factory.UserDataFlags

class LightmapUVBuilder {
  var collectedGeometries: List[Geometry] = List.empty

  val precision = 1000f
  val padding = precision.toInt / 4

  def collectGeometries(spatial: Spatial): Unit = {
    if (!UserDataFlags.isSkipLighmap(spatial)) {
      if (spatial.isInstanceOf[Geometry]) {
        val geom = spatial.asInstanceOf[Geometry]
        collectedGeometries = geom :: collectedGeometries
      } else if (spatial.isInstanceOf[Node]) {
        for (child <- spatial.asInstanceOf[Node].getChildren().asScala)
          collectGeometries(child)
      }
    }
  }

  def pack = {
    val quads = collectedGeometries.map(GeomRec(_)).toSeq.sortBy(q => -q.h)
    val sums = maxAndMins(quads)
    val worst = max(sums.sumW, sums.sumH)
    val ideal = max(sums.maxW, sums.maxH)
    val (width, recs) = findOptimalPack(ideal, worst, quads, Seq())

    for ((quad, rec) <- recs) yield {
      QuadUV(
        quad,
        toFloat(rec.x + padding) / toFloat(width),
        toFloat(rec.y + padding) / toFloat(width),
        toFloat(rec.x + rec.width - (2 * padding)) / toFloat(width),
        toFloat(rec.y + rec.height - (2 * padding)) / toFloat(width)
      )
    }
  }

  abstract class UvRec(val w: Int, val h: Int, val geom: Geometry) {
    def applyUV(uv: QuadUV): Unit
    override def toString = s"UvRec($w,$h,${geom.getName})"
  }

  object GeomRec {
    // Will find a rectangle to fit the geometry coordinates.<br>
    // This only works for surfaces in a plane.<br>
    // If need to map convex geometries, then it needs to be modified to support them.
    def apply(geom: Geometry) = {
      val minSize = toInt(2f)
      // To find proportional geom width and height:
      val positions = getPositions(geom)
      val indices = getIndices(geom)
      // grab three vertices and create two vectors
      var normal = Vector3f.ZERO
      val first = positions(indices.get(0))
      val v1 = positions(indices.get(1)).subtract(first).normalize
      var v2 = v1
      var idx = 2
      // do cross product (if result null vector, grab another vertex)
      while (normal.length == 0f && idx < indices.size()) {
        val pos = positions(indices.get(idx))
        v2 = pos.subtract(first).normalize
        // 4. normalize to get plane normal
        normal = v1.cross(v2).normalize
        idx += 1
      }
      // cross product normal with one vector from the start
      // new vector and original vector become the axis
      val uv = v1
      val vv = normal.cross(v1).normalize
      var (umin, umax) = (Float.MaxValue, Float.MinValue)
      var (vmin, vmax) = (Float.MaxValue, Float.MinValue)
      for (vertex <- positions) {
        // for every vertex: dot product with axis to find distances (UV)
        val u = uv.dot(vertex)
        val v = vv.dot(vertex)
        // collect min and max UVs, subtract to get dimensions
        umin = Math.min(umin, u)
        umax = Math.max(umax, u)
        vmin = Math.min(vmin, v)
        vmax = Math.max(vmax, v)
      }
      new GeomRec(
        toInt(vmax - vmin).max(minSize),
        toInt(umax - umin).max(minSize),
        geom
      )
    }

    private def getPositions(geom: Geometry) = {
      val buffer = geom.getMesh.getBuffer(VertexBuffer.Type.Position)
      val data = buffer.getDataReadOnly.asInstanceOf[FloatBuffer]
      BufferUtils.getVector3Array(data)
    }

    private def getIndices(geom: Geometry) = geom.getMesh.getIndexBuffer()

  }

  class GeomRec(w: Int, h: Int, geom: Geometry) extends UvRec(w, h, geom) {
    def applyUV(uv: QuadUV): Unit = {
      val m = uv.rec.geom.getMesh
      val cb = getTextCoor(m)
      // It has to be a FloatBuffer
      val fb = cb.getDataReadOnly.asInstanceOf[FloatBuffer]
      val floats = BufferUtils.getVector2Array(fb)
      for (v <- floats) {
        v.x = uv.a + v.x * (uv.c - uv.a)
        v.y = uv.b + v.y * (uv.d - uv.b)
      }
      val coors = BufferUtils.createFloatBuffer(floats: _*)
      m.setBuffer(VertexBuffer.Type.TexCoord2, 2, coors)
      m.updateBound()
      uv.rec.geom.updateModelBound()
    }

    def getTextCoor(m: Mesh) = {
      if (m.getBuffer(VertexBuffer.Type.TexCoord2) != null)
        m.getBuffer(VertexBuffer.Type.TexCoord2)
      else // fallback to the texture
        m.getBuffer(VertexBuffer.Type.TexCoord)
    }
  }

  case class QuadUV(rec: UvRec, a: Float, b: Float, c: Float, d: Float) {
    def applyUV(): Unit = {
      rec.applyUV(this)
    }
    override def toString = s"QuadUV($rec,$a,$b,$c,$d)"
  }

  def applyUVs = {
    for (uv <- pack) uv.applyUV()
  }

  def toInt(f: Float) = (f * precision).toInt
  def toFloat(i: Int) = (i.toFloat) / precision

  def findOptimalPack(
      ideal: Int,
      worst: Int,
      quads: Seq[UvRec],
      bestSoFar: Seq[(UvRec, Rectangle)]
  ): (Int, Seq[(UvRec, Rectangle)]) = {
    if (worst - ideal < 2) (worst, bestSoFar)
    else {

      val pivot = (ideal + worst) / 2
      val packer = new RectanglePacker[UvRec](pivot, pivot, padding)

      def insert(quads: Seq[UvRec], collected: Seq[Rectangle]): Seq[Rectangle] =
        quads match {
          case Seq() => collected
          case q :: qa => {
            val result = packer.insert(q.w, q.h, q)
            if (result != null) insert(qa, result +: collected) else Seq()
          }
        }

      def asPairs: Seq[(UvRec, Rectangle)] = {
        quads.map(q => (q, packer.findRectangle(q)))
      }

      val recs = insert(quads, Seq())
      val (nextIdeal, nextWorst, nextBest) =
        if (recs.isEmpty) { // Failed can't shrink any longer
          (pivot, worst, bestSoFar)
        } else { // Fitted, lets try another time
          (ideal, pivot, asPairs)
        }
      findOptimalPack(nextIdeal, nextWorst, quads, nextBest)
    }
  }

  case class Sums(maxW: Int, maxH: Int, sumW: Int, sumH: Int) {
    def sum(rec: UvRec) =
      Sums(max(maxW, rec.w), max(maxH, rec.h), sumW + rec.w, sumH + rec.h)
  }

  def maxAndMins(quads: Seq[UvRec]): Sums =
    quads.foldLeft(Sums(0, 0, 0, 0))(_ sum _)

}
