package com.stovokor.editor.lighting

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.stovokor.editor.factory.Mesh2dFactory
import com.stovokor.editor.model.Point
import com.stovokor.editor.state.BaseState
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.OpenMap
import com.stovokor.util.PointClicked
import com.stovokor.util.PointDragged
import com.stovokor.util.StartNewMap
import com.stovokor.util.ViewModeSwitch
import com.jme3.scene.Geometry
import com.stovokor.editor.gui.Mode2DLayers
import com.jme3.scene.Node
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.control.ConstantSizeOnScreenControl
import com.stovokor.editor.control.PointDragControl
import com.jme3.math.Quaternion
import com.jme3.math.FastMath
import com.jme3.scene.shape.Box
import com.jme3.scene.Spatial.CullHint
import com.jme3.scene.shape.Sphere
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.util.CoordinateSystem
import com.jme3.scene.shape.Cylinder
import com.jme3.scene.VertexBuffer.Type
import com.jme3.util.BufferUtils
import com.jme3.shader.VarType
import com.jme3.material.Material
import jme3tools.optimize.GeometryBatchFactory

class LightTool2DViewState(val view: LightToolView)
    extends BaseState
    with EditorEventListener
    with MaterialFactoryClient {

  def entities2dNode = getOrCreateNode(get2DNode, "lights")

  var meshFactory: Mesh2dLightFactory = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    meshFactory = new Mesh2dLightFactory(this)
    EventBus.subscribeByType(this, classOf[PointClicked])
    EventBus.subscribeByType(this, classOf[ViewModeSwitch])
    EventBus.subscribeByType(this, classOf[PointDragged])
    EventBus.subscribeByType(this, classOf[LightAdded])
    EventBus.subscribeByType(this, classOf[LightDeleted])
    EventBus.subscribeByType(this, classOf[LightUpdated])
    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])
    redraw()
  }

  def onEvent(event: EditorEvent) = event match {
    case PointClicked(point)    => pointClicked(point)
    case PointDragged(from, to) => move(from, to)
    case LightAdded(id) => {
      redraw()
      view.present(id, LightRepository.get(id))
    }
    case LightDeleted(id) => {
      redraw()
      view.clear()
    }
    case LightUpdated(id) => {
      redraw()
      view.present(id, LightRepository.get(id))
    }
    case StartNewMap()    => view.clear()
    case OpenMap()        => view.clear()
    case ViewModeSwitch() => // TODO
    case _                =>
  }

  def pointClicked(point: Point): Unit = {
    LightRepository.find(point) match {
      case Some((id, light)) => view.present(id, light)
      case None              => view.clear()
    }
  }

  def move(from: Point, to: Point): Unit = {
    LightRepository.find(from) match {
      case Some((id, light)) => {
        val updated =
          light.updatePos(new Vector3f(to.x, to.y, light.position.z))
        LightRepository.update(id, updated)
        EventBus.trigger(LightUpdated(id))
      }
      case None =>
    }
  }

  def cancel(): Unit = {
    EventBus.removeFromAll(this)
    view.clear()
    entities2dNode.detachAllChildren()
    stateManager.detach(this)
  }

  def redraw(): Unit = {
    entities2dNode.detachAllChildren()
    LightRepository.all.foreach(meshFactory.drawLight(_))
  }

  class Mesh2dLightFactory(materialFactory: MaterialFactoryClient) {

    def drawLight(light: Light): Unit = {
      val point = Point(light.pos.x, light.pos.y)
      val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
      mat.setParam("VertexColor", VarType.Boolean, true)
      val node = new Node("light")

      val innerRadius = 0.12f
      val outerRadius = 0.16f
      val clickableRadius = 0.2f
      // view as a small circle
      val inside =
        createCircle(innerRadius, 1, mat, light.col.toPreview.toColorRGBA)
      val outside = createCircle(outerRadius, 0.5f, mat, ColorRGBA.White)

      // add invisible geometry to click
      val clickableVertex = new Geometry(
        "clickableLight",
        new Box(clickableRadius, clickableRadius, 0.1f)
      )
      clickableVertex.setMaterial(mat)
      clickableVertex.setCullHint(CullHint.Always)

      node.setLocalTranslation(point.x, point.y, Mode2DLayers.vertices)
      node.addControl(new ConstantSizeOnScreenControl())
      node.addControl(new PointDragControl(point))

      node.attachChild(inside)
      node.attachChild(outside)
      node.attachChild(clickableVertex)
      // GeometryBatchFactory.optimize(node) - it breaks the position and size
      entities2dNode.attachChild(node)
    }

    def createCircle(
        radius: Float,
        height: Float,
        mat: Material,
        color: ColorRGBA
    ) = {
      val cyl = new Cylinder(2, 16, radius, height, true)
      val vertexCount = 2 * (16 + 1) + 2 + 2 * (16 + 1)
      val array = (0 to vertexCount).map(_ => color)
      cyl.setBuffer(Type.Color, 4, BufferUtils.createFloatBuffer(array: _*))
      val geo = new Geometry("light", cyl)
      geo.setMaterial(mat)
      geo
    }

  }
}
