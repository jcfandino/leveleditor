package com.stovokor.editor.lighting.baking

object BakeSettings {
  def filterOptions = List(
    "box",
    "triangle",
    "gaussian",
    "mitchell",
    "catmull-rom",
    "blackman-harris",
    "sinc",
    "lanczos",
    "bspline"
  )

  def aaOptions = List(
    ("off", 0, 0),
    ("quickest", -2, 0),
    ("preview", 0, 1),
    ("final", 1, 2),
    ("extra", 1, 3)
  )

  def previewSettings(size: Int) = new BakeSettings(
    width = size,
    height = size,
    aaMin = 0,
    aaMax = 1,
    samples = 2,
    contrast = 0.1f,
    filter = "gaussian",
    jitter = true,
    amboccSamples = 4,
    amboccMaxDist = 10f
  )

  def goodSettings(size: Int) = new BakeSettings(
    width = size,
    height = size,
    aaMin = 1,
    aaMax = 2,
    samples = 8,
    contrast = 0.1f,
    filter = "gaussian",
    jitter = true,
    amboccSamples = 16,
    amboccMaxDist = 10f
  )
}

case class BakeSettings(
    width: Int,
    height: Int,
    aaMin: Int,
    aaMax: Int,
    samples: Int,
    contrast: Float,
    filter: String,
    jitter: Boolean,
    amboccSamples: Int,
    amboccMaxDist: Float,
    colorFactor: Float = 1000
) {}
