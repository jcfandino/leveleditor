package com.stovokor.editor.lighting

import scala.jdk.CollectionConverters._
import scala.util.Try

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Button
import com.simsilica.lemur.Checkbox
import com.simsilica.lemur.Container
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Label
import com.simsilica.lemur.ListBox
import com.simsilica.lemur.TextField
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.core.VersionedList
import com.simsilica.lemur.event.DefaultMouseListener
import com.stovokor.editor.export.MapExporterRegistry
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.input.Modes.EditMode
import com.stovokor.editor.input.Modes.SelectionMode
import com.stovokor.editor.model.repository.ExtensionRepository
import com.stovokor.editor.state.BaseState
import com.stovokor.editor.state.ExtensionPanelState
import com.stovokor.editor.state.ExtensionToolView
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.OpenMap
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.StartNewMap
import com.stovokor.util.LemurExtensions._

import com.stovokor.editor.state.OptionPanelAccess
import com.simsilica.lemur.OptionPanelState
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.core.VersionedHolder
import com.simsilica.lemur.ColorChooser

class LightToolGuiState
    extends BaseState
    with EditorEventListener
    with ExtensionToolView
    with OptionPanelAccess {

  var panel: Container = null
  var view: LightToolView = null

  val width = 220
  def windowWidth = app.getCamera.getWidth
  def windowHeight = app.getCamera.getHeight

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    // register file extension
    ExtensionRepository().register("lights", LightDataExtension)
    // register lightmap exporter
    MapExporterRegistry.register(new LightmapExporter())
    clear()

    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])

    panel = createPanel()
    stateManager
      .getState(classOf[ExtensionPanelState])
      .registerExtensionPanel("Lights", panel, this)
  }

  def onEvent(event: EditorEvent) = event match {
    case StartNewMap() => clear()
    case OpenMap()     => clear()
  }

  def clear(): Unit = {
    Option(stateManager.getState(classOf[LightTool2DViewState]))
      .foreach(_.cancel())
    Option(stateManager.getState(classOf[LightTool3DViewState]))
      .foreach(_.cancel())
  }

  override def setOpen(open: Boolean): Unit = {
    if (open) {
      stateManager.attach(new LightTool2DViewState(view))
      stateManager.attach(new LightTool3DViewState(view))
    } else {
      Option(stateManager.getState(classOf[LightTool2DViewState]))
        .foreach(_.cancel())
      Option(stateManager.getState(classOf[LightTool3DViewState]))
        .foreach(_.cancel())
    }
  }

  def createPanel() = {
    // container for all elements
    val content = new Container
    content.addMouseListener(new DefaultMouseListener) // ignore clicks

    // buttons panel
    val buttons = content.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    val add = buttons.addChild(
      GuiFactory.button(
        icon = "edit-add-3.png",
        description = "",
        label = "Add"
      )
    )
    add.onClick(() =>
      addNewLight(
        PointLight("light", true, Color(ColorRGBA.White), Vector()),
        None
      )
    )

    buttons.addChild(new Container)

    // light view
    val viewPanel =
      content.addChild(new Container(new SpringGridLayout(Axis.Y, Axis.X)))
    view = new LightToolView(viewPanel, this)

    // filling
    val filling = content.addChild(new Container)
    filling.setPreferredSize(new Vector3f(220, 50, 0))
    content
  }

  def addNewLight(prototype: Light, zPos: Option[Float]): Unit = {
    EventBus.trigger(EditModeSwitch(EditMode.Select))
    EventBus.trigger(SelectionModeSwitch(SelectionMode.Point))
    stateManager.attach(new LightToolAddState(prototype, zPos))
  }

}

class LightToolView(panel: Container, controller: LightToolGuiState) {

  implicit val decimals: Int = 3 // for number fields
  // initialize
  clear()

  val width = 216

  type Selection = (Long, Light)
  var current: Selection = null

  object Mutators {
    var name: TextField = null
    var baked: Checkbox = null
    var posx: TextField = null
    var posy: TextField = null
    var posz: TextField = null
    var dirx: TextField = null
    var diry: TextField = null
    var dirz: TextField = null
    var colr: TextField = null
    var colg: TextField = null
    var colb: TextField = null

    def valid = {
      //      proto != null &&
      val posOk = posx != null && Try(() => posx.getText.toFloat).isSuccess &&
        posy != null && Try(() => posy.getText.toFloat).isSuccess &&
        posz != null && Try(() => posz.getText.toFloat).isSuccess
      val dirOk = dirx != null && Try(() => dirx.getText.toFloat).isSuccess &&
        diry != null && Try(() => diry.getText.toFloat).isSuccess &&
        dirz != null && Try(() => dirz.getText.toFloat).isSuccess
      val colOk = colr != null && Try(() => colr.getText.toFloat).isSuccess &&
        colg != null && Try(() => colg.getText.toFloat).isSuccess &&
        colb != null && Try(() => colb.getText.toFloat).isSuccess

      posOk && dirOk && colOk
    }

    def toFloat(text: TextField) =
      if (text.getText.isEmpty) 0f else text.getText.toFloat

    def updated = current._2
      .updateName(name.getText)
      .updateBaked(baked.isChecked)
      .updateColor(
        new ColorRGBA(toFloat(colr), toFloat(colg), toFloat(colb), 1f)
      )
      .updatePos(new Vector3f(toFloat(posx), toFloat(posy), toFloat(posz)))
      .updateDir(new Vector3f(toFloat(dirx), toFloat(diry), toFloat(dirz)))
  }

  def clear(): Unit = {
    current = null
    panel.clearChildren()
    val text = panel.addChild(new Label("Select or add light..."))
    val filling = panel.addChild(new Container)
    filling.setPreferredSize(new Vector3f(width.toFloat, 300, 0))
  }

  def present(id: Long, light: Light): Unit = {
    current = (id, light)
    panel.clearChildren()
    buttonsBar(panel, id, light)
    typeSelection(panel, id, light)
    detailsPanel(panel, id, light)
    val filling = panel.addChild(new Container)
    filling.setPreferredSize(new Vector3f(width.toFloat, 0, 0))
  }

  def redraw(): Unit = {
    present(current._1, current._2)
  }

  def applyChanges(): Unit = {
    if (Mutators.valid) {
      val updated = Mutators.updated
      LightRepository.update(current._1, updated)
      EventBus.trigger(LightUpdated(current._1))
    } else {
      println("Cannot update: Invalid values!") // TODO  show in UI
    }
  }

  def copyCurrent(): Unit = {
    controller.addNewLight(current._2, Some(current._2.pos.z))
  }

  private def buttonsBar(panel: Container, id: Long, light: Light): Unit = {
    val buttons = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    // delete
    val delete = buttons.addChild(
      GuiFactory.button(icon = "dialog-cancel-3.png", description = "")
    )
    delete.onClick(() => {
      LightRepository.remove(id)
      EventBus.trigger(LightDeleted(id))
    })
    // update
    val update = buttons.addChild(
      GuiFactory.button(icon = "dialog-apply.png", description = "")
    )
    update.onClick(applyChanges)
    // copy
    val copy = buttons.addChild(
      GuiFactory.button(icon = "edit-copy-3.png", description = "")
    )
    copy.onClick(copyCurrent)
    // padding
    buttons.addChild(new Container)
  }

  private def typeSelection(panel: Container, id: Long, light: Light): Unit = {
    var bar = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    bar.addChild(new Label("Type:"))
    val options = new VersionedList(
      List("ambient", "directional", "point", "spot").asJava
    )
    val listbox = bar.addChild(new ListBox(options))
    listbox.setVisibleItems(4)

    light match {
      case AmbientLight(_, _, _) => listbox.getSelectionModel.setSelection(0)
      case DirectionalLight(_, _, _, _) =>
        listbox.getSelectionModel.setSelection(1)
      case PointLight(_, _, _, _)   => listbox.getSelectionModel.setSelection(2)
      case SpotLight(_, _, _, _, _) => listbox.getSelectionModel.setSelection(3)
    }
    listbox.onClick(() => {
      val selected = current._2
      listbox.getSelectionModel.getSelection.toInt match {
        case 0 =>
          current = (
            current._1,
            AmbientLight(selected.name, selected.col, selected.pos)
          )
        case 1 =>
          current = (
            current._1,
            DirectionalLight(
              selected.name,
              selected.col,
              selected.pos,
              Vector(0, 0, -1)
            )
          )
        case 2 =>
          current = (
            current._1,
            PointLight(
              selected.name,
              selected.baked,
              selected.col,
              selected.pos
            )
          )
        case 3 =>
          current = (
            current._1,
            SpotLight(
              selected.name,
              selected.baked,
              selected.col,
              selected.pos,
              Vector(0, 0, -1)
            )
          )
        case _ =>
      }
      redraw()
    })
  }

  private def detailsPanel(panel: Container, id: Long, light: Light): Unit = {
    panel.addChild(new Label("Properties"))

    // name
    var bar = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    bar.addChild(new Label("Name:"))
    Mutators.name = bar.addChild(GuiFactory.textField(light.name))
    // coordinates
    val coords = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    coords.addChild(new Label("Pos"))
    coords.addChild(new Label("X:"))
    Mutators.posx = coords.addChild(GuiFactory.numberField(light.position.x))
    coords.addChild(new Label("Y:"))
    Mutators.posy = coords.addChild(GuiFactory.numberField(light.position.y))
    coords.addChild(new Label("Z:"))
    Mutators.posz = coords.addChild(GuiFactory.numberField(light.position.z))
    // direction
    if (light.directed) {
      var vector = light match {
        case DirectionalLight(_, _, _, dir) => dir
        case SpotLight(_, _, _, _, dir)     => dir
      }
      val direction = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      direction.addChild(new Label("Dir"))
      direction.addChild(new Label("X:"))
      Mutators.dirx = direction.addChild(GuiFactory.numberField(vector.x))
      direction.addChild(new Label("Y:"))
      Mutators.diry = direction.addChild(GuiFactory.numberField(vector.y))
      direction.addChild(new Label("Z:"))
      Mutators.dirz = direction.addChild(GuiFactory.numberField(vector.z))
    } else {
      // TODO hack
      Mutators.dirx = GuiFactory.numberField(0)
      Mutators.diry = GuiFactory.numberField(0)
      Mutators.dirz = GuiFactory.numberField(-1)
    }
    // color
    val color = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    color.addChild(new Label("Col"))
    color.addChild(new Label("R:"))
    Mutators.colr = color.addChild(GuiFactory.numberField(light.color.r, false))
    color.addChild(new Label("G:"))
    Mutators.colg = color.addChild(GuiFactory.numberField(light.color.g, false))
    color.addChild(new Label("B:"))
    Mutators.colb = color.addChild(GuiFactory.numberField(light.color.b, false))
    val preview = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    val prevbox = preview.addChild(new Button(" "))
    prevbox.setBackground(
      new QuadBackgroundComponent(light.col.toPreview.toColorRGBA)
    )
    prevbox.onClick(() => openColorPicker())
    // baked checkbox
    if (light.canBake) {
      var baked = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
        )
      )
      Mutators.baked = baked.addChild(new Checkbox("Baked"))
      Mutators.baked.setChecked(light.baked)
    } else {
      Mutators.baked = new Checkbox("Baked")
    }

    def openColorPicker(): Unit = {
      ColorDialog.showDialog(
        light.col,
        picked => {
          def format = s"%.${decimals}f"
          Mutators.colr.setText(format.format(picked.r))
          Mutators.colg.setText(format.format(picked.g))
          Mutators.colb.setText(format.format(picked.b))
          applyChanges()
        }
      )
    }
  }

  object ColorDialog {
    val panel = new ColorChooser()
    var last = List[Color]()
    val maxLast = 20

    def showDialog(initial: Color, callback: Color => Unit) = {
      // lemur's default chooser
      panel.setModel(new VersionedHolder(initial.toColorRGBA))
      val cancel = GuiFactory.action("x", "Cancel")
      val ok = GuiFactory.action(
        "ok",
        "Ok",
        () => {
          val selected = Color(panel.getModel.getObject)
          last = (selected :: last).slice(0, maxLast)
          callback(selected)
        }
      )
      val window = new OptionPanel("Color chooser", "", null, cancel, ok)
      window.getContainer.addChild(panel)
      // latest colors
      val lastPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
      last.foreach(color => {
        val btn = GuiFactory.actionButton(
          "",
          () => {
            controller.closeModal()
            callback(color)
          }
        )
        btn.setBackground(new QuadBackgroundComponent(color.toColorRGBA))
        lastPanel.addChild(btn)
      })
      for (i <- last.size to maxLast - 1) {
        lastPanel.addChild(new Label(""))
      }
      window.getContainer.addChild(lastPanel)
      controller.showModal(window)
    }
  }

}
