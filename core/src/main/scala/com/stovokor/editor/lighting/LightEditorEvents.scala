package com.stovokor.editor.lighting

import com.stovokor.util.EditorEvent
import com.jme3.math.Vector3f

case class LightAdded(id: Long) extends EditorEvent
case class LightDeleted(id: Long) extends EditorEvent
case class LightUpdated(id: Long) extends EditorEvent

case class LightDragged(id: Long, movement: Vector3f) extends EditorEvent
