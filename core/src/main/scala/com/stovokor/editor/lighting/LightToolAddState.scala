package com.stovokor.editor.lighting

import com.stovokor.editor.state.BaseState
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.PointClicked
import com.stovokor.util.EventBus
import com.stovokor.util.EditorEventListener
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.editor.input.Modes.EditMode
import com.stovokor.editor.model.Point
import com.jme3.math.Vector3f
import com.stovokor.editor.model.repository.SectorRepository
import com.jme3.math.ColorRGBA
import com.stovokor.editor.model.Sector

class LightToolAddState(prototype: Light, zPos: Option[Float])
    extends BaseState
    with EditorEventListener {

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[PointClicked])
    EventBus.subscribeByType(this, classOf[EditModeSwitch])
  }
  def onEvent(event: EditorEvent) = event match {
    case PointClicked(point) => addLight(point)
    case ViewModeSwitch()    => cancel()
    case EditModeSwitch(m)   => cancel()
    case _                   =>
  }

  def addLight(point: Point): Unit = {
    val pos = new Vector3f(point.x, point.y, guessZ(point))
    val id = LightRepository.add(prototype.updatePos(pos))
    EventBus.trigger(LightAdded(id))
    cancel()
  }

  def cancel(): Unit = {
    EventBus.removeFromAll(this)
    stateManager.detach(this)
  }

  def guessZ(point: Point) = {
    val sector = SectorRepository()
      .findInside(point)
      .headOption
      .map(_._2)

    // use the original pos if present
    zPos
      // but only if it fits inside the sector
      .flatMap(z =>
        sector
          .filter(s =>
            s.floorHeightOn(point) < z && s.ceilingHeightOn(point) > z
          )
          .map(_ => z)
      )
      // otherwise place it in the middle
      .orElse(sector.map(zOf))
      // default to 0 if no sector found
      .getOrElse(0f)
  }

  def zOf(sector: Sector) = (sector.floor.height + sector.ceiling.height) / 2f
}
