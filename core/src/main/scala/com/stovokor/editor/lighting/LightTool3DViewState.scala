package com.stovokor.editor.lighting

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.control.AbstractControl
import com.jme3.scene.shape.Box
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.state.BaseState
import com.stovokor.util.CoordinateSystem
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.GridSnapper
import com.stovokor.util.LemurExtensions.SpatialExtension
import com.stovokor.util.OpenMap
import com.stovokor.util.StartNewMap
import com.stovokor.util.ViewModeSwitch
import com.jme3.scene.shape.Sphere
import com.jme3.scene.shape.Line
import com.jme3.scene.shape.Cylinder
import com.stovokor.util.Arrow
import com.stovokor.editor.control.DragArrowControl

class LightTool3DViewState(val view: LightToolView)
    extends BaseState
    with EditorEventListener
    with MaterialFactoryClient {

  def lights3dNode = getOrCreateNode(get3DNode, "lights")

  var meshFactory: Mesh3dLightFactory = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    meshFactory = new Mesh3dLightFactory(this)
    EventBus.subscribeByType(this, classOf[LightAdded])
    EventBus.subscribeByType(this, classOf[LightDeleted])
    EventBus.subscribeByType(this, classOf[LightUpdated])
    EventBus.subscribeByType(this, classOf[LightDragged])
    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])
    redraw()
  }

  def onEvent(event: EditorEvent) = event match {
    case LightAdded(id) => {
      redraw()
    }
    case LightDeleted(id) => {
      redraw()
    }
    case LightUpdated(id) => {
      redraw()
    }
    case LightDragged(id, movement) => {
      val e = LightRepository.get(id)
      val pos = e.position.add(movement)
      val snapped =
        new Vector3f(GridSnapper.snapX(pos.x), GridSnapper.snapY(pos.y), pos.z)
      LightRepository.update(id, e.updatePos(snapped))
      EventBus.trigger(new LightUpdated(id))
    }
    case ViewModeSwitch() => // TODO
    case _                =>
  }

  def cancel(): Unit = {
    EventBus.removeFromAll(this)
    lights3dNode.detachAllChildren()
    stateManager.detach(this)
  }

  def redraw(): Unit = {
    lights3dNode.detachAllChildren()
    for ((id, light) <- LightRepository.lights) {
      val node = new Node("light")
      // draw different shapes depending on the type of light
      val geom = light match {
        case AmbientLight(_, col, pos) =>
          meshFactory.drawSun(node, pos.toVector3f, col.toPreview.toColorRGBA)
        case DirectionalLight(_, col, pos, dir) =>
          meshFactory.drawDirectional(
            node,
            pos.toVector3f,
            dir.toVector3f,
            col.toPreview.toColorRGBA
          )
        case PointLight(_, _, col, pos) =>
          meshFactory.drawPoint(node, pos.toVector3f, col.toPreview.toColorRGBA)
        case SpotLight(_, _, col, pos, dir) =>
          meshFactory.drawSpot(
            node,
            pos.toVector3f,
            dir.toVector3f,
            col.toPreview.toColorRGBA
          )
      }
      geom.addControl(new LightSelectionControl(() => view.present(id, light)))
      // axis
      val axis = new Node("light-axis")
      meshFactory.drawArrow(
        id,
        axis,
        node,
        0.1f,
        Vector3f.UNIT_X,
        .5f,
        ColorRGBA.Red
      )
      meshFactory.drawArrow(
        id,
        axis,
        node,
        0.1f,
        Vector3f.UNIT_Y,
        .5f,
        ColorRGBA.Green
      )
      meshFactory.drawArrow(
        id,
        axis,
        node,
        0.1f,
        Vector3f.UNIT_Z,
        .5f,
        ColorRGBA.Blue
      )
      node.setLocalTranslation(CoordinateSystem.toJme(light.position))
      node.attachChild(axis)
      lights3dNode.attachChild(node)
    }
  }
}

class Mesh3dLightFactory(materialFactory: MaterialFactoryClient) {
  def drawSun(node: Node, pos: Vector3f, color: ColorRGBA): Spatial = {
    val mat = materialFactory.plainColor(color)
    // core
    val geo = new Geometry("light", new Sphere(16, 16, 0.1f))
    geo.setMaterial(mat)
    node.attachChild(geo)
    // rays
    for (x <- -1 to 1; y <- -1 to 1; z <- -1 to 1) {
      val dir = new Vector3f(x.toFloat, y.toFloat, z.toFloat)
      val ray = new Geometry(
        "ray",
        new Line(dir.normalize.mult(0.12f), dir.normalize.mult(0.2f))
      )
      ray.setMaterial(mat)
      node.attachChild(ray)
    }
    geo
  }

  def drawPoint(node: Node, pos: Vector3f, color: ColorRGBA): Spatial = {
    val mat = materialFactory.plainColor(color)
    // core
    val geo = new Geometry("light", new Sphere(4, 4, 0.05f))
    geo.setMaterial(mat)
    node.attachChild(geo)
    // rays
    for (
      line <- List(
        new Line(
          new Vector3f(-1, -1, -1).mult(0.12f),
          new Vector3f(1, 1, 1).mult(0.12f)
        ),
        new Line(
          new Vector3f(1, -1, -1).mult(0.12f),
          new Vector3f(-1, 1, 1).mult(0.12f)
        ),
        new Line(
          new Vector3f(-1, 1, -1).mult(0.12f),
          new Vector3f(1, -1, 1).mult(0.12f)
        ),
        new Line(
          new Vector3f(-1, -1, 1).mult(0.12f),
          new Vector3f(1, 1, -1).mult(0.12f)
        )
      )
    ) {
      val ray = new Geometry("ray", line)
      ray.setMaterial(mat)
      node.attachChild(ray)
    }
    geo
  }

  def drawDirectional(
      node: Node,
      pos: Vector3f,
      dir: Vector3f,
      color: ColorRGBA
  ): Spatial = {
    val mat = materialFactory.plainColor(color)
    val dirnode = new Node("light")
    // core
    val radius = 0.2f
    val geo = new Geometry("light", new Cylinder(2, 16, radius, 0.02f, true))
    geo.setMaterial(mat)
    dirnode.attachChild(geo)
    // rays
    for (ang <- BigDecimal(0) to (FastMath.TWO_PI, FastMath.QUARTER_PI)) {
      val x = radius * FastMath.cos(ang.toFloat)
      val y = radius * FastMath.sin(ang.toFloat)
      val line = new Line(new Vector3f(x, y, 0), new Vector3f(x, y, 0.2f))
      val ray = new Geometry("ray", line)
      ray.setMaterial(mat)
      dirnode.attachChild(ray)
    }
    node.attachChild(dirnode)
    dirnode
      .getLocalRotation()
      .lookAt(CoordinateSystem.toJme(dir), Vector3f.UNIT_Y)
    dirnode
  }

  def drawSpot(
      node: Node,
      pos: Vector3f,
      dir: Vector3f,
      color: ColorRGBA
  ): Spatial = {
    val mat = materialFactory.plainColor(color)
    val dirnode = new Node("light")
    // core
    val length = 0.2f
    val radius = 0.15f
    val geo = new Geometry(
      "light",
      new Cylinder(2, 16, radius, radius / 2f, length, true, false)
    )
    geo.setMaterial(mat)
    dirnode.attachChild(geo)
    // rays
    for (ang <- BigDecimal(0) to (FastMath.TWO_PI, FastMath.QUARTER_PI)) {
      val x = radius * FastMath.cos(ang.toFloat)
      val y = radius * FastMath.sin(ang.toFloat)
      val line = new Line(
        new Vector3f(x, y, length / 2f),
        new Vector3f(1.5f * x, 1.5f * y, length * 1.5f)
      )
      val ray = new Geometry("ray", line)
      ray.setMaterial(mat)
      dirnode.attachChild(ray)
    }
    node.attachChild(dirnode)
    dirnode
      .getLocalRotation()
      .lookAt(CoordinateSystem.toJme(dir), Vector3f.UNIT_Y)
    dirnode
  }

  def drawArrow(
      id: Long,
      node: Node,
      target: Node,
      offset: Float,
      dir: Vector3f,
      length: Float,
      color: ColorRGBA
  ): Unit = {
    val mat = materialFactory.plainColor(color)
    val arrow = new Geometry("arrow", new Arrow(length))
    val jmepos = CoordinateSystem.toJme(dir.normalize.mult(offset))
    val jmedir = CoordinateSystem.toJme(dir)
    arrow.setMaterial(mat)
    arrow.setLocalRotation(
      new Quaternion().fromAngles(
        jmedir.z * FastMath.HALF_PI,
        0,
        -jmedir.x * FastMath.HALF_PI
      )
    )
    arrow.setLocalTranslation(jmepos)
    arrow.addControl(
      new DragArrowControl(
        jmedir,
        target,
        movement =>
          EventBus.trigger(
            LightDragged(id, CoordinateSystem.toEditor(movement))
          )
      )
    )
    node.attachChild(arrow)
  }

}

class LightSelectionControl(callback: () => Unit) extends AbstractControl {
  var initialized = false
  override def controlUpdate(tpf: Float): Unit = {
    if (!initialized) {
      initialized = true
      getSpatial.onCursorClick((event, target, capture) => callback())
    }
  }

  def controlRender(rm: RenderManager, vp: ViewPort): Unit = {}
}
