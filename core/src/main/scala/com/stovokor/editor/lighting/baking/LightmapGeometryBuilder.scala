package com.stovokor.editor.lighting.baking

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.BatchNode
import com.jme3.scene.Geometry
import com.jme3.material.Material
import scala.jdk.CollectionConverters._
import com.stovokor.util.JmeExtensions._

class LightmapGeometryBuilder {

  val mat = new Material
  val batch = new GeometryAccessBatchNode

  def add(spatial: Spatial): Unit = {
    val clone = spatial.clone(false)
    clone.setMaterial(mat)
    // remove skipped
    clone.depthFirst(
      _.ifGeometry(g =>
        if (g.getUserData[Boolean]("skipLightmap")) g.removeFromParent
      )
    )
    batch.attachChild(clone)
  }

  def build = {
    batch.geometry
  }

  class GeometryAccessBatchNode extends BatchNode {
    def geometry = {
      this.batch()
      val batches = this.batches
      val theBatch = batches.get(0)
      val geom = theBatch.getClass.getDeclaredField("geometry")
      geom.setAccessible(true)
      geom.get(theBatch).asInstanceOf[Geometry]
    }
  }
}
