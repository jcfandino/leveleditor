package com.stovokor.editor.lighting

import com.jme3.math.Vector3f
import java.util.concurrent.atomic.AtomicLong
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.repository.DataExtension
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JObject
import org.json4s.DefaultFormats
import org.json4s.ShortTypeHints
import org.json4s.TypeHints
import org.json4s.{Extraction, NoTypeHints}
import org.json4s.JsonDSL.WithDouble._
import org.json4s.jackson.Serialization
import com.jme3.math.ColorRGBA
import org.json4s.FieldSerializer

// make case class wrappers for serialization
object Vector {
  def apply() = new Vector(0, 0, 0)
  def apply(v: Vector3f) = new Vector(v.x, v.y, v.z)
}
case class Vector(x: Float, y: Float, z: Float) {
  def toVector3f = new Vector3f(x, y, z)
}
object Color {
  def apply(c: ColorRGBA) = new Color(c.r, c.g, c.b)
}
case class Color(r: Float, g: Float, b: Float) {
  def toColorRGBA = new ColorRGBA(r, g, b, 1)

  def toPreview = {
    var max = r.max(g).max(g)
    if (max > 1) {
      val vec = new Vector3f(r, g, b).divide(max)
      new Color(vec.x, vec.y, vec.z)
    } else this
  }

}

abstract class Light {

  def updateName(name: String): Light
  def updateBaked(baked: Boolean): Light
  def updateColor(color: ColorRGBA): Light
  def updatePos(pos: Vector3f): Light
  def updateDir(dir: Vector3f): Light

  def name: String
  def pos: Vector
  def col: Color
  def baked: Boolean
  def canBake: Boolean
  def directed: Boolean

  def position = pos.toVector3f
  def color = col.toColorRGBA
}

case class AmbientLight(name: String, col: Color, pos: Vector) extends Light {
  def updateName(name: String) = AmbientLight(name, col, pos)
  def updateBaked(baked: Boolean) = AmbientLight(name, col, pos)
  def updateColor(color: ColorRGBA) = AmbientLight(name, Color(color), pos)
  def updatePos(pos: Vector3f) = AmbientLight(name, col, Vector(pos))
  def updateDir(dir: Vector3f) = this
  val baked = false
  val canBake = false
  val directed = false
}
case class DirectionalLight(name: String, col: Color, pos: Vector, dir: Vector)
    extends Light {
  def updateName(name: String) = DirectionalLight(name, col, pos, dir)
  def updateBaked(baked: Boolean) = DirectionalLight(name, col, pos, dir)
  def updateColor(color: ColorRGBA) =
    DirectionalLight(name, Color(color), pos, dir)
  def updatePos(pos: Vector3f) = DirectionalLight(name, col, Vector(pos), dir)
  def updateDir(dir: Vector3f) = DirectionalLight(name, col, pos, Vector(dir))
  val baked = false
  val canBake = false
  val directed = true
}
case class PointLight(name: String, baked: Boolean, col: Color, pos: Vector)
    extends Light {
  def updateName(name: String) = PointLight(name, baked, col, pos)
  def updateBaked(baked: Boolean) = PointLight(name, baked, col, pos)
  def updateColor(color: ColorRGBA) = PointLight(name, baked, Color(color), pos)
  def updatePos(pos: Vector3f) = PointLight(name, baked, col, Vector(pos))
  def updateDir(dir: Vector3f) = this
  val canBake = true
  val directed = false
}
case class SpotLight(
    name: String,
    baked: Boolean,
    col: Color,
    pos: Vector,
    dir: Vector
) extends Light {
  def updateName(name: String) = SpotLight(name, baked, col, pos, dir)
  def updateBaked(baked: Boolean) = SpotLight(name, baked, col, pos, dir)
  def updateColor(color: ColorRGBA) =
    SpotLight(name, baked, Color(color), pos, dir)
  def updatePos(pos: Vector3f) = SpotLight(name, baked, col, Vector(pos), dir)
  def updateDir(dir: Vector3f) = SpotLight(name, baked, col, pos, Vector(dir))
  val canBake = true
  val directed = true
}

object LightDataExtension extends DataExtension {

  implicit val formats: org.json4s.DefaultFormats = new DefaultFormats {
    override val typeHintFieldName: String = "type"
    override val typeHints: TypeHints =
      ShortTypeHints(
        List(
          classOf[AmbientLight],
          classOf[DirectionalLight],
          classOf[PointLight],
          classOf[SpotLight]
        )
      )
  }

  def fetch = {
    Extraction.decompose(LightRepository.lights)
  }

  def load(data: JValue): Unit = {
    data match {
      case data: JObject => {
        LightRepository.clear()
        data
          .extract[Map[Long, Light]]
          .foreach({ case (id, e) => LightRepository.add(e) })
      }
      case _ =>
    }
  }

  def clear: Unit = {
    LightRepository.clear()
  }
}

object LightRepository {

  val idGenerator = new AtomicLong(0)
  var lights: Map[Long, Light] = Map()

  def all = lights.values.toSet

  def add(light: Light) = {
    val id = idGenerator.getAndIncrement
    lights = lights.updated(id, light)
    id
  }

  def get(id: Long) = {
    lights(id)
  }

  def find(point: Point) = {
    lights
      .find({ case (id, e) =>
        e.position.x == point.x && e.position.y == point.y
      })
  }

  def remove(id: Long) = {
    val light = lights(id)
    lights = lights - id
    light
  }

  def update(id: Long, light: Light) = {
    val old = lights(id)
    lights = lights.updated(id, light)
    old
  }

  def clear(): Unit = {
    lights = Map()
    idGenerator.set(0)
  }
}
