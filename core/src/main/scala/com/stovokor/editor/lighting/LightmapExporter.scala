package com.stovokor.editor.lighting

import com.simsilica.lemur.Container
import java.io.File
import com.stovokor.editor.lighting.baking.LightmapBaker
import com.stovokor.editor.lighting.LightRepository
import com.stovokor.editor.factory.Mesh3dFactory
import com.stovokor.editor.lighting.baking.LightmapUVBuilder
import com.stovokor.editor.lighting.baking.DebugLightmapBaker
import com.stovokor.editor.lighting.baking.BakeSettings
import com.simsilica.lemur.TextField
import com.simsilica.lemur.text.DefaultDocumentModel
import com.simsilica.lemur.text.DocumentModelFilter
import com.simsilica.lemur.text.TextFilters
import com.simsilica.lemur.CheckboxModel
import com.simsilica.lemur.DefaultCheckboxModel
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Panel
import com.simsilica.lemur.Label
import com.simsilica.lemur.Checkbox
import com.simsilica.lemur.ListBox
import com.simsilica.lemur.list.SelectionModel
import com.simsilica.lemur.core.VersionedList
import scala.jdk.CollectionConverters._
import com.simsilica.lemur.Button
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.export.MapDataExporter

object LightmapExporter {
  def fileName(mapName: String) = s"$mapName-lightmap.png"
}

class LightmapExporter extends MapDataExporter {

  object ViewModel {
    val size = new PositiveIntModel(1024)
    val colorFactor = new PositiveIntModel(1000)
    val aa = {
      val m = new SelectionModel()
      m.setSelection(2)
      m
    }
    val samples = new PositiveIntModel(2)
    val contrast = new PositiveFloatModel(0.1f)
    val filter = {
      val m = new SelectionModel()
      m.setSelection(BakeSettings.filterOptions.indexOf("gaussian"))
      m
    }
    val jitter = new DefaultCheckboxModel(true)
    val amboccSamples = new PositiveIntModel(4)
    val amboccMaxDist = new PositiveFloatModel(10f)

    def setPreview: Unit = {
      val s = BakeSettings.previewSettings(size.getValue)
      aa.setSelection(BakeSettings.aaOptions.indexWhere(o => o._1 == "preview"))
      samples.setValue(s.samples)
      contrast.setValue(s.contrast)
      filter.setSelection(BakeSettings.filterOptions.indexOf(s.filter))
      jitter.setChecked(s.jitter)
      amboccSamples.setValue(s.amboccSamples)
      amboccMaxDist.setValue(s.amboccMaxDist)
    }

    def setGoodQuality: Unit = {
      val s = BakeSettings.goodSettings(size.getValue)
      aa.setSelection(BakeSettings.aaOptions.indexWhere(o => o._1 == "final"))
      samples.setValue(s.samples)
      contrast.setValue(s.contrast)
      filter.setSelection(BakeSettings.filterOptions.indexOf(s.filter))
      jitter.setChecked(s.jitter)
      amboccSamples.setValue(s.amboccSamples)
      amboccMaxDist.setValue(s.amboccMaxDist)
    }

    def toSettings = new BakeSettings(
      width = size.getValue,
      height = size.getValue,
      aaMin = BakeSettings.aaOptions(aa.getSelection)._2,
      aaMax = BakeSettings.aaOptions(aa.getSelection)._3,
      samples = samples.getValue,
      contrast = contrast.getValue,
      filter = BakeSettings.filterOptions(filter.getSelection),
      jitter = jitter.isChecked,
      amboccSamples = amboccSamples.getValue,
      amboccMaxDist = amboccMaxDist.getValue,
      colorFactor.getValue.toFloat
    )
  }

  val vm = ViewModel

  val name = "Lightmap"

  def exportPanel = {
    def addRow(p: Container, label: String, field: Panel): Unit = {
      val row = p.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
        )
      )
      row.addChild(new Label(label))
      row.addChild(field)
    }
    val panel = new Container
    panel.addChild(new Label("Lightmap baker options"))
    val sizePanel = new Container(
      new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
    )
    val sizeField = sizePanel.addChild(new TextField(vm.size))
    sizePanel.addChild(
      GuiFactory.actionButton(
        "-",
        "",
        () => vm.size.setValue((vm.size.getValue / 2).max(64))
      )
    )
    sizePanel.addChild(
      GuiFactory.actionButton(
        "+",
        "",
        () => vm.size.setValue(vm.size.getValue * 2)
      )
    )
    addRow(panel, "Size (squared)", sizePanel)
    addRow(panel, "Color factor", new TextField(vm.colorFactor))
    val aaField = new ListBox(
      new VersionedList(BakeSettings.aaOptions.map(_._1).asJava)
    )
    aaField.setSelectionModel(vm.aa)
    addRow(panel, "Anti-Aliasing", aaField)
    addRow(panel, "Samples", new TextField(vm.samples))
    addRow(panel, "Contrast", new TextField(vm.contrast))
    val filterField = new ListBox(
      new VersionedList(BakeSettings.filterOptions.asJava)
    )
    filterField.setSelectionModel(vm.filter)
    addRow(panel, "Filter", filterField)
    addRow(panel, "Jitter", new Checkbox("enabled", vm.jitter))
    addRow(panel, "Ambient Occlusion Samples", new TextField(vm.amboccSamples))
    addRow(panel, "Ambient Occlusion Distance", new TextField(vm.amboccMaxDist))
    val presetBar = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    presetBar.addChild(new Label("Presets:"))
    presetBar.addChild(new Button("Preview")).onClick(() => vm.setPreview)
    presetBar
      .addChild(new Button("Good Quality"))
      .onClick(() => vm.setGoodQuality)
    presetBar.addChild(new Container)
    panel
  }

  def exportData(baseDir: String, mapName: String): Unit = {
    try {
      // build geometry with uv coordinates
      val node = Mesh3dFactory.mapLightmapUV(Mesh3dFactory()).createAll
      // bake
      val file = s"$baseDir/${LightmapExporter.fileName(mapName)}"
      val baker = new LightmapBaker(file, vm.toSettings)
      val lights = LightRepository.all.filter(_.baked)
      baker.bake(node, lights, Set())
    } catch {
      case e: Exception => println(s"ERROR: Cannot bake: $e")
    }
  }
}

class PositiveIntModel(value: Int)
    extends DocumentModelFilter(
      new DocumentModelFilter(new DefaultDocumentModel(value.abs.toString))
    ) {
  setInputTransform(TextFilters.numeric())
  def getValue = getText.toInt
  def setValue(i: Int) = setText(i.abs.toString)
}
class IntModel(value: Int)
    extends DocumentModelFilter(
      new DocumentModelFilter(new DefaultDocumentModel(value.toString))
    ) {
  setInputTransform(
    TextFilters.charFilter(
      TextFilters.isInChars('-', '0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9')
    )
  )
  def getValue =
    try { getText.toInt }
    catch { case e: NumberFormatException => value }
  def setValue(i: Int) = setText(i.toString)
}

class PositiveFloatModel(value: Float)
    extends DocumentModelFilter(
      new DocumentModelFilter(new DefaultDocumentModel(value.abs.toString))
    ) {
  setInputTransform(
    TextFilters.charFilter(
      TextFilters.isInChars('.', '0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9')
    )
  )
  def getValue =
    try { getText.toFloat }
    catch { case e: NumberFormatException => value }
  def setValue(f: Float) = setText(f.abs.toString)
}
