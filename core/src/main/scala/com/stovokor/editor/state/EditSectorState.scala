package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.FastMath
import com.jme3.scene.Spatial.CullHint
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Button
import com.simsilica.lemur.Checkbox
import com.simsilica.lemur.Container
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Label
import com.simsilica.lemur.RollupPanel
import com.simsilica.lemur.TextField
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.model.Platform
import com.stovokor.editor.model.Slope
import com.stovokor.editor.model.{Target, SectorTarget}
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.service.SectorService
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.OpenMap
import com.stovokor.util.StartNewMap

object EditSectorState {
  def open(
      sm: AppStateManager,
      sectorId: Long,
      platformIdx: Option[Int] = None
  ): Unit = {
    Option(sm.getState(classOf[EditSectorState])).foreach(sm.detach)
    Option(sm.getState(classOf[EditSurfaceState])).foreach(sm.detach)
    PanelStateMemory.collapse(sectorId)
    platformIdx.foreach(idx => PanelStateMemory.update(sectorId, idx, true))
    sm.attach(new EditSectorState(sectorId))
  }
  // remember state of rolled-up panels after redrawing
  object PanelStateMemory {
    type Key = (Long, Int)
    var toggled: Map[Key, Boolean] = Map().withDefault(_ => false)
    def update(sectorId: Long, idx: Int, value: Boolean): Unit = {
      toggled = toggled.updated((sectorId, idx), value)
    }
    def collapse(sectorId: Long): Unit = {
      toggled =
        toggled.view.filterKeys(_._1 != sectorId).toMap.withDefault(_ => false)
    }
    def apply(sectorId: Long, idx: Int) = toggled((sectorId, idx))
  }
}

class EditSectorState(sectorId: Long)
    extends BaseState
    with EditorEventListener {

  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()
  val sectorService = SectorService()

  var panel: Container = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SectorUpdated])
    EventBus.subscribe(this, ViewModeSwitch())
    EventBus.subscribe(this, OpenMap())
    EventBus.subscribe(this, StartNewMap())
    panel = new Container(
      new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.None)
    )
    guiNode.attachChild(panel)
    redrawPanel()
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    guiNode.detachChild(panel)
  }

  def onEvent(event: EditorEvent) = event match {
    case SectorUpdated(_, _, _) => redrawPanel()
    case ViewModeSwitch()       => cancel()
    case OpenMap()              => cancel()
    case StartNewMap()          => cancel()
    case _                      =>
  }

  def redrawPanel(): Unit = {
    panel.clearChildren()
    panel.setLocalTranslation(4, (cam.getHeight - 30).toFloat, 0)
    panel.setBackground(
      new QuadBackgroundComponent(new ColorRGBA(0, 0, 0, .6f))
    )

    val sector = sectorRepository.get(sectorId)
    val builder = new PanelBuilder(panel)

    builder.title()
    builder.subtitle("Ceiling")
    builder.heightPanel(Target.Ceiling(sectorId), sector.ceiling.height)
    builder.slopesPanel(Target.Ceiling(sectorId), sector.ceiling.slope)
    builder.surfacePanel(Target.Ceiling(sectorId))

    builder.subtitle("Floor")
    builder.heightPanel(Target.Floor(sectorId), sector.floor.height)
    builder.slopesPanel(Target.Floor(sectorId), sector.floor.slope)
    builder.surfacePanel(Target.Floor(sectorId))

    builder.collapsePanel()

    builder.addPlatformHeaderPanel()

    sector.platforms.zipWithIndex.foreach({
      case (platform, idx) => {
        val container = new Container
        val toggle =
          panel.addChild(new RollupPanel(s" Platform $idx", container, null))
        toggle.setOpen(EditSectorState.PanelStateMemory(sectorId, idx))
        toggle.getTitleElement.onClick(() =>
          EditSectorState.PanelStateMemory.update(sectorId, idx, toggle.isOpen)
        )
        val builder2 = new PanelBuilder(container)

        builder2.platformHeaderPanel(idx, platform)

        builder2.subtitle("Top")
        builder2.heightPanel(
          Target.PlatformTop(sectorId, idx),
          platform.top.height
        )
        builder2.surfacePanel(Target.PlatformTop(sectorId, idx))
        builder2.slopesPanel(
          Target.PlatformTop(sectorId, idx),
          platform.top.slope
        )

        builder2.subtitle("Bottom")
        builder2.heightPanel(
          Target.PlatformBottom(sectorId, idx),
          platform.bottom.height
        )
        builder2.surfacePanel(Target.PlatformBottom(sectorId, idx))
        builder2.slopesPanel(
          Target.PlatformBottom(sectorId, idx),
          platform.bottom.slope
        )

        builder2.platformPropertiesPanel(idx, platform)
      }
    })

    builder.padding()
  }

  def cancel(): Unit = {
    stateManager.detach(this)
  }

  class PanelBuilder(panel: Container) {

    def title(): Unit = {
      val title = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      title.addChild(
        new Label(
          s"Sector: ${sectorId}",
          panel.getElementId.child("title.label")
        )
      )
      val close = title.addChild(new Button(""))
      close.setIcon(GuiFactory.createIcon("dialog-cancel-3.png"))
      close.onClick(cancel)
    }

    def subtitle(title: String): Unit = {
      panel.addChild(new Label(title))
    }

    def padding(): Unit = {
      panel.addChild(new Container)
    }

    def surfacePanel(target: SectorTarget): Unit = {
      val surfacePanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      surfacePanel.addChild(new Label("Surface"))
      surfacePanel.addChild(
        GuiFactory.actionButton(
          "Edit",
          () => EditSurfaceState.open(stateManager, target)
        )
      )
    }

    def addPlatformHeaderPanel(): Unit = {
      val surfacePanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      surfacePanel.addChild(new Label("Platforms"))
      surfacePanel.addChild(
        GuiFactory.actionButton("+", () => sectorService.addPlatform(sectorId))
      )
    }

    def heightPanel(target: SectorTarget, height: Float): Unit = {
      def heightButton(dir: String, target: SectorTarget, factor: Float) = {
        GuiFactory.actionButton(
          dir,
          () => sectorService.changeHeight(target, factor)
        )
      }
      def heightOkButton(
          dir: String,
          target: SectorTarget,
          original: Float,
          field: TextField
      ) = {
        GuiFactory.actionButton(
          dir,
          () =>
            try {
              sectorService
                .changeHeight(target, field.getText.toFloat - original)
            } catch {
              case e: Exception => { redrawPanel() }
            }
        )
      }
      val heightStep = 0.1f
      val heightPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      heightPanel.addChild(new Label("Height"))
      val field = heightPanel.addChild(GuiFactory.numberField(height)(2))
      val okButton =
        heightPanel.addChild(heightOkButton("ok", target, height, field))
      heightPanel.addChild(heightButton("v", target, -heightStep))
      heightPanel.addChild(heightButton("^", target, heightStep))
    }

    def slopesPanel(target: SectorTarget, slope: Slope): Unit = {
      def tiltButton(
          dir: String,
          target: SectorTarget,
          mutation: Slope => Slope
      ) = {
        GuiFactory.actionButton(
          dir,
          () => sectorService.mutateSlope(target, mutation)
        )
      }
      def slopeOkButton(
          dir: String,
          target: SectorTarget,
          original: Float,
          field: TextField,
          f: (Slope, Float) => Slope
      ) = {
        GuiFactory.actionButton(
          dir,
          () =>
            try {
              val newAngle = field.getText.toFloat * FastMath.DEG_TO_RAD
              sectorService.mutateSlope(target, s => f(s, newAngle - original))
            } catch {
              case e: Exception => { redrawPanel() }
            }
        )
      }
      val slopeStep = FastMath.PI / 180f
      // angle X
      val slopeXPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      slopeXPanel.addChild(new Label("Angle X:"))
      val slopeX = slopeXPanel.addChild(
        GuiFactory.numberField(slope.angleX * FastMath.RAD_TO_DEG)(0)
      )
      val slopeXOk = slopeXPanel.addChild(
        slopeOkButton("ok", target, slope.angleX, slopeX, _ tiltX _)
      )
      slopeXPanel.addChild(tiltButton("<", target, _.tiltX(-slopeStep)))
      slopeXPanel.addChild(tiltButton(">", target, _.tiltX(slopeStep)))
      // angle Y
      val slopeYPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      slopeYPanel.addChild(new Label("Angle Y:"))
      val slopeY = slopeYPanel.addChild(
        GuiFactory.numberField(slope.angleY * FastMath.RAD_TO_DEG)(0)
      )
      val slopeYOk = slopeYPanel.addChild(
        slopeOkButton("ok", target, slope.angleY, slopeY, _ tiltY _)
      )
      slopeYPanel.addChild(tiltButton("v", target, _.tiltY(-slopeStep)))
      slopeYPanel.addChild(tiltButton("^", target, _.tiltY(slopeStep)))
    }

    def collapsePanel(): Unit = {
      val collapsePanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      collapsePanel.addChild(new Label("Collapse"))
      val btn = GuiFactory.actionButton(
        "",
        () => sectorService.collapseSector(sectorId)
      )
      btn.setIcon(GuiFactory.createIcon("table-table-row-hide.png"))
      collapsePanel.addChild(btn)
    }

    def platformHeaderPanel(idx: Int, platform: Platform): Unit = {
      val headerPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      headerPanel.addChild(new Label(s"Platform $idx"))
      headerPanel.addChild(
        GuiFactory.actionButton(
          "-",
          () => sectorService.removePlatform(sectorId, idx)
        )
      )
    }

    def platformPropertiesPanel(idx: Int, platform: Platform): Unit = {
      val propertiesPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.Y, Axis.X, FillMode.First, FillMode.None)
        )
      )
      val checkBoxesPanel = propertiesPanel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      // inherit holes
      val holesCheckbox =
        checkBoxesPanel.addChild(new Checkbox("Inherit holes"))
      holesCheckbox.setChecked(platform.flags.inheritHoles)
      holesCheckbox.onClick(() =>
        sectorService.mutatePlatform(
          sectorId,
          idx,
          p =>
            p.updatedFlags(p.flags.updateInheritHoles(holesCheckbox.isChecked))
        )
      )
      // include in lightmap generation
      val lightmapCheckbox =
        checkBoxesPanel.addChild(new Checkbox("In lightmap"))
      lightmapCheckbox.setChecked(platform.isAllInLightmap)
      lightmapCheckbox.onClick(() =>
        sectorService.mutatePlatform(
          sectorId,
          idx,
          p => p.updateAllInLightmap(lightmapCheckbox.isChecked)
        )
      )
      // batch id
      def setBatchId(id: String) = sectorService.mutatePlatform(
        sectorId,
        idx,
        p => p.updatedFlags(p.flags.updateBatchId(id))
      )
      val detachedPanel = propertiesPanel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
        )
      )
      val detachedCheckbox = detachedPanel.addChild(new Checkbox("Detached"))
      val batchIdPanel = detachedPanel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      val batchIdText =
        batchIdPanel.addChild(new TextField(platform.flags.batchId))
      val batchIdOk =
        GuiFactory.actionButton("ok", () => setBatchId(batchIdText.getText))
      batchIdPanel.addChild(batchIdOk)
      batchIdPanel.setCullHint(visible(platform.flags.isDetached))
      detachedCheckbox.setChecked(platform.flags.isDetached)
      detachedCheckbox.onClick(() => {
        batchIdPanel.setCullHint(visible(detachedCheckbox.isChecked))
        if (!detachedCheckbox.isChecked) setBatchId("")
      })
    }

    def visible(visible: Boolean) =
      if (visible) CullHint.Inherit else CullHint.Always
  }
}
