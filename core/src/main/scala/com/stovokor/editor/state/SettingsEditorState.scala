package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.Vector3f
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.gui.Palette
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.model.Settings
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.util.EditSettings
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.SettingsUpdated
import com.stovokor.util.LemurExtensions.ButtonExtension

import java.io.File
import com.simsilica.lemur.FillMode

class SettingsEditorState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  val settingsRepository = Repositories.settingsRepository

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribe(this, EditSettings())
    setupInput
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.settings)
    inputMapper.removeStateListener(this, InputFunction.cancel)
  }

  def onEvent(event: EditorEvent) = event match {
    case EditSettings() => openSettingsDialog()
    case _              =>
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.settings)
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.activateGroup(InputFunction.general)
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (value == InputState.Positive) func match {
      case InputFunction.settings => openSettingsDialog()
      case InputFunction.cancel   => closeModal()
      case _                      =>
    }
  }
  var updatedSettings = Settings()

  def openSettingsDialog(): Unit = {
    updatedSettings = settingsRepository.get
    val dialog = createSettingsDialog(
      cam.getWidth,
      cam.getHeight,
      updatedSettings,
      setSettings,
      saveChanges
    )
    showModal(dialog)
  }

  def setSettings(updated: Settings): Unit = {
    updatedSettings = updated
  }

  def saveChanges(): Unit = {
    println(s"Settings saved $updatedSettings")
    settingsRepository.update(updatedSettings)
    EventBus.trigger(SettingsUpdated())
  }

  def createSettingsDialog(
      width: Int,
      height: Int,
      current: => Settings,
      update: Settings => Unit,
      callback: () => Unit
  ) = {
    def openDirectoryFinder(callback: File => Unit) = {
      val fileChooser = new FileChooser()
      val currentDir = new File(current.assetsBasePath)
      if (currentDir.exists && currentDir.isDirectory) {
        fileChooser.setDirectory(currentDir)
      }
      fileChooser.setDirectorySelection()
      fileChooser.setCallback(callback)
      fileChooser.showOpenDialog()
    }
    val optionsPanel = new Container(
      new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.First)
    )
    optionsPanel.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 150).toFloat, 0)
    )
    // asset path
    val assetPathPanel =
      optionsPanel.addChild(new Container(new SpringGridLayout(Axis.X, Axis.Y)))
    assetPathPanel.addChild(new Label("Assets path:"))
    val currentPath = assetPathPanel.addChild(new Label(current.assetsBasePath))
    currentPath.setBackground(new QuadBackgroundComponent(Palette.pathLabel))
    val change = assetPathPanel.addChild(
      GuiFactory.button("magnifier.png", "", label = "Find...")
    )
    change.setPreferredSize(new Vector3f())
    change.onClick(() => {
      openDirectoryFinder(dir => {
        val path = dir.getPath
        println(s"Selected assets path: $path")
        update(current.updateAssetBasePath(path))
        currentPath.setText(path)
      })
    })
    // default texture density
    val defaultTextureDensity =
      optionsPanel.addChild(new Container(new SpringGridLayout(Axis.X, Axis.Y)))
    defaultTextureDensity.addChild(
      new Label("Default texture density (does not affect current map):")
    )
    val currentDensity = current.defaultTextureDensity
    val densityField = defaultTextureDensity.addChild(
      GuiFactory.numberField(currentDensity, false)(4)
    )
    // bottom padding
    val filling = new Container
    filling.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 200).toFloat, 0)
    )
    optionsPanel.addChild(filling)
    val save = () => {
      if (!densityField.getText.isEmpty && densityField.getText.toFloat > 0) {
        update(
          current.updateDefaultTextureDensity(densityField.getText.toFloat)
        )
        callback()
      }
    }
    val window = new OptionPanel(
      "Settings",
      "",
      null,
      GuiFactory.action("x", "Cancel"),
      GuiFactory.action("ok", "Save", save)
    )
    window.getContainer.addChild(optionsPanel)
    window
  }

}
