package com.stovokor.editor.state

import com.jme3.app.state.AppStateManager
import com.simsilica.lemur.OptionPanelState
import com.stovokor.editor.gui.GuiFactory
import java.io.File
import com.simsilica.lemur.Label
import com.simsilica.lemur.ListBox
import com.simsilica.lemur.TextField
import com.simsilica.lemur.Action
import com.simsilica.lemur.core.VersionedList
import scala.jdk.CollectionConverters._
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.Container
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.stovokor.util.LemurExtensions._
import com.jme3.math.Vector3f
import com.simsilica.lemur.event.PopupState
import com.jme3.math.ColorRGBA

trait OptionPanelAccess {

  def stateManager: AppStateManager
  def isModalOpen = popupState.hasActivePopups

  def popupState = stateManager.getState(classOf[PopupState])

  private var opened: List[OptionPanel] = Nil

  def showModal(panel: OptionPanel): Unit = {
    opened = panel :: opened
    val screen = popupState.getGuiSize
    val size = panel.getPreferredSize
    panel.setLocalTranslation(
      (screen.x - size.x) / 2,
      (screen.y + size.y) / 2,
      0
    )
    popupState.showModalPopup(panel, new ColorRGBA(0, 0, 0, .7f))
  }

  def closeModal(): Unit = {
    opened.headOption.foreach(panel => {
      opened = opened.tail
      popupState.closePopup(panel)
    })
  }

  object ConfirmDialog {
    def showConfirmDialog(
        title: String,
        message: String,
        callback: () => Unit
    ): Unit = {
      val ok = GuiFactory.action("ok", "Yes", callback)
      val cancel = GuiFactory.action("x", "No")
      showModal(new OptionPanel(title, message, null, ok, cancel))
    }
  }

  class FileChooser() {

    var dir: File = new File(System.getProperty("user.dir"))
    var confirm: Option[String] = None
    var mustExist = false
    var filter: File => Boolean = (_ => true)
    var selectDirs = false
    var callback: File => Unit = (_ => {})

    // elements
    val current = new Label("")
    val files = new ListBox[String]()
    val newname = new TextField("untitled")
    var ok: Action = GuiFactory.action("ok", "Select", selectNewFile _)

    def setCallback(cb: File => Unit): Unit = {
      callback = cb
    }

    def setDirectory(d: File): Unit = {
      if (d.exists && d.isDirectory) {
        dir = d
        refresh()
        ok.setEnabled(!mustExist || selectDirs)
      }
    }

    def setDirectorySelection(): Unit = {
      filter = f => f.isDirectory
      newname.setText("")
      selectDirs = true
      ok.setEnabled(true)
    }

    def setFileSelection(ext: String): Unit = {
      filter = f => f.isDirectory || f.getName.endsWith("." + ext)
      selectDirs = false
    }

    def getSelectedOption =
      files.getModel.get(files.getSelectionModel.getSelection)
    def getSelectedFile = new File(dir.getPath + "/" + getSelectedOption)

    def refresh(): Unit = {
      current.setText(dir.getPath())
      val fileList = dir
        .listFiles(f => filter(f))
        .sortBy(f => s"${!f.isDirectory}${f.getName}")
        .map(_.getName)
      files.setModel(new VersionedList(fileList.toList.asJava))
      files.setVisibleItems(20)
    }

    def showDialog(action: String): Unit = {
      val cancel = GuiFactory.action("x", "Cancel")
      ok.setName(action.capitalize)
      val window = new OptionPanel(action.capitalize, "", null, cancel, ok)
      val panel = new Container(
        new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.Last)
      )
      // breadcrum
      val breadcrum = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
        )
      )
      // file list
      panel.addChild(files)
      files.setVisibleItems(20)
      files.onClick(onFileClicked)
      // new file name
      if (action == "save") {
        val newbar = panel.addChild(
          new Container(
            new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
          )
        )
        newbar.addChild(new Label("Name:"))
        newbar.addChild(newname)
      }
      // up dir
      val up = GuiFactory.actionButton(
        "^",
        () => {
          if (dir.getParent != null) {
            setDirectory(new File(dir.getParent))
          }
        }
      )
      breadcrum.addChild(up)
      breadcrum.addChild(current)
      val filling = panel.addChild(new Container)
      filling.setPreferredSize(new Vector3f(450, 0, 0))
      window.getContainer.addChild(panel)
      refresh()
      showModal(window)
    }

    def showOpenDialog(): Unit = {
      showDialog("open")
      mustExist = true
      ok.setEnabled(selectDirs)
    }

    def showSaveDialog(): Unit = {
      showDialog("save")
      confirm = Some("override")
    }

    var lastClick = 0L
    var lastOptClicked = ""
    var doubleClickMs = 200L

    def onFileClicked(): Unit = {
      val isDoubleClick =
        System.currentTimeMillis - lastClick < doubleClickMs &&
          lastOptClicked == getSelectedOption

      lastClick = System.currentTimeMillis
      lastOptClicked = getSelectedOption

      val file = getSelectedFile
      if (file.exists && file.isDirectory) {
        if (isDoubleClick) {
          setDirectory(file)
        }
      } else {
        newname.setText(getSelectedOption)
        ok.setEnabled(true)
        if (isDoubleClick) {
          closeModal()
          selectFile(getSelectedFile)
        }
      }
    }

    def selectNewFile(): Unit = {
      selectFile(new File(dir.toString + "/" + newname.getText))
    }

    def selectFile(file: File): Unit = {
      if (confirm.isDefined && file.exists) {
        val action = confirm.get
        ConfirmDialog.showConfirmDialog(
          action,
          s"$action ${file.getName}?",
          () => callback(file)
        )
      } else if (file.exists || !mustExist) {
        callback(file)
      }
    }
  }

}
