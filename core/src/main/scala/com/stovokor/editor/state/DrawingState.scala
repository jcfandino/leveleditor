package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.Vector3f
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.shape.Line
import com.simsilica.lemur.input.AnalogFunctionListener
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.builder.SectorBuilder
import com.stovokor.editor.control.ConstantSizeOnScreenControl
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.gui.K
import com.stovokor.editor.gui.Palette
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.input.Modes.EditMode
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.service.SectorService
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.util.ViewModeSwitch
import com.stovokor.editor.service.SectorDrawingService
import com.stovokor.editor.service.SectorDrawingService.Drawing
import com.stovokor.editor.gui.Mode2DLayers

class DrawingState
    extends BaseState
    with EditorEventListener
    with MaterialFactoryClient
    with CanMapInput
    with AnalogFunctionListener
    with StateFunctionListener {

  val drawingService = SectorDrawingService()

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    get2DNode.attachChild(new Node("currentDraw"))
    EventBus.subscribeByType(this, classOf[PointClicked])
    EventBus.subscribeByType(this, classOf[EditModeSwitch])
    EventBus.subscribe(this, ViewModeSwitch())
    setupInput
    drawingService.cancel()
  }

  override def cleanup(): Unit = {
    super.cleanup
    cancel()
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.cancel)
    inputMapper.removeStateListener(this, InputFunction.revert)
    inputMapper.removeStateListener(this, InputFunction.test1)
    inputMapper.removeStateListener(this, InputFunction.test2)
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.addStateListener(this, InputFunction.revert)
    inputMapper.addStateListener(this, InputFunction.test1)
    inputMapper.addStateListener(this, InputFunction.test2)
    inputMapper.activateGroup(InputFunction.general)
  }

  def onEvent(event: EditorEvent) = event match {
    case PointClicked(point) => if (isEnabled) addPoint(point)
    case ViewModeSwitch()    => drawingService.cancel()
    case EditModeSwitch(m)   => if (m != EditMode.Draw) drawingService.cancel()
    case _                   =>
  }

  def addPoint(point: Point): Unit = {
    drawingService.addPoint(point)
    redrawCurrent()
  }

  def redrawCurrent(): Unit = {
    val drawing = drawingService.getCurrentDrawing
    val node = getDrawNode
    node.detachAllChildren
    for (point <- drawing.points) {
      val vertex = new Geometry("point", K.vertexBox)
      vertex.setLocalTranslation(point.x, point.y, Mode2DLayers.drawing)
      vertex.setMaterial(plainColor(Palette.drawing))
      vertex.addControl(new ConstantSizeOnScreenControl())
      node.attachChild(vertex)
    }
    for (line <- drawing.lines) {
      val geo = new Geometry(
        "line",
        new Line(
          new Vector3f(line.a.x, line.a.y, Mode2DLayers.drawing),
          new Vector3f(line.b.x, line.b.y, Mode2DLayers.drawing)
        )
      )
      geo.setMaterial(plainColor(Palette.drawing))
      node.attachChild(geo)
    }
    get2DNode.attachChild(node)
  }

  def getDrawNode = getOrCreateNode(get2DNode, "currentDraw")

  def valueActive(func: FunctionId, value: Double, tpf: Double): Unit = {}

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (value == InputState.Positive) func match {
      case InputFunction.cancel => cancel()
      case InputFunction.revert => revert()
      case InputFunction.test1  => drawTestRoom(0f, 0f)
      case InputFunction.test2  => drawTestRoom(0f, 10f)
      case _                    =>
    }
  }

  def cancel(): Unit = {
    drawingService.cancel()
    redrawCurrent()
  }

  def revert(): Unit = {
    drawingService.revert()
    redrawCurrent()
  }

  def drawTestRoom(x: Float, y: Float): Unit = {
    println("drawing test room")
    EventBus.trigger(PointClicked(Point(-5, -5).move(x, y)))
    Thread.sleep(200)
    EventBus.trigger(PointClicked(Point(5, -5).move(x, y)))
    Thread.sleep(200)
    EventBus.trigger(PointClicked(Point(5, 5).move(x, y)))
    Thread.sleep(200)
    EventBus.trigger(PointClicked(Point(-5, 5).move(x, y)))
    EventBus.trigger(PointClicked(Point(-5, 5).move(x, y)))
  }

}
