package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.simsilica.lemur.Container
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Label
import com.jme3.util.MemoryUtils
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.editor.input.InputFunction
import com.simsilica.lemur.input.StateFunctionListener
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.jme3.scene.Spatial.CullHint

class MemoryMonitorState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener {

  val updatesUntilGc = 20
  var counter = 0
  var statsEnabled = false

  var usage: Label = new Label("")
  var count: Label = new Label("")
  var total: Label = new Label("")

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    val main = new Container(new SpringGridLayout(Axis.Y, Axis.X))

    val usagePanel = main.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    usagePanel.addChild(new Label("Usage: "))
    usagePanel.addChild(usage)

    val totalPanel = main.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    totalPanel.addChild(new Label("Total: "))
    totalPanel.addChild(total)

    val countPanel = main.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    countPanel.addChild(new Label("Count: "))
    countPanel.addChild(count)

    main.setLocalTranslation(0, 100, 0)
    node.attachChild(main)
    updateVisible

    EventBus.subscribeByType(this, classOf[SectorUpdated])
    inputMapper.addStateListener(this, InputFunction.toggleStats)
  }

  override def cleanup(): Unit = {
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.toggleStats)
  }

  override def update(tpf: Float): Unit = {
    if (statsEnabled) {
      usage.setText(s"${MemoryUtils.getDirectMemoryUsage / 1024} kB")
      total.setText(s"${MemoryUtils.getDirectMemoryTotalCapacity / 1024} kB")
      count.setText(s"${MemoryUtils.getDirectMemoryCount}")
    }
    if (counter > updatesUntilGc) {
      counter = 0
      System.gc()
    }
  }

  def node = getOrCreateNode(guiNode, "memory-stats")

  def onEvent(event: EditorEvent) = event match {
    case e: SectorUpdated => counter += 1
    case _                =>
  }

  def valueChanged(
      functionId: FunctionId,
      state: InputState,
      factor: Double
  ): Unit = {
    functionId match {
      case InputFunction.toggleStats => if (state == InputState.Positive) toggle
      case _                         =>
    }
  }

  def toggle: Unit = {
    statsEnabled = !statsEnabled
    updateVisible
  }

  def updateVisible: Unit = {
    val cull = if (statsEnabled) CullHint.Inherit else CullHint.Always
    node.setCullHint(cull)
  }
}
