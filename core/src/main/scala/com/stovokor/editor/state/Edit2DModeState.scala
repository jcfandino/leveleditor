package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppState
import com.jme3.app.state.AppStateManager
import com.jme3.scene.Spatial.CullHint
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.SelectionChange
import com.stovokor.editor.input.Modes.EditMode
import com.simsilica.lemur.FillMode
import com.stovokor.editor.input.InputFunction
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.StateFunctionListener
import com.simsilica.lemur.input.InputState
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.editor.input.Modes.SelectionMode

class Edit2DModeState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  var modeKey = EditMode.Select
  val modes: Map[EditMode, EditModeStrategy] =
    Map((EditMode.Select, SelectingMode), (EditMode.Draw, DrawingMode))

  def mode = modes(modeKey)

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[EditModeSwitch])
    modes(EditMode.Draw).exit
    mode.enter
    setupInput
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.clickKey)
    inputMapper.removeStateListener(this, InputFunction.insertMode)
    inputMapper.removeStateListener(this, InputFunction.selectPoints)
    inputMapper.removeStateListener(this, InputFunction.selectLines)
    inputMapper.removeStateListener(this, InputFunction.selectSectors)
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.addStateListener(this, InputFunction.clickKey)
    inputMapper.addStateListener(this, InputFunction.insertMode)
    inputMapper.addStateListener(this, InputFunction.selectPoints)
    inputMapper.addStateListener(this, InputFunction.selectLines)
    inputMapper.addStateListener(this, InputFunction.selectSectors)
    inputMapper.activateGroup(InputFunction.general)
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (!isModalOpen && value == InputState.Positive) func match {
      case InputFunction.insertMode => {
        EventBus.trigger(EditModeSwitch(EditMode.Draw))
        EventBus.trigger(SelectionModeSwitch(SelectionMode.None))
      }
      case InputFunction.selectPoints => {
        EventBus.trigger(EditModeSwitch(EditMode.Select))
        EventBus.trigger(SelectionModeSwitch(SelectionMode.Point))
      }
      case InputFunction.selectLines => {
        EventBus.trigger(EditModeSwitch(EditMode.Select))
        EventBus.trigger(SelectionModeSwitch(SelectionMode.Line))
      }
      case InputFunction.selectSectors => {
        EventBus.trigger(EditModeSwitch(EditMode.Select))
        EventBus.trigger(SelectionModeSwitch(SelectionMode.Sector))
      }
      case _ =>
    }
  }

  def onEvent(event: EditorEvent) = event match {
    case EditModeSwitch(m) => setMode(m)
    case _                 =>
  }

  def setMode(newMode: EditMode): Unit = {
    if (newMode != modeKey) {
      println(s"edit mode $newMode")
      mode.exit
      EventBus.trigger(SelectionChange(Set()))
      modeKey = newMode
      mode.enter
    }
  }

  abstract class EditModeStrategy(val id: String) {
    def enter: Unit
    def exit: Unit
  }

  object SelectingMode extends EditModeStrategy("selecting") {

    def exit: Unit = {
      println("exiting selection")
      disableStates(classOf[SelectionState], classOf[ModifyingState])
      removeStates(classOf[SelectionState], classOf[ModifyingState])
    }

    def enter: Unit = {
      println("entering selection")
      stateManager.attach(new SelectionState)
      stateManager.attach(new ModifyingState)
    }
  }

  object DrawingMode extends EditModeStrategy("drawing") {
    def exit: Unit = {
      println("exiting drawing")
      disableStates(classOf[DrawingState])
      removeStates(classOf[DrawingState])
    }
    def enter: Unit = {
      println("entering drawing")
      stateManager.attach(new DrawingState)
    }
  }

}
