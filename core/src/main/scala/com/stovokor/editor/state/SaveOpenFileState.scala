package com.stovokor.editor.state

import com.stovokor.editor.model.repository.SectorRepository
import com.jme3.app.state.AppStateManager
import com.stovokor.util.PointClicked
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.ExtensionRepository
import com.jme3.scene.Node
import com.stovokor.util.EventBus
import com.jme3.app.Application
import com.stovokor.util.EditorEventListener
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.util.SaveMap
import com.stovokor.util.EditorEvent
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import java.io.FileWriter
import java.io.BufferedWriter
import java.io.File
import com.stovokor.util.OpenMap
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.Border
import java.io.FileReader
import java.io.BufferedReader
import scala.io.Source
import com.stovokor.util.SectorDeleted
import com.stovokor.util.SectorUpdated
import com.stovokor.util.JsonFiles
import com.stovokor.editor.model.MapFile
import com.stovokor.util.StartNewMap
import com.stovokor.util.ExitApplication
import org.json4s.JsonAST.JObject
import com.stovokor.editor.model.repository.SettingsRepository
import com.stovokor.editor.model.Settings
import com.stovokor.editor.model.repository.MaterialRepository
import com.stovokor.editor.model.Polygon
import com.stovokor.editor.gui.GuiFactory
import scala.jdk.CollectionConverters._
import com.jme3.math.Vector3f
import com.simsilica.lemur.OptionPanel

// TODO move logic into service layer
class SaveOpenFileState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()

  var currentFile: Option[File] = None
  var dirty = false

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SaveMap])
    EventBus.subscribe(this, OpenMap())
    EventBus.subscribeByType(this, classOf[SectorUpdated])
    EventBus.subscribeByType(this, classOf[SectorDeleted])
    EventBus.subscribe(this, StartNewMap())
    EventBus.subscribe(this, ExitApplication())
    setupInput
    startNewMap()
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.newFile)
    inputMapper.removeStateListener(this, InputFunction.open)
    inputMapper.removeStateListener(this, InputFunction.save)
    inputMapper.removeStateListener(this, InputFunction.saveAs)
    inputMapper.removeStateListener(this, InputFunction.exit)
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.newFile)
    inputMapper.addStateListener(this, InputFunction.open)
    inputMapper.addStateListener(this, InputFunction.save)
    inputMapper.addStateListener(this, InputFunction.saveAs)
    inputMapper.addStateListener(this, InputFunction.exit)
    inputMapper.activateGroup(InputFunction.files)
  }

  def exitApp() = confirmAction("exit editor", app.stop)

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (value == InputState.Positive) func match {
      case InputFunction.newFile => EventBus.trigger(StartNewMap())
      case InputFunction.open    => EventBus.trigger(OpenMap())
      case InputFunction.save    => EventBus.trigger(SaveMap(true))
      case InputFunction.saveAs  => EventBus.trigger(SaveMap(false))
      case InputFunction.exit    => EventBus.trigger(ExitApplication())
      case _                     =>
    }
  }

  def onEvent(event: EditorEvent) = event match {
    case StartNewMap()          => startNewMap()
    case OpenMap()              => openFile()
    case SaveMap(false)         => saveAsNewFile()
    case SaveMap(true)          => saveCurrent()
    case ExitApplication()      => exitApp()
    case SectorUpdated(_, _, _) => dirty = true
    case SectorDeleted(_)       => dirty = true
    case _                      =>
  }

  def startNewMap(): Unit = {
    confirmAction(
      "start new map",
      () => {
        for ((id, sec) <- SectorRepository().sectors) {
          EventBus.trigger(SectorDeleted(id))
        }
        SectorRepository().removeAll
        BorderRepository().removeAll
        MaterialRepository().updateDensity(
          SettingsRepository().get.defaultTextureDensity
        )
        // reset extensions data
        ExtensionRepository().clear()
        currentFile = None
        dirty = false
      }
    )
  }

  def openFile(): Unit = {
    confirmAction(
      "open file",
      () => {
        val fileChooser = new FileChooser()
        fileChooser.setDirectory(lastOpenedDir)
        fileChooser.setFileSelection("m8")
        fileChooser.setCallback(openFile)
        fileChooser.showOpenDialog()
      }
    )
  }

  def lastOpenedDir: File = {
    val path = SettingsRepository().get.lastProjectPath
    val dir = new File(path)
    if (dir.exists() && dir.isDirectory()) dir
    else new File(Settings.defaultDir)
  }

  def updateLastOpenedDir(file: File): Unit = {
    val settings =
      SettingsRepository().get.updateLastProjectPath(file.getAbsolutePath)
    SettingsRepository().update(settings)
  }

  // this ensures the polygons are presorted
  def updateSectorData(sector: Sector) = {
    sector
      .updatedPolygon(Polygon(sector.polygon.vertices))
      .updatedHoles(sector.holes.map(hole => Polygon(hole.vertices)))
  }

  def openFile(file: File): Unit = {
    println(s"opening file: $file")
    try {
      // update last opened dir
      updateLastOpenedDir(file.getParentFile)
      // read file
      val map: MapFile = JsonFiles.load[MapFile](file.getAbsolutePath)
      currentFile = Some(file)
      // clean and load
      for ((id, sec) <- SectorRepository().sectors) {
        EventBus.trigger(SectorDeleted(id))
      }
      SectorRepository().removeAll
      BorderRepository().removeAll
      // reset extensions data
      ExtensionRepository().clear()
      // populate sectors
      val newIds = map.sectors
        .map({ case (id, sector) => (id, updateSectorData(sector)) })
        .map({ case (id, sector) => (id, SectorRepository().add(sector)) })
      // populate borders
      map.borders.map({
        case (id, border) => {
          val updated =
            border.updateSectors(newIds(border.sectorA), newIds(border.sectorB))
          BorderRepository().add(updated)
        }
      })
      // reload materials
      stateManager
        .getState(classOf[SettingsLoaderState])
        .reloadMaterials(map.materials)
      // set texture density
      map.textureDensity.foreach(MaterialRepository().updateDensity)
      // populate extensions data
      map.extensions
        .foreach({ case (key, data) => ExtensionRepository().load(key, data) })
      dirty = false
      // draw everything
      SectorRepository().sectors
        .foreach(entry =>
          EventBus.trigger(SectorUpdated(entry._1, entry._2, true))
        )
    } catch {
      case e: Exception => {
        e.printStackTrace()
        val cancel = GuiFactory.action("x", "OK")
        showModal(
          new OptionPanel(
            "Error",
            s"Cannot open $file:\n${e.getMessage}",
            null,
            cancel
          )
        )
      }
    }
  }

  def saveAsNewFile(): Unit = {
    val fileChooser = new FileChooser()
    fileChooser.setDirectory(lastOpenedDir)
    fileChooser.setFileSelection("m8")
    fileChooser.setCallback(file => {
      currentFile = Some(file)
        .map(file =>
          if (file.getName.endsWith(".m8")) file
          else new File(file.getAbsolutePath + ".m8")
        )
      updateLastOpenedDir(file.getParentFile)
      saveCurrent()
    })
    fileChooser.showSaveDialog()
  }

  def saveCurrent(): Unit = {
    if (currentFile.isEmpty) {
      saveAsNewFile()
    } else {
      val sectors = SectorRepository().sectors
      val borders = BorderRepository().borders
      val materials = MaterialRepository().materials
      val density = MaterialRepository().density
      val extensions = ExtensionRepository().get
      val map =
        MapFile(1, sectors, borders, materials, Some(density), extensions)
      JsonFiles.save(currentFile.get.getAbsolutePath, map)
    }
    dirty = false
  }

  def confirmAction(action: String, callback: () => Unit): Unit = {
    if (!dirty) callback()
    else
      ConfirmDialog.showConfirmDialog(
        action.capitalize,
        s"You will lose your progress, $action anyway?",
        callback
      )
  }

}
