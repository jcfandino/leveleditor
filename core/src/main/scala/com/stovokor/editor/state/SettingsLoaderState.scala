package com.stovokor.editor.state

import java.io.File
import java.io.FileFilter

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.asset.plugins.FileLocator
import com.stovokor.editor.model.MatDefMaterial
import com.stovokor.editor.model.SimpleMaterial
import com.stovokor.editor.model.SurfaceMaterial
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.SettingsUpdated
import com.stovokor.editor.model.repository.MaterialRepository
import com.jme3.material.MaterialDef

class SettingsLoaderState extends BaseState with EditorEventListener {

  val settingsRepository = Repositories.settingsRepository
  val materialRepository = Repositories.materialRepository

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribe(this, SettingsUpdated())
    reloadSettings()
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
  }

  def onEvent(event: EditorEvent) = event match {
    case SettingsUpdated() => reloadSettings()
    case _                 =>
  }

  def reloadSettings(): Unit = {
    val settings = settingsRepository.get
    reloadMaterials(settings.assetsBasePath)
  }

  def reloadMaterials(path: String): Unit = {
    val dir = new File(path)
    if (dir.exists() && dir.isDirectory()) {
      assetManager.registerLocator(dir.getPath, classOf[FileLocator])
      loadSimpleTextures(dir)
      loadMaterialDefinitions(dir)
      // remove missing materials
      materialRepository.materials
        .filterNot(filterMaterial)
        .foreach({ case (id, _) => materialRepository.remove(id) })
    } else {
      println(s"Error: cannot load settings $path")
    }
  }

  // reload using the given ids
  def reloadMaterials(list: Map[Long, SurfaceMaterial]): Unit = {
    materialRepository.removeAll
    list
      .filter(filterMaterial)
      // add them to repository keeping the id
      .foreach({ case (id, material) =>
        materialRepository.update(id, material)
      })
    // reload to find new files
    reloadSettings()
  }

  def filterMaterial(tuple: (Long, SurfaceMaterial)) = {
    val dir = new File(settingsRepository.get.assetsBasePath)
    def exists(path: String) = new File(dir, path).exists()
    tuple match {
      case (id, SimpleMaterial(path)) => exists(path)
      case (id, MatDefMaterial(path)) => exists(path)
      case _                          => false
    }
  }

  def loadRecursively(
      baseDir: File,
      dir: File,
      filter: FileFilter,
      builder: String => SurfaceMaterial
  ): Unit = {
    if (dir.exists() && dir.isDirectory()) {
      dir
        .listFiles(filter)
        .foreach(file => {
          val relPath = baseDir.toPath.relativize(file.toPath)
          materialRepository.add(builder(relPath.toString))
        })
      dir
        .listFiles(f => f.isDirectory)
        .foreach(f => loadRecursively(baseDir, f, filter, builder))
    }
  }

  def loadSimpleTextures(dir: File): Unit = {
    // TODO accept more file types
    val extensions = Set(".png", ".jpg", ".bmp")
    def isImage(file: File) =
      file.isFile() && extensions.find(file.getName.endsWith).isDefined
    loadRecursively(dir, new File(dir, "Textures"), isImage, SimpleMaterial(_))
  }

  def loadMaterialDefinitions(dir: File): Unit = {
    def isMatDef(file: File) = file.isFile() && file.getName.endsWith(".j3m")
    loadRecursively(
      dir,
      new File(dir, "Materials"),
      isMatDef,
      MatDefMaterial(_)
    )
  }
}
