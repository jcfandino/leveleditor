package com.stovokor.editor.state

import com.stovokor.util.EditorEventListener
import com.simsilica.lemur.Panel
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.util.EventBus
import com.stovokor.util.StartNewMap
import com.stovokor.util.OpenMap
import com.stovokor.util.EditorEvent
import com.simsilica.lemur.Container
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.jme3.math.Vector3f
import com.simsilica.lemur.event.DefaultMouseListener
import com.stovokor.editor.gui.GuiFactory
import com.simsilica.lemur.RollupPanel
import com.stovokor.util.LemurExtensions.ButtonExtension

class ExtensionPanelState extends BaseState with EditorEventListener {

  var togglePanels: List[(RollupPanel, ExtensionToolView)] = List()
  var panel: Container = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    createPanel()

    EventBus.subscribe(this, StartNewMap())
    EventBus.subscribe(this, OpenMap())
  }

  def onEvent(event: EditorEvent) = event match {
    case StartNewMap() => clear()
    case OpenMap()     => clear()
  }

  def clear(): Unit = {
    togglePanels.foreach({
      case (p, v) => {
        v.setOpen(false)
        p.setOpen(false)
      }
    })
  }

  def createPanel() = {
    panel = new Container(
      new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.None)
    )
    panel.setLocalTranslation(
      (windowWidth - width - 10).toFloat,
      (windowHeight - 30).toFloat,
      0
    )
    panel.addMouseListener(new DefaultMouseListener) // ignore clicks

    val node = getOrCreateNode(guiNode, "extension-panel")
    node.attachChild(panel)
  }

  def registerExtensionPanel(
      title: String,
      extensionPanel: Container,
      view: ExtensionToolView
  ): Unit = {
    // Toggle panel
    val toggle =
      panel.addChild(new RollupPanel("[+] " + title, extensionPanel, null))

    toggle.setOpen(false)
    toggle.getTitleElement.onClick(() => {
      view.setOpen(toggle.isOpen)
      if (toggle.isOpen) {
        toggle.getTitleElement.setText("[-] " + title)
        togglePanels
          .filter(_._1 != toggle)
          .foreach({
            case (p, v) => {
              v.setOpen(false)
              p.setOpen(false)
              p.getTitleElement
                .setText(p.getTitleElement.getText.replace("[-]", "[+]"))
            }
          })
      } else {
        toggle.getTitleElement.setText("[+] " + title)
      }
    })
    togglePanels = togglePanels ++ List((toggle, view))
  }

  val width = 240
  def windowWidth = app.getCamera.getWidth
  def windowHeight = app.getCamera.getHeight
}

trait ExtensionToolView {
  def setOpen(open: Boolean): Unit
}
