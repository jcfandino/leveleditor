package com.stovokor.editor.state

import scala.jdk.CollectionConverters._

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.scene.Node
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.factory.Mesh2dFactory
import com.stovokor.editor.factory.Mesh3dFactory
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.JmeExtensions.SpatialExtensions
import com.stovokor.util.LemurExtensions.SpatialExtension
import com.stovokor.util.PointerTargetChange
import com.stovokor.util.SectorDeleted
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.{Target, SectorTarget}
import com.stovokor.editor.model.SavableTarget
import com.stovokor.editor.model.repository.MaterialRepository
import com.jme3.scene.Spatial
import com.stovokor.util.SectorTargetClicked
import com.stovokor.util.CoordinateSystem

// this state works in 2d and 3d
class SectorPresenterState
    extends BaseState
    with MaterialFactoryClient
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener {

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SectorUpdated])
    EventBus.subscribeByType(this, classOf[SectorDeleted])
    inputMapper.addStateListener(this, InputFunction.snapToGrid)
    inputMapper.activateGroup(InputFunction.general)
  }

  override def update(tpf: Float): Unit = {}

  def onEvent(event: EditorEvent) = event match {
    case SectorUpdated(id, sector, fullRedraw) =>
      redrawSector(id, sector, fullRedraw)
    case SectorDeleted(id) => eraseSector(id)
    case _                 =>
  }

  def eraseSector(id: Long): Unit = {
    getOrCreateNode(get2DNode, "sector-" + id).removeFromParent
    getOrCreateNode(get3DNode, "sector-" + id).removeFromParent
  }

  def redrawSector(id: Long, sector: Sector, fullRedraw: Boolean): Unit = {
    //    println(s"Sector Updated: $sector")
    if (fullRedraw) draw2d(id, sector)
    draw3d(id, sector)
  }

  def draw2d(id: Long, sector: Sector): Unit = {
    val node = getOrCreateNode(get2DNode, "sector-" + id)
    cleanup(node)
    val meshNode = Mesh2dFactory(assetManager).createMesh(id, sector)
    node.attachChild(meshNode)
  }

  def draw3d(id: Long, sector: Sector): Unit = {
    val node = getOrCreateNode(get3DNode, "sector-" + id)
    cleanup(node)
    val borders = BorderRepository().findFrom(id).map(_._2)
    val borderingSectors = borders
      .flatMap(b => List(b.sectorA, b.sectorB))
      .map(i => (i, SectorRepository().get(i)))
      .toMap
    val density = MaterialRepository().density
    val meshNode = Mesh3dFactory(assetManager, true, density)
      .createMesh(id, sector, borders, borderingSectors)
    setup3dInput(id, meshNode)
    node.attachChild(meshNode)
  }

  def cleanup(node: Node): Unit = {
    node.depthFirst(s => { // I don't think this is any good
      //      s.removeEvents
      for (i <- 1 to s.getNumControls) {
        val ctrl = s.getControl(0)
        s.removeControl(ctrl)
        if (ctrl.isInstanceOf[EditorEventListener]) {
          EventBus.removeFromAll(ctrl.asInstanceOf[EditorEventListener])
        }
      }
    })
    node.detachAllChildren()
  }
  var lastTarget: Option[Target] = None

  // TODO consider moving this into the mesh 3d factory
  def setup3dInput(sectorId: Long, meshNode: Node): Unit = {
    def target(spatial: Spatial) =
      SavableTarget(sectorId, spatial.getUserData("target"))
    meshNode.depthFirst(spatial =>
      if (spatial.getUserDataKeys.contains("target")) {
        spatial.onCursorMove((event, _, _) => {
          event.setConsumed()
          if (spatial.isVisible) {
            updateTarget(target(spatial))
          }
        })
        spatial.onCursorClick((event, spatial, capture, collision) => {
          if (event.isPressed) {
            val position =
              collision.map(_.getContactPoint).map(CoordinateSystem.toEditor)
            val normal =
              collision.map(_.getContactNormal).map(CoordinateSystem.toEditor)
            EventBus.trigger(
              new SectorTargetClicked(
                event.getButtonIndex,
                target(spatial),
                position,
                normal
              )
            )
          }
        })
      }
    )
  }

  def updateTarget(target: SectorTarget): Unit = {
    if (lastTarget != Some(target)) {
      lastTarget = Some(target)
      EventBus.trigger(PointerTargetChange(target))
    }
  }

  def valueChanged(f: FunctionId, i: InputState, d: Double): Unit = {}
}
