package com.stovokor.editor.state

import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.jme3.math.Vector3f
import com.jme3.input.controls.ActionListener
import com.jme3.input.KeyInput
import com.jme3.input.controls.KeyTrigger
import com.simsilica.lemur.input.AnalogFunctionListener
import com.simsilica.lemur.input.StateFunctionListener
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.stovokor.editor.input.InputFunction
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.ChangeZoom
import com.stovokor.util.EventBus
import com.stovokor.editor.gui.Mode2DLayers

object Camera2DState {
  var zoom = 0f
  var x = 0f
  var y = 0f

  def setZoom(z: Float): Unit = {
    this.zoom = z
  }

  def setLocation(x: Float, y: Float): Unit = {
    this.x = x
    this.y = y
  }
}

class Camera2DState
    extends BaseState
    with CanMapInput
    with AnalogFunctionListener
    with StateFunctionListener
    with EditorEventListener
    with OptionPanelAccess {

  var animZoom = zoom + 0.1f

  def zoom = Camera2DState.zoom
  def speed = 20f
  def cameraX = Camera2DState.x
  def cameraY = Camera2DState.y
  def animSpeed = 0.04f * animZoom

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    if (Camera2DState.zoom == 0f) {
      Camera2DState.setZoom(defaultZoom())
    }
    animZoom = zoom - .001f // force an update
    app.getFlyByCamera.setEnabled(false)
    cam.setParallelProjection(true)
    cam.setLocation(new Vector3f(cameraX, cameraY, Mode2DLayers.camera))
    cam.lookAt(new Vector3f(cameraX, cameraY, 0f), Vector3f.UNIT_Z.negate)
    cam.update
    setupInput()
    EventBus.subscribeByType(this, classOf[ChangeZoom])
  }

  override def cleanup(): Unit = {
    inputMapper.removeAnalogListener(
      this,
      InputFunction.moveX,
      InputFunction.moveY,
      InputFunction.moveZ,
      InputFunction.mouseWheel
    )
  }

  override def update(tpf: Float): Unit = {
    if (animZoom != zoom) {
      animZoom += (if (animZoom > zoom) -animSpeed else animSpeed)
      if ((animZoom - zoom).abs < animSpeed) animZoom = zoom
      val aspect = cam.getWidth().toFloat / cam.getHeight()
      cam.setFrustum(
        0f,
        10000f,
        -animZoom * aspect,
        animZoom * aspect,
        animZoom,
        -animZoom
      )
      cam.update()
    }
  }

  def move(x: Float, y: Float): Unit = {
    val location = cam.getLocation.add(x, y, 0)
    cam.setLocation(location)
    Camera2DState.setLocation(location.x, location.y)
  }

  def moveZoom(delta: Float): Unit = {
    Camera2DState.setZoom(Math.max(1f, zoom + delta))
  }

  def defaultZoom() = 10f * cam.getWidth / 1024f

  def setupInput() = {
    inputMapper.addAnalogListener(
      this,
      InputFunction.moveX,
      InputFunction.moveY,
      InputFunction.moveZ,
      InputFunction.mouseWheel
    )
    inputMapper.activateGroup(InputFunction.camera)
    inputMapper.activateGroup(InputFunction.mouse)
  }

  def valueActive(func: FunctionId, value: Double, tpf: Double): Unit = {
    if (!isModalOpen) {
      def boundSpeed = Math.max(.1f, Math.min(50f, zoom * speed))
      func match {
        case InputFunction.moveX => move((value * speed * tpf).toFloat, 0)
        case InputFunction.moveY => move(0, (value * speed * tpf).toFloat)
        case InputFunction.moveZ => moveZoom((value * speed * tpf).toFloat)
        case InputFunction.mouseWheel =>
          moveZoom((-1 * value * boundSpeed * tpf).toFloat)
        case _ =>
      }
    }
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {}

  def onEvent(event: EditorEvent) = event match {
    case ChangeZoom(f) => moveZoom(f)
    case _             =>
  }

}
