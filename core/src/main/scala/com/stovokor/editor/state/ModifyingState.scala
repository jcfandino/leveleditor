package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.service.SectorService
import com.stovokor.util.DeleteSelection
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.LineDragged
import com.stovokor.util.PointDragged
import com.stovokor.util.SectorDragged
import com.stovokor.util.SelectionChange
import com.stovokor.util.SplitSelection
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.input.InputFunction
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.stovokor.editor.model.Line
import com.stovokor.editor.model.SelectionUnit
import com.stovokor.editor.model.SelectionLine

// in 2d to modify sector shapes
class ModifyingState
    extends BaseState
    with CanMapInput
    with StateFunctionListener
    with EditorEventListener
    with OptionPanelAccess {

  val sectorRepository = SectorRepository()
  def sectorService = SectorService()

  var selection: Set[SelectionUnit] = Set()
  def selectedPoints: Set[Point] = selection.flatMap(_.getPoints)

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[PointDragged])
    EventBus.subscribeByType(this, classOf[LineDragged])
    EventBus.subscribeByType(this, classOf[SectorDragged])
    EventBus.subscribeByType(this, classOf[SelectionChange])
    EventBus.subscribeByType(this, classOf[SplitSelection])
    EventBus.subscribeByType(this, classOf[DeleteSelection])
    inputMapper.addStateListener(this, InputFunction.split)
    inputMapper.activateGroup(InputFunction.general)
  }

  override def cleanup(): Unit = {
    super.cleanup
    inputMapper.removeStateListener(this, InputFunction.split)
    EventBus.removeFromAll(this)
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (!isModalOpen && value == InputState.Positive) func match {
      case InputFunction.split => EventBus.trigger(SplitSelection())
      case _                   =>
    }
  }

  def onEvent(event: EditorEvent) = event match {
    case PointDragged(from, to) =>
      movePoints(to.x - from.x, to.y - from.y, from)
    case LineDragged(line, dx, dy) => moveLine(dx, dy, line)
    case SectorDragged(id, dx, dy) =>
      movePoints(dx, dy, sectorRepository.get(id).polygon.vertices: _*)
    case SelectionChange(ps) => selection = ps
    case SplitSelection()    => splitSelection()
    case DeleteSelection()   => deleteSelection()
    case _                   =>
  }

  def movePoints(dx: Float, dy: Float, extraPoints: Point*): Unit = {
    val pointsToMove = (selectedPoints) ++ extraPoints
    sectorService.movePoints(dx, dy, pointsToMove)
  }

  def moveLine(dx: Float, dy: Float, line: Line): Unit = {
    sectorService.movePoints(dx, dy, Set(line.a, line.b))
  }

  def splitSelection(): Unit = {
    selection.toList match {
      case List(SelectionLine(line)) => {
        EditLineState.open(stateManager, line)
      }
      case _ =>
    }
  }

  def deleteSelection(): Unit = {}
}
