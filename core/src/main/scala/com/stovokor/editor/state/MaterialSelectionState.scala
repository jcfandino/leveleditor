package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.simsilica.lemur.OptionPanelState
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.model.SurfaceMaterial
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.editor.service.SectorSurfaceMutator
import com.stovokor.util.ChangeMaterial
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.editor.model.Target
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import com.simsilica.lemur.Panel
import com.simsilica.lemur.grid.ArrayGridModel
import com.simsilica.lemur.GridPanel
import com.jme3.math.Vector3f
import com.simsilica.lemur.OptionPanel
import com.stovokor.editor.model.SimpleMaterial
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.stovokor.editor.model.MatDefMaterial
import com.jme3.material.MaterialDef
import com.jme3.texture.Texture2D
import com.jme3.material.MatParamTexture
import com.stovokor.editor.model.SurfaceMaterialThumbnail
import com.stovokor.editor.model.SurfaceMaterialOrdering
import com.stovokor.util.SettingsUpdated
import com.simsilica.lemur.Action
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.util.MaterialChosen

class MaterialSelectionState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  val materialRepository = Repositories.materialRepository

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[ChangeMaterial])
    setupInput
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.activateGroup(InputFunction.general)
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.cancel)
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (value == InputState.Positive) func match {
      case InputFunction.cancel => closeModal()
      case _                    =>
    }
  }

  def onEvent(event: EditorEvent) = event match {
    case ChangeMaterial(target) => openMaterialDialog(target)
    case _                      =>
  }

  def openMaterialDialog(target: Target): Unit = {
    println("opening material dialog")
    val dialog = createMaterialDialog(target)
    populateMaterialDialog(target, dialog)
    showModal(dialog)
  }

  def populateMaterialDialog(target: Target, dialog: OptionPanel): Unit = {
    val options = materialRepository.materials.toList
      .sortWith({ case ((_, m1), (_, m2)) =>
        SurfaceMaterialOrdering.compare(m1, m2) < 0
      })
    def refresh = () => refreshMaterials(target, dialog)
    val materialPanel = createMaterialPanel(
      cam.getWidth,
      cam.getHeight,
      options,
      materialChosen(target),
      refresh
    )
    dialog.getContainer.clearChildren()
    dialog.getContainer.addChild(materialPanel)
  }

  def materialChosen(target: Target)(index: Long): Unit = {
    println(s"Changing material $target -> $index")
    EventBus.trigger(MaterialChosen(target, index))
    closeModal()
  }

  def refreshMaterials(target: Target, dialog: OptionPanel): Unit = {
    EventBus.trigger(SettingsUpdated())
    populateMaterialDialog(target, dialog)
  }

  def createMaterialPanel(
      width: Int,
      height: Int,
      options: List[(Long, SurfaceMaterial)],
      callback: Long => Unit,
      refresh: () => Unit
  ) = {
    val (buttonWidth, buttonHeight) = (75, 75)
    val materialPanel = new Container(new SpringGridLayout())
    val infoText = new Label("")
    val cols = ((width - 150) / buttonWidth) - 1
    val rows = (height - 200) / buttonHeight
    def padding(i: Int) = 1 to i map (_ => new Panel)
    val matrix: Array[Array[Panel]] = options
      .map({
        case (id, mat) => {
          val preview = SurfaceMaterialThumbnail.load(mat)
          val matButton =
            GuiFactory.button(description = preview.path, infoText = infoText)
          val tex =
            GuiGlobals.getInstance().loadTexture(preview.image, false, false)
          matButton.setBackground(new QuadBackgroundComponent(tex))
          matButton.setSize(
            new Vector3f(buttonWidth.toFloat, buttonHeight.toFloat, 0)
          )
          matButton.setMaxWidth(buttonWidth.toFloat)
          matButton.onClick(() => callback(id))
          matButton.asInstanceOf[Panel]
        }
      })
      .sliding(cols, cols)
      .map(l => if (l.size == cols) l else l ++ padding(cols - l.size))
      .map(_.toArray)
      .toArray

    val gridModel = new ArrayGridModel(matrix)
    val optionsPanel = materialPanel.addChild(new GridPanel(gridModel))
    optionsPanel.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 150).toFloat, 0)
    )
    optionsPanel.setVisibleColumns(cols)
    optionsPanel.setVisibleRows(rows)

    val navPanel = materialPanel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.First)
      )
    )
    navPanel.addChild(infoText)
    val maxRow = (matrix.length - rows).max(0)
    val pgup = navPanel.addChild(
      GuiFactory.actionButton(
        "^^",
        () => optionsPanel.setRow((optionsPanel.getRow - rows).max(0))
      )
    )
    val less = navPanel.addChild(
      GuiFactory.actionButton(
        "^",
        () => optionsPanel.setRow((optionsPanel.getRow - 1).max(0))
      )
    )
    val more = navPanel.addChild(
      GuiFactory.actionButton(
        "v",
        () => optionsPanel.setRow((optionsPanel.getRow + 1).min(maxRow))
      )
    )
    val pgdn = navPanel.addChild(
      GuiFactory.actionButton(
        "vv",
        () => optionsPanel.setRow((optionsPanel.getRow + rows).min(maxRow))
      )
    )

    val reload = navPanel.addChild(GuiFactory.actionButton("r", refresh))
    materialPanel
  }

  def createMaterialDialog(target: Target) =
    new OptionPanel(
      s"Material selection for $target",
      "",
      null,
      GuiFactory.action("x", "Cancel")
    )

}
