package com.stovokor.editor.state

import com.simsilica.lemur.input.StateFunctionListener
import com.simsilica.lemur.Container
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.stovokor.util.EventBus
import com.simsilica.lemur.input.{FunctionId, InputState}
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.jme3.math.ColorRGBA
import com.simsilica.lemur.Label
import com.jme3.math.Vector3f
import com.simsilica.lemur.Button
import com.stovokor.editor.model.repository.MaterialRepository
import com.simsilica.lemur.GuiGlobals
import com.stovokor.editor.model.SurfaceMaterialThumbnail
import com.simsilica.lemur.HAlignment
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.util.TargetTracker
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.service.SectorService
import scala.jdk.CollectionConverters._
import com.simsilica.lemur.event.DefaultCursorListener
import com.simsilica.lemur.event.DefaultMouseListener
import com.jme3.input.event.MouseButtonEvent
import com.jme3.scene.Spatial
import com.stovokor.editor.model.Target
import com.stovokor.util.ChangeMaterial
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.MaterialChosen
import scala.collection.mutable.ListBuffer
import com.stovokor.util.SectorTargetClicked
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.model.SectorTarget
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Selection

object TextureDeckState {

  private case class TextureCard(index: Long)

  private var textures = List(
    TextureCard(0),
    TextureCard(1),
    TextureCard(2),
    TextureCard(3),
    TextureCard(4)
  )

  def update(cardIndex: Int, textureIndex: Long): Unit = {
    textures = textures.updated(cardIndex, new TextureCard(textureIndex))
  }

  def getIndex(cardIndex: Int) = textures(cardIndex).index
}

case class DeckTarget(idx: Int) extends Target {
  val sectorId = 0
  def name = s"deck-$idx"
}

class TextureDeckState
    extends BaseState
    with CanMapInput
    with StateFunctionListener
    with EditorEventListener {

  val materialRepository = MaterialRepository()
  val sectorService = SectorService()

  val panel = new Container()
  val cursor = new Container()
  val buttons = ListBuffer[Button]()

  var active: Option[Int] = None
  val targetTracker = new TargetTracker()

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    targetTracker.init()
    EventBus.subscribeByType(this, classOf[MaterialChosen])
    EventBus.subscribeByType(this, classOf[SectorTargetClicked])
    guiNode.attachChild(cursor)
    guiNode.attachChild(panel)
    redrawPanel()
    setupInput()
  }

  def setupInput(): Unit = {
    inputMapper.addStateListener(this, InputFunction.clickKey)
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.activateGroup(InputFunction.general)
  }

  def redrawPanel(): Unit = {
    panel.clearChildren()
    val width = 450
    val height = 92
    panel.setLayout(
      new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
    )
    panel.setLocalTranslation(
      ((cam.getWidth - width) / 2).toFloat,
      (height + 20).toFloat,
      0
    )
    panel.setBackground(
      new QuadBackgroundComponent(new ColorRGBA(0, 0, 0, .6f))
    )

    for (idx <- 0 to 4) {
      val btn = new Button("")
      panel.addChild(btn)
      buttons.addOne(btn)
      // set size
      btn.setTextHAlignment(HAlignment.Center)
      btn.setPreferredSize(new Vector3f(90, 90, 0))
      btn.setMaxWidth(90)
      btn.onClick(() => activateCard(idx))
      btn.onRightClick(() =>
        EventBus.trigger(ChangeMaterial(new DeckTarget(idx)))
      )
      updateCard(idx)
    }
  }

  def updateCard(idx: Int): Unit = {
    val material = materialRepository.get(TextureDeckState.getIndex(idx))
    val preview = SurfaceMaterialThumbnail.load(material)
    val image =
      GuiGlobals.getInstance().loadTexture(preview.image, false, false)
    buttons(idx).setBackground(new QuadBackgroundComponent(image))
  }

  def activateCard(idx: Int): Unit = {
    if (active.contains(idx)) cancel()
    else {
      cancel()
      active = Some(idx)
      val material = materialRepository.get(TextureDeckState.getIndex(idx))
      val preview = SurfaceMaterialThumbnail.load(material)
      val image =
        GuiGlobals.getInstance().loadTexture(preview.image, false, false)
      cursor.setBackground(new QuadBackgroundComponent(image))
      cursor.setPreferredSize(new Vector3f(30, 30, 0))
      buttons(idx).setIcon(GuiFactory.createIcon("arrow-right-3.png"))
    }
  }

  override def onEvent(event: EditorEvent) = event match {
    case MaterialChosen(DeckTarget(cardIndex), matIndex) => {
      cancel()
      TextureDeckState.update(cardIndex, matIndex)
      updateCard(cardIndex)
      activateCard(cardIndex)
    }
    case SectorTargetClicked(0, target, _, _) => {
      applyTexture()
    }
    case SectorTargetClicked(1, target, _, _) => {
      if (active.isDefined)
        pickTexture(target)
    }
    case _ =>
  }

  override def update(tpf: Float): Unit = {
    active.foreach(index => {
      val pos = app.getInputManager().getCursorPosition()
      cursor.setLocalTranslation(pos.x, pos.y, 0)
    })

    if (active == None) cursor.setLocalTranslation(-100, 0, 0)

  }

  def applyTexture(): Unit = {
    active
      .map(TextureDeckState.getIndex)
      .zip(targetTracker.target)
      .foreach({
        case (textureIndex, target: SectorTarget) =>
          sectorService
            .mutate(target, surface => surface.updateIndex(textureIndex))
        case _ =>
      })
  }

  def pickTexture(target: SectorTarget): Unit = {
    val sector = SectorRepository().get(target.sectorId)
    val selection = Selection(target)
    val texture = selection.texture
    EventBus.trigger(
      MaterialChosen(DeckTarget(active.getOrElse(0)), texture.index)
    )
  }

  def cancel(): Unit = {
    active = None
    for (child <- panel.getChildren.asScala if child.isInstanceOf[Button]) {
      child.asInstanceOf[Button].setIcon(null)
    }
  }

  override def cleanup(): Unit = {
    super.cleanup
    targetTracker.cleanup()
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.clickKey)
    inputMapper.removeStateListener(this, InputFunction.cancel)
    guiNode.detachChild(panel)
    guiNode.detachChild(cursor)
  }

  override def valueChanged(
      function: FunctionId,
      state: InputState,
      value: Double
  ): Unit = {
    function match {
      case InputFunction.clickKey =>
        if (state == InputState.Positive) applyTexture()
      case InputFunction.cancel => if (state == InputState.Positive) cancel()
      case _                    =>
    }
  }

}
