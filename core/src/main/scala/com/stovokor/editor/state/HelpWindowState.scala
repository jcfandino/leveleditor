package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.Vector3f
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Container
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.Label
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.gui.Palette
import com.stovokor.editor.input.InputFunction
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.ShowHelp

class HelpWindowState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  var open = false

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribe(this, ShowHelp())
    setupInput
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.help)
    inputMapper.removeStateListener(this, InputFunction.cancel)
  }

  def onEvent(event: EditorEvent) = event match {
    case ShowHelp() => openHelpDialog()
    case _          =>
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.help)
    inputMapper.addStateListener(this, InputFunction.cancel)
    inputMapper.activateGroup(InputFunction.general)
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (value == InputState.Positive) func match {
      case InputFunction.help   => openHelpDialog()
      case InputFunction.cancel => closeDialog()
      case _                    =>
    }
  }

  def openHelpDialog(): Unit = {
    if (!open) {
      open = true
      showModal(createHelpDialog(cam.getWidth, cam.getHeight))
    }
  }

  def closeDialog(): Unit = {
    if (open) {
      open = false
      closeModal()
    }
  }

  lazy val helpText = io.Source
    .fromInputStream(getClass.getResourceAsStream("/help.txt"))
    .mkString

  def createHelpDialog(width: Int, height: Int) = {
    val helpPanel = new Container(new SpringGridLayout(Axis.Y, Axis.X))
    helpPanel.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 150).toFloat, 0)
    )
    val label = helpPanel.addChild(new Label(helpText))
    label.setFont(
      GuiGlobals.getInstance.loadFont("Interface/Fonts/Console.fnt")
    )
    label.setColor(Palette.helpForeground)
    label.setFontSize(11f)
    label.setBackground(new QuadBackgroundComponent(Palette.helpBackground))
    val window =
      new OptionPanel("Help", "", null, GuiFactory.action("x", "Dismiss"))
    window.getContainer.addChild(helpPanel)
    window
  }

}
