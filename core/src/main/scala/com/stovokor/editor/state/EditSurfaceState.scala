package com.stovokor.editor.state

import com.stovokor.util.EditorEventListener
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.input.InputFunction
import com.jme3.app.state.AppStateManager
import com.stovokor.util.EventBus
import com.jme3.app.Application
import com.stovokor.util.EditorEvent
import com.simsilica.lemur.Container
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.jme3.math.Vector3f
import com.simsilica.lemur.Label
import com.simsilica.lemur.Button
import com.stovokor.util.ChangeMaterial
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.Wall
import com.stovokor.editor.model.BorderStrip
import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.BorderRepository
import com.simsilica.lemur.FillMode
import com.stovokor.editor.model.repository.MaterialRepository
import com.stovokor.editor.service.SectorService
import com.stovokor.editor.model.StretchMode
import com.jme3.math.FastMath
import com.simsilica.lemur.component.IconComponent
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.HAlignment
import com.stovokor.editor.model.SimpleMaterial
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.gui.EditSurfaceView
import com.stovokor.editor.gui.SurfaceMutator
import com.jme3.math.ColorRGBA
import com.stovokor.editor.model.{Target, SectorTarget}
import com.simsilica.lemur.Checkbox
import com.stovokor.editor.model.PlatformSide
import com.stovokor.editor.model.SurfaceMaterialThumbnail
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.OpenMap
import com.stovokor.util.StartNewMap
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.editor.model.Selection

object EditSurfaceState {
  def open(sm: AppStateManager, target: SectorTarget): Unit = {
    Option(sm.getState(classOf[EditSurfaceState])).foreach(sm.detach)
    Option(sm.getState(classOf[EditSectorState])).foreach(sm.detach)
    sm.attach(new EditSurfaceState(target))
  }
}

class EditSurfaceState(target: SectorTarget)
    extends BaseState
    with EditorEventListener {

  var panel: Container = null
  var surfaceView: EditSurfaceView[SectorTarget] = null
  var sectorView: EditSectorView = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SectorUpdated])
    EventBus.subscribe(this, ViewModeSwitch())
    EventBus.subscribe(this, OpenMap())
    EventBus.subscribe(this, StartNewMap())
    panel = new Container(
      new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.None)
    )
    guiNode.attachChild(panel)
    surfaceView = new EditSurfaceView(panel, target, SectorSurfaceMutator)
    sectorView = new EditSectorView(panel)
    redrawPanel()
  }

  def redrawPanel(): Unit = {
    panel.clearChildren()
    panel.setLocalTranslation(4, (cam.getHeight - 30).toFloat, 0)
    panel.setBackground(
      new QuadBackgroundComponent(new ColorRGBA(0, 0, 0, .6f))
    )
    // title
    val title = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    title.addChild(
      new Label(
        s"Surface: ${Selection(target).describe}",
        panel.getElementId.child("title.label")
      )
    )
    val close = title.addChild(new Button(""))
    close.setIcon(GuiFactory.createIcon("dialog-cancel-3.png"))
    close.onClick(() => cancel())
    // controls
    surfaceView.redrawPanel()
    sectorView.redrawPanel()
    // filling
    panel.addChild(new Container)
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    guiNode.detachChild(panel)
  }

  def cancel(): Unit = {
    stateManager.detach(this)
  }

  def onEvent(event: EditorEvent) = event match {
    case SectorUpdated(sectorId, target, _) =>
      try {
        redrawPanel()
      } catch {
        // opening a file triggers update events before getting the chance to remove listener
        // catch and ignore (hacky solution).
        case e: RuntimeException => println("failed to draw panel")
      }
    case ViewModeSwitch() => cancel()
    case OpenMap()        => cancel()
    case StartNewMap()    => cancel()
    case _                =>
  }

  object SectorSurfaceMutator extends SurfaceMutator[SectorTarget] {
    val sectorService = SectorService()
    override def mutate(
        target: SectorTarget,
        mutation: SurfaceTexture => SurfaceTexture
    ): Unit = {
      sectorService.mutate(target, mutation)
    }
  }

  class EditSectorView(panel: Container) {

    // val materialRepository = MaterialRepository()
    val sectorService = SectorService()

    def redrawPanel(): Unit = {
      // stretch mode
      val selection = Selection(target)
      selection.stretchMode.foreach(mode => {
        val alignModePanel = panel.addChild(
          new Container(
            new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
          )
        )
        alignModePanel.addChild(new Label(s"Align: ${mode}"))
        val changeMode = alignModePanel.addChild(
          GuiFactory.actionButton(
            "Change",
            () => sectorService.rotateStretchMode(target)
          )
        )
      })
      // sector button
      val sectorPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      sectorPanel.addChild(new Label(s"Sector ${target.sectorId}"))
      sectorPanel.addChild(
        GuiFactory.actionButton(
          "Edit",
          () => EditSectorState.open(stateManager, target.sectorId)
        )
      )
    }
  }
}
