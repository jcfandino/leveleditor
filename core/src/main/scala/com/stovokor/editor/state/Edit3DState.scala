package com.stovokor.editor.state

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.simsilica.lemur.input.AnalogFunctionListener
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.service.SectorService
import com.stovokor.util.ChangeMaterial
import com.jme3.math.FastMath
import com.stovokor.util.TargetTracker
import com.stovokor.util.EventBus
import com.stovokor.editor.service.SectorSurfaceMutator
import com.stovokor.editor.model.{Target, SectorTarget}
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.MaterialChosen
import com.stovokor.editor.model.PlatformTarget

class Edit3DState
    extends BaseState
    with CanMapInput
    with AnalogFunctionListener
    with StateFunctionListener
    with OptionPanelAccess
    with EditorEventListener {

  val targetTracker = new TargetTracker()

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[MaterialChosen])
    targetTracker.init()
    setupInput()
  }

  def setupInput(): Unit = {
    inputMapper.addStateListener(this, InputFunction.editHeight)
    inputMapper.addStateListener(this, InputFunction.editHeightSlow)
    inputMapper.addStateListener(this, InputFunction.editTextureOffsetX)
    inputMapper.addStateListener(this, InputFunction.editTextureOffsetY)
    inputMapper.addStateListener(this, InputFunction.editTextureScaleX)
    inputMapper.addStateListener(this, InputFunction.editTextureScaleY)
    inputMapper.addStateListener(this, InputFunction.editTextureOffsetXSlow)
    inputMapper.addStateListener(this, InputFunction.editTextureOffsetYSlow)
    inputMapper.addStateListener(this, InputFunction.editTextureScaleXSlow)
    inputMapper.addStateListener(this, InputFunction.editTextureScaleYSlow)
    inputMapper.addStateListener(this, InputFunction.editSlopeAngleX)
    inputMapper.addStateListener(this, InputFunction.editSlopeAngleY)
    inputMapper.addStateListener(this, InputFunction.changeSlopeStretch)
    inputMapper.addStateListener(this, InputFunction.changeMaterial)
    inputMapper.addStateListener(this, InputFunction.editSurface)
    inputMapper.addStateListener(this, InputFunction.editSector)
    inputMapper.addAnalogListener(this, InputFunction.mouseWheel)
    inputMapper.addAnalogListener(this, InputFunction.mouseWheelShift)
    inputMapper.activateGroup(InputFunction.edit3d)
    inputMapper.activateGroup(InputFunction.mouse)
  }

  override def cleanup(): Unit = {
    super.cleanup()
    targetTracker.cleanup()
    inputMapper.removeStateListener(
      this,
      InputFunction.editHeight,
      InputFunction.editHeightSlow,
      InputFunction.editTextureOffsetX,
      InputFunction.editTextureOffsetY,
      InputFunction.editTextureScaleX,
      InputFunction.editTextureScaleY,
      InputFunction.editTextureOffsetXSlow,
      InputFunction.editTextureOffsetYSlow,
      InputFunction.editTextureScaleXSlow,
      InputFunction.editTextureScaleYSlow,
      InputFunction.editSlopeAngleX,
      InputFunction.changeSlopeStretch,
      InputFunction.editSlopeAngleY,
      InputFunction.changeMaterial,
      InputFunction.editSurface,
      InputFunction.editSector
    )
    inputMapper.removeAnalogListener(
      this,
      InputFunction.mouseWheel,
      InputFunction.mouseWheelShift
    )
    EventBus.removeFromAll(this)
  }

  // TODO extract to texture to be used in EditSurfaceState
  val heightStep = 0.1f
  val heightStepSlow = heightStep / 10f
  val offsetStep = 0.01f
  val offsetStepSlow = offsetStep / 10f
  val scaleStep = 0.01f
  val scaleStepSlow = scaleStep / 10f
  val slopeStep = FastMath.PI / 180f

  def valueChanged(
      functionId: FunctionId,
      state: InputState,
      factor: Double
  ): Unit = {
    if (!isModalOpen) {
      functionId match {
        case InputFunction.editHeight =>
          if (state == InputState.Positive) changeHeight(heightStep)
          else if (state == InputState.Negative) changeHeight(-heightStep)
        case InputFunction.editHeightSlow =>
          if (state == InputState.Positive) changeHeight(heightStepSlow)
          else if (state == InputState.Negative) changeHeight(-heightStepSlow)
        case InputFunction.editTextureOffsetX =>
          if (state == InputState.Positive) changeTextureOffset(-offsetStep, 0f)
          else if (state == InputState.Negative)
            changeTextureOffset(offsetStep, 0f)
        case InputFunction.editTextureOffsetY =>
          if (state == InputState.Positive) changeTextureOffset(0f, offsetStep)
          else if (state == InputState.Negative)
            changeTextureOffset(0f, -offsetStep)
        case InputFunction.editTextureScaleX =>
          if (state == InputState.Positive) changeTextureScale(scaleStep, 0f)
          else if (state == InputState.Negative)
            changeTextureScale(-scaleStep, 0f)
        case InputFunction.editTextureScaleY =>
          if (state == InputState.Positive) changeTextureScale(0f, scaleStep)
          else if (state == InputState.Negative)
            changeTextureScale(0f, -scaleStep)
        case InputFunction.editTextureOffsetXSlow =>
          if (state == InputState.Positive)
            changeTextureOffset(-offsetStepSlow, 0f)
          else if (state == InputState.Negative)
            changeTextureOffset(offsetStepSlow, 0f)
        case InputFunction.editTextureOffsetYSlow =>
          if (state == InputState.Positive)
            changeTextureOffset(0f, offsetStepSlow)
          else if (state == InputState.Negative)
            changeTextureOffset(0f, -offsetStepSlow)
        case InputFunction.editTextureScaleXSlow =>
          if (state == InputState.Positive)
            changeTextureScale(scaleStepSlow, 0f)
          else if (state == InputState.Negative)
            changeTextureScale(-scaleStepSlow, 0f)
        case InputFunction.editTextureScaleYSlow =>
          if (state == InputState.Positive)
            changeTextureScale(0f, scaleStepSlow)
          else if (state == InputState.Negative)
            changeTextureScale(0f, -scaleStepSlow)
        case InputFunction.editSlopeAngleX =>
          if (state == InputState.Positive) changeSlopeX(slopeStep)
          else if (state == InputState.Negative) changeSlopeX(-slopeStep)
        case InputFunction.editSlopeAngleY =>
          if (state == InputState.Positive) changeSlopeY(slopeStep)
          else if (state == InputState.Negative) changeSlopeY(-slopeStep)
        case InputFunction.changeSlopeStretch =>
          if (state == InputState.Positive) changeSlopeStretch()
        case InputFunction.changeMaterial =>
          if (state == InputState.Positive) changeMaterial()
        case InputFunction.editSurface =>
          if (state == InputState.Positive) editSurface()
        case InputFunction.editSector =>
          if (state == InputState.Positive) editSector()
        case _ => println(s"got: $functionId")
      }
    }
  }

  def valueActive(functionId: FunctionId, value: Double, tpf: Double) =
    if (!isModalOpen) {
      functionId match {
        case InputFunction.mouseWheel => {
          if (value > 0.0) changeHeight(heightStep)
          else if (value < 0.0) changeHeight(-heightStep)
        }
        case InputFunction.mouseWheelShift => {
          if (value > 0.0) changeHeight(heightStepSlow)
          else if (value < 0.0) changeHeight(-heightStepSlow)
        }
      }
    }

  override def onEvent(event: EditorEvent) = event match {
    case MaterialChosen(target: SectorTarget, index) =>
      materialChosen(target, index)
    case _ =>
  }

  def sectorService = SectorService()

  def changeHeight(factor: Float): Unit = {
    targetTracker.ifSector(target => sectorService.changeHeight(target, factor))
  }

  def changeMaterial(): Unit = {
    targetTracker.foreach(target => EventBus.trigger(ChangeMaterial(target)))
  }

  def materialChosen(target: SectorTarget, index: Long): Unit = {
    SectorSurfaceMutator.mutate(target, surface => surface.updateIndex(index))
  }

  def changeTextureOffset(factorX: Float, factorY: Float): Unit = {
    targetTracker.ifSector(target =>
      sectorService.mutate(target, _.move(factorX, factorY))
    )
  }

  def changeTextureScale(factorX: Float, factorY: Float): Unit = {
    targetTracker.ifSector(target =>
      sectorService.mutate(target, _.scale(factorX, factorY))
    )
  }

  def changeSlopeX(factor: Float): Unit = {
    targetTracker.ifSector(target =>
      sectorService.mutateSlope(target, _.tiltX(factor))
    )
  }

  def changeSlopeY(factor: Float): Unit = {
    targetTracker.ifSector(target =>
      sectorService.mutateSlope(target, _.tiltY(factor))
    )
  }

  def changeSlopeStretch(): Unit = {
    targetTracker.ifSector(target => sectorService.rotateStretchMode(target))
  }

  def editSurface(): Unit = {
    targetTracker.ifSector(target =>
      EditSurfaceState.open(stateManager, target)
    )
  }

  def editSector(): Unit = {
    targetTracker.ifSector({
      case target: PlatformTarget =>
        EditSectorState.open(stateManager, target.sectorId, Some(target.idx))
      case target: SectorTarget =>
        EditSectorState.open(stateManager, target.sectorId)
    })
  }
}
