package com.stovokor.editor.state

import com.stovokor.util.EditorEventListener
import com.stovokor.editor.model.Line
import com.simsilica.lemur.Container
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.stovokor.util.EditorEvent
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.jme3.math.ColorRGBA
import com.simsilica.lemur.Label
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.factory.Mesh2dFactory
import com.jme3.scene.Node
import com.stovokor.editor.gui.Palette
import com.stovokor.editor.gui.Mode2DLayers
import com.jme3.scene.Geometry
import com.jme3.math.Vector3f
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.Point
import com.stovokor.editor.gui.K
import java.util.Objects
import com.stovokor.util.SelectionChange
import com.stovokor.util.EventBus
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.ViewModeSwitch
import com.stovokor.editor.model.SelectionLine
import com.stovokor.editor.service.SectorService
import com.stovokor.editor.service.LineService
import com.jme3.math.FastMath
import com.jme3.math.Vector2f
import com.simsilica.lemur.Checkbox
import com.jme3.scene.Spatial.CullHint
import com.stovokor.util.LemurExtensions._
import com.stovokor.editor.control.ConstantSizeOnScreenControl

object Parameters {
  def apply() = new Parameters

  class Mod(
      var value: Float,
      step: Float,
      min: Float = Float.MinValue,
      max: Float = Float.MaxValue
  ) {
    def increment(): Unit = {
      value = max.min(value + step)
    }

    def decrement(): Unit = {
      value = min.max(value - step)
    }

    def negate(): Unit = {
      value = -value
    }

    def set(v: Float): Unit = {
      value = v
    }

    def enabled = value != 0f
  }

}

class Parameters {
  var segments = 2

  def incSegments(): Unit = {
    segments = 100.min(segments + 1)
  }

  def decSegments(): Unit = {
    segments = 2.max(segments - 1)
  }

  var angle = new Parameters.Mod(0, 5, -355, 355)
  var jagged = new Parameters.Mod(0, 0.1f)
  var stepped = new Parameters.Mod(0, 0.1f)

  override def toString(): String = Objects.toString(segments, "segments")
}

object EditLineState {
  def open(
      sm: AppStateManager,
      line: Line,
      params: Parameters = Parameters(),
      renderView: Boolean = true
  ): Unit = {
    Option(sm.getState(classOf[EditLineState])).foreach(sm.detach)
    sm.attach(new EditLineState(line, params, renderView))
  }
}

class EditLineState(line: Line, var params: Parameters, renderView: Boolean)
    extends BaseState
    with EditorEventListener {

  var panel: Container = null
  def previewRoot = getOrCreateNode(get2DNode, "edit-line-preview")

  val sectorService = SectorService()
  val lineService = LineService()

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SelectionChange])
    EventBus.subscribeByType(this, classOf[SelectionModeSwitch])
    EventBus.subscribe(this, ViewModeSwitch())

    if (renderView) {
      panel = new Container(
        new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.None)
      )
      guiNode.attachChild(panel)
      redrawPanel()
      redrawPreview()
    }
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)

    if (renderView) {
      guiNode.detachChild(panel)
      previewRoot.removeFromParent()
    }
  }

  def onEvent(event: EditorEvent) = event match {
    case SelectionModeSwitch(_) => cancel()
    case ViewModeSwitch()       => cancel()
    case SelectionChange(selection) =>
      selection.headOption.foreach({
        case (SelectionLine(newLine)) => {
          EditLineState.open(stateManager, newLine, params)
        }
      })
    case _ =>
  }

  def redrawPanel(): Unit = {
    panel.clearChildren()
    panel.setLocalTranslation(4, (cam.getHeight - 30).toFloat, 0)
    panel.setBackground(
      new QuadBackgroundComponent(new ColorRGBA(0, 0, 0, .6f))
    )

    val builder = new PanelBuilder(panel)
    builder.title()
    builder.segments()
    builder.modsTitle()
    builder.mod(params.angle, "Arc", 0, 90)
    builder.mod(params.jagged, "Jagged", 2, 0.5f)
    builder.mod(params.stepped, "Stepped", 2, 0.5f)
    builder.apply()
  }

  def redrawPreview(): Unit = {
    // calculate new state
    val lines = LineSplitter.split(line)
    val points = lines.flatMap(_.points).toSet
    // update scene
    previewRoot.detachAllChildren()
    val node = new Node()
    val builder = new PreviewBuilder(node)
    lines.foreach(builder.drawLine)
    points.foreach(builder.drawPoint)

    // arc
    val p = LineSplitter.center(line)
    builder.drawPoint(p)

    previewRoot.attachChild(node)
  }

  def cancel(): Unit = {
    stateManager.detach(this)
  }

  def applyChanges(): Unit = {
    val lines = LineSplitter.split(line)
    println(s"Split params = ${Parameters}")
    println(s"      result = $lines")
    lineService.replaceLine(line, lines)
    cancel()
  }

  case class PreviewBuilder(node: Node) {

    def plainColor(color: ColorRGBA) = MaterialFactory().plainColor(color)

    def clear(): Unit = {
      node.detachAllChildren()
    }

    def drawLine(line: Line): Unit = {
      val lineGeo = new Geometry(
        "line",
        new com.jme3.scene.shape.Line(
          new Vector3f(line.a.x, line.a.y, Mode2DLayers.drawing),
          new Vector3f(line.b.x, line.b.y, Mode2DLayers.drawing)
        )
      )
      lineGeo.setMaterial(plainColor(Palette.drawing))
      node.attachChild(lineGeo)
    }

    def drawPoint(point: Point): Unit = {
      val pointGeo = new Geometry("point", K.vertexBox)
      pointGeo.setMaterial(plainColor(Palette.drawing))
      pointGeo.setLocalTranslation(point.x, point.y, Mode2DLayers.drawing)
      pointGeo.addControl(new ConstantSizeOnScreenControl)
      node.attachChild(pointGeo)
    }

  }

  object LineSplitter {

    trait PointPositioner {
      def point(i: Int): Point
    }

    class AlignedPositioner(line: Line) extends PointPositioner {
      lazy val ortho = orthoDir(line)
      val (a, b) = (line.a, line.b)

      def point(i: Int) = {
        val mod = if (params.stepped.enabled) 2 else 1
        val j = i / mod
        val segs = params.segments / mod
        val factor = j.toFloat / segs.toFloat
        // jagged
        val disp1 =
          if (j % 2 == 0) Vector2f.ZERO else ortho.mult(-params.jagged.value)
        // stepped
        val disp2 =
          if (((i - 1) / 2) % 2 == 0) Vector2f.ZERO
          else ortho.mult(-params.stepped.value)
        val disp = disp1.add(disp2)
        Point(
          disp.x + a.x + factor * (b.x - a.x),
          disp.y + a.y + factor * (b.y - a.y)
        )
      }
    }
    class ArcPositioner(line: Line) extends PointPositioner {
      val base = baseang(line)
      val orig = center(line)
      val rad1 = radius(line)

      def point(i: Int) = {
        val mod = if (params.stepped.enabled) 2 else 1
        val j = (i - 0) / mod
        val segs = params.segments / mod
        val factor = 1 - j.toFloat / segs.toFloat
        val ang = base + params.angle.value * factor
        val sign = if (params.angle.value < 0) -1 else 1
        // jagged
        val extra1 = if (j % 2 == 0) 0 else params.jagged.value
        // stepped
        val extra2 = if (((i - 1) / 2) % 2 == 0) 0 else params.stepped.value
        val rad = rad1 + extra1 + extra2

        Point(
          orig.x + sign * rad * FastMath.cos(FastMath.DEG_TO_RAD * ang),
          orig.y + sign * rad * FastMath.sin(FastMath.DEG_TO_RAD * ang)
        )
      }
    }

    def split(line: Line): List[Line] = {
      val positioner =
        if (params.angle.enabled) new ArcPositioner(line)
        else new AlignedPositioner(line)

      val points = (1 to params.segments - 1)
        .map(positioner.point)

      val allpoints = (List(line.a) ++ points ++ List(line.b))
      val nodup = allpoints.foldLeft(List[Point]())((l, curr) => {
        val same = l.lastOption.exists(pre => pre.distance(curr) < 0.002)
        if (same) l else l ++ List(curr)
      })
      val res = nodup.sliding(2).map(l => Line(l(0), l(1))).toList
      res
    }

    def radius(l: Line) = {
      l.length / (2 * FastMath.sin(
        .5f * FastMath.DEG_TO_RAD * params.angle.value
      ))
    }

    def orthoDir(l: Line) = new Vector2f(l.b.y - l.a.y, l.a.x - l.b.x).normalize

    def center(l: Line) = {
      val v2 = orthoDir(l)
      val dist2 =
        radius(l) * FastMath.cos(.5f * FastMath.DEG_TO_RAD * params.angle.value)
      val v3 = v2.multLocal(dist2)
      val middle = l.split(0.5f)._1.b
      val res = new Vector2f(middle.x, middle.y).add(v3)
      Point(res.x, res.y)
    }

    def baseang(l: Line) = {
      val o = center(l)
      def ang(p: Point) =
        FastMath.RAD_TO_DEG * FastMath.atan2(p.y - o.y, p.x - o.x)
      ang(l.b)
    }

  }

  class PanelBuilder(panel: Container) {

    def title(): Unit = {
      val title = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      title.addChild(
        new Label(
          s"Line: ${line.a} - ${line.b}",
          panel.getElementId.child("title.label")
        )
      )
      title.addChild(GuiFactory.actionButton("x", cancel _))
    }

    def segments(): Unit = {
      val segmentsPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      segmentsPanel.addChild(new Label("Segments"))
      val field = segmentsPanel.addChild(numberField(params.segments, 0))
      def refresh() = {
        field.setText(s"${params.segments}")
        redrawPreview()
      }
      segmentsPanel.addChild(
        GuiFactory.actionButton(
          "-",
          () => {
            params.decSegments()
            refresh()
          }
        )
      )
      segmentsPanel.addChild(
        GuiFactory.actionButton(
          "+",
          () => {
            params.incSegments()
            refresh()
          }
        )
      )
    }

    def visible(visible: Boolean) =
      if (visible) CullHint.Inherit else CullHint.Always

    def modsTitle(): Unit = {
      panel.addChild(new Label("Modifiers"))
    }

    def numberField(value: Number, precision: Int) = {
      val label = new Label(s"%.${precision}f".format(value.floatValue))
      label.setColor(Palette.fieldForeground)
      label.setBackground(new QuadBackgroundComponent(Palette.fieldBackground))
      label
    }

    def mod(
        mod: Parameters.Mod,
        label: String,
        precision: Int,
        prefill: Float = 0f
    ): Unit = {
      val arcPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      val checkbox = arcPanel.addChild(new Checkbox(label))
      val container = arcPanel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      val field =
        container.addChild(numberField(params.jagged.value, precision))

      def refresh() = {
        field.setText(s"%.${precision}f".format(mod.value))
        redrawPreview()
      }
      container.setCullHint(visible(mod.enabled))
      checkbox.setChecked(mod.enabled)
      checkbox.onClick(() => {
        if (checkbox.isChecked) mod.set(prefill)
        else mod.set(0)
        container.setCullHint(visible(mod.enabled))
        refresh()
      })
      container.addChild(
        GuiFactory.actionButton(
          "-",
          () => {
            mod.decrement()
            refresh()
          }
        )
      )
      container.addChild(
        GuiFactory.actionButton(
          "+",
          () => {
            mod.increment()
            refresh()
          }
        )
      )
      container.addChild(
        GuiFactory.actionButton(
          "<>",
          () => {
            mod.negate()
            refresh()
          }
        )
      )
    }

    def apply(): Unit = {
      val applyPanel = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
        )
      )
      applyPanel.addChild(new Container)
      applyPanel.addChild(GuiFactory.actionButton("ok", "Apply", applyChanges))
    }
  }
}
