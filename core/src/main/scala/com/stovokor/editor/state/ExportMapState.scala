package com.stovokor.editor.state

import java.io.File

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.export.binary.BinaryExporter
import com.jme3.scene.Node
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.StateFunctionListener
import com.stovokor.editor.control.HighlightControl
import com.stovokor.editor.input.InputFunction
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.ExportMap
import com.stovokor.util.JmeExtensions.SpatialExtensions

import com.stovokor.editor.gui.GuiFactory
import com.simsilica.lemur.OptionPanelState
import com.stovokor.editor.export.MapExporterRegistry
import com.stovokor.editor.export.J3oExporter
import com.simsilica.lemur.Container
import scala.collection.mutable.ListBuffer
import com.stovokor.editor.export.MapDataExporter
import com.stovokor.editor.model.repository.SettingsRepository
import com.stovokor.editor.navmesh.NavMeshExporter
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.jme3.math.Vector3f
import com.simsilica.lemur.Label
import com.simsilica.lemur.Checkbox
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.stovokor.editor.gui.Palette
import com.simsilica.lemur.TextField
import com.stovokor.editor.nemesismate.lemur.ViewportPanel2D
import com.simsilica.lemur.Panel
import com.simsilica.lemur.style.ElementId
import com.simsilica.lemur.event.MouseListener
import com.jme3.input.event.MouseButtonEvent
import com.jme3.scene.Spatial
import com.jme3.input.event.MouseMotionEvent
import com.simsilica.lemur.event.DefaultMouseListener
import com.jme3.input.MouseInput
import com.stovokor.editor.gui.ScrollPanelMouseListener
import com.stovokor.util.LemurExtensions.ButtonExtension

class ExportMapState
    extends BaseState
    with EditorEventListener
    with CanMapInput
    with StateFunctionListener
    with OptionPanelAccess {

  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()

  var currentFile: Option[String] = None

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    MapExporterRegistry.register(new J3oExporter(assetManager))
    EventBus.subscribe(this, ExportMap())
    setupInput
  }

  override def cleanup(): Unit = {
    super.cleanup
    EventBus.removeFromAll(this)
    inputMapper.removeStateListener(this, InputFunction.export)
  }

  def setupInput: Unit = {
    inputMapper.addStateListener(this, InputFunction.export)
    inputMapper.activateGroup(InputFunction.files)
  }

  def onEvent(event: EditorEvent) = event match {
    case ExportMap() => openExportWindow()
    case _           =>
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {
    if (!isModalOpen && value == InputState.Positive) func match {
      case InputFunction.export => EventBus.trigger(ExportMap())
      case _                    =>
    }
  }

  // Create a Lemur window with options for the export
  def openExportWindow(): Unit = {
    val container = new Container()
    val exporters = MapExporterRegistry.exporters.map(x => x.name -> x).toMap
    val exporterPanels = exporters.map({ case (name, exporter) =>
      (name, exporter.exportPanel)
    })
    val exporterNames = exporters.keys.toList
    val enabledExporters: ListBuffer[MapDataExporter] = ListBuffer()
    def selectionCallback(exporter: String, value: Boolean): Unit = {
      if (value) {
        enabledExporters += (exporters(exporter))
        container.addChild(exporterPanels(exporter))
      } else {
        enabledExporters -= (exporters(exporter))
        container.removeChild(exporterPanels(exporter))
      }
    }
    def acceptCallback(path: String, name: String): Unit = {
      val settings = SettingsRepository().get.updateLastExportPath(path)
      SettingsRepository().update(settings)
      enabledExporters.foreach(exporter => exporter.exportData(path, name))
    }
    val dialog = createExportDialog(
      exporterNames,
      container,
      acceptCallback,
      selectionCallback
    )
    showModal(dialog)
  }

  def predictMapName() = {
    Option(stateManager.getState(classOf[SaveOpenFileState]))
      .flatMap(_.currentFile)
      .map(_.getName)
      .map(_.replaceAll("(\\.v[0-9]+)?\\.m8$", ""))
      .getOrElse("untitled")
  }

  def createExportDialog(
      exporterNames: List[String],
      panels: Container,
      acceptCallback: (String, String) => Unit,
      selectionCallback: (String, Boolean) => Unit
  ) = {
    val (width, height) = (cam.getWidth, cam.getHeight)
    val mainPanel = new Container(
      new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.Last)
    )
    mainPanel.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 150).toFloat, 0)
    )

    // enable exporters
    val exportersPanel = mainPanel.addChild(
      new Container(
        new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.Last)
      )
    )
    exportersPanel.setPreferredSize(new Vector3f(200, height.toFloat, 0))
    exportersPanel.addChild(new Label("Exporters: "))
    for (exporter <- exporterNames) {
      val cb = exportersPanel.addChild(new Checkbox(exporter))
      cb.onClick(() => selectionCallback(exporter, cb.getModel.isChecked))
    }
    exportersPanel.addChild(new Container)

    // big panel on the right
    val exportPanel = new Container(
      new SpringGridLayout(Axis.Y, Axis.X, FillMode.Last, FillMode.Last)
    )
    val viewportPanel = new ViewportPanel2D(
      stateManager,
      new ElementId(Container.ELEMENT_ID),
      null
    )
    viewportPanel.attachScene(exportPanel)
    viewportPanel.addMouseListener(
      new ScrollPanelMouseListener(viewportPanel, exportPanel)
    )
    mainPanel.addChild(viewportPanel)

    // set directory
    val lastDir = SettingsRepository().get.lastExportPath
    val lastDirFile = new File(lastDir)
    val directoryPanel =
      exportPanel.addChild(new Container(new SpringGridLayout(Axis.X, Axis.Y)))
    val directoryLabel = directoryPanel.addChild(new Label("Directory: "))
    val currentPath = directoryPanel.addChild(new Label(lastDir))
    currentPath.setBackground(new QuadBackgroundComponent(Palette.pathLabel))
    val change = directoryPanel.addChild(
      GuiFactory.button("magnifier.png", "", label = "Browse...")
    )

    // name
    val namePanel = exportPanel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    namePanel.addChild(new Label("Name:"))
    val nameField = namePanel.addChild(new TextField(predictMapName()))
    nameField.setBackground(new QuadBackgroundComponent(Palette.pathLabel))

    // add export panels
    exportPanel.addChild(panels)
    exportPanel.addChild(new Container)
    // the finish export action (disabled by default and activated by selecting a dir)
    val exportAction = GuiFactory.action(
      "ok",
      "Export",
      () => acceptCallback(currentPath.getText, nameField.getText)
    )
    exportAction.setEnabled(lastDirFile.exists)
    change.onClick(() => {
      val fileChooser = new FileChooser()
      fileChooser.setDirectory(lastDirFile)
      fileChooser.setDirectorySelection()
      fileChooser.setCallback(file => {
        currentPath.setText(file.getPath)
        exportAction.setEnabled(true)
      })
      fileChooser.showOpenDialog()
    })

    // modal window
    val window = new OptionPanel(
      "Export",
      "",
      null,
      GuiFactory.action("x", "Cancel"),
      exportAction
    )
    window.getContainer.addChild(mainPanel)
    window
  }
}
