package com.stovokor.editor.state

import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.jme3.math.Vector3f
import com.jme3.input.controls.ActionListener
import com.jme3.input.KeyInput
import com.jme3.input.controls.KeyTrigger
import com.simsilica.lemur.input.AnalogFunctionListener
import com.simsilica.lemur.input.StateFunctionListener
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.stovokor.editor.input.InputFunction
import com.jme3.input.InputManager
import com.jme3.input.FlyByCamera
import com.jme3.input.CameraInput
import com.jme3.input.controls.MouseAxisTrigger
import com.jme3.input.controls.MouseButtonTrigger
import com.jme3.input.MouseInput
import com.jme3.light.AmbientLight
import com.jme3.math.ColorRGBA
import com.jme3.scene.debug.Arrow
import com.jme3.scene.Geometry
import com.stovokor.editor.factory.MaterialFactoryClient
import com.jme3.math.Matrix3f
import com.stovokor.util.CoordinateSystem
import com.jme3.math.Quaternion

object Camera3DState {
  var location = new Vector3f(0, 2, 0)
  var rotation = new Quaternion().lookAt(Vector3f.UNIT_Z, Vector3f.UNIT_Y)
}

class Camera3DState
    extends BaseState
    with CanMapInput
    with AnalogFunctionListener
    with MaterialFactoryClient {

  var zoom = 10f
  val speed = 10f
  val light = new AmbientLight(ColorRGBA.White)

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    app.getFlyByCamera.setEnabled(true)
    app.getFlyByCamera.setDragToRotate(false)
    app.getFlyByCamera.setMoveSpeed(10f)
    app.getFlyByCamera.setRotationSpeed(2f)
    cam.setParallelProjection(false)
    cam.setLocation(Camera3DState.location)
    cam.setRotation(Camera3DState.rotation)
    cam.setFrustumPerspective(
      45f,
      cam.getWidth.toFloat / cam.getHeight,
      1f,
      1000f
    )
    cam.update

    // Add a light for lighted materials
    rootNode.addLight(light)
    setupInput()

    // show coordinate system
    drawAxis()
  }

  override def cleanup(): Unit = {
    Camera3DState.location = cam.getLocation.clone
    Camera3DState.rotation = cam.getRotation.clone
    app.getFlyByCamera.setEnabled(false)
    rootNode.removeLight(light)
  }

  override def update(tpf: Float): Unit = {}

  def setupInput() = {
    // rewrite flyCam mappings, ignore mouse, keep keys
    inputManager.setCursorVisible(true)
    inputManager.deleteMapping(CameraInput.FLYCAM_LEFT)
    inputManager.deleteMapping(CameraInput.FLYCAM_RIGHT)
    inputManager.deleteMapping(CameraInput.FLYCAM_UP)
    inputManager.deleteMapping(CameraInput.FLYCAM_DOWN)
    if (inputManager.hasMapping(CameraInput.FLYCAM_ZOOMIN)) {
      inputManager.deleteMapping(CameraInput.FLYCAM_ZOOMIN)
      inputManager.deleteMapping(CameraInput.FLYCAM_ZOOMOUT)
      inputManager.deleteMapping(CameraInput.FLYCAM_ROTATEDRAG)
    }

    inputManager.addMapping(
      CameraInput.FLYCAM_LEFT,
      new KeyTrigger(KeyInput.KEY_LEFT)
    )
    inputManager.addMapping(
      CameraInput.FLYCAM_RIGHT,
      new KeyTrigger(KeyInput.KEY_RIGHT)
    )
    inputManager.addMapping(
      CameraInput.FLYCAM_UP,
      new KeyTrigger(KeyInput.KEY_UP)
    )
    inputManager.addMapping(
      CameraInput.FLYCAM_DOWN,
      new KeyTrigger(KeyInput.KEY_DOWN)
    )

    inputManager.addListener(
      app.getFlyByCamera,
      CameraInput.FLYCAM_LEFT,
      CameraInput.FLYCAM_RIGHT,
      CameraInput.FLYCAM_UP,
      CameraInput.FLYCAM_DOWN
    )
  }

  def valueActive(func: FunctionId, value: Double, tpf: Double): Unit = {
    func match {
      case InputFunction.moveX =>
      case InputFunction.moveY =>
      case InputFunction.moveZ =>
      case _                   =>
    }
  }

  def valueChanged(func: FunctionId, value: InputState, tpf: Double): Unit = {}

  def drawAxis(): Unit = {
    val node = getOrCreateNode(get3DNode, "axis")
    node.detachAllChildren()
    def drawArrow(vector: Vector3f, color: ColorRGBA): Unit = {
      val mat = plainColor(color)
      val arrow = new Geometry("arrow", new Arrow(vector))
      arrow.setMaterial(mat)
      node.attachChild(arrow)
    }
    drawArrow(CoordinateSystem.toJme(Vector3f.UNIT_X), ColorRGBA.Red)
    drawArrow(CoordinateSystem.toJme(Vector3f.UNIT_Y), ColorRGBA.Green)
    drawArrow(CoordinateSystem.toJme(Vector3f.UNIT_Z), ColorRGBA.Blue)
  }

}
