package com.stovokor.editor.factory

import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.Line
import com.stovokor.editor.model.Border
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.BorderStrip
import com.stovokor.editor.model.StretchMode.Normal

object BorderFactory {

  def createBorders(
      fromId: Long,
      from: Sector,
      borders: List[(Long, Sector, List[Line])]
  ): List[Border] = {
    borders.flatMap({ case (otherId, other, lines) =>
      createBorders(fromId, from, otherId, other, lines)
    })
  }

  def createBorders(
      fromId: Long,
      from: Sector,
      otherId: Long,
      other: Sector,
      lines: List[Line]
  ): List[Border] = {
    lines.flatMap(line => {
      val (textureAB, modeAB) = findSurfaceInfo(from, line)
      val (textureBA, modeBA) = findSurfaceInfo(other, line)
      List(
        Border(
          fromId,
          otherId,
          line,
          BorderStrip(textureAB, modeAB),
          BorderStrip(textureAB, modeAB)
        ),
        Border(
          otherId,
          fromId,
          line.reverse,
          BorderStrip(textureBA, modeBA),
          BorderStrip(textureBA, modeBA)
        )
      )
    })
  }

  def findSurfaceInfo(sector: Sector, line: Line) =
    sector.openWalls
      .find(_.line.alike(line))
      .map(w => (w.texture, w.stretchMode))
      .orElse(Some((SurfaceTexture(), Normal)))
      .get
}
