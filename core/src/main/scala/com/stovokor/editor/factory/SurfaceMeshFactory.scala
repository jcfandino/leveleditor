package com.stovokor.editor.factory

import com.stovokor.editor.model.Point
import com.stovokor.editor.model.Triangle
import com.jme3.math.Vector3f
import com.jme3.scene.Mesh
import com.jme3.scene.VertexBuffer.Type
import com.jme3.util.BufferUtils
import com.jme3.math.Vector2f
import com.jme3.util.TangentBinormalGenerator
import com.jme3.scene.Geometry
import com.stovokor.util.CoordinateSystem
import com.stovokor.editor.model.Surface

object SurfaceMeshFactory {

  def createMesh(
      triangles: List[Triangle],
      center: Point,
      surface: Surface,
      faceUp: Boolean,
      normal: Vector3f,
      mode3d: Boolean,
      density: Float = 1f
  ): Mesh = {
    // we invert the triangle because we are inverting the X coordinate.
    def sortTriangle(t: Triangle) = if (!faceUp) t else t.reverse

    val uniquePoints = triangles
      .flatMap(_.vertices)
      .distinct
      .sortBy(p => p.distance(Point(0f, 0f)))

    val mesh = new Mesh
    mesh.setBuffer(
      Type.Position,
      3,
      BufferUtils.createFloatBuffer(
        uniquePoints
          .map(p => new Vector3f(p.x, p.y, surface.heightOnPoint(p, center)))
          .map(p => // change coordinates
            if (mode3d) CoordinateSystem.toJme(p) else p
          )
          .toArray: _*
      )
    )

    val inv = if (faceUp) 1f else -1f
    mesh.setBuffer(
      Type.TexCoord,
      2,
      BufferUtils.createFloatBuffer(
        uniquePoints
          .map(p => {
            val surf = surface.texture
            new Vector2f(surf.uvx(p.x), inv * surf.uvy(p.y))
          })
          .map(_.mult(density))
          .toArray: _*
      )
    )

    mesh.setBuffer(
      Type.Index,
      1,
      BufferUtils.createIntBuffer(
        triangles
          .map(sortTriangle)
          .flatMap(_.vertices)
          .map(uniquePoints.indexOf)
          .toArray: _*
      )
    )

    mesh.setBuffer(
      Type.Normal,
      3,
      BufferUtils.createFloatBuffer(uniquePoints.map(_ => normal): _*)
    )

    if (surface.texture.flags.inLightmap) {
      // set lightmap uv
      val (xo, yo, xe, ye) = uniquePoints
        .foldLeft(
          (Float.MinValue, Float.MinValue, Float.MaxValue, Float.MaxValue)
        )({ case ((xo, yo, xe, ye), Point(x, y)) =>
          (xo max x, yo max y, xe min x, ye min y)
        })
      mesh.setBuffer(
        Type.TexCoord2,
        2,
        BufferUtils.createFloatBuffer(
          uniquePoints
            .map({ case Point(px, py) =>
              new Vector2f((px - xo) / (xe - xo), (py - yo) / (ye - yo))
            })
            .toArray: _*
        )
      )
    }
    mesh.updateBound()
    TangentBinormalGenerator.generate(mesh)
    mesh
  }
}
