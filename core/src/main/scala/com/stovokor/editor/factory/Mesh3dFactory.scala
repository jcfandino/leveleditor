package com.stovokor.editor.factory

import com.jme3.asset.AssetManager
import com.jme3.math.Plane
import com.jme3.math.Vector2f
import com.jme3.math.Vector3f
import com.jme3.scene.Geometry
import com.jme3.scene.Mesh
import com.jme3.scene.Node
import com.jme3.scene.VertexBuffer.Type
import com.jme3.scene.shape.Box
import com.jme3.texture.Texture
import com.jme3.util.BufferUtils
import com.stovokor.editor.control.HighlightControl
import com.stovokor.editor.lighting.AmbientLight
import com.stovokor.editor.lighting.DirectionalLight
import com.stovokor.editor.lighting.Light
import com.stovokor.editor.lighting.PointLight
import com.stovokor.editor.lighting.SpotLight
import com.stovokor.editor.lighting.baking.LightmapUVBuilder
import com.stovokor.editor.model.Border
import com.stovokor.editor.model.Line
import com.stovokor.editor.model.Platform
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.StretchMode
import com.stovokor.editor.model.StretchMode.FollowCeiling
import com.stovokor.editor.model.StretchMode.FollowFloor
import com.stovokor.editor.model.StretchMode.Normal
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.Target
import com.stovokor.editor.model.Triangle
import com.stovokor.editor.model.Wall
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.CoordinateSystem
import com.stovokor.util.JmeExtensions._

import jme3tools.optimize.GeometryBatchFactory
import com.stovokor.editor.model.PlatformSide
import com.stovokor.editor.model.SavableTarget
import scala.jdk.CollectionConverters._
import com.jme3.scene.Spatial
import com.jme3.material.Material
import com.jme3.material.RenderState.BlendMode
import com.jme3.util.TangentBinormalGenerator
import com.stovokor.editor.decal.Decal
import com.stovokor.editor.decal.DecalProjector
import com.jme3.math.Quaternion
import com.jme3.math.FastMath

object Mesh3dFactory {

  // basic one used by presenter
  def apply(assetManager: AssetManager, preview: Boolean, density: Float) =
    new WithMaterialMesh3dFactory(assetManager, preview, density)
  // when only a basic mesh is needed
  def apply() = new GeometryOnlyMesh3dFactory()

  // decorators
  def mapLightmapUV(delegate: Mesh3dFactory) =
    new LightmapUVMappingMesh3dFactory(delegate)

  def textureLightmap(
      delegate: Mesh3dFactory,
      lightmap: Texture,
      assetManager: AssetManager,
      customMaterial: Boolean
  ) =
    new LightmapTextureMesh3dFactory(
      assetManager,
      delegate,
      lightmap,
      customMaterial
    )

  def lightmapUv(delegate: Mesh3dFactory) = new LightmapUVMappingMesh3dFactory(
    delegate
  )

  def withLights(delegate: Mesh3dFactory, lights: Set[Light]) =
    new AddLightsMesh3dFactory(delegate, lights)

  def batched(delegate: Mesh3dFactory) = new BatchedMesh3dFactory(delegate)

  def withDecals(
      assetManager: AssetManager,
      delegate: Mesh3dFactory,
      decals: Set[Decal]
  ) =
    new AddDecalsMesh3dFactory(assetManager, delegate, decals)

}

object UserDataFlags {

  val skipLightmapKey = "skipLightmap"

  def setSkipLightmap(spatial: Spatial): Unit = {
    spatial.setUserData(skipLightmapKey, true)
  }

  def isSkipLighmap(spatial: Spatial) =
    true == spatial.getUserData(skipLightmapKey)

}

trait Mesh3dFactory {

  def setMaterial(geom: Geometry, texture: SurfaceTexture): Unit = {}
  def addControl(geom: Geometry, target: Target): Unit = {}
  def setUserData(geom: Geometry, target: Target): Unit = {}
  def density = 1f

  def createAll = {
    val root = new Node("m8")
    SectorRepository().sectors.foreach({
      case (id, sector) => {
        val node = new Node("sector-" + id)
        root.attachChild(node)
        val borders = BorderRepository().findFrom(id).map(_._2)
        val borderingSectors = borders
          .flatMap(b => List(b.sectorA, b.sectorB))
          .map(i => (i, SectorRepository().get(i)))
          .toMap
        val meshNode = createMesh(id, sector, borders, borderingSectors)
        node.attachChild(meshNode)
      }
    })
    root
  }

  def createMesh(
      id: Long,
      sec: Sector,
      borders: List[Border],
      borderingSectors: Map[Long, Sector]
  ): Node = {
    val triangles = sec.triangulate
    val center = sec.polygon.center
    val node = new Node(s"sector:$id")

    // floor
    val floor =
      createSurface(Target.Floor(id), triangles, center, sec.floor, true)
    node.attachChild(floor)

    // ceiling
    val ceiling =
      createSurface(Target.Ceiling(id), triangles, center, sec.ceiling, false)
    node.attachChild(ceiling)

    // walls
    sec.closedWalls.zipWithIndex
      .map({ case (wall, idx) => createWall(id, wall, idx, sec) })
      .foreach(node.attachChild)

    // borders
    createBordersMesh(node, id, sec, borders, borderingSectors)

    // platforms
    sec.platforms.zipWithIndex
      .map({ case (platform, idx) => createPlatform(id, sec, idx, platform) })
      .foreach(node.attachChild)

    node
  }

  def createPlatform(
      secId: Long,
      sector: Sector,
      pIdx: Int,
      platform: Platform
  ) = {
    val node = new Node(s"platform:$secId:$pIdx")
    // set the batch id if detached
    if (platform.flags.isDetached)
      node.setUserData("batchId", platform.flags.batchId)
    // find triangles
    val triangles =
      if (platform.flags.inheritHoles) sector.triangulate
      else sector.updatedHoles(Set()).triangulate
    // bottom
    val btm = createSurface(
      Target.PlatformBottom(secId, pIdx),
      triangles,
      sector.polygon.center,
      platform.bottom,
      false
    )
    node.attachChild(btm)
    // top
    val top = createSurface(
      Target.PlatformTop(secId, pIdx),
      triangles,
      sector.polygon.center,
      platform.top,
      true
    )
    node.attachChild(top)
    // sides
    val sideByLine = platform.sides.map(s => (s.line, s)).toMap
    // 1. outer sides (not holes)
    val outerlines = sector.polygon.edges.toSet
    platform.sides.zipWithIndex
      .filter({ case (s, _) => outerlines.contains(s.line) })
      .map({ case (side, sIdx) =>
        createSide(secId, pIdx, sIdx, sector, side.line, platform, side)
      })
      .foreach(node.attachChild)
    // 2. hole sides
    if (platform.flags.inheritHoles) {
      platform.sides.zipWithIndex
        .filterNot({ case (s, _) => outerlines.contains(s.line) })
        .map({ case (side, sIdx) =>
          createSide(secId, pIdx, sIdx, sector, side.line, platform, side)
        })
        .foreach(node.attachChild)
    }
    node
  }

  def intersection(
      line: Line,
      sectorA: Sector,
      sectorB: Sector,
      zf: (Sector, Point) => Float
  ) = {
    val pa1 = new Vector3f(line.a.x, line.a.y, zf(sectorA, line.a))
    val pa2 = new Vector3f(line.a.x, line.a.y, zf(sectorB, line.a))
    val pb1 = new Vector3f(line.b.x, line.b.y, zf(sectorA, line.b))
    val pb2 = new Vector3f(line.b.x, line.b.y, zf(sectorB, line.b))
    // get the directed vectors of the lines
    val vecA = pb1 subtract pa1
    val vecB = pb2 subtract pa2
    // project over plane XZ or YZ
    // find the best plane to project
    val coord: Vector3f => Float = if (vecA.x.abs > vecA.y.abs) _.x else _.y
    // use the line equations y=mx+d
    val m1 = vecA.z / coord(vecA)
    val d1 = pa1.z
    val m2 = vecB.z / coord(vecB)
    val d2 = pa2.z
    // equal equations solve x value (or y if projected over YZ)
    val value = (d2 - d1) / (m1 - m2)
    // find the parameterized value on the segment
    val t = value / (coord(pb1) - coord(pa1))
    // use the form p=po+tv to obtain the point
    pa1 add (vecA mult t)
  }

  def createBordersMesh(
      node: Node,
      id: Long,
      sector: Sector,
      borders: List[Border],
      borderingSectors: Map[Long, Sector]
  ) = {
    val idxLookup = sector.openWalls.zipWithIndex
      .map(p => (p._1.line, p._2))
      .toMap
      .withDefault(l => {
        println(
          s"ERROR: Drawing a border but missing open wall in sector, line: $l"
        )
        -1
      })
    def idx(border: Border) = idxLookup(border.line)
    def bottomMaybe(border: Border): Option[Geometry] = {
      val line = border.line
      var sectorA = borderingSectors(border.sectorA)
      var sectorB = borderingSectors(border.sectorB)
      val a1 = sectorA.floorHeightOn(line.a)
      val a2 = sectorB.floorHeightOn(line.a)
      val b1 = sectorA.floorHeightOn(line.b)
      val b2 = sectorB.floorHeightOn(line.b)
      val mode = border.surfaceFloor.stretchMode
      if (!(a1 > a2 && b1 > b2)) { // is any part visible?
        if (a2 > a1 && b1 > b2 || a1 > a2 && b2 > b1) { // lines cross?
          // lines cross so it must draw only one triangle
          val inter = intersection(line, sectorA, sectorB, _ floorHeightOn _)
          if (a2 > a1) // draw left triangle
            Some(
              createTriangle(
                Target.BorderLow(id, idx(border)),
                Line(line.a, Point(inter.x, inter.y)),
                border.surfaceFloor.texture,
                true,
                a1,
                a2,
                inter.z,
                mode
              )
            )
          else // draw right triangle
            Some(
              createTriangle(
                Target.BorderLow(id, idx(border)),
                Line(Point(inter.x, inter.y), line.b),
                border.surfaceFloor.texture,
                false,
                b1,
                b2,
                inter.z,
                mode
              )
            )
        } else { // don't cross so draw a normal quad
          Some(
            createQuad(
              Target.BorderLow(id, idx(border)),
              line,
              border.surfaceFloor.texture,
              a1,
              b1,
              a2,
              b2,
              mode
            )
          )
        }
      } else None
    }
    def topMaybe(border: Border): Option[Geometry] = {
      val line = border.line
      var sectorA = borderingSectors(border.sectorA)
      var sectorB = borderingSectors(border.sectorB)
      val a1 = sectorA.ceilingHeightOn(line.a)
      val a2 = sectorB.ceilingHeightOn(line.a)
      val b1 = sectorA.ceilingHeightOn(line.b)
      val b2 = sectorB.ceilingHeightOn(line.b)
      val mode = border.surfaceCeiling.stretchMode
      if (!(a2 > a1 && b2 > b1)) { // is any part visible?
        if (a2 > a1 && b1 > b2 || a1 > a2 && b2 > b1) { // lines cross?
          // lines cross so it must draw only one triangle
          val inter = intersection(line, sectorA, sectorB, _ ceilingHeightOn _)
          if (a1 > a2) // draw left triangle
            Some(
              createTriangle(
                Target.BorderHi(id, idx(border)),
                Line(line.a, Point(inter.x, inter.y)),
                border.surfaceCeiling.texture,
                true,
                a2,
                a1,
                inter.z,
                mode
              )
            )
          else // draw right triangle
            Some(
              createTriangle(
                Target.BorderHi(id, idx(border)),
                Line(Point(inter.x, inter.y), line.b),
                border.surfaceCeiling.texture,
                false,
                b2,
                b1,
                inter.z,
                mode
              )
            )
        } else { // don't cross so draw a normal quad
          Some(
            createQuad(
              Target.BorderHi(id, idx(border)),
              line,
              border.surfaceCeiling.texture,
              a2,
              b2,
              a1,
              b1,
              mode
            )
          )
        }
      } else None
    }
    // it would be easy to make this concurrent and serialize the last step.
    borders.zipWithIndex
      .flatMap({ case (border, idx) =>
        List(bottomMaybe(border), topMaybe(border))
      })
      .filter(_.isDefined)
      .map(_.get)
      .foreach(node.attachChild)
  }

  def createSide(
      id: Long,
      platformIdx: Int,
      sideIdx: Int,
      sector: Sector,
      sectorLine: Line,
      platform: Platform,
      side: PlatformSide
  ): Geometry = {
    val tex = side.texture
    val line = sectorLine.reverse
    val heightBottomA = sector.platformBottomHeightOn(platformIdx, line.a)
    val heightBottomB = sector.platformBottomHeightOn(platformIdx, line.b)
    val heightTopA = sector.platformTopHeightOn(platformIdx, line.a)
    val heightTopB = sector.platformTopHeightOn(platformIdx, line.b)
    val mode = side.stretchMode
    createQuad(
      Target.PlatformSide(id, platformIdx, sideIdx),
      line,
      tex,
      heightBottomA,
      heightBottomB,
      heightTopA,
      heightTopB,
      mode
    )
  }

  def createWall(id: Long, wall: Wall, idx: Int, sector: Sector): Geometry = {
    val line = wall.line
    val tex = wall.texture
    val heightBottomA = sector.floorHeightOn(line.a)
    val heightBottomB = sector.floorHeightOn(line.b)
    val heightTopA = sector.ceilingHeightOn(line.a)
    val heightTopB = sector.ceilingHeightOn(line.b)
    val mode = wall.stretchMode
    createQuad(
      Target.Wall(id, idx),
      line,
      tex,
      heightBottomA,
      heightBottomB,
      heightTopA,
      heightTopB,
      mode
    )
  }

  def createTriangle(
      target: Target,
      line: Line,
      texture: SurfaceTexture,
      left: Boolean,
      lo: Float,
      hi: Float,
      mid: Float,
      stretchMode: StretchMode
  ) = {
    val (p1, p2, p3) =
      if (left)
        (
          new Vector3f(line.a.x, line.a.y, hi),
          new Vector3f(line.a.x, line.a.y, lo),
          new Vector3f(line.b.x, line.b.y, mid)
        )
      else
        (
          new Vector3f(line.a.x, line.a.y, mid),
          new Vector3f(line.b.x, line.b.y, lo),
          new Vector3f(line.b.x, line.b.y, hi)
        )

    val m = new Mesh
    m.setBuffer(
      Type.Position,
      3,
      BufferUtils.createFloatBuffer(
        // change coordinate system
        CoordinateSystem.toJme(p1),
        CoordinateSystem.toJme(p2),
        CoordinateSystem.toJme(p3)
      )
    )

    // uv coordinates for texture
    val (ux1, ux2, ux3) =
      if (p1.x == p2.x && p1.y == p2.y) (0f, 0f, 1f) else (-1f, 0f, 0f)
    val (uy1, uy2, uy3) = (left, stretchMode) match {
      case (_, Normal) =>
        (texture.uvy(p1.z), texture.uvy(p2.z), texture.uvy(p3.z))
      case (true, FollowFloor) =>
        (texture.uvy(hi - lo), texture.uvy(0), texture.uvy(0))
      case (true, FollowCeiling) =>
        (texture.uvy(0), -texture.uvy(hi - lo), texture.uvy(0))
      case (false, FollowFloor) =>
        (texture.uvy(0), texture.uvy(0), texture.uvy(hi - lo))
      case (false, FollowCeiling) =>
        (texture.uvy(0), -texture.uvy(hi - lo), texture.uvy(0))
    }
    m.setBuffer(
      Type.TexCoord,
      2,
      BufferUtils.createFloatBuffer(
        new Vector2f(texture.uvx(ux1 * line.length), uy1).mult(density),
        new Vector2f(texture.uvx(ux2 * line.length), uy2).mult(density),
        new Vector2f(texture.uvx(ux3 * line.length), uy3).mult(density)
      )
    )

    val normal =
      CoordinateSystem.toJme(p2 subtract p1).cross(p3 subtract p1).normalize

    m.setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(2, 0, 1))
    m.setBuffer(
      Type.Normal,
      3,
      BufferUtils.createFloatBuffer(normal, normal, normal)
    )

    if (texture.flags.inLightmap) {
      // uv coordinates for light map
      m.setBuffer(
        Type.TexCoord2,
        2,
        BufferUtils.createFloatBuffer(
          new Vector2f(1, 1),
          new Vector2f(0, 1),
          new Vector2f(0, 0)
        )
      )
    }

    m.updateBound()
    TangentBinormalGenerator.generate(m)
    val geom = new Geometry(target.name, m)
    if (!texture.flags.inLightmap) UserDataFlags.setSkipLightmap(geom)
    setUserData(geom, target)
    setMaterial(geom, texture)
    addControl(geom, target)
    geom
  }

  def createQuad(
      target: Target,
      line: Line,
      texture: SurfaceTexture,
      heightBottomLeft: Float,
      heightBottomRight: Float,
      heightTopLeft: Float,
      heightTopRight: Float,
      stretchMode: StretchMode
  ): Geometry = {
    val m = new Mesh
    m.setBuffer(
      Type.Position,
      3,
      BufferUtils.createFloatBuffer(
        // change coordinate system
        CoordinateSystem.toJme(
          new Vector3f(line.b.x, line.b.y, heightTopRight)
        ),
        CoordinateSystem.toJme(new Vector3f(line.a.x, line.a.y, heightTopLeft)),
        CoordinateSystem.toJme(
          new Vector3f(line.b.x, line.b.y, heightBottomRight)
        ),
        CoordinateSystem.toJme(
          new Vector3f(line.a.x, line.a.y, heightBottomLeft)
        )
      )
    )

    // uv coordinates for texture
    val (uy1, uy2, uy3, uy4) = stretchMode match {
      case Normal =>
        (heightTopRight, heightTopLeft, heightBottomRight, heightBottomLeft)
      case FollowFloor =>
        (
          heightTopRight - heightBottomRight,
          heightTopLeft - heightBottomLeft,
          0f,
          0f
        )
      case FollowCeiling =>
        (
          0f,
          0f,
          -heightTopRight + heightBottomRight,
          -heightTopLeft + heightBottomLeft
        )
    }
    m.setBuffer(
      Type.TexCoord,
      2,
      BufferUtils.createFloatBuffer(
        new Vector2f(texture.uvx(line.length), texture.uvy(uy1)).mult(density),
        new Vector2f(texture.uvx(0), texture.uvy(uy2)).mult(density),
        new Vector2f(texture.uvx(line.length), texture.uvy(uy3)).mult(density),
        new Vector2f(texture.uvx(0), texture.uvy(uy4)).mult(density)
      )
    )

    val normal =
      CoordinateSystem
        .toJme(
          new Vector3f(line.b.x - line.a.x, line.b.y - line.a.y, 0f)
            .cross(Vector3f.UNIT_Z)
        )
        .normalize

    m.setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(2, 0, 1, 1, 3, 2))
    m.setBuffer(
      Type.Normal,
      3,
      BufferUtils.createFloatBuffer(normal, normal, normal, normal)
    )

    if (texture.flags.inLightmap) {
      // uv coordinates for light map
      m.setBuffer(
        Type.TexCoord2,
        2,
        BufferUtils.createFloatBuffer(
          new Vector2f(1, 1),
          new Vector2f(0, 1),
          new Vector2f(1, 0),
          new Vector2f(0, 0)
        )
      )
    }

    m.updateBound()
    TangentBinormalGenerator.generate(m)
    val geom = new Geometry(target.name, m)
    if (!texture.flags.inLightmap) UserDataFlags.setSkipLightmap(geom)
    setUserData(geom, target)
    setMaterial(geom, texture)
    addControl(geom, target)
    geom
  }

  def createSurface(
      target: Target,
      triangles: List[Triangle],
      center: Point,
      surface: Surface,
      faceUp: Boolean
  ) = {
    def normal =
      if (faceUp) surface.normal(Vector3f.UNIT_Y)
      else surface.normal(Vector3f.UNIT_Y).negate

    val mesh = SurfaceMeshFactory.createMesh(
      triangles,
      center,
      surface,
      faceUp,
      normal,
      true,
      density
    )
    val geom = new Geometry(target.name, mesh)
    if (!surface.texture.flags.inLightmap) UserDataFlags.setSkipLightmap(geom)
    setUserData(geom, target)
    setMaterial(geom, surface.texture)
    addControl(geom, target)
    geom
  }

}
class LightmapTextureMesh3dFactory(
    assetManager: AssetManager,
    delegate: Mesh3dFactory,
    lightmap: Texture,
    customMaterial: Boolean
) extends Mesh3dFactory {

  override def createAll = {
    val node = delegate.createAll
    // also add the ligthmap
    node.depthFirst(
      _.ifGeometry(g =>
        if (!UserDataFlags.isSkipLighmap(g))
          Option(g.getMaterial).foreach(mat => {
            // if want to use the custom shader create a new material
            if (customMaterial) {
              val material =
                new Material(assetManager, "shaders/LightingCustom.j3md")
              g.getMaterial.getParamsMap.asScala.foreach({
                case (k, v) => {
                  if (material.getMaterialDef.getMaterialParam(k) != null) {
                    material.setParam(k, v.getVarType, v.getValue)
                  } else {
                    println(s"Custom shader. Skipping material param $k")
                  }
                }
              })
              g.setMaterial(material)
            }
            // apply lightmap to material
            val material = g.getMaterial
            material.setKey(null) // unreference original .j3m
            material.setTexture("LightMap", lightmap)
            material.setBoolean("SeparateTexCoord", true)
            material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha)
          })
      )
    )
    node
  }
}

class LightmapUVMappingMesh3dFactory(delegate: Mesh3dFactory)
    extends Mesh3dFactory {
  override def createAll = {
    val node = delegate.createAll
    val uvBuilder = new LightmapUVBuilder
    uvBuilder.collectGeometries(node)
    uvBuilder.applyUVs
    node
  }
}

class GeometryOnlyMesh3dFactory() extends Mesh3dFactory {}

class WithMaterialMesh3dFactory(
    val assetManager: AssetManager,
    preview: Boolean,
    override val density: Float
) extends Mesh3dFactory
    with MaterialFactoryClient {

  val materialRepository = Repositories.materialRepository
  def defaultTexture(index: Long) = texture(materialRepository.get(index))

  override def setMaterial(geom: Geometry, texture: SurfaceTexture): Unit = {
    geom.setMaterial(defaultTexture(texture.index))
  }
  override def addControl(geom: Geometry, target: Target): Unit = {
    if (preview) geom.addControl(new HighlightControl(target))
  }
  override def setUserData(geom: Geometry, target: Target): Unit = {
    geom.setUserData("target", SavableTarget(target))
  }
}

class AddLightsMesh3dFactory(delegate: Mesh3dFactory, lights: Set[Light])
    extends Mesh3dFactory {

  def named(name: String, light: com.jme3.light.Light) = {
    println(s"Adding light $name")
    light.setName(name)
    light
  }

  override def createAll = {
    val node = delegate.createAll
    lights
      .filterNot(_.baked)
      .map({
        case AmbientLight(name, col, _) =>
          named(name, new com.jme3.light.AmbientLight(col.toColorRGBA))
        case DirectionalLight(name, col, pos, dir) =>
          named(
            name,
            new com.jme3.light.DirectionalLight(
              CoordinateSystem.toJme(dir.toVector3f),
              col.toColorRGBA
            )
          )
        case PointLight(name, _, col, pos) =>
          named(
            name,
            new com.jme3.light.PointLight(
              CoordinateSystem.toJme(pos.toVector3f),
              col.toColorRGBA
            )
          )
        case SpotLight(name, _, col, pos, dir) =>
          named(
            name,
            new com.jme3.light.SpotLight(
              CoordinateSystem.toJme(pos.toVector3f),
              CoordinateSystem.toJme(dir.toVector3f),
              col.toColorRGBA
            )
          )
      })
      .foreach(node.addLight)
    node
  }
}

class BatchedMesh3dFactory(delegate: Mesh3dFactory) extends Mesh3dFactory {
  override def createAll = {
    val base = delegate.createAll
    val detached = processDetached(base)
    GeometryBatchFactory.optimize(base)
    val node = new Node("map")
    node.attachChild(base)
    node.attachChild(detached)
    // clean-up empty nodes after batching
    node.depthFirst(
      _.ifNode(n => if (n.getChildren.isEmpty) n.removeFromParent)
    )
    printSpatial("", node)
    node
  }

  // debug
  def printSpatial(indent: String, spatial: Spatial): Unit = spatial match {
    case geom: Geometry => println(s"${indent}- Geom: ${geom.getName}")
    case node: Node => {
      println(s"${indent}- Node: ${node.getName}")
      node.getChildren.asScala.foreach(child =>
        printSpatial(indent + "  ", child)
      )
    }
    case _ =>
  }

  // it will detach the nodes with "batchId" property from the base node
  private def processDetached(node: Node) = {
    val detached = new Node("detached")
    def getOrCreate(id: String): Node = {
      if (detached.getChild(id) == null) detached.attachChild(new Node(id))
      detached.getChild(id).asNode
    }
    node.depthFirst(spatial =>
      Option(spatial.getUserData[String]("batchId"))
        .foreach(batchId => getOrCreate(batchId).attachChild(spatial))
    )
    for (batch <- detached.getChildren.asScala) {
      GeometryBatchFactory.optimize(batch.asNode)
    }
    detached
  }
}

class AddDecalsMesh3dFactory(
    val assetManager: AssetManager,
    delegate: Mesh3dFactory,
    decals: Set[Decal]
) extends Mesh3dFactory
    with MaterialFactoryClient {

  val materialRepository = Repositories.materialRepository

  override def createAll = {
    val node = new Node("decals")
    val base = delegate.createAll
    decals.foreach(decal => {
      val jmepos = CoordinateSystem.toJme(decal.position.toVector3f)
      val jmedir = CoordinateSystem.toJme(decal.normal.toVector3f)
      val angle = -FastMath.DEG_TO_RAD * decal.rotation
      val jmerot = new Quaternion()
        .lookAt(jmedir, Vector3f.UNIT_Y)
        .mult(new Quaternion().fromAngleAxis(angle, Vector3f.UNIT_Z));
      val projector = new DecalProjector(
        base,
        jmepos,
        jmerot,
        decal.size.toVector3f,
        decal.separation
      )
      projector.setUvOffset(decal.texture.texOffsetX, decal.texture.texOffsetY)
      projector.setUvScale(decal.texture.texScaleX, decal.texture.texScaleY)
      val inLightmap = decal.texture.flags.inLightmap
      projector.setCopyTextCoord2(inLightmap)
      val geometry = projector.project()
      var material = texture(materialRepository.get(decal.texture.index))
      material.setKey(null)
      geometry.setMaterial(material)
      if (!inLightmap) {
        UserDataFlags.setSkipLightmap(geometry)
      }
      if (decal.batchId != "base") {
        geometry.setUserData("batchId", decal.batchId)
      }
      node.attachChild(geometry)
    })
    val root =
      if (base.isNode) base
      else {
        val newRoot = new Node("map")
        newRoot.attachChild(base)
        newRoot
      }
    root.attachChild(node)
    root
  }
}
