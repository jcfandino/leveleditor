package com.stovokor.editor.factory

import scala.util.Random
import com.jme3.math.ColorRGBA

object ColorPaletteGenerator {

  def generateAnalogousPalette(
      numColors: Int,
      baseHue: Float,
      saturation: Float = 0.7f,
      brightness: Float = 0.5f
  ): List[ColorRGBA] = {
    List.tabulate(numColors)(i => {
      val hue = (baseHue + (i * 0.05f)) % 1.0f // Slight shift in hue
      hslToRgb(hue, saturation, brightness)
    })
  }

  private def hslToRgb(h: Float, s: Float, l: Float): ColorRGBA = {
    if (s == 0) {
      new ColorRGBA(l, l, l, 1f)
    } else {
      val q = if (l < 0.5) l * (1 + s) else l + s - l * s
      val p = 2 * l - q
      new ColorRGBA(
        hueToRgb(p, q, h + 1.0f / 3.0f),
        hueToRgb(p, q, h),
        hueToRgb(p, q, h - 1.0f / 3.0f),
        1f
      )
    }
  }

  private def hueToRgb(p: Float, q: Float, t: Float): Float = {
    val t1 = if (t < 0) t + 1 else if (t > 1) t - 1 else t
    if (t1 < 1.0 / 6.0) p + (q - p) * 6 * t1
    else if (t1 < 1.0 / 2.0) q
    else if (t1 < 2.0 / 3.0) p + (q - p) * (2.0f / 3.0f - t1) * 6
    else p
  }
}
