package com.stovokor.editor.factory

import com.jme3.asset.AssetManager
import com.jme3.material.Material
import com.jme3.material.MaterialDef
import com.jme3.material.RenderState.BlendMode
import com.jme3.math.ColorRGBA
import com.jme3.texture.Texture
import com.jme3.texture.Texture.WrapMode
import com.stovokor.editor.model.MatDefMaterial
import com.stovokor.editor.model.NullMaterial
import com.stovokor.editor.model.SimpleMaterial
import com.stovokor.editor.model.SurfaceMaterial
import com.stovokor.editor.model.MissingMaterial
import com.jme3.asset.AssetNotFoundException
import com.jme3.asset.AssetLoadException

trait MaterialFactoryClient {

  implicit def assetManager: AssetManager

  def plainColor(color: ColorRGBA) = MaterialFactory().plainColor(color)

  def texture(material: SurfaceMaterial) = MaterialFactory().texture(material)
  def texture(material: SurfaceMaterial, definition: String) =
    MaterialFactory().texture(material, definition)
}

object MaterialFactory {
  private var instance = new MaterialFactory

  def setInstance(mf: MaterialFactory): Unit = { instance = mf }

  def apply() = instance
}

class MaterialFactory {

  def plainColor(
      color: ColorRGBA
  )(implicit assetManager: AssetManager): Material = {
    val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
    mat.setColor("Color", color)
    mat
  }

  def texture(
      material: SurfaceMaterial,
      definition: String = "Common/MatDefs/Light/Lighting.j3md"
  )(implicit assetManager: AssetManager) = {
    def shaded(path: String) = {
      val tex = assetManager.loadTexture(path)
      tex.setWrap(WrapMode.Repeat)
      val mat = new Material(assetManager, definition)
      mat.setTexture("DiffuseMap", tex)
      mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha)
      mat
    }

    def unshaded(path: String) = {
      val tex = assetManager.loadTexture(path)
      tex.setWrap(WrapMode.Repeat)
      //      tex.setMagFilter(Texture.MagFilter.Nearest)
      //      tex.setMinFilter(Texture.MinFilter.NearestNoMipMaps)
      //      tex.setAnisotropicFilter(0)
      val mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
      mat.setTexture("ColorMap", tex)
      mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha)
      mat
    }
    material match {
      case NullMaterial         => unshaded("Textures/Debug3.png")
      case MissingMaterial      => unshaded("Textures/Missing.png")
      case SimpleMaterial(path) => shaded(path)
      case MatDefMaterial(path) => {
        try {
          val mat = assetManager.loadMaterial(path)
          makeTextureRepeatable(mat)
          mat
        } catch {
          case e: AssetNotFoundException => unshaded("Textures/Missing.png")
          case e: AssetLoadException     => unshaded("Textures/Debug2.png")
        }
      }
    }
  }

  // TODO this is a guess, should be configurable
  def repeatableTextures = List(
    // For Lightning
    "DiffuseMap",
    "NormalMap",
    "SpecularMap",
    "ParallaxMap",
    "AlphaMap",
    "ColorRamp",
    "GlowMap",
    "LightMap",
    // For Unshaded
    "ColorMap"
  )

  def makeTextureRepeatable(mat: Material) = {
    repeatableTextures.foreach(tex => {
      if (mat.getTextureParam(tex) != null) {
        mat.getTextureParam(tex).getTextureValue().setWrap(WrapMode.Repeat)
      }
    })
  }
}
