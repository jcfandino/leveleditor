package com.stovokor.editor.export

import com.simsilica.lemur.Container
import java.nio.file.Path

trait MapDataExporter {

  /* TODO refactor idea:
   * Create a ModelView class that contains name, Container and a ViewModel (also new).
   * Return that from implementation.
   * Pass ViewModel to export().
   */
  def exportPanel: Container
  def exportData(baseDir: String, mapName: String): Unit
  def name: String
}
