package com.stovokor.editor.export

import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.JmeExtensions.SpatialExtensions
import java.nio.file.Path
import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import com.jme3.export.binary.BinaryExporter
import com.jme3.scene.Node
import java.io.File
import com.stovokor.editor.factory.Mesh3dFactory
import com.jme3.asset.AssetManager
import jme3tools.optimize.GeometryBatchFactory
import com.stovokor.editor.lighting.LightRepository
import com.stovokor.editor.lighting.AmbientLight
import com.stovokor.editor.lighting.PointLight
import com.stovokor.editor.lighting.DirectionalLight
import com.stovokor.editor.lighting.SpotLight
import com.simsilica.lemur.Checkbox
import com.stovokor.util.CoordinateSystem
import com.simsilica.lemur.CheckboxModel
import com.simsilica.lemur.DefaultCheckboxModel
import com.jme3.texture.Texture
import com.jme3.asset.plugins.FileLocator
import com.jme3.asset.AssetLocator
import com.jme3.asset.AssetKey
import com.jme3.asset.AssetInfo
import java.io.InputStream
import java.io.ByteArrayInputStream
import com.jme3.asset.AssetNotFoundException
import java.nio.file.Files
import java.nio.file.Paths
import com.stovokor.editor.model.repository.MaterialRepository
import com.stovokor.editor.decal.DecalRepository
import com.stovokor.editor.lighting.LightmapExporter

class J3oExporter(assetManager: AssetManager) extends MapDataExporter {

  val name = "jMonkeyEngine3 (*.j3o)"
  var addLightsViewModel: CheckboxModel = new DefaultCheckboxModel(false)
  var applyLightmapViewModel: CheckboxModel = new DefaultCheckboxModel(true)
  var useCustomMaterialWithLightmap: CheckboxModel = new DefaultCheckboxModel(
    false
  )

  def exportPanel = {
    val panel = new Container()
    panel.addChild(new Label(".j3o options"))
    panel.addChild(
      new Checkbox("Add dynamic lights to root node", addLightsViewModel)
    )
    panel.addChild(
      new Checkbox("Apply lightmap (texture and UV)", applyLightmapViewModel)
    )
    panel.addChild(
      new Checkbox(
        "Use custom material with lightmap (shaders/LightingCustom.j3md)",
        useCustomMaterialWithLightmap
      )
    )
    panel
  }

  def exportData(baseDir: String, mapName: String): Unit = {
    val file = new File(s"$baseDir/$mapName.j3o")
    println(s"Exporting .j3o to ${file.getAbsolutePath}")
    val exporter = BinaryExporter.getInstance();
    val root = buildNode(baseDir, mapName)
    exporter.save(root, file);
  }

  def buildNode(baseDir: String, mapName: String) = {
    def getFactory() = {
      // TODO refactor into "pluggable enhancers"
      // init the base factory
      val density = MaterialRepository().density
      var factory: Mesh3dFactory = Mesh3dFactory(assetManager, false, density)

      // enhance with lightmap (1st step only applies UV coords)
      if (applyLightmapViewModel.isChecked) {
        factory = Mesh3dFactory.lightmapUv(factory)
      }

      // enhance with decals
      factory =
        Mesh3dFactory.withDecals(assetManager, factory, DecalRepository.all)

      // enhance with lightmap (2nd step set up material)
      if (applyLightmapViewModel.isChecked) {
        val customMaterial = useCustomMaterialWithLightmap.isChecked
        factory = Mesh3dFactory.textureLightmap(
          factory,
          lightmap(baseDir, mapName),
          assetManager,
          customMaterial
        )
      }

      // enhance with dynamic lights
      if (addLightsViewModel.isChecked) {
        factory = Mesh3dFactory.withLights(factory, LightRepository.all)
      }

      // TODO add a checkbox for this
      factory = Mesh3dFactory.batched(factory)
      factory
    }
    val root = getFactory().createAll
    assetManager.unregisterLocator(
      baseDir,
      classOf[FileLocator]
    ) // TODO move into factory
    root.setName(mapName)
    root
  }

  def lightmap(baseDir: String, mapName: String): Texture = {
    val filePath = s"$baseDir/${lightmapFile(mapName)}"
    val file = new File(filePath)
    if (!file.exists) {
      // create a dummy file as placeholder
      val in = classOf[J3oExporter].getResourceAsStream("/Textures/Debug1.png")
      Files.copy(in, Paths.get(file.getPath))
    }
    try {
      assetManager.registerLocator(baseDir, classOf[FileLocator])
      assetManager.loadTexture(lightmapFile(mapName))
    } finally {
      assetManager.unregisterLocator(baseDir, classOf[FileLocator])
    }
  }

  def lightmapFile(mapName: String) = LightmapExporter.fileName(mapName)

}
