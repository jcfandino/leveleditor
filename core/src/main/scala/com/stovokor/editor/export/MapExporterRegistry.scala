package com.stovokor.editor.export

object MapExporterRegistry {

  var exporters: List[MapDataExporter] = Nil

  def register(exporter: MapDataExporter): Unit = {
    exporters = exporters :+ exporter
  }

}
