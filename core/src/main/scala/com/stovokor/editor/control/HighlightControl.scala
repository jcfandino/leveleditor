package com.stovokor.editor.control

import com.jme3.math.ColorRGBA
import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.Geometry
import com.jme3.scene.control.AbstractControl
import com.stovokor.editor.gui.Palette
import com.stovokor.editor.model.Point
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.PointerTargetChange
import com.jme3.math.FastMath
import com.jme3.material.Material
import com.stovokor.editor.model.Target

class HighlightControl(target: Target)
    extends AbstractControl
    with EditorEventListener {

  var selected = false
  var initialized = false

  def controlUpdate(tpf: Float): Unit = {
    if (!initialized) {
      initialized = false
      EventBus.subscribeByType(this, classOf[PointerTargetChange])
    }
    doIfGeometry(geo => {
      val material = geo.getMaterial
      def hasParam(param: String) =
        material.getMaterialDef.getMaterialParam(param) != null
      if (hasParam("Color")) {
        material.setColor("Color", getColor(selected))
      } else if (
        hasParam("UseMaterialColors") && hasParam("Diffuse") && hasParam(
          "Ambient"
        )
      ) {
        material.setBoolean("UseMaterialColors", true)
        material.setColor("Diffuse", getColor(selected))
        material.setColor("Ambient", getColor(selected))
      }
      // It will not highlight other material types.
    })
    t += tpf
  }

  def setSelected(s: Boolean): Unit = {
    selected = s
  }
  var t = 0f
  def getColor(select: Boolean) = if (select) {
    val color = Palette.hoveredSurfaceMin.clone
    color.interpolateLocal(
      Palette.hoveredSurfaceMax,
      .5f * (0.5f + FastMath.sin(5 * t).abs)
    )
  } else ColorRGBA.White

  def doIfGeometry(action: Geometry => Unit): Unit = {
    if (spatial.isInstanceOf[Geometry]) {
      action.apply(spatial.asInstanceOf[Geometry])
    }
  }

  def controlRender(rm: RenderManager, vp: ViewPort): Unit = {}

  def onEvent(event: EditorEvent) = event match {
    case PointerTargetChange(pointTarget) => setSelected(pointTarget == target)
    case _                                =>
  }
}
