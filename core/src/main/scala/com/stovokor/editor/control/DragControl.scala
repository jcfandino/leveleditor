package com.stovokor.editor.control

import com.jme3.math.Vector2f
import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.control.AbstractControl
import com.stovokor.editor.gui.Mode2DLayers
import com.stovokor.editor.model.Point
import com.stovokor.util.EventBus
import com.stovokor.util.GridSnapper
import com.stovokor.util.JmeExtensions.SpatialExtensions
import com.stovokor.util.JmeExtensions.Vector2fExtensions
import com.stovokor.util.JmeExtensions.Vector3fExtensions
import com.stovokor.util.LemurExtensions.SpatialExtension
import com.stovokor.util.PointClicked
import com.stovokor.util.PointDragged
import com.stovokor.editor.model.Line
import com.stovokor.util.LineDragged
import com.stovokor.util.LineClicked
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.editor.input.Modes.SelectionMode
import com.stovokor.editor.model.Sector
import com.stovokor.util.SectorDragged
import com.stovokor.util.SectorClicked

abstract class DragControl extends AbstractControl {

  var isDragging = false
  var draggingStarted = false
  var oldPos = new Vector2f()
  var newPos = new Vector2f()
  var initialized = false

  override def controlUpdate(tpf: Float) = {
    if (!initialized) {
      setup()
      EventBus.subscribeByType(
        SelectionModeHolder,
        classOf[SelectionModeSwitch]
      )
      initialized = true
    }
    spatial.setVisible(visibleModes.contains(SelectionModeHolder.current))
  }

  def setup(): Unit = {
    val z = spatial.getLocalTranslation.z
    val minDist = GridSnapper.gridStep * 0.75f
    // click
    spatial.onCursorClick((event, target, capture) => {
      if (event.getButtonIndex == 0) {
        if (!isDragging) {
          // set initial state
          oldPos.set(currentPos)
          newPos.set(currentPos)
        } else if (!event.isPressed) {
          // released
          if (isActive && oldPos.distance(currentPos) > minDist) { // button released
            spatial.setLocalTranslation(oldPos.to3f(z)) // move back.
            dragged(newPos.subtract(oldPos))
          } else {
            clicked
          }
        }
        isDragging = event.isPressed
        // cancel drag by right clicking
      } else if (event.getButtonIndex == 1 && event.isPressed) {
        // println("right click")
        newPos.set(oldPos)
        spatial.setLocalTranslation(newPos.to3f(z))
        isDragging = false
        draggingStarted = false
        // clicked
      }
      // println(s"- click button: ${event.getButtonIndex} - ${event.isPressed}")
    })
    // move
    spatial.onCursorMove((event, target, capture) => {
      if (isDragging && isActive) {
        val cam = event.getViewPort.getCamera
        val coord = cam.getWorldCoordinates(event.getLocation, 0f)
        if (oldPos.distance(coord.to2f()) > minDist) { // avoid accidental drag
          draggingStarted = true
        }
        if (draggingStarted) {
          newPos.set(GridSnapper.snapX(coord.x), GridSnapper.snapY(coord.y))
          spatial.setLocalTranslation(newPos.to3f(z))
        }
      }
    })
  }

  def currentPos = spatial.getLocalTranslation.to2f()
  def isActive = dragActiveModes.contains(SelectionModeHolder.current)

  // abstract methods
  def dragged(movement: Vector2f): Unit
  def clicked: Unit
  def visibleModes: Set[SelectionMode]
  def dragActiveModes: Set[SelectionMode]

  override def controlRender(rm: RenderManager, vp: ViewPort) = {}

}

class PointDragControl(point: Point) extends DragControl {

  override def dragged(movement: Vector2f): Unit = {
    EventBus.trigger(
      PointDragged(point, GridSnapper.snap(point.move(movement.x, movement.y)))
    )
  }

  override def clicked: Unit = {
    EventBus.trigger(PointClicked(point))
  }

  val visibleModes = Set(SelectionMode.Point, SelectionMode.None)
  val dragActiveModes = Set(SelectionMode.Point)
}

class LineDragControl(line: Line) extends DragControl {

  override def dragged(movement: Vector2f): Unit = {
    EventBus.trigger(LineDragged(line, movement.x, movement.y))
  }

  override def clicked: Unit = {
    EventBus.trigger(LineClicked(line))
  }

  val visibleModes = Set(SelectionMode.Line)
  val dragActiveModes = Set(SelectionMode.Line)
}

class SectorDragControl(sectorId: Long) extends DragControl {

  override def dragged(movement: Vector2f): Unit = {
    EventBus.trigger(SectorDragged(sectorId, movement.x, movement.y))
  }

  override def clicked: Unit = {
    EventBus.trigger(SectorClicked(sectorId))
  }

  val visibleModes = Set(SelectionMode.Sector)
  val dragActiveModes = Set(SelectionMode.Sector)
}
