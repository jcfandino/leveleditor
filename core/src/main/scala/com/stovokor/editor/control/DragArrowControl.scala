package com.stovokor.editor.control

import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.control.AbstractControl
import com.jme3.math.Vector3f

import com.stovokor.util.JmeExtensions.SpatialExtensions
import com.stovokor.util.JmeExtensions.Vector2fExtensions
import com.stovokor.util.JmeExtensions.Vector3fExtensions
import com.stovokor.util.LemurExtensions.SpatialExtension
import com.jme3.math.Vector2f
import com.stovokor.util.GridSnapper
import com.jme3.scene.Spatial

class DragArrowControl(
    axis: Vector3f,
    dragTarget: Spatial,
    callback: Vector3f => Unit,
    snap: Boolean = true
) extends AbstractControl {

  val speed = 0.02f
  val threshold = 0.05f

  var isDragging = false
  var origpos = new Vector3f()
  var oldcur = new Vector2f()
  var newcur = new Vector2f()
  var initialized = false

  def controlUpdate(tpf: Float): Unit = {
    if (!initialized) {
      setup()
      initialized = true
    }
  }

  def setup(): Unit = {
    spatial.onCursorClick((event, target, capture) => {
      if (event.getButtonIndex == 0) {
        if (!isDragging) {
          // start dragging
          oldcur.set(event.getLocation)
          newcur.set(event.getLocation)
          origpos.set(currentPos)
        } else if (!event.isPressed()) {
          reset()
          dropped()
        }
        isDragging = event.isPressed()
      }
    })
    spatial.onCursorMove((event, target, capture) => {
      if (isDragging) {
        // move
        newcur.set(event.getLocation)
        val newpos = origpos.add(movement())
        // we are in jme coordinates, invert y and z
        val adjusted =
          if (snap)
            new Vector3f(
              GridSnapper.snapX(newpos.x),
              newpos.y,
              GridSnapper.snapY(newpos.z)
            )
          else newpos
        dragTarget.setLocalTranslation(adjusted)
      }
    })
  }

  def currentPos = dragTarget.getLocalTranslation // .to2f
  def controlRender(rm: RenderManager, vp: ViewPort): Unit = {}

  def movement() = {
    val dis = speed * (newcur.x - oldcur.x + newcur.y - oldcur.y)
    if (dis > threshold || dis < -threshold) axis.mult(dis)
    else Vector3f.ZERO
  }

  def reset(): Unit = {
    dragTarget.setLocalTranslation(origpos) // move back.
  }

  def dropped(): Unit = {
    callback(movement())
  }

}
