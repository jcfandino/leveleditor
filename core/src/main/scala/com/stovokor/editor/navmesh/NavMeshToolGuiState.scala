package com.stovokor.editor.navmesh

import com.stovokor.editor.state.BaseState
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.editor.state.ExtensionToolView
import com.stovokor.editor.state.ExtensionPanelState
import com.simsilica.lemur.Container
import com.stovokor.editor.gui.GuiFactory
import com.jme3.scene.Node
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.factory.Mesh3dFactory
import com.stovokor.util.JmeExtensions.SpatialExtensions
import com.jme3.scene.Geometry
import com.stovokor.editor.factory.MaterialFactoryClient
import com.jme3.math.ColorRGBA
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.stovokor.util.EventBus
import com.simsilica.lemur.Label
import com.simsilica.lemur.TextField
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.jme3.math.Vector3f
import com.simsilica.lemur.GuiGlobals
import com.stovokor.editor.gui.Palette
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.Action
import com.simsilica.lemur.component.IconComponent
import com.simsilica.lemur.Button
import com.stovokor.editor.state.OptionPanelAccess
import com.stovokor.editor.model.repository.DataExtension
import org.json4s.DefaultFormats
import org.json4s.TypeHints
import org.json4s.ShortTypeHints
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JObject
import org.json4s.Extraction
import com.stovokor.editor.model.repository.ExtensionRepository
import com.jme3.material.RenderState.BlendMode
import com.jme3.material.RenderState.FaceCullMode
import com.jme3.renderer.queue.RenderQueue.Bucket
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.stovokor.editor.nemesismate.lemur.ViewportPanel2D
import com.simsilica.lemur.style.ElementId
import com.stovokor.editor.gui.ScrollPanelMouseListener
import com.stovokor.editor.export.MapExporterRegistry

class NavMeshToolGuiState
    extends BaseState
    with ExtensionToolView
    with MaterialFactoryClient
    with OptionPanelAccess {

  var panel: Container = null
  var propertiesPanel: Container = null
  var mutators: Mutators = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    panel = createPanel()
    mutators =
      new Mutators(propertiesPanel, NavMeshPropertiesRepository.initGenerator)
    stateManager
      .getState(classOf[ExtensionPanelState])
      .registerExtensionPanel("NavMesh", panel, this)
    ExtensionRepository().register("navmesh", NavMeshDataExtension)
    MapExporterRegistry.register(new NavMeshExporter())
  }

  def createPanel() = {
    val container = new Container
    val buttons = container.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    // help
    val help =
      buttons.addChild(GuiFactory.button(icon = "help.png", description = ""))
    help.onClick(() => showHelp())
    // clear
    val clear = buttons.addChild(
      GuiFactory.button(icon = "edit-clear-3.png", description = "")
    )
    clear.onClick(() => clearView())
    // generate
    val preview = buttons.addChild(
      GuiFactory.button(icon = "dialog-apply.png", description = "")
    )
    preview.onClick(() => recalculatePreview(mutators))
    buttons.addChild(new Container)

    propertiesPanel = container.addChild(new Container)
    val filling = container.addChild(new Container)
    filling.setPreferredSize(new Vector3f(220, 50, 0))
    container
  }

  def populatePropertiesPanel(): Unit = {
    propertiesPanel.detachAllChildren

    mutators.paramField(
      "Cell size",
      _.getCellSize,
      (gen, text) => gen.setCellSize(text.toFloat)
    )
    mutators.paramField(
      "Cell height",
      _.getCellHeight,
      (gen, text) => gen.setCellHeight(text.toFloat)
    )
    mutators.paramField(
      "Min traversable height",
      _.getMinTraversableHeight,
      (gen, text) => gen.setMinTraversableHeight(text.toFloat)
    )
    mutators.paramField(
      "Max traversable step",
      _.getMaxTraversableStep,
      (gen, text) => gen.setMaxTraversableStep(text.toFloat)
    )
    mutators.paramField(
      "Max traversable slope",
      _.getMaxTraversableSlope,
      (gen, text) => gen.setMaxTraversableSlope(text.toFloat)
    )
    mutators.paramField(
      "Clip ledges",
      _.isClipLedges(),
      (gen, text) => gen.setClipLedges(text.toBoolean)
    )
    mutators.paramField(
      "Traversable area border size",
      _.getTraversableAreaBorderSize,
      (gen, text) => gen.setTraversableAreaBorderSize(text.toFloat)
    )
    mutators.paramField(
      "Smoothing threshold",
      _.getSmoothingThreshold,
      (gen, text) => gen.setSmoothingThreshold(text.toInt)
    )
    mutators.paramField(
      "Use conservative expansion",
      _.isUseConservativeExpansion,
      (gen, text) => gen.setUseConservativeExpansion(text.toBoolean)
    )
    mutators.paramField(
      "Min unconnected region size",
      _.getMinUnconnectedRegionSize,
      (gen, text) => gen.setMinUnconnectedRegionSize(text.toInt)
    )
    mutators.paramField(
      "Merge region size",
      _.getMergeRegionSize,
      (gen, text) => gen.setMergeRegionSize(text.toInt)
    )
    mutators.paramField(
      "Max edge length",
      _.getMaxEdgeLength,
      (gen, text) => gen.setMaxEdgeLength(text.toFloat)
    )
    mutators.paramField(
      "Edge max deviation",
      _.getEdgeMaxDeviation,
      (gen, text) => gen.setEdgeMaxDeviation(text.toFloat)
    )
    mutators.paramField(
      "Max verts per poly",
      _.getMaxVertsPerPoly,
      (gen, text) => gen.setMaxVertsPerPoly(text.toInt)
    )
    mutators.paramField(
      "Contour sample dist",
      _.getContourSampleDistance,
      (gen, text) => gen.setContourSampleDistance(text.toFloat)
    )
    mutators.paramField(
      "Contour max deviation",
      _.getContourMaxDeviation,
      (gen, text) => gen.setContourMaxDeviation(text.toFloat)
    )
    mutators.paramField(
      "Timeout",
      _.getTimeout,
      (gen, text) => gen.setTimeout(text.toInt)
    )
  }

  def get3DNavNode = getOrCreateNode(get3DNode, "navmesh")
  def get2DNavNode = getOrCreateNode(get2DNode, "navmesh")

  def setOpen(open: Boolean): Unit = {
    if (!open) {
      clearView()
    } else {
      mutators =
        new Mutators(propertiesPanel, NavMeshPropertiesRepository.initGenerator)
      populatePropertiesPanel()
    }
  }

  def showHelp(): Unit = {
    val action = GuiFactory.action("x", "Dismiss")
    val helpText = io.Source
      .fromInputStream(getClass.getResourceAsStream("/navmesh-help.txt"))
      .mkString
    val helpPanel = new Container(new SpringGridLayout(Axis.Y, Axis.X))
    val label = helpPanel.addChild(new Label(helpText))
    label.setColor(Palette.helpForeground)
    label.setBackground(new QuadBackgroundComponent(Palette.helpBackground))
    val window =
      new OptionPanel("NavMesh Help", "Field nomenclature", null, action)
    // to scroll
    val viewportPanel = new ViewportPanel2D(
      stateManager,
      new ElementId(Container.ELEMENT_ID),
      null
    )
    viewportPanel.attachScene(helpPanel)
    viewportPanel.addMouseListener(
      new ScrollPanelMouseListener(viewportPanel, helpPanel)
    )
    val mainPanel = new Container(
      new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.Last)
    )
    val (width, height) = (cam.getWidth, cam.getHeight)
    mainPanel.setPreferredSize(
      new Vector3f((width - 100).toFloat, (height - 150).toFloat, 0)
    )
    mainPanel.addChild(viewportPanel)
    window.getContainer.addChild(mainPanel)
    showModal(window)
  }

  def clearView(): Unit = {
    get2DNavNode.detachAllChildren()
    get3DNavNode.detachAllChildren()
  }

  def recalculatePreview(mutators: Mutators): Unit = {
    clearView()
    try {
      val gen = mutators.mutate
      NavMeshPropertiesRepository.update(gen)
      val mesh = generate(gen)
      // 3d
      val geom3d = new Geometry("navmesh", mesh)
      val mat = plainColor(ColorRGBA.Green)
      mat.getAdditionalRenderState.setBlendMode(BlendMode.Alpha)
      mat.getAdditionalRenderState.setFaceCullMode(FaceCullMode.Off)
      geom3d.setMaterial(mat)
      geom3d.setQueueBucket(Bucket.Transparent)
      geom3d.move(0, 0.01f, 0)
      get3DNavNode.attachChild(geom3d)
    } catch {
      case e: Exception => println(s"Failed to generate mesh: $e")
    }
  }

  def generate(generator: NavMeshGenerator) = {
    val root = Mesh3dFactory().createAll
    new NavMeshFactory().create(generator, root, Set())
  }

  class Mutators(container: Container, generator: NavMeshGenerator) {

    var mutators: List[(TextField, (NavMeshGenerator, String) => Unit)] = List()

    def paramField[T](
        name: String,
        getter: NavMeshGenerator => T,
        setter: (NavMeshGenerator, String) => Unit
    ) = {
      val param = container.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
        )
      )
      param.addChild(new Label(name))
      val field = param.addChild(new TextField(s"${getter(generator)}"))
      field.setColor(new ColorRGBA(.9f, .9f, .9f, 1f))
      field.setBackground(
        new QuadBackgroundComponent(new ColorRGBA(.2f, .2f, .2f, .9f))
      )
      field.setPreferredWidth(50)
      add(field, setter)
      field
    }

    def add(
        field: TextField,
        setter: (NavMeshGenerator, String) => Unit
    ): Unit = {
      mutators ++= List((field, setter))
    }

    def mutate = {
      mutators.foreach({ case (field, setter) =>
        setter(generator, field.getText)
      })
      generator
    }
  }
}

object NavMeshPropertiesRepository {
  var properties: Map[String, String] = Map()
  def clear(): Unit = { properties = Map() }
  def put(k: String, v: String): Unit = {
    properties = properties.updated(k, v)
  }

  def update(generator: NavMeshGenerator): Unit = {
    put("cellSize", generator.getCellSize.toString)
    put("cellHeight", generator.getCellHeight.toString)
    put("minTraversableHeight", generator.getMinTraversableHeight.toString)
    put("maxTraversableStep", generator.getMaxTraversableStep.toString)
    put("maxTraversableSlopw", generator.getMaxTraversableSlope.toString)
    put("clipLedges", generator.isClipLedges.toString)
    put(
      "traversableAreaBorderSize",
      generator.getTraversableAreaBorderSize.toString
    )
    put("smoothingThreshold", generator.getSmoothingThreshold.toString)
    put(
      "useConservativeExpansion",
      generator.isUseConservativeExpansion.toString
    )
    put(
      "minUnconnectedRegionSize",
      generator.getMinUnconnectedRegionSize.toString
    )
    put("mergeRegionSize", generator.getMergeRegionSize.toString)
    put("maxEdgeLength", generator.getMaxEdgeLength.toString)
    put("edgeMaxDeviation", generator.getEdgeMaxDeviation.toString)
    put("maxVertsPerPoly", generator.getMaxVertsPerPoly.toString)
    put("contourSampleDistance", generator.getContourSampleDistance.toString)
    put("contourMaxDeviation", generator.getContourMaxDeviation.toString)
    put("timeout", generator.getTimeout.toString)
  }

  def initGenerator = {
    val generator = new NavMeshGenerator()
    generator.setCellSize(properties.getOrElse("cellSize", "0.25").toFloat)
    generator.setCellHeight(properties.getOrElse("cellHeight", "0.125").toFloat)
    generator.setMinTraversableHeight(
      properties.getOrElse("minTraversableHeight", "2").toFloat
    )
    generator.setMaxTraversableStep(
      properties.getOrElse("maxTraversableStep", "0.3").toFloat
    )
    generator.setMaxTraversableSlope(
      properties.getOrElse("maxTraversableSlopw", "50.0").toFloat
    )
    generator.setClipLedges(
      properties.getOrElse("clipLedges", "false").toBoolean
    )
    generator.setTraversableAreaBorderSize(
      properties.getOrElse("traversableAreaBorderSize", "0.4").toFloat
    )
    generator.setSmoothingThreshold(
      properties.getOrElse("smoothingThreshold", "1").toInt
    )
    generator.setUseConservativeExpansion(
      properties.getOrElse("useConservativeExpansion", "true").toBoolean
    )
    generator.setMinUnconnectedRegionSize(
      properties.getOrElse("minUnconnectedRegionSize", "8").toInt
    )
    generator.setMergeRegionSize(
      properties.getOrElse("mergeRegionSize", "20").toInt
    )
    generator.setMaxEdgeLength(
      properties.getOrElse("maxEdgeLength", "4.0").toFloat
    )
    generator.setEdgeMaxDeviation(
      properties.getOrElse("edgeMaxDeviation", "1.3").toFloat
    )
    generator.setMaxVertsPerPoly(
      properties.getOrElse("maxVertsPerPoly", "6").toInt
    )
    generator.setContourSampleDistance(
      properties.getOrElse("contourSampleDistance", "10.0").toFloat
    )
    generator.setContourMaxDeviation(
      properties.getOrElse("contourMaxDeviation", "5.0").toFloat
    )
    generator.setTimeout(properties.getOrElse("timeout", "60000").toInt)
    generator
  }
}

object NavMeshDataExtension extends DataExtension {

  implicit val formats: org.json4s.DefaultFormats = new DefaultFormats {}

  def fetch = {
    Extraction.decompose(NavMeshPropertiesRepository.properties)
  }

  def load(data: JValue): Unit = {
    data match {
      case data: JObject => {
        NavMeshPropertiesRepository.clear()
        data
          .extract[Map[String, String]]
          .foreach({ case (k, v) => NavMeshPropertiesRepository.put(k, v) })
      }
      case _ =>
    }
  }

  def clear: Unit = {
    NavMeshPropertiesRepository.clear()
  }
}
