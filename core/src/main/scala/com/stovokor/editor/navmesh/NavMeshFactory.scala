package com.stovokor.editor.navmesh

import scala.jdk.CollectionConverters._

import com.jme3.scene.Geometry
import com.jme3.scene.Mesh
import com.jme3.scene.Node
import com.jme3.scene.SceneGraphVisitor
import com.jme3.scene.Spatial

import jme3tools.optimize.GeometryBatchFactory

class NavMeshFactory {

  def create(generator: NavMeshGenerator, level: Node, props: Set[Spatial]) = {
    val mesh = new Mesh()

    val baseMesh = generateBaseMesh(level, props)
    val geometries = findGeometries(baseMesh)
    GeometryBatchFactory.mergeGeometries(geometries.asJava, mesh)
    val optiMesh = generator.optimize(mesh)
    val validMesh = Option(optiMesh).orElse(Some(mesh)).get
    validMesh
  }

  def findGeometries(node: Node): Seq[Geometry] = {
    val lists =
      for (spatial <- node.getChildren.asScala)
        yield
          if (spatial.isInstanceOf[Geometry]) {
            List(spatial.asInstanceOf[Geometry])
          } else if (spatial.isInstanceOf[Node]) {
            findGeometries(spatial.asInstanceOf[Node])
          } else {
            List()
          }
    lists.flatten.toSeq
  }

  def generateBaseMesh(level: Node, props: Set[Spatial]) = {
    val baseNode = new Node("navMap")

    level.depthFirstTraversal(new SceneGraphVisitor() {
      def visit(spatial: Spatial) = {
        baseNode.attachChild(spatial.clone)
      }
    })
    for (p <- props) baseNode.attachChild(p)
    baseNode
  }

}
