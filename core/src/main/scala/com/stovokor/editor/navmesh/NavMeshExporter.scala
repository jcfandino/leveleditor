package com.stovokor.editor.navmesh

import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import java.io.File
import com.jme3.export.binary.BinaryExporter
import com.jme3.scene.Node
import com.stovokor.editor.navmesh.NavMeshPropertiesRepository
import com.stovokor.editor.navmesh.NavMeshGenerator
import com.stovokor.editor.factory.Mesh3dFactory
import com.stovokor.editor.navmesh.NavMeshFactory
import com.stovokor.editor.export.MapDataExporter

class NavMeshExporter extends MapDataExporter {
  def name = "NavMesh (*.j3o)"

  def exportPanel = {
    val panel = new Container()
    panel.addChild(new Label("Nav mesh properties:"))
    val info = NavMeshPropertiesRepository.properties
      .map({ case (k, v) => s"$k: $v" })
      .mkString("\n")
    panel.addChild(new Label(info))
    panel
  }

  def `exportData`(baseDir: String, mapName: String): Unit = {
    val file = new File(s"$baseDir/$mapName-nav.j3o")
    println(s"Exporting .j3o to ${file.getAbsolutePath}")
    val exporter = BinaryExporter.getInstance();
    val mesh = generateMesh()
    exporter.save(mesh, file)
  }

  def generateMesh() = {
    val generator = NavMeshPropertiesRepository.initGenerator
    val root = Mesh3dFactory().createAll
    new NavMeshFactory().create(generator, root, Set())
  }
}
