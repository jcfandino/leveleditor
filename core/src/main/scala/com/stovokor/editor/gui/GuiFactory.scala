package com.stovokor.editor.gui

import com.jme3.input.event.MouseMotionEvent
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.jme3.scene.Spatial
import com.simsilica.lemur.Action
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Button
import com.simsilica.lemur.Container
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.HAlignment
import com.simsilica.lemur.Label
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.TextField
import com.simsilica.lemur.component.IconComponent
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.event.DefaultMouseListener
import com.stovokor.editor.input.Modes.EditMode
import com.stovokor.editor.input.Modes.SelectionMode
import com.stovokor.editor.model.Settings
import com.stovokor.util.ChangeGridSize
import com.stovokor.util.ChangeZoom
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditSettings
import com.stovokor.util.EventBus
import com.stovokor.util.ExitApplication
import com.stovokor.util.ExportMap
import com.stovokor.util.OpenMap
import com.stovokor.util.SaveMap
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.ShowHelp
import com.stovokor.util.SplitSelection
import com.stovokor.util.StartNewMap
import com.stovokor.util.ToggleEffects
import com.stovokor.util.ToggleSnapToGrid
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.LemurExtensions._
import com.jme3.input.event.MouseButtonEvent
import com.jme3.input.KeyInput
import com.simsilica.lemur.event.KeyAction
import com.simsilica.lemur.component.TextEntryComponent
import com.simsilica.lemur.focus.FocusTraversal.TraversalDirection
import com.simsilica.lemur.text.DocumentModelFilter
import com.simsilica.lemur.text.TextFilters

object GuiFactory {

  val iconBasePath = "Interface/Icons/"

  def toolbar(width: Int, height: Int) = {
    val bar = createMainPanel(width, height)
    val infoText = createInfoText(width)
    bar.addChild(createGeneralPanel(infoText))
    bar.addChild(createSelectionPanel(infoText))
    bar.addChild(createEditPanel(infoText))
    bar.addChild(createViewPanel(infoText))
    val filling = new Container
    filling.setPreferredSize(new Vector3f(width.toFloat, 0, 0))
    filling.addChild(infoText)
    bar.addChild(filling)
    bar
  }

  def statusbar(width: Int, height: Int) = {
    val offset = 200
    val bar = new Container(
      new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
    )
    bar.setPreferredSize(new Vector3f((width - offset).toFloat, 0, 0))
    bar.setLocalTranslation(offset.toFloat, 22, 0)

    val gridText = new Label("")
    gridText.setName("gridText")
    bar.addChild(gridText)
    bar.addChild(new Label("|"))

    val sectorCount = new Label("")
    sectorCount.setName("sectorCount")
    sectorCount.setPreferredSize(new Vector3f(200, 0, 0))
    bar.addChild(sectorCount)

    val mode = new Label("")
    mode.setName("mode")
    mode.setPreferredSize(new Vector3f(100, 0, 0))
    bar.addChild(mode)

    val mousePos = new Label("")
    mousePos.setColor(Palette.coordinates)
    mousePos.setTextHAlignment(HAlignment.Right)
    mousePos.setName("positionText")
    mousePos.setPreferredSize(new Vector3f(200, 0, 0))
    bar.addChild(mousePos)

    bar
  }

  def createMainPanel(width: Int, height: Int) = {
    val toolbarPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
    val title = new Label(" M8 Editor ")
    title.setColor(Palette.title)
    toolbarPanel.addChild(title)
    toolbarPanel.setLocalTranslation(0, height.toFloat, 0)
    toolbarPanel
  }

  def createInfoText(width: Int) = {
    val infoText = new Label("")
    infoText
  }

  def createGeneralPanel(infoText: Label) = {
    val generalPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
    generalPanel.addChild(new Label("|"))
    val newMap =
      generalPanel.addChild(button("document-new-8.png", "New", infoText))
    val open =
      generalPanel.addChild(button("document-open-2.png", "Open", infoText))
    val save =
      generalPanel.addChild(button("document-save-5.png", "Save", infoText))
    val saveAs = generalPanel.addChild(
      button("document-save-as-5.png", "Save as...", infoText)
    )
    val `export` =
      generalPanel.addChild(button("lorry-go.png", "Export...", infoText))
    val settings =
      generalPanel.addChild(button("configure-5.png", "Settings...", infoText))
    val help = generalPanel.addChild(button("help.png", "Help...", infoText))
    val exit = generalPanel.addChild(
      button("application-exit-2.png", "Exit editor", infoText)
    )
    exit.onClick(() => EventBus.trigger(ExitApplication()))
    newMap.onClick(() => EventBus.trigger(StartNewMap()))
    open.onClick(() => EventBus.trigger(OpenMap()))
    save.onClick(() => EventBus.trigger(SaveMap(true)))
    saveAs.onClick(() => EventBus.trigger(SaveMap(false)))
    export.onClick(() => EventBus.trigger(ExportMap()))
    settings.onClick(() => EventBus.trigger(EditSettings()))
    help.onClick(() => EventBus.trigger(ShowHelp()))
    val mode3d = generalPanel.addChild(
      button("blockdevice.png", "Switch 2D/3D mode", infoText)
    )
    mode3d.onClick(() => EventBus.trigger(ViewModeSwitch()))
    val draw = generalPanel.addChild(
      button("draw-freehand.png", "Draw sector", infoText)
    )
    draw.setName("drawSector")
    draw.onClick(() => {
      EventBus.trigger(EditModeSwitch(EditMode.Draw))
      EventBus.trigger(SelectionModeSwitch(SelectionMode.None))
    })
    generalPanel
  }

  def createSelectionPanel(infoText: Label) = {
    val selectionPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
    selectionPanel.addChild(new Label("|"))
    val point = selectionPanel.addChild(
      button("edit-node.png", "Select points", infoText)
    )
    val line = selectionPanel.addChild(
      button("draw-line-3.png", "Select lines", infoText)
    )
    val sector = selectionPanel.addChild(
      button("office-chart-polar-stacked.png", "Select sectors", infoText)
    )
    point.setName("selectPoint")
    point.onClick(() => {
      EventBus.trigger(EditModeSwitch(EditMode.Select))
      EventBus.trigger(SelectionModeSwitch(SelectionMode.Point))
    })
    line.setName("selectLine")
    line.onClick(() => {
      EventBus.trigger(EditModeSwitch(EditMode.Select))
      EventBus.trigger(SelectionModeSwitch(SelectionMode.Line))
    })
    sector.setName("selectSector")
    sector.onClick(() => {
      EventBus.trigger(EditModeSwitch(EditMode.Select))
      EventBus.trigger(SelectionModeSwitch(SelectionMode.Sector))
    })
    selectionPanel
  }

  def createViewPanel(infoText: Label) = {
    val selectionPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
    selectionPanel.addChild(new Label("|"))
    val grid =
      selectionPanel.addChild(button("view-grid.png", "Grid size", infoText))
    val snap =
      selectionPanel.addChild(button("snap-orto.png", "Snap to grid", infoText))
    val zoomout =
      selectionPanel.addChild(button("zoom-out-3.png", "Zoom out", infoText))
    val zoomin =
      selectionPanel.addChild(button("zoom-in-3.png", "Zoom in", infoText))
    val fog = selectionPanel.addChild(
      button("weather-fog-2.png", "Enable 3D Effects", infoText)
    )
    grid.onClick(() => EventBus.trigger(ChangeGridSize(true)))
    snap.setName("snapToGrid")
    snap.onClick(() => EventBus.trigger(ToggleSnapToGrid()))
    zoomout.onClick(() => EventBus.trigger(ChangeZoom(1)))
    zoomin.onClick(() => EventBus.trigger(ChangeZoom(-1)))
    fog.onClick(() => EventBus.trigger(ToggleEffects()))
    selectionPanel
  }

  def createEditPanel(infoText: Label) = {
    val editPanel = new Container(new SpringGridLayout(Axis.X, Axis.Y))
    editPanel.addChild(new Label("|"))
    val split =
      editPanel.addChild(button("format-add-node.png", "Split line", infoText))
    split.onClick(() => EventBus.trigger(SplitSelection()))
    editPanel
  }

  def button(
      icon: String = null,
      description: String,
      infoText: Label = null,
      label: String = ""
  ): Button = {
    val button = new Button(label)
    if (icon != null) {
      val base = if (icon.contains("/")) "" else iconBasePath
      val iconComponent = new IconComponent(base + icon)
      button.setIcon(iconComponent)
    }
    if (infoText != null) {
      button.addMouseListener(new DefaultMouseListener() {
        override def mouseEntered(
            e: MouseMotionEvent,
            t: Spatial,
            s: Spatial
        ): Unit = {
          infoText.setText(description)
        }
        override def mouseExited(
            e: MouseMotionEvent,
            t: Spatial,
            s: Spatial
        ): Unit = {
          infoText.setText("")
        }
      })
    }
    button
  }

  def action(name: String, text: String): Action = {
    action(name, text, () => {})
  }

  def action(name: String, text: String, callback: () => Unit): Action = {
    commonIcon(name) match {
      case Some(icon) =>
        new Action(text, icon) { def execute(b: Button) = callback() }
      case None => new Action(text) { def execute(b: Button) = callback() }
    }
  }

  def action(
      label: String,
      icon: String,
      action: Button => Unit,
      enabled: Boolean = true
  ): Action = {
    new Action(label, new IconComponent(iconBasePath + icon), enabled) {
      def execute(b: Button) = action(b)
    }
  }

  def createIcon(name: String) = new IconComponent(iconBasePath + name)

  def actionButton(dir: String, action: () => Unit): Button = {
    actionButton(dir, "", action)
  }

  def actionButton(dir: String, label: String, action: () => Unit): Button = {
    val button = new Button(label)
    commonIcon(dir) match {
      case Some(icon) => button.setIcon(icon)
      case None       => button.setText(dir)
    }
    button.onClick(action)
    button
  }

  private def commonIcon(dir: String): Option[IconComponent] = dir match {
    case "<"  => Some(createIcon("arrow-left-3.png"))
    case ">"  => Some(createIcon("arrow-right-3.png"))
    case "v"  => Some(createIcon("arrow-down-3.png"))
    case "^"  => Some(createIcon("arrow-up-3.png"))
    case ">>" => Some(createIcon("arrow-right-double-3.png"))
    case "<<" => Some(createIcon("arrow-left-double-3.png"))
    case "^^" => Some(createIcon("arrow-up-double-3.png"))
    case "vv" => Some(createIcon("arrow-down-double-3.png"))
    case "<>" => Some(createIcon("object-flip-horizontal.png"))
    case "+"  => Some(createIcon("edit-add-3.png"))
    case "-"  => Some(createIcon("edit-remove-3.png"))
    case "x"  => Some(createIcon("dialog-cancel-3.png"))
    case "ok" => Some(createIcon("dialog-apply.png"))
    case "r"  => Some(createIcon("view-refresh-3.png"))
    case "cr" => Some(createIcon("edit-clear-3.png"))
    case _    => None
  }

  def numberField(n: Int, allowNegative: Boolean): TextField =
    numberField(n.toFloat, allowNegative)(0)

  def numberField(n: Float, allowNegative: Boolean = true)(implicit
      decimals: Int
  ): TextField = {
    val format = s"%.${decimals}f"
    val formatted = format.format(n)
    val model = new NumberDocumentModel
    model.setText(format.format(n))
    model.setInputTransform(
      TextFilters.charFilter(c =>
        c >= '0' && c <= '9' ||
          c == '.' && !model.getText.contains('.') && decimals > 0 ||
          c == '-' && !model.getText.contains(
            '-'
          ) && model.getCarat == 0 && allowNegative
      )
    )
    val field = new TextField(model)
    field.setColor(Palette.fieldForeground)
    field.setBackground(new QuadBackgroundComponent(Palette.fieldBackground))
    field.setPreferredWidth(45)
    field.addMouseListener(new DefaultMouseListener() {
      var lastClick = 0L
      override def click(e: MouseButtonEvent, t: Spatial, c: Spatial): Unit = {
        val time = System.currentTimeMillis
        if (e.getButtonIndex == 0 && !e.isPressed && time - lastClick < 250) {
          field.setText("")
        }
        lastClick = time
      }
    })
    field
      .getActionMap()
      .put(new KeyAction(KeyInput.KEY_ESCAPE), TextEntryComponent.FOCUS_NEXT)
    field
  }

  def textField(s: String) = {
    val field = new TextField(s)
    field.setColor(Palette.fieldForeground)
    field.setBackground(new QuadBackgroundComponent(Palette.fieldBackground))
    field
  }

  class NumberDocumentModel extends DocumentModelFilter {
    // do not filter when seting text in bulk
    override def filterInput(text: String) = text
  }
}
