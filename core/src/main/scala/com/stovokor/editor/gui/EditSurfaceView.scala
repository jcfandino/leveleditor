package com.stovokor.editor.gui

import com.stovokor.editor.model.Target
import com.stovokor.editor.model.SurfaceTexture
import com.simsilica.lemur.Container
import com.stovokor.editor.model.repository.MaterialRepository
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.jme3.math.ColorRGBA
import com.stovokor.editor.model.Selection
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Label
import com.simsilica.lemur.Button
import com.simsilica.lemur.HAlignment
import com.jme3.math.Vector3f
import com.stovokor.editor.model.SurfaceMaterialThumbnail
import com.simsilica.lemur.GuiGlobals
import com.stovokor.util.EventBus
import com.stovokor.util.ChangeMaterial
import com.stovokor.util.LemurExtensions.ButtonExtension
import com.jme3.asset.AssetManager
import com.simsilica.lemur.Checkbox

trait SurfaceMutator[T <: Target] {
  def mutate(target: T, mutation: SurfaceTexture => SurfaceTexture): Unit
}

object EditSurfaceView {
  var factor = 1f // save in a global var so redrawing doesn't reset it
}

class EditSurfaceView[T <: Target](
    panel: Container,
    target: T,
    mutator: SurfaceMutator[T]
)(implicit assetManager: AssetManager) {

  val materialRepository = MaterialRepository()

  def redrawPanel(): Unit = {
    val selection = Selection(target)
    val texture = selection.texture
    def textureButton(
        dir: String,
        mutation: SurfaceTexture => SurfaceTexture
    ) = {
      GuiFactory.actionButton(dir, () => mutator.mutate(target, mutation))
    }
    def format(n: Float) = "% .3f".format(n)
    def factor = EditSurfaceView.factor

    // material
    val material = materialRepository.get(texture.index)
    val name = material.name
    panel.addChild(
      new Label(if (name.length > 33) "..." + name.takeRight(30) else name)
    )
    val materialPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    materialPanel.addChild(new Container)
    val changeMaterial = panel.addChild(new Button(""))
    changeMaterial.setTextHAlignment(HAlignment.Center)
    changeMaterial.setPreferredSize(new Vector3f(90, 90, 0))
    changeMaterial.setMaxWidth(90)
    val preview = SurfaceMaterialThumbnail.load(material)
    val image =
      GuiGlobals.getInstance().loadTexture(preview.image, false, false)
    changeMaterial.setBackground(new QuadBackgroundComponent(image))
    changeMaterial.onClick(() => EventBus.trigger(ChangeMaterial(target)))
    materialPanel.addChild(new Container)

    // offset
    val offsetTitle = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    offsetTitle.addChild(new Label("Offset"))
    offsetTitle.addChild(textureButton("cr", _.resetOffset))

    val offsetPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val offsetStep = 0.1f
    offsetPanel.addChild(
      new Label(
        s"X:${format(texture.texOffsetX)}/Y:${format(texture.texOffsetY)}"
      )
    )
    offsetPanel.addChild(textureButton("<", _.move(factor * offsetStep, 0f)))
    offsetPanel.addChild(textureButton("v", _.move(0f, factor * offsetStep)))
    offsetPanel.addChild(textureButton("^", _.move(0f, factor * -offsetStep)))
    offsetPanel.addChild(textureButton(">", _.move(factor * -offsetStep, 0f)))

    // scale
    val scaleTitle = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    scaleTitle.addChild(new Label("Scale"))
    scaleTitle.addChild(textureButton("cr", _.resetScale))
    val scalePanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val scaleStep = 0.1f
    scalePanel.addChild(
      new Label(
        s"X:${format(texture.texScaleX)}/Y:${format(texture.texScaleY)}"
      )
    )
    scalePanel.addChild(textureButton("<", _.scale(factor * -scaleStep, 0f)))
    scalePanel.addChild(textureButton("v", _.scale(0f, factor * -scaleStep)))
    scalePanel.addChild(textureButton("^", _.scale(0f, factor * scaleStep)))
    scalePanel.addChild(textureButton(">", _.scale(factor * scaleStep, 0f)))

    // fine modifiers
    val finePanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val fineCheckbox = finePanel.addChild(new Label("Increment level"))
    val fineCheckbox1 = finePanel.addChild(new Checkbox(""))
    fineCheckbox1.setChecked(factor == 1f)
    val fineCheckbox2 = finePanel.addChild(new Checkbox(""))
    fineCheckbox2.setChecked(factor == 0.1f)
    val fineCheckbox3 = finePanel.addChild(new Checkbox(""))
    fineCheckbox3.setChecked(factor == 0.01f)
    fineCheckbox1.onClick(() => {
      EditSurfaceView.factor = 1f
      fineCheckbox1.setChecked(true)
      fineCheckbox2.setChecked(false)
      fineCheckbox3.setChecked(false)
    })
    fineCheckbox2.onClick(() => {
      EditSurfaceView.factor = 0.1f
      fineCheckbox1.setChecked(false)
      fineCheckbox2.setChecked(true)
      fineCheckbox3.setChecked(false)
    })
    fineCheckbox3.onClick(() => {
      EditSurfaceView.factor = 0.01f
      fineCheckbox1.setChecked(false)
      fineCheckbox2.setChecked(false)
      fineCheckbox3.setChecked(true)
    })
    // flags
    val flagsPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val lightmapCheckbox = flagsPanel.addChild(new Checkbox("In lightmap"))
    lightmapCheckbox.setChecked(texture.flags.inLightmap)
    lightmapCheckbox.onClick(() =>
      mutator.mutate(
        target,
        s => s.updateFlags(s.flags.updateInLightmap(lightmapCheckbox.isChecked))
      )
    )
  }

}
