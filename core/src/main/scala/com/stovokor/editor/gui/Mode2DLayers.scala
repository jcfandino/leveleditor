package com.stovokor.editor.gui

object Mode2DLayers {
  val grid = 0f
  val axis = 1f
  val origin = 2f
  val pickPlane = 86f
  val lines = 88f
  val vertices = 89f
  val drawing = 90f
  val camera = 100f
}