package com.stovokor.editor.gui

import com.jme3.scene.shape.Box
import com.jme3.scene.shape.Quad
import com.jme3.scene.Mesh
import com.jme3.scene.VertexBuffer.Type
import com.stovokor.util.CenteredQuad

object K {

  val vertexBox = CenteredQuad(0.15f)
  val lineBox = CenteredQuad(0.2f, 0.6f)
  val sectorBox = CenteredQuad(0.2f)

  val zPortalArea = -10f
  val zPortalLine = 10f
}
