package com.stovokor.editor.gui

import com.simsilica.lemur.event.DefaultMouseListener
import com.simsilica.lemur.Panel
import com.jme3.input.event.MouseMotionEvent
import com.jme3.scene.Spatial

class ScrollPanelMouseListener(
    outerPanel: Panel,
    innerPanel: Panel,
    speed: Float = -1f / 60
) extends DefaultMouseListener {

  override def mouseMoved(
      event: MouseMotionEvent,
      target: Spatial,
      capture: Spatial
  ): Unit = {
    event.setConsumed()
    val delta = event.getDeltaWheel
    if (delta != 0) {
      val max = (innerPanel.getSize.y - outerPanel.getSize.y).max(0)
      val pos =
        (innerPanel.getLocalTranslation.y + delta * speed).max(0).min(max)
      innerPanel.setLocalTranslation(0, pos, 0)
    }
  }
}
