package com.stovokor.editor.algo

import com.jme3.math.Vector2f
import scala.collection.mutable.ListBuffer
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.Polygon
import com.stovokor.editor.model.Line
import com.jme3.math.FastMath

case class GraphFaces(
    // currently only one in the set, it can be changed back to simplify.
    outer: Set[Polygon],
    inner: Set[Polygon]
) {
  lazy val innerLines = inner.flatMap(_.edges).filterNot(outerLines.contains)
  lazy val outerLines = outer.flatMap(_.edges).toSet
}

/** Implements the paper: "Finding faces in a planar embedding of a graph"<br>
  * by MOSAIC group, see: https://mosaic.mpi-cbg.de/docs/Schneider2015.pdf
  */
object FindFaces {
  def apply(lines: List[Line]) = {
    val all = lines.flatMap(_.andReverse)
    val adjacents = all
      .map(l => (l.a, l.b))
      .foldLeft(Map[Point, Set[Point]]().withDefaultValue(Nil))({
        case (map, (a, b)) => map.updated(a, Set(b) ++ map(a))
      })
    val graph = new Graph(
      adjacents.map({ case (k, v) => new Vertex(k, v.toList) }).toList
    )
    graph.findFaces()
  }

  private class Graph(val vertices: List[Vertex]) {

    lazy val byPoint = vertices.map(v => (v.point, v)).toMap

    def findFaces(): GraphFaces = {
      val faces = ListBuffer[List[Point]]()
      // for each vertex
      vertices.foreach(vert => {
        // get the adjacents (except one)
        vert.adjacent
          .drop(1)
          .foreach(pcurr => {
            var vprev = vert
            var vcurr = byPoint(pcurr)
            val face = ListBuffer[Point](pcurr)
            var continue = true
            var clockwise: Option[Boolean] = None // assume clockwise
            while (continue) {
              val pnext =
                vcurr.next(vprev.point, clockwise.orElse(Some(true)).get)
              // fix clockwise after the first iteration
              clockwise =
                clockwise.orElse(Some(vcurr.isClockwise(vprev.point, pnext)))
              vprev = vcurr
              vcurr = byPoint(pnext)
              if (pnext != face(0)) {
                face += pnext
              } else {
                continue = false
              }
            }
            faces += face.toList
          })
      })
      val polygons = faces.map(Polygon(_)).toSet
      if (polygons.size > 1) {
        // Remove the outer face if found:
        // Filter internal polygons using the orientation of the lines
        // if a line a walked in both directions is internal,
        // otherwise (one direction of twice the same) must be external.
        val outerLines = polygons
          .flatMap(_.edges)
          .foldLeft(Map[Line, Int]())((map, line) =>
            if (map.contains(line)) map.updated(line, map(line) + 1)
            else if (map.contains(line.reverse))
              map.updated(line.reverse, map(line.reverse) - 1)
            else map.updated(line, 1)
          )
          .filter({ case (line, count) => count != 0 })
          .keys
          .toSet
        val outerFace = {
          var ps = ListBuffer[Point](outerLines.head.b)
          while (ps.size < 3 || ps.head != ps.last) {
            outerLines
              .find({ case Line(a, _) => a == ps.last })
              .foreach({ case Line(_, b) => ps append b })
          }
          ps.remove(ps.size - 1)
          Polygon(ps.toList)
        }
        // if a polygon has all external lines is the outer face (remove it)
        val innerFaces = polygons.filterNot(_.edges.forall(outerLines.contains))
        GraphFaces(Set(outerFace), innerFaces)
      } else GraphFaces(polygons, polygons)
    }
  }

  private class Vertex(val point: Point, val adjacent: List[Point]) {

    def next(prev: Point, clockwise: Boolean) = {
      val others = adjacent.filterNot(_ == prev)
      val res =
        if (clockwise) others.sortBy(o => angle(prev, o)).head
        else others.sortBy(o => -angle(prev, o)).head
      res
    }

    def angle(prev: Point, next: Point) = {
      val va = new Vector2f(point.x - prev.x, point.y - prev.y)
      val vb = new Vector2f(next.x - point.x, next.y - point.y)
      var a = va.normalize.angleBetween(vb.normalize)
      if (a <= -FastMath.PI) a = a + FastMath.TWO_PI
      else if (a >= FastMath.PI) a = a - FastMath.TWO_PI
      a
    }

    def isClockwise(prev: Point, next: Point) = {
      val va = new Vector2f(point.x - prev.x, point.y - prev.y)
      val vc = new Vector2f(next.x - prev.x, next.y - prev.y)
      va.determinant(vc) <= 0 // same as (va cross vc).z
    }
  }

}
