package com.stovokor.editor.decal

import scala.jdk.CollectionConverters._
import scala.util.Try

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.simsilica.lemur.Axis
import com.simsilica.lemur.Button
import com.simsilica.lemur.Checkbox
import com.simsilica.lemur.Container
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Label
import com.simsilica.lemur.ListBox
import com.simsilica.lemur.TextField
import com.simsilica.lemur.component.QuadBackgroundComponent
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.core.VersionedList
import com.simsilica.lemur.event.DefaultMouseListener
import com.stovokor.editor.export.MapExporterRegistry
import com.stovokor.editor.gui.GuiFactory
import com.stovokor.editor.input.Modes.EditMode
import com.stovokor.editor.input.Modes.SelectionMode
import com.stovokor.editor.model.repository.ExtensionRepository
import com.stovokor.editor.state.BaseState
import com.stovokor.editor.state.ExtensionPanelState
import com.stovokor.editor.state.ExtensionToolView
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.OpenMap
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.StartNewMap
import com.stovokor.util.LemurExtensions._
import com.stovokor.editor.model.Target.DecalTarget

import com.stovokor.editor.state.OptionPanelAccess
import com.simsilica.lemur.OptionPanelState
import com.simsilica.lemur.OptionPanel
import com.simsilica.lemur.core.VersionedHolder
import com.simsilica.lemur.ColorChooser
import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.MaterialRepository
import com.simsilica.lemur.HAlignment
import com.stovokor.editor.model.SurfaceMaterialThumbnail
import com.simsilica.lemur.GuiGlobals
import com.stovokor.util.ChangeMaterial
import com.jme3.asset.AssetManager
import com.stovokor.util.MaterialChosen
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.stovokor.editor.gui.EditSurfaceView
import com.stovokor.editor.gui.SurfaceMutator

class DecalToolGuiState
    extends BaseState
    with EditorEventListener
    with ExtensionToolView
    with OptionPanelAccess {

  var panel: Container = null
  var view: DecalToolView = null

  val width = 220
  def windowWidth = app.getCamera.getWidth
  def windowHeight = app.getCamera.getHeight

  def repository = DecalRepository

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    // register file extension
    ExtensionRepository().register("decals", DecalDataExtension)
    clear()

    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])
    EventBus.subscribeByType(this, classOf[DecalAdded])
    EventBus.subscribeByType(this, classOf[DecalUpdated])
    EventBus.subscribeByType(this, classOf[DecalDeleted])
    EventBus.subscribeByType(this, classOf[MaterialChosen])

    panel = createPanel()
    stateManager
      .getState(classOf[ExtensionPanelState])
      .registerExtensionPanel("Decals", panel, this)
  }

  def onEvent(event: EditorEvent) = event match {
    case StartNewMap()    => clear()
    case OpenMap()        => clear()
    case DecalDeleted(_)  => view.clear()
    case DecalAdded(id)   => view.present(id, repository.get(id))
    case DecalUpdated(id) => view.present(id, repository.get(id))
    case MaterialChosen(DecalTarget(id), idx) => {
      val decal = DecalRepository.get(id)
      update(id, decal.updateTexture(decal.texture.updateIndex(idx)))
    }
    case _ =>
  }

  def clear(): Unit = {
    Option(stateManager.getState(classOf[DecalTool3DViewState]))
      .foreach(_.cancel())
    Option(view).foreach(_.clear())
  }

  override def setOpen(open: Boolean): Unit = {
    if (open) {
      stateManager.attach(new DecalTool3DViewState(view))
    } else {
      clearAddTool()
      clear()
    }
  }

  private def clearAddTool(): Unit = {
    Option(stateManager.getState(classOf[DecalToolAddState]))
      .foreach(_.cancel())
  }

  def createPanel() = {
    // container for all elements
    val content = new Container
    content.addMouseListener(new DefaultMouseListener) // ignore clicks

    // buttons panel
    val buttons = content.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    val add = buttons.addChild(
      GuiFactory.button(
        icon = "edit-add-3.png",
        description = "",
        label = "Add"
      )
    )
    add.onClick(() => addNewDecal(Decal.initial))

    buttons.addChild(new Container)

    // decal view
    val viewPanel =
      content.addChild(new Container(new SpringGridLayout(Axis.Y, Axis.X)))
    view = new DecalToolView(viewPanel, this)

    // filling
    val filling = content.addChild(new Container)
    filling.setPreferredSize(new Vector3f(width.toFloat, 50, 0))
    content
  }

  def addNewDecal(prototype: Decal): Unit = {
    clearAddTool()
    stateManager.attach(new DecalToolAddState(prototype))
  }

  def update(id: Long, decal: Decal): Unit = {
    DecalRepository.update(id, decal)
    EventBus.trigger(DecalUpdated(id))
  }
}

class DecalToolView(panel: Container, controller: DecalToolGuiState)(implicit
    assetManager: AssetManager
) {

  val materialRepository = MaterialRepository()

  implicit val decimals: Int = 3 // for number fields
  clear()

  val width = 216

  type Selection = (Long, Decal)
  var current: Selection = null

  private def toFloat(text: TextField) =
    if (text.getText.isEmpty) 0f else text.getText.toFloat

  object PosMutator {
    var posx: TextField = null
    var posy: TextField = null
    var posz: TextField = null

    def valid =
      posx != null && Try(() => posx.getText.toFloat).isSuccess &&
        posy != null && Try(() => posy.getText.toFloat).isSuccess &&
        posz != null && Try(() => posz.getText.toFloat).isSuccess

    def updated = current._2
      .updatePosition(new Vector3f(toFloat(posx), toFloat(posy), toFloat(posz)))
  }

  object DirMutator {
    var anga: TextField = null
    var angb: TextField = null

    def valid =
      anga != null && Try(() => anga.getText.toFloat).isSuccess &&
        angb != null && Try(() => angb.getText.toFloat).isSuccess

    def normal = new Vector3f(
      FastMath.cos(FastMath.DEG_TO_RAD * toFloat(anga)) * FastMath.sin(
        FastMath.DEG_TO_RAD * toFloat(angb)
      ),
      FastMath.sin(FastMath.DEG_TO_RAD * toFloat(anga)) * FastMath.sin(
        FastMath.DEG_TO_RAD * toFloat(angb)
      ),
      FastMath.cos(FastMath.DEG_TO_RAD * toFloat(angb))
    )

    def updated = current._2.updateNormal(normal)
  }

  def clear(): Unit = {
    current = null
    panel.clearChildren()
    val text = panel.addChild(new Label("Select or add decal..."))
    val filling = panel.addChild(new Container)
    filling.setPreferredSize(new Vector3f(width.toFloat, 300, 0))
  }

  def present(id: Long, decal: Decal): Unit = {
    current = (id, decal)
    panel.clearChildren()
    buttonsBar(panel, id, decal)
    detailsPanel(panel, id, decal)
    val filling = panel.addChild(new Container)
    filling.setPreferredSize(new Vector3f(width.toFloat, 0, 0))
  }

  def redraw(): Unit = {
    present(current._1, current._2)
  }

  def copyCurrent(): Unit = {
    controller.addNewDecal(current._2)
  }

  private def buttonsBar(panel: Container, id: Long, decal: Decal): Unit = {
    val buttons = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.Last, FillMode.None)
      )
    )
    // delete
    val delete = buttons.addChild(
      GuiFactory.button(icon = "dialog-cancel-3.png", description = "")
    )
    delete.onClick(() => {
      DecalRepository.remove(id)
      EventBus.trigger(DecalDeleted(id))
    })
    // copy
    val copy = buttons.addChild(
      GuiFactory.button(icon = "edit-copy-3.png", description = "")
    )
    copy.onClick(copyCurrent)
    // padding
    buttons.addChild(new Container)
  }

  private def detailsPanel(panel: Container, id: Long, decal: Decal): Unit = {
    // position
    val posTitle = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    posTitle.addChild(new Label("Position", "title"))
    val position = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    position.addChild(new Label("X:"))
    PosMutator.posx =
      position.addChild(GuiFactory.numberField(decal.position.x))
    position.addChild(new Label("Y:"))
    PosMutator.posy =
      position.addChild(GuiFactory.numberField(decal.position.y))
    position.addChild(new Label("Z:"))
    PosMutator.posz =
      position.addChild(GuiFactory.numberField(decal.position.z))
    position.addChild(changeButton("ok", _ => PosMutator.updated))

    // direction (cartesian => spherical coords)
    val angleA =
      FastMath.RAD_TO_DEG * FastMath.atan2(decal.normal.y, decal.normal.x)
    val angleB = FastMath.RAD_TO_DEG * FastMath.acos(decal.normal.z)
    val direction = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    direction.addChild(new Label("Direction", "title"))
    direction.addChild(new Label("A:"))
    DirMutator.anga =
      direction.addChild(GuiFactory.numberField(angleA, true)(1))
    direction.addChild(new Label("B:"))
    DirMutator.angb =
      direction.addChild(GuiFactory.numberField(angleB, true)(1))
    direction.addChild(changeButton("ok", _ => DirMutator.updated))

    // angle
    val anglePanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val angleStep = 5f
    anglePanel.addChild(new Label(s"Rotation", "title"))
    anglePanel.addChild(new Label(format(decal.rotation)))
    anglePanel.addChild(changeButton("-", _.changeRotation(-angleStep)))
    anglePanel.addChild(changeButton("+", _.changeRotation(angleStep)))

    // size
    val sizeTitle = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    sizeTitle.addChild(new Label("Size", "title"))
    val sizeStep = 0.1f
    val widthPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    widthPanel.addChild(new Label(s"Width: ${format(decal.size.x)}"))
    widthPanel.addChild(changeButton("-", _.changeSize(-sizeStep, 0, 0)))
    widthPanel.addChild(changeButton("+", _.changeSize(sizeStep, 0, 0)))
    // height
    val heightPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    heightPanel.addChild(new Label(s"Height: ${format(decal.size.y)}"))
    heightPanel.addChild(changeButton("-", _.changeSize(0, -sizeStep, 0)))
    heightPanel.addChild(changeButton("+", _.changeSize(0, sizeStep, 0)))
    // depth
    val depthPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    depthPanel.addChild(new Label(s"Depth: ${format(decal.size.z)}"))
    depthPanel.addChild(changeButton("-", _.changeSize(0, 0, -sizeStep)))
    depthPanel.addChild(changeButton("+", _.changeSize(0, 0, sizeStep)))

    // separation
    val separationPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    separationPanel.addChild(
      new Label(s"Separation: ${format(decal.separation, 6)}")
    )
    separationPanel.addChild(
      changeButton(
        "-",
        _.updateSeparation((decal.separation / 10f).max(0.000001f))
      )
    )
    separationPanel.addChild(
      changeButton("+", _.updateSeparation((decal.separation * 10f).min(1f)))
    )

    // texture
    val surfView =
      new EditSurfaceView(panel, DecalTarget(id), DecalSurfaceMutator)
    surfView.redrawPanel()

    // batch id
    val batchIdPanel = panel.addChild(
      new Container(
        new SpringGridLayout(Axis.X, Axis.Y, FillMode.First, FillMode.None)
      )
    )
    val batchIdText = batchIdPanel.addChild(new TextField(decal.batchId))
    batchIdPanel.addChild(
      GuiFactory.actionButton(
        "ok",
        () => controller.update(id, decal.updateBatchId(batchIdText.getText))
      )
    )
  }

  def changeButton(dir: String, mutation: Decal => Decal) = {
    GuiFactory.actionButton(
      dir,
      () => controller.update(current._1, mutation(current._2))
    )
  }

  def format(n: Float, d: Int = 2) = s"% .${d}f".format(n)

  object DecalSurfaceMutator extends SurfaceMutator[DecalTarget] {
    override def mutate(
        target: DecalTarget,
        mutation: SurfaceTexture => SurfaceTexture
    ): Unit = {
      controller.update(
        current._1,
        current._2.updateTexture(mutation(current._2.texture))
      )
    }
  }
}
