package com.stovokor.editor.decal

import com.stovokor.editor.state.BaseState
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.util.PointClicked
import com.stovokor.util.EventBus
import com.stovokor.util.EditorEventListener
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.EditorEvent
import com.stovokor.editor.model.Point
import com.jme3.math.Vector3f
import com.stovokor.editor.model.repository.SectorRepository
import com.jme3.math.ColorRGBA
import com.stovokor.editor.model.Sector
import com.stovokor.util.SectorTargetClicked

class DecalToolAddState(prototype: Decal)
    extends BaseState
    with EditorEventListener {

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    EventBus.subscribeByType(this, classOf[SectorTargetClicked])
    EventBus.subscribeByType(this, classOf[ViewModeSwitch])
  }
  def onEvent(event: EditorEvent) = event match {
    case SectorTargetClicked(0, target, Some(pos), Some(norm)) =>
      addDecal(pos, norm)
    case ViewModeSwitch() => cancel()
    case _                =>
  }

  def addDecal(position: Vector3f, normal: Vector3f): Unit = {
    println(s"-> decal $position - $normal")
    val decal = prototype.updatePosition(position).updateNormal(normal)
    val id = DecalRepository.add(decal)
    EventBus.trigger(DecalAdded(id))
    cancel()
  }

  def cancel(): Unit = {
    EventBus.removeFromAll(this)
    stateManager.detach(this)
  }
}
