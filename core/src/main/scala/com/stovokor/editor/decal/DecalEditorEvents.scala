package com.stovokor.editor.decal

import com.stovokor.util.EditorEvent
import com.jme3.math.Vector3f

case class DecalAdded(id: Long) extends EditorEvent
case class DecalDeleted(id: Long) extends EditorEvent
case class DecalUpdated(id: Long) extends EditorEvent

case class DecalDragged(id: Long, movement: Vector3f) extends EditorEvent
