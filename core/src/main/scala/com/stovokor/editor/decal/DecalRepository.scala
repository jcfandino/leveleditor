package com.stovokor.editor.decal

import com.jme3.math.Vector3f
import java.util.concurrent.atomic.AtomicLong
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.repository.DataExtension
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JObject
import org.json4s.DefaultFormats
import org.json4s.ShortTypeHints
import org.json4s.TypeHints
import org.json4s.{Extraction, NoTypeHints}
import org.json4s.JsonDSL.WithDouble._
import org.json4s.jackson.Serialization
import com.jme3.math.ColorRGBA
import org.json4s.FieldSerializer
import com.stovokor.editor.model.SurfaceTexture

// make case class wrappers for serialization
object Vector {
  def apply() = new Vector(0, 0, 0)
  def apply(v: Vector3f) = new Vector(v.x, v.y, v.z)
}
case class Vector(x: Float, y: Float, z: Float) {
  def toVector3f = new Vector3f(x, y, z)
}

object Decal {
  val initial = Decal(
    Vector(),
    Vector(1, 0, 0),
    0,
    Vector(1, 1, 0.2f),
    0.0001f,
    SurfaceTexture(1, 1),
    "base"
  )
}
case class Decal(
    position: Vector,
    normal: Vector,
    rotation: Float,
    size: Vector,
    separation: Float,
    texture: SurfaceTexture,
    batchId: String
) {

  def updatePosition(v: Vector3f) =
    new Decal(Vector(v), normal, rotation, size, separation, texture, batchId)

  def updateNormal(v: Vector3f) =
    new Decal(position, Vector(v), rotation, size, separation, texture, batchId)

  def updateSize(v: Vector3f) =
    new Decal(
      position,
      normal,
      rotation,
      Vector(v),
      separation,
      texture,
      batchId
    )

  def updateRotation(n: Float) =
    new Decal(position, normal, n, size, separation, texture, batchId)

  def updateSeparation(s: Float) =
    new Decal(position, normal, rotation, size, s, texture, batchId)

  def updateTexture(t: SurfaceTexture) =
    new Decal(position, normal, rotation, size, separation, t, batchId)

  def updateBatchId(id: String) =
    new Decal(position, normal, rotation, size, separation, texture, id)

  def changePosition(change: Vector3f) =
    updatePosition(position.toVector3f.add(change))

  def changeSize(w: Float, h: Float, d: Float) =
    updateSize(new Vector3f(size.x + w, size.y + h, size.z + d))

  def changeRotation(r: Float) =
    updateRotation(rotation + r)
}

object DecalDataExtension extends DataExtension {

  implicit val formats: org.json4s.DefaultFormats = new DefaultFormats {}

  def fetch = {
    Extraction.decompose(DecalRepository.decals)
  }

  def load(data: JValue): Unit = {
    data match {
      case data: JObject => {
        DecalRepository.clear()
        data
          .extract[Map[Long, Decal]]
          .foreach({ case (id, e) => DecalRepository.add(e) })
      }
      case _ =>
    }
  }

  def clear: Unit = {
    DecalRepository.clear()
  }
}

object DecalRepository {

  val idGenerator = new AtomicLong(0)
  var decals: Map[Long, Decal] = Map()

  def all = decals.values.toSet

  def add(decal: Decal) = {
    val id = idGenerator.getAndIncrement
    decals = decals.updated(id, decal)
    id
  }

  def get(id: Long) = {
    decals(id)
  }

  def remove(id: Long) = {
    val decal = decals(id)
    decals = decals - id
    decal
  }

  def update(id: Long, decal: Decal) = {
    val old = decals(id)
    decals = decals.updated(id, decal)
    old
  }

  def clear(): Unit = {
    decals = Map()
    idGenerator.set(0)
  }
}
