package com.stovokor.editor.decal

import scala.jdk.CollectionConverters._
import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.math.ColorRGBA
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.control.AbstractControl
import com.jme3.scene.shape.Box
import com.stovokor.util.JmeExtensions._
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.state.BaseState
import com.stovokor.util.CoordinateSystem
import com.stovokor.util.EditorEvent
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EventBus
import com.stovokor.util.LemurExtensions.SpatialExtension
import com.stovokor.util.OpenMap
import com.stovokor.util.StartNewMap
import com.stovokor.util.ViewModeSwitch
import com.jme3.scene.shape.Sphere
import com.jme3.scene.shape.Line
import com.jme3.scene.shape.Cylinder
import com.stovokor.util.Arrow
import com.stovokor.editor.control.DragArrowControl
import com.jme3.scene.shape.Quad
import com.stovokor.editor.model.repository.Repositories
import scala.collection.mutable.ListBuffer
import com.stovokor.editor.control.HighlightControl
import com.stovokor.editor.model.Target
import com.stovokor.util.PointerTargetChange

class DecalTool3DViewState(val view: DecalToolView)
    extends BaseState
    with EditorEventListener
    with MaterialFactoryClient {

  def decals3dNode = getOrCreateNode(get3DNode, "decals")

  var meshFactory: Mesh3dDecalFactory = null

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    meshFactory = new Mesh3dDecalFactory(this)
    EventBus.subscribeByType(this, classOf[DecalAdded])
    EventBus.subscribeByType(this, classOf[DecalDeleted])
    EventBus.subscribeByType(this, classOf[DecalUpdated])
    EventBus.subscribeByType(this, classOf[DecalDragged])
    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])
    redrawAll()
  }

  def onEvent(event: EditorEvent) = event match {
    case DecalAdded(id)   => redraw(id)
    case DecalDeleted(id) => clear(id)
    case DecalUpdated(id) => redraw(id)
    case DecalDragged(id, movement) => {
      val decal = DecalRepository.get(id)
      val pos = decal.position.toVector3f.add(movement)
      DecalRepository.update(id, decal.updatePosition(pos))
      EventBus.trigger(new DecalUpdated(id))
    }
    case ViewModeSwitch() => // TODO
    case _                =>
  }

  def cancel(): Unit = {
    EventBus.removeFromAll(this)
    decals3dNode.detachAllChildren()
    stateManager.detach(this)
  }

  def redrawAll(): Unit = {
    for ((id, decal) <- DecalRepository.decals) redraw(id, decal)
  }

  def redraw(id: Long): Unit = {
    redraw(id, DecalRepository.get(id))
  }

  def redraw(id: Long, decal: Decal): Unit = {
    val node = getOrCreateNode(decals3dNode, s"decal-$id")
    node.detachAllChildren()

    val controlsNode = new Node("controls")
    node.attachChild(controlsNode)

    // box
    val box = meshFactory.create(decal)
    val jmedir = CoordinateSystem.toJme(decal.normal.toVector3f)
    val angle = -FastMath.DEG_TO_RAD * decal.rotation
    // combine rotations, first look at normal, then rotate around it
    var boxrot = new Quaternion()
      .lookAt(jmedir, Vector3f.UNIT_Y)
      .mult(new Quaternion().fromAngleAxis(angle, Vector3f.UNIT_Z));
    box.setLocalRotation(boxrot)
    controlsNode.attachChild(box)

    // axis
    val axis = new Node("decal-axis")
    val size = decal.size
    val rot = new Quaternion().lookAt(decal.normal.toVector3f, Vector3f.UNIT_Z)
    // point axis according to normal and rotation
    meshFactory.drawArrow(
      id,
      axis,
      controlsNode,
      size.x / -2f,
      Vector3f.UNIT_X,
      rot.mult(Vector3f.UNIT_X),
      .5f,
      ColorRGBA.Red
    )
    meshFactory.drawArrow(
      id,
      axis,
      controlsNode,
      size.y / 2f,
      Vector3f.UNIT_Z,
      rot.mult(Vector3f.UNIT_Y),
      .5f,
      ColorRGBA.Green
    )
    meshFactory.drawArrow(
      id,
      axis,
      controlsNode,
      0,
      Vector3f.UNIT_Y,
      rot.mult(Vector3f.UNIT_Z),
      .5f,
      ColorRGBA.Blue
    )
    val jmerot = new Quaternion().lookAt(jmedir, Vector3f.UNIT_Y)
    axis.move(0, 0, size.z / -2f)
    axis.setLocalRotation(jmerot)
    controlsNode.attachChild(axis)

    // test
    val test = new Geometry("test", new Box(size.x / 2f, size.y / 2f, 0.01f))
    val materialRepository = Repositories.materialRepository
    val material = texture(materialRepository.get(decal.texture.index))
    test.setMaterial(material)
    test.setLocalRotation(boxrot)
    // node.attachChild(test)

    // transform node
    val jmepos = CoordinateSystem.toJme(decal.position.toVector3f)
    controlsNode.setLocalTranslation(jmepos)

    // projection preview
    val target = new Target.DecalTarget(id)
    val geometries = new ListBuffer[Geometry]()
    get3DNode.getChildren.asScala
      .filter(_.getName.startsWith("sector"))
      .foreach(_.ifNode(_.breadthFirst(_.ifGeometry(geometries.addOne(_)))))
    val projector = new DecalProjector(
      geometries.asJava,
      jmepos,
      boxrot,
      decal.size.toVector3f,
      decal.separation
    )
    projector.setUvOffset(decal.texture.texOffsetX, decal.texture.texOffsetY)
    projector.setUvScale(decal.texture.texScaleX, decal.texture.texScaleY)
    val projection = projector.project()
    projection.setMaterial(material)
    projection.addControl(new HighlightControl(target))
    projection.onCursorMove((event, _, _) => {
      event.setConsumed()
      updateTarget(target)
    })
    projection.onCursorClick((event, spatial, capture, collision) =>
      if (event.isPressed) EventBus.trigger(DecalDragged(id, Vector3f.ZERO))
    )

    node.attachChild(projection)
  }

  def clear(id: Long): Unit = {
    val node = getOrCreateNode(decals3dNode, s"decal-$id")
    node.removeFromParent()
  }

  var lastTarget: Option[Target] = None

  def updateTarget(target: Target.DecalTarget): Unit = {
    if (lastTarget != Some(target)) {
      lastTarget = Some(target)
      EventBus.trigger(PointerTargetChange(target))
    }
  }
}

class Mesh3dDecalFactory(materialFactory: MaterialFactoryClient) {
  def create(decal: Decal): Spatial = {
    val mat = materialFactory.plainColor(ColorRGBA.LightGray)
    mat.getAdditionalRenderState.setWireframe(true)
    mat.getAdditionalRenderState.setLineWidth(2)
    val size = CoordinateSystem.toJme(decal.size.toVector3f)
    // TODO coords are flipped, dont know why
    val geo =
      new Geometry("decal", new Box(size.x / 2f, size.z / 2f, size.y / 2f))
    geo.setMaterial(mat)
    geo
  }

  // TODO reuse with light tool
  def drawArrow(
      id: Long,
      node: Node,
      target: Node,
      offset: Float,
      dir: Vector3f,
      axis: Vector3f,
      length: Float,
      color: ColorRGBA
  ): Unit = {
    val mat = materialFactory.plainColor(color)
    val arrow = new Geometry("arrow", new Arrow(length))
    val jmepos = CoordinateSystem.toJme(dir.normalize.mult(offset))
    val jmedir = CoordinateSystem.toJme(dir).normalize
    arrow.setMaterial(mat)
    arrow.setLocalTranslation(jmepos)

    val jmerot =
      new Quaternion().fromAngles(
        jmedir.z * FastMath.HALF_PI,
        0,
        jmedir.x * FastMath.HALF_PI
      )
    arrow.setLocalRotation(jmerot)
    val jmeaxis = CoordinateSystem.toJme(axis).normalize
    arrow.addControl(
      new DragArrowControl(
        jmeaxis,
        target,
        movement =>
          EventBus.trigger(
            DecalDragged(id, CoordinateSystem.toEditor(movement))
          ),
        false
      )
    )
    node.attachChild(arrow)
  }
}

// TODO
class DecalSelectionControl(callback: () => Unit) extends AbstractControl {
  var initialized = false
  override def controlUpdate(tpf: Float): Unit = {
    if (!initialized) {
      initialized = true
      getSpatial.onCursorClick((event, target, capture) => callback())
    }
  }

  def controlRender(rm: RenderManager, vp: ViewPort): Unit = {}
}
