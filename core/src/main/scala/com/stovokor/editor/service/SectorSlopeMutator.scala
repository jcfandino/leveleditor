package com.stovokor.editor.service

import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.Slope
import com.stovokor.editor.model.Target
import com.stovokor.editor.model.SectorTarget
import com.stovokor.editor.model.Sector

object SectorSlopeMutator {
  val sectorRepository = SectorRepository()

  def mutate(target: SectorTarget, mutation: Slope => Slope): Unit = {
    val sectorId = target.sectorId
    val sector = sectorRepository.get(sectorId)
    val updated = target match {
      case Target.Floor(_) =>
        sector.updatedFloor(
          sector.floor.updateSlope(mutation(sector.floor.slope))
        )
      case Target.Ceiling(_) =>
        sector.updatedCeiling(
          sector.ceiling.updateSlope(mutation(sector.ceiling.slope))
        )
      case Target.PlatformTop(_, idx) =>
        updatePlatformTopSlope(sector, idx, mutation)
      case Target.PlatformBottom(_, idx) =>
        updatePlatformBottomSlope(sector, idx, mutation)
      case _ => sector
    }
    sectorRepository.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, true))
    // neighbor sectors by need to be redrawn
    updated.openWalls
      .flatMap(w => BorderRepository().find(w.line))
      .foreach(b => {
        val sec = sectorRepository.get(b._2.sectorB)
        EventBus.trigger(SectorUpdated(b._2.sectorB, sec, true))
      })
  }

  def updatePlatformTopSlope(
      sector: Sector,
      idx: Int,
      mutation: Slope => Slope
  ) = {
    val platform = sector.platforms(idx)
    val top = platform.top
    val updated = platform.updatedTop(top.updateSlope(mutation(top.slope)))
    sector.updatedPlatform(idx, updated)
  }

  def updatePlatformBottomSlope(
      sector: Sector,
      idx: Int,
      mutation: Slope => Slope
  ) = {
    val platform = sector.platforms(idx)
    val bottom = platform.bottom
    val updated =
      platform.updatedBottom(bottom.updateSlope(mutation(bottom.slope)))
    sector.updatedPlatform(idx, updated)
  }
}
