package com.stovokor.editor.service

import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.builder.SectorBuilder
import com.stovokor.editor.model.Point
import com.jme3.math.Vector2f
import com.jme3.math.FastMath
import scala.math.Ordering
import scala.collection.mutable.ListBuffer
import com.stovokor.editor.model.Polygon
import com.stovokor.util.SectorUpdated
import com.stovokor.util.EventBus
import com.stovokor.util.SectorDeleted
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.Wall
import com.stovokor.editor.model.SurfaceTexture
import scala.util.Random
import com.stovokor.editor.factory.BorderFactory
import com.stovokor.editor.model.Line
import com.stovokor.editor.model.PolygonBuilder
import com.stovokor.editor.algo.FindFaces

object SectorDrawingService {
  def apply() = this

  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()
  val sectorService = SectorService()

  val minDistance = .1f

  var currentBuilder: Option[SectorBuilder] = None
  var lastClick = (0L, Point(0, 0))

  def cancel(): Unit = {
    currentBuilder = None
    lastClick = (0L, Point(0, 0))
  }

  // removes last point
  def revert(): Unit = {
    currentBuilder = currentBuilder.flatMap(_.removeLast)
    lastClick = (0L, Point(0, 0))
  }

  def addPoint(point: Point): Unit = {
    val doubleClick = isDoubleClick(point)
    lastClick = (System.currentTimeMillis, point)
    currentBuilder = currentBuilder
      .map(builder =>
        if (doubleClick && builder.size > 2) {
          builder.add(builder.first)
        } else if (point.distance(builder.last) > minDistance) {
          builder.add(point)
        } else {
          builder
        }
      )
      .orElse(Some(SectorBuilder.start(point)))

    for (builder <- currentBuilder if (builder.isLoop())) {
      completeDrawingInsertNew(builder.points)
      cancel()
    }
  }

  case class Drawing(points: List[Point], lines: List[Line])

  def getCurrentDrawing = currentBuilder
    .map(b => Drawing(b.points, b.lines))
    .orElse(Some(Drawing(Nil, Nil)))
    .get

  private def isDoubleClick(point: Point) =
    System.currentTimeMillis - lastClick._1 < 200 &&
      currentBuilder
        .map(b => b.last.distance(point) < minDistance)
        .orElse(Some(false))
        .get

  private def completeDrawingInsertNew(points: List[Point]): Unit = {
    // build polygon to insert
    val lines = (points zip points.tail).map({ case (a, b) => Line(a, b) })
    // Transitively discover the graph by repeating searches to border repository.
    val neighbourSectors = sectorService.findRecursivelyConnected(points.toSet)

    // if the drawing touches empty holes, add those lines to the graph
    // they will become sectors.
    val holeLines = neighbourSectors
      .flatMap({ case (_, sec) => sec.openHoles })
      .filterNot(hole => hole.vertices.forall(p => points.contains(p)))
      .flatMap(hole => hole.edges)

    // find faces
    val faces = FindFaces(
      lines ++ neighbourSectors.values.flatMap(_.polygon.edges) ++ holeLines
    )

    // merge with old stuff
    val preExistingOpenWalls = neighbourSectors.flatMap({ case (_, sec) =>
      sec.openWalls
    })

    // create a map polygon => sector
    val oldSectorByPolygon = neighbourSectors
      .map({ case (id, sec) => (sec.polygon, (id, sec)) })
      .toMap
    val oldPolygons = oldSectorByPolygon.keys.toSet
    val closedWallLines =
      // lines of the outer face that may have existed before but were not open
      faces.outerLines.filter(l => preExistingOpenWalls.forall(_.line != l))
    val sectorsWithSameShape = faces.inner
      .flatMap(p =>
        oldSectorByPolygon.find({ case (o, (i, s)) => o == p }).map(_._2)
      )
      .toMap
    def isOld(polygon: Polygon) =
      oldSectorByPolygon.find({ case (old, _) => old == polygon }).isDefined

    // update sectors that kept the same polygon
    val updatedSectors = sectorsWithSameShape.map({ case (id, sec) =>
      (
        id,
        sec.polygon.edges.foldLeft(sec)((s, l) =>
          if (faces.outerLines.contains(l)) s else s.openWall(l)
        )
      )
    })
    updatedSectors
      .foreach({ case (id, sec) => sectorRepository.update(id, sec) })

    //////////////////////////////////////
    // delete the ones not in the new set
    val sectorsToRemove = neighbourSectors.keys.toSet
      .filterNot(sectorsWithSameShape.contains)
    // collect borders for later (may have to recreate)
    val removedBorders = sectorsToRemove
      .flatMap(id =>
        borderRepository.findFrom(id) ++ borderRepository.findTo(id)
      )
      .map({ case (id, border) => (border.line, border) })
      .toMap
    // call service to delete
    sectorsToRemove.foreach(sectorService.removeSector)

    //////////////////////////////////////
    // deleted sectors might have had holes in them
    val holesToRelocate = neighbourSectors
      .filter({ case (id, _) => sectorsToRemove.contains(id) })
      .flatMap({ case (_, sec) => sec.holes })

    //////////////////////////////////////
    // add the new ones
    val newSectors =
      faces.inner
        .filterNot(isOld)
        .map(polygon => {
          val preExistingOpenWallsByLine =
            preExistingOpenWalls.map(w => (w.line, w)).toMap
          // helper functions
          def fallback() = findCuttingHole(polygon.vertices)
            .map({ case (_, sec) =>
              Sector(polygon, sec.floor, sec.ceiling, Nil)
            })
            .getOrElse(
              Sector(polygon, Surface.empty, Surface.empty.move(4), Nil)
            )
          def isInside(sec: Sector) = polygon.vertices.exists(p =>
            !sec.polygon.vertices.contains(p) && sec.polygon.inside(p)
          )
          def isBorder(sec: Sector) = sec.polygon.edges
            .find(l1 => polygon.edges.find(_.alike(l1)).isDefined)
            .isDefined

          // try to find a sector to copy from
          val initial =
            // first priority find a sector in which any point is completely inside
            neighbourSectors
              .find({ case (_, sec) => isInside(sec) })
              // second priority find a sector with a bordering line
              .orElse(neighbourSectors.find({ case (_, sec) => isBorder(sec) }))
              // copy the properties from the chosen sector if any
              .map({ case (_, prev) =>
                Sector(
                  polygon,
                  prev.floor,
                  prev.ceiling,
                  Nil,
                  prev.wallTexture(polygon.edges)
                )
              })
              // if no match use the fallback to handle case when new sector is a hole
              .getOrElse(fallback())

          def shouldOpen(l: Line) = {
            def isPreexistent = preExistingOpenWallsByLine.contains(l)
            def isOuterFace = faces.outerLines.find(_.alike(l)).isEmpty
            isPreexistent || isOuterFace
          }
          polygon.edges.foldLeft(initial)((sector, line) =>
            if (shouldOpen(line)) sector.openWall(line) else sector
          )
        })
        .map(sector => {
          holesToRelocate
            .filter(sector.polygon.inside)
            .foldLeft(sector)((s, h) => {
              s.cutHole(h, false)
              // TODO the walls textures will be lost?
            })
        })
        .map(sector => (sectorRepository.add(sector), sector))

    //////////////////////////////////////
    // discover borders
    val sectorByLine = (newSectors ++ updatedSectors)
      .flatMap({ case (id, sector) =>
        sector.polygon.edges.map(l => (l, (id, sector)))
      })
      .toMap

    // inner lines need new borders
    newSectors.foreach({
      case (id, sector) => {
        sector.polygon.edges
          .filter(faces.innerLines.contains)
          .flatMap(line => {
            val (id1, s1) = sectorByLine(line)
            // TODO no such element here
            // when drawing inside sector and touches only one point
            val (id2, s2) = sectorByLine(line.reverse)
            BorderFactory.createBorders(id1, s1, id2, s2, List(line))
          })
          // save the borders
          .foreach(borderRepository.add)
      }
    })
    // new sector holes may new new borders
    newSectors.foreach({ case (id, sector) =>
      sector.holes
        .flatMap(_.edges)
        .foreach(line => {
          sectorRepository
            .find(line)
            // only pick the sectors with the line outside (not hole)
            .filter({ case (_, sec) => sec.polygon.edges.contains(line) })
            .flatMap({ case (innerSecId, innerSec) =>
              BorderFactory
                .createBorders(innerSecId, innerSec, id, sector, List(line))
            })
            // save the borders
            .foreach(borderRepository.add)
        })
    })
    // outer lines may need borders if touch pre-existing sectors
    faces.outerLines.foreach(line => {
      removedBorders
        .get(line)
        .foreach(border => {
          val (innerSectorId, innerSector) = sectorByLine(line)
          sectorRepository
            .find(line.reverse)
            // need to look into the holes too
            .find({ case (id, sec) =>
              sec.polygon.edges.contains(line.reverse) || sec.holes
                .flatMap(_.edges)
                .contains(line)
            })
            .foreach({
              case (outerSectorId, outerSector) => {
                val borders = BorderFactory.createBorders(
                  innerSectorId,
                  innerSector,
                  outerSectorId,
                  outerSector,
                  List(line)
                )
                borders.foreach(borderRepository.add)
                EventBus
                  .trigger(SectorUpdated(outerSectorId, outerSector, true))
              }
            })
        })
    })

    ///////////////////////////////////
    // Send update events
    (newSectors ++ updatedSectors)
      .foreach({ case (id, sector) =>
        EventBus.trigger(SectorUpdated(id, sector, true))
      })

    ////////////////////// stitch outer borders after the fact
    val facePoints = faces.innerLines.flatMap(_.points).toSet
    faces.outer.foreach(face => {
      findCuttingHole(face.vertices).foreach({
        case (outerSectorId, outerSector) => {
          // clean-up holes in the graph, will be added later.
          val cleanedUpOuterSector = outerSector
            .updatedHoles(
              outerSector.holes
                .filterNot(_.vertices.exists(facePoints.contains))
            )
          // Use the outer face to reduce the number of holes in the sector.
          // The triangulator algorithm sometimes fails when too many holes.
          val newHoles = List(face).filter(cleanedUpOuterSector.polygon.inside)
          val updatedOuterSector = newHoles
            .foldLeft(cleanedUpOuterSector)((sec, face) =>
              sec.cutHole(face, false)
            )

          sectorRepository.update(outerSectorId, updatedOuterSector)

          // create the new borders
          newSectors.foreach({
            case (id, sec) => {
              val updatedNewSector =
                faces.outerLines.foldLeft(sec)((sec, line) =>
                  sec.openWall(line)
                )
              sectorRepository.update(id, updatedNewSector)
              // make the borders
              val newBorderLines = updatedNewSector.polygon.edges
                // pick the lines that are in the perimeter
                .filter(line => faces.outerLines.contains(line))
              BorderFactory
                .createBorders(
                  id,
                  updatedNewSector,
                  outerSectorId,
                  updatedOuterSector,
                  newBorderLines
                )
                .foreach(borderRepository.add)
              EventBus.trigger(SectorUpdated(id, updatedNewSector, true))
            }
          })
          EventBus.trigger(
            SectorUpdated(outerSectorId, updatedOuterSector, true)
          )
        }
      })
    })
  }

  private def findCuttingHole(points: List[Point]) = {
    val polygon = Polygon(points)
    val cuttingHole = points
      .flatMap(p => sectorRepository.findInside(p, true))
      .filter({ case (_, sec) => sec.polygon.inside(polygon) })
      .filterNot({ case (_, sec) => sec.polygon == polygon })
    if (cuttingHole.isEmpty) {
      None
    } else {
      Some(sectorService.findInnermostSector(cuttingHole))
    }
  }

}
