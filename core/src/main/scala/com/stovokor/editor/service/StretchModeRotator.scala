package com.stovokor.editor.service

import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.Wall
import com.stovokor.editor.model.{Target, SectorTarget}

object StretchModeRotator {
  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()

  def rotate(target: SectorTarget): Unit = {
    val sectorId = target.sectorId
    val sector = sectorRepository.get(sectorId)
    val updated = target match {
      case Target.Wall(_, idx) => {
        val wall = sector.closedWalls(idx)
        sector.updatedClosedWall(idx, wall.rotateStretchMode)
      }
      case Target.BorderLow(_, idx) => {
        val wall = sector.openWalls(idx)
        borderRepository
          .find(wall.line)
          .foreach({ case (id, border) =>
            borderRepository.update(
              id,
              border.updateSurfaceFloor(border.surfaceFloor.rotateStretchMode)
            )
          })
        sector
      }
      case Target.BorderHi(_, idx) => {
        val wall = sector.openWalls(idx)
        borderRepository
          .find(wall.line)
          .foreach({ case (id, border) =>
            borderRepository.update(
              id,
              border
                .updateSurfaceCeiling(border.surfaceCeiling.rotateStretchMode)
            )
          })
        sector
      }
      case Target.PlatformSide(_, pIdx, sIdx) => {
        val side = sector.platforms(pIdx).sides(sIdx)
        val updated = side.rotateStretchMode
        sector.updatedPlatform(
          pIdx,
          sector.platforms(pIdx).updatedSide(sIdx, updated)
        )
      }
      case _ => sector
    }
    sectorRepository.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, false))
  }

}
