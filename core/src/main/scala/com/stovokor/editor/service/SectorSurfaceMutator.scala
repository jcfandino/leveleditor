package com.stovokor.editor.service

import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.Target
import com.stovokor.editor.model.SectorTarget

object SectorSurfaceMutator {
  val sectorRepository = SectorRepository()
  val borderRepository = BorderRepository()

  def mutate(
      target: SectorTarget,
      mutation: SurfaceTexture => SurfaceTexture
  ): Unit = {
    val sectorId = target.sectorId
    val sector = sectorRepository.get(target.sectorId)
    val updated = target match {
      case Target.Floor(_) => {
        sector.updatedFloor(
          sector.floor.updateTexture(mutation(sector.floor.texture))
        )
      }
      case Target.Ceiling(_) => {
        sector.updatedCeiling(
          sector.ceiling.updateTexture(mutation(sector.ceiling.texture))
        )
      }
      case Target.Wall(_, idx) => {
        val wall = sector.closedWalls(idx)
        sector.updatedClosedWall(
          idx,
          wall.updateTexture(mutation(wall.texture))
        )
      }
      case Target.BorderLow(_, idx) => {
        val wall = sector.openWalls(idx)
        borderRepository
          .find(wall.line)
          .foreach({ case (id, border) =>
            borderRepository.update(
              id,
              border.updateSurfaceFloor(
                border.surfaceFloor
                  .updateTexture(mutation(border.surfaceFloor.texture))
              )
            )
          })
        sector
      }
      case Target.BorderHi(_, idx) => {
        val wall = sector.openWalls(idx)
        borderRepository
          .find(wall.line)
          .foreach({ case (id, border) =>
            borderRepository.update(
              id,
              border.updateSurfaceCeiling(
                border.surfaceCeiling
                  .updateTexture(mutation(border.surfaceCeiling.texture))
              )
            )
          })
        sector
      }
      case Target.PlatformTop(_, idx) => {
        val platform = sector.platforms(idx)
        val top = platform.top
        val updated =
          platform.updatedTop(top.updateTexture(mutation(top.texture)))
        sector.updatedPlatform(idx, updated)
      }
      case Target.PlatformBottom(_, idx) => {
        val platform = sector.platforms(idx)
        val bottom = platform.bottom
        val updated =
          platform.updatedBottom(bottom.updateTexture(mutation(bottom.texture)))
        sector.updatedPlatform(idx, updated)
      }
      case Target.PlatformSide(_, pidx, sidx) => {
        val platform = sector.platforms(pidx)
        val side = platform.sides(sidx)
        val updated =
          platform.updatedSide(sidx, side.updateTexture(mutation(side.texture)))
        sector.updatedPlatform(pidx, updated)
      }
      case _ => sector
    }
    sectorRepository.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, false))
  }
}
