package com.stovokor.editor.service

import com.stovokor.editor.model.Polygon
import com.stovokor.util.SectorUpdated
import com.stovokor.util.EventBus
import com.stovokor.editor.model.Sector
import com.stovokor.util.SectorDeleted
import com.stovokor.editor.model.Point
import com.stovokor.editor.model.SurfaceTexture
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.Border
import com.stovokor.editor.model.Slope
import com.stovokor.editor.model.BorderStrip
import com.stovokor.editor.model.Wall
import com.stovokor.editor.model.Target
import com.stovokor.editor.model.SectorTarget
import com.stovokor.editor.model.Platform
import scala.collection.mutable.ListBuffer
import com.stovokor.util.EditorEvent
import com.stovokor.editor.model.Line
import scala.collection.mutable.Builder

object SectorService {

  def apply() = this

  val sectorRepo = SectorRepository()
  val borderRepo = BorderRepository()

  def cutHole(polygon: Polygon) = {
    findCuttingHole(polygon)
      .foreach({
        case (id, sector) => {
          val updated = sectorRepo.update(id, sector.cutHole(polygon, true))
          EventBus.trigger(SectorUpdated(id, updated, true))
        }
      })
  }

  def findCuttingHole(polygon: Polygon) = {
    val cuttingHole = polygon.vertices
      .map(p => sectorRepo.findInside(p, true))
      .flatten
    if (cuttingHole.isEmpty) {
      None
    } else {
      Some(findInnermostSector(cuttingHole))
    }
  }

  def removeSector(sectorId: Long) = {
    val removed = sectorRepo.remove(sectorId)
    borderRepo
      .findFrom(sectorId)
      .foreach({ case (id, _) => borderRepo.remove(id) })
    borderRepo
      .findTo(sectorId)
      .foreach({ case (id, _) => borderRepo.remove(id) })
    EventBus.trigger(SectorDeleted(sectorId))
    removed
  }

  def collapseSector(sectorId: Long): Unit = {
    val sector = sectorRepo.get(sectorId)
    val height = borderRepo
      .findFrom(sectorId)
      .map({ case (_, b) => sectorRepo.get(b.sectorB) })
      .map(_.minFloorHeight(sector.polygon.vertices))
      .min
    changeHeight(Target.Floor(sectorId), height - sector.floor.height)
    changeHeight(Target.Ceiling(sectorId), height - sector.ceiling.height)
  }

  def changeHeight(target: SectorTarget, factor: Float): Unit = {
    val sectorId = target.sectorId
    val sector = sectorRepo.get(sectorId)
    val updated = target match {
      case Target.Floor(_) => sector.updatedFloor(sector.floor.move(factor))
      case Target.PlatformBottom(_, idx) => {
        val platform = sector.platforms(idx)
        val updated = platform.updatedBottom(platform.bottom.move(factor))
        sector.updatedPlatform(idx, updated)
      }
      case Target.PlatformTop(_, idx) => {
        val platform = sector.platforms(idx)
        val updated = platform.updatedTop(platform.top.move(factor))
        sector.updatedPlatform(idx, updated)
      }
      // match any to pointing to walls also changes ceiling height
      case _ => sector.updatedCeiling(sector.ceiling.move(factor))
    }
    sectorRepo.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, false))
    redrawNeighbours(sectorId, updated)
  }

  def redrawNeighbours(sectorId: Long, sector: Sector): Unit = {
    borderRepo
      .findTo(sectorId)
      .distinct
      .map({ case (_, border) =>
        (border.sectorA, sectorRepo.get(border.sectorA))
      })
      .map({ case (id, sec) => SectorUpdated(id, sec, false) })
      .foreach(EventBus.trigger)
  }

  def mutate(
      target: SectorTarget,
      mutation: SurfaceTexture => SurfaceTexture
  ): Unit = {
    SectorSurfaceMutator.mutate(target, mutation)
  }

  def rotateStretchMode(target: SectorTarget): Unit = {
    StretchModeRotator.rotate(target)
  }

  def mutateSlope(target: SectorTarget, mutation: Slope => Slope): Unit = {
    SectorSlopeMutator.mutate(target, mutation)
  }

  // TODO clean this up
  def movePoints(dx: Float, dy: Float, pointsToMove: Set[Point]): Unit = {
    var toUpdate: Map[Long, Sector] = Map()
    var toDelete: Set[Long] = Set()
    // move points in sectors
    pointsToMove.foreach(point => {
      val sectors = sectorRepo.findByPoint(point)
      for ((sectorId, sector) <- sectors) {
        val updated = sector.moveSinglePoint(
          point,
          dx,
          dy
        ) // this collapses holes if needed
        if (updated.polygon.isDegenerate) {
          sectorRepo.remove(sectorId)
          toDelete = toDelete ++ Set(sectorId)
        } else {
          sectorRepo.update(sectorId, updated)
          toUpdate = toUpdate.updated(sectorId, updated)
        }
      }
    })
    // update borders
    pointsToMove.foreach(point => {
      borderRepo
        .find(point)
        .filterNot(p => toDelete.contains(p._1))
        .map(pair =>
          pair match {
            case (id, border) => {
              val updated = border
                .updateLine(border.line.moveEnd(point, point.move(dx, dy)))
              if (updated.line.length > 0) {
                borderRepo.update(id, updated)
              } else {
                borderRepo.remove(id)
              }
              updated.sectorA
            }
          }
        )
    })
    // find new borders that may be created
    toUpdate.foreach(p1 =>
      p1 match {
        case (sectorId1, sector1) => {
          sector1.closedWalls.foreach(w => {
            sectorRepo
              .find(w.line)
              .filterNot(_._1 == sectorId1)
              .foreach(p2 =>
                p2 match {
                  case (sectorId2, sector2) => {
                    println(s"~~~ Found new border with ${sector2}")
                    val updated1 = sector1.openWall(w.line)
                    val updated2 = sector2.openWall(w.line)
                    sectorRepo.update(sectorId1, updated1)
                    sectorRepo.update(sectorId2, updated2)
                    borderRepo.add(
                      Border(
                        sectorId1,
                        sectorId2,
                        w.line,
                        BorderStrip(w.texture),
                        BorderStrip(w.texture),
                        BorderStrip(w.texture)
                      )
                    )
                    borderRepo.add(
                      Border(
                        sectorId2,
                        sectorId1,
                        w.line.reverse,
                        BorderStrip(w.texture),
                        BorderStrip(w.texture),
                        BorderStrip(w.texture)
                      )
                    )
                    toUpdate = toUpdate
                      .updated(sectorId1, updated1)
                      .updated(sectorId2, updated2)
                  }
                }
              )
          })
        }
      }
    )
    // when sector collapses over border, find border with to=sector and delete
    toDelete
      .flatMap(borderRepo.findFrom)
      .map(_._1)
      .foreach(borderRepo.remove)
    toDelete
      .flatMap(id => borderRepo.findTo(id))
      .foreach(p =>
        p match {
          case (borderId, border) => {
            borderRepo.remove(borderId)
            sectorRepo
              .get(border.sectorA)
              .openWalls
              .find(_.line == border.line)
              .map(wall => sectorRepo.get(border.sectorA).closeWall(wall.line))
              .foreach(sector => {
                sectorRepo.update(border.sectorA, sector)
                toUpdate = toUpdate.updated(border.sectorA, sector)
              })
          }
        }
      )
    toUpdate.foreach({ case (id, sec) =>
      EventBus.trigger(SectorUpdated(id, sec, true))
    })
    toDelete.foreach(id => EventBus.trigger(SectorDeleted(id)))
  }

  def findInnermostSector(sectors: List[(Long, Sector)]) = {
    sectors.sortBy(_._2.polygon.boundBox.area).head
  }

  def findRecursivelyConnected(ps: Set[Point]): Map[Long, Sector] = {
    def noholes(
        res: (Long, Sector),
        points: Set[Point],
        allIteration: Set[Point]
    ) = {
      // exclude holes and sectors touching by a single vertex
      val commonVertices = points.intersect(res._2.polygon.vertices.toSet).size
      val hasCommonLine = commonVertices > 1
      // if touches a corner is possible it's still connected by another sector
      val isCornerButConnected =
        commonVertices == 1 && allIteration.intersect(points).size > 1
      hasCommonLine || isCornerButConnected
    }
    def findrec(
        currIds: Set[Long],
        prevIds: Set[Long],
        points: Set[Point],
        found: Set[(Long, Sector)]
    ): Set[(Long, Sector)] = {
      if (currIds.isEmpty) found
      else {
        val search = currIds.map(id => (id, sectorRepo.get(id)))
        val iterVerts = search.flatMap(_._2.polygon.vertices).toSet
        val sectors = search.filter(noholes(_, points, iterVerts))
        val borders = currIds.flatMap(borderRepo.findFrom)
        val morePoints = points ++ sectors.flatMap(_._2.polygon.vertices)
        val seenIds =
          prevIds ++ sectors.map(_._1) // do now include filtered out
        val edges = found.map(_._2.polygon).flatMap(_.edges).toSet
        val moreIds = borders.map({ case (_, b) => b.sectorB }).diff(seenIds)
        // this condition breaks an infinite loop. not nice, should rethink findrec
        if (prevIds == seenIds) found ++ sectors
        else findrec(moreIds, seenIds, morePoints, found ++ sectors)
      }
    }
    val init = ps.flatMap(sectorRepo.findByPoint).map(_._1)
    findrec(init, Set(), ps, Set()).toMap
  }

  def isCuttingHole(points: List[Point]) = {
    val cuttingHole = points
      .map(p => sectorRepo.findInside(p, true))
      .flatten
    !cuttingHole.isEmpty
  }

  def addPlatform(sectorId: Long): Unit = {
    println(s"Adding platform to sector $sectorId")
    val sector = sectorRepo.get(sectorId)
    val updated = sector.addPlatform()
    sectorRepo.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, true))
  }

  def mutatePlatform(
      sectorId: Long,
      idx: Int,
      mutation: Platform => Platform
  ): Unit = {
    println(s"Mutating platform $sectorId $idx")
    val sector = sectorRepo.get(sectorId)
    val updated = sector.updatedPlatform(idx, mutation(sector.platforms(idx)))
    sectorRepo.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, true))
  }

  def removePlatform(sectorId: Long, idx: Int) = {
    println(s"Remove platform $idx from sector $sectorId")
    val sector = sectorRepo.get(sectorId)
    val updated = sector.removePlatform(idx)
    sectorRepo.update(sectorId, updated)
    EventBus.trigger(SectorUpdated(sectorId, updated, true))
  }
}
