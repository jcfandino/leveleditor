package com.stovokor.editor.service

import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.model.Sector
import com.stovokor.util.EventBus
import com.stovokor.util.SectorUpdated

object LineService {

  def apply() = this

  val sectorRepo = SectorRepository()
  val borderRepo = BorderRepository()

  def replaceLine(line: Line, lines: List[Line]): Unit = {
    val sectors = sectorRepo.find(line)
    // validate input
    if (
      sectors.isEmpty
      || lines.isEmpty
      || lines.head.a != line.a
      || lines.last.b != line.b
    ) {
      println(s"ERROR: wrong params when replacing line $line")
      return
    }
    val changes = // new ListBuffer[EditorEvent]()
      Map.newBuilder[Long, Sector]
    // update sectors
    sectors.foreach({
      case (sectorId, sector) => {
        val updated = sector.replaceLine(line, lines)
        sectorRepo.update(sectorId, updated)
        changes += (sectorId -> updated)
      }
    })
    // update inside borders
    borderRepo
      .find(line)
      .foreach({
        case (id, border) => {
          val old = borderRepo.remove(id)
          lines.map(old.updateLine).foreach(borderRepo.add)
        }
      })
    // update outside borders
    borderRepo
      .find(line.reverse)
      .foreach({
        case (id, border) => {
          val old = borderRepo.remove(id)
          lines.map(_.reverse).map(old.updateLine).foreach(borderRepo.add)
          changes += (border.sectorA -> sectorRepo.get(border.sectorA))
        }
      })
    changes
      .result()
      .foreach({
        case (id, sec) => {
          EventBus.trigger(new SectorUpdated(id, sec, true))
        }
      })
  }

}
