package com.stovokor.editor.portals

import com.stovokor.aperture.Graph
import scala.jdk.CollectionConverters._
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Polygon
import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.algo.FindFaces
import com.stovokor.aperture.Portal
import com.jme3.math.Vector2f
import com.jme3.math.Vector3f
import com.jme3.math.Quaternion
import com.jme3.math.FastMath
import com.stovokor.aperture.PolygonalArea

object PortalGraphFactory {

  def create: Graph = {

    val polygons = createPolygons

    val polygonsByLine =
      polygons.flatMap(pl => pl.edges.map(l => (l, pl))).toMap

    val polygonById = polygons.zipWithIndex.toMap

    val portalLines = PortalsRepository.all

    val edges = portalLines
      .filter(_.isPortal)
      .flatMap(portalLine => {
        val line = portalLine.line
        val sectors = BorderRepository()
          .find(line)
          .flatMap({ case (_, border) =>
            List(border.sectorA, border.sectorB)
          })
          .map(SectorRepository().get)

        val (floor, ceiling) = vertLimits(sectors, line)
        val height = ceiling - floor
        val width = line.length

        val location = new Vector3f(
          -(line.a.x + line.b.x) / 2,
          (ceiling + floor) / 2,
          (line.a.y + line.b.y) / 2
        )
        val dimensions = new Vector2f(line.length, ceiling - floor)
        val angle = FastMath.atan2(line.b.y - line.a.y, line.b.x - line.a.x)
        val rotation = new Quaternion().fromAngleAxis(angle, Vector3f.UNIT_Y)

        val portal = new Portal(location, rotation, dimensions)

        if (
          !polygonsByLine.contains(line) ||
          !polygonsByLine.contains(line.reverse)
        ) {
          println(s"ERROR: Failed to find polygon for line $line")
        }
        for (
          backPoly <- polygonsByLine.get(line);
          frontPoly <- polygonsByLine.get(line.reverse)
        ) yield {

          val backArea = new PolygonalArea(
            backPoly.vertices.map(p => new Vector2f(-p.x, p.y)).asJava
          )
          val frontArea = new PolygonalArea(
            frontPoly.vertices.map(p => new Vector2f(-p.x, p.y)).asJava
          )

          new Graph.Edge(portal, frontArea, backArea)
        }
      })

    new Graph(edges.toList.asJava)
  }

  private def vertLimits(secs: List[Sector], line: Line): (Float, Float) = {
    val (a: Float, b: Float) =
      secs.fold((Float.MinValue, Float.MaxValue))({
        case ((maxFloor: Float, minCeiling: Float), sector: Sector) => {
          val currFloor = line.points.map(sector.floorHeightOn(_)).min
          val currCeiling = line.points.map(sector.ceilingHeightOn(_)).max
          (maxFloor.max(currFloor), minCeiling.min(currCeiling))
        }
      })
    (a, b)
  }

  def createPolygons: Set[Polygon] = {

    val portalLines = PortalsRepository.all
    val portalByLine =
      portalLines.flatMap(pl => pl.line.andReverse.map(l => (l, pl))).toMap
    val (sectors, holeIds) = outerSectors()

    val areaPolygons = ListBuffer[Polygon]()
    val processed = mutable.Set[Long]()
    // do not process those sectors inside holes.
    processed ++= holeIds

    for ((sid, sector) <- sectors if !processed.contains(sid)) {
      var currSeen = mutable.Set(sid)
      visitAreaPolygonsFrom(sid, portalByLine, holeIds, currSeen)
      val sectors = currSeen.map(id => SectorRepository().get(id))
      val holes = sectors.flatMap(_.holes).toSet
      val edges = sectors
        .map(_.polygon)
        .flatMap(_.edges)
        .toList
      val faces = FindFaces(edges)
      val polygon = FindFaces(edges).outer.head
      areaPolygons += polygon
      processed += sid
      processed ++= currSeen
    }
    areaPolygons.toSet
  }

  def outerSectors(): (Map[Long, Sector], Set[Long]) = {
    val allSectors = SectorRepository().sectors

    val holeLines = allSectors.flatMap(_._2.holes).flatMap(_.edges).toSet
    val banned = mutable.Set[Long]()

    val filtered = allSectors.filterNot({
      case (sid, sec) => {
        val isHole = sec.polygon.edges.exists(holeLines.contains(_))
        if (isHole) banned += sid
        isHole
      }
    })
    (filtered, banned.toSet)
  }

  def visitAreaPolygonsFrom(
      sectorId: Long,
      portalByLine: Map[Line, PortalLine],
      holeIds: Set[Long],
      visited: mutable.Set[Long]
  ): Unit = {
    val neighbors = for (
      (bid, border) <- BorderRepository().findFrom(sectorId)
      if !visited.contains(border.sectorB) && //
        !holeIds.contains(border.sectorB) && //
        !portalByLine.contains(border.line)
    ) {
      visited += border.sectorB
      visitAreaPolygonsFrom(border.sectorB, portalByLine, holeIds, visited)
    }
  }
}
