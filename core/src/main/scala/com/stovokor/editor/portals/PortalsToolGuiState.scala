package com.stovokor.editor.portals

import com.stovokor.editor.state.BaseState
import com.stovokor.util.EditorEventListener
import com.stovokor.editor.state.ExtensionToolView
import com.stovokor.editor.state.OptionPanelAccess
import com.stovokor.util.EditorEvent
import com.stovokor.editor.portals.PortalsExporter
import com.stovokor.editor.export.MapExporterRegistry
import com.stovokor.util.EventBus
import com.stovokor.util.StartNewMap
import com.stovokor.util.OpenMap
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.editor.state.ExtensionPanelState
import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import com.stovokor.editor.model.repository.ExtensionRepository
import com.stovokor.util.ViewModeSwitch
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.SelectionChange
import com.stovokor.editor.model.SelectionLine
import com.stovokor.editor.model.Line
import com.simsilica.lemur.component.SpringGridLayout
import com.simsilica.lemur.Axis
import com.simsilica.lemur.FillMode
import com.simsilica.lemur.Checkbox
import com.stovokor.util.LemurExtensions._
import com.stovokor.editor.factory.Mesh3dFactory
import com.stovokor.editor.model.Surface
import com.stovokor.editor.model.Target
import com.stovokor.editor.algo.Triangulator
import com.jme3.math.ColorRGBA
import com.stovokor.editor.factory.MaterialFactoryClient
import com.stovokor.editor.factory.SurfaceMeshFactory
import com.jme3.math.Vector3f
import com.jme3.scene.Geometry
import com.stovokor.editor.factory.ColorPaletteGenerator
import com.jme3.scene.Node
import com.stovokor.editor.factory.Mesh2dFactory
import com.stovokor.editor.gui.K
import com.stovokor.editor.gui.Palette
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.Sector
import com.stovokor.editor.model.repository.BorderRepository

class PortalsToolGuiState
    extends BaseState
    with EditorEventListener
    with ExtensionToolView
    with OptionPanelAccess
    with MaterialFactoryClient {

  var panel: Container = new Container
  var selection: List[Line] = Nil
  var isOpen = false

  override def initialize(
      stateManager: AppStateManager,
      simpleApp: Application
  ): Unit = {
    super.initialize(stateManager, simpleApp)
    // register file extension
    ExtensionRepository().register("portals", PortalsDataExtension)
    // register exporter
    MapExporterRegistry.register(new PortalsExporter())
    clear()

    EventBus.subscribeByType(this, classOf[StartNewMap])
    EventBus.subscribeByType(this, classOf[OpenMap])
    EventBus.subscribeByType(this, classOf[SelectionChange])
    EventBus.subscribeByType(this, classOf[SectorUpdated])
    EventBus.subscribe(this, ViewModeSwitch())

    // createPanel()
    updateSelection(Nil)
    stateManager
      .getState(classOf[ExtensionPanelState])
      .registerExtensionPanel("Portals", panel, this)
  }

  def onEvent(event: EditorEvent) = event match {
    case StartNewMap()    => setOpen(false)
    case OpenMap()        => setOpen(false)
    case ViewModeSwitch() => updateVisualization()
    case SelectionChange(selected) =>
      if (isOpen)
        updateSelection(
          selected
            .filter(_.isInstanceOf[SelectionLine])
            .map(_.asInstanceOf[SelectionLine].line)
            .toList
        )
    case SectorUpdated(id, sector, true) => onSectorUpdated(id, sector)
    case _                               =>
  }

  override def setOpen(open: Boolean): Unit = {
    isOpen = open
    updateVisualization()
    if (open) updateSelection(Nil)
  }

  private def updateSelection(newSelection: List[Line]): Unit = {
    selection = newSelection
    updatePanel()
  }

  private def onSectorUpdated(id: Long, sector: Sector): Unit = {
    for ((id, line) <- PortalsRepository.lines) {
      if (BorderRepository().find(line.line).isEmpty) {
        update(id, line.cleared)
      }
    }
    updateVisualization()
  }

  private def updatePanel(): Unit = {
    panel.detachAllChildren()
    val builder = new PanelBuilder(panel)
    for (line <- selection) {
      val (id, portal): (Long, PortalLine) =
        PortalsRepository.find(line).getOrElse((-1, new PortalLine(line, 0)))
      builder.lineOptions(id, portal)
    }
    if (selection.isEmpty) {
      builder.emptySelection()
    }
  }

  private def clear(): Unit = {
    panel.detachAllChildren()
    get2DPortalsNode.detachAllChildren()
    get3DPortalsNode.detachAllChildren()
  }

  class PanelBuilder(panel: Container) {

    def emptySelection(): Unit = {
      panel.addChild(new Label("select lines to edit"))
    }

    def lineOptions(id: Long, portal: PortalLine): Unit = {

      val section = panel.addChild(
        new Container(
          new SpringGridLayout(Axis.Y, Axis.X, FillMode.None, FillMode.None)
        )
      )
      section.addChild(
        new Label(
          s"Edit line: (${portal.line.a.x},${portal.line.a.y})-(${portal.line.b.x},${portal.line.b.y})"
        )
      )

      val options = section.addChild(
        new Container(
          new SpringGridLayout(Axis.X, Axis.Y, FillMode.None, FillMode.None)
        )
      )
      val isPortal = options.addChild(new Checkbox("Portal"))
      isPortal.setChecked(portal.isPortal)
      val isLimit = options.addChild(new Checkbox("Area limit"))
      isLimit.setChecked(portal.isAreaLimit)
      isPortal.onClick(() => {
        update(id, if (isPortal.isChecked) portal.asPortal else portal.cleared)
      })
      isLimit.onClick(() => {
        update(
          id,
          if (isLimit.isChecked) portal.asAreaLimit else portal.cleared
        )
      })
    }
  }

  private def update(id: Long, line: PortalLine): Unit = {
    updatePortalLine(id, line)
    updatePanel()
    updateVisualization()
  }

  private def updatePortalLine(id: Long, portal: PortalLine): Unit = {
    if (id == -1) PortalsRepository.add(portal)
    else if (portal.isNotSet) PortalsRepository.delete(id)
    else PortalsRepository.update(id, portal)
  }

  private def updateVisualization(): Unit = {
    get2DPortalsNode.detachAllChildren()
    if (!isOpen) return
    val polygons = PortalGraphFactory.createPolygons
    val palette = ColorPaletteGenerator.generateAnalogousPalette(
      polygons.size,
      0.24f,
      0.4f,
      0.2f
    )
    for ((polygon, i) <- polygons.toList.zipWithIndex) {
      val triangles = Triangulator.triangulate(polygon, Set())
      var mesh = SurfaceMeshFactory.createMesh(
        triangles,
        polygon.center,
        Surface(1),
        true,
        Vector3f.UNIT_Z,
        false
      )
      val surface = new Geometry("portal-area", mesh)
      val material = plainColor(palette(i))
      surface.setMaterial(material)
      surface.getLocalTranslation().setZ(K.zPortalArea)
      get2DPortalsNode.attachChild(surface)
    }

    val lines = new Node("portal-lines")
    val meshFactory = Mesh2dFactory(assetManager)
    for (line <- PortalsRepository.all if !line.isNotSet) {
      val color =
        if (line.isPortal) Palette.linePortalPortal
        else Palette.linePortalAreaLimit
      meshFactory.draw2dLine(lines, color, line.line)
    }
    lines.getLocalTranslation().setZ(K.zPortalLine)
    get2DPortalsNode.attachChild(lines)
  }

  private def get3DPortalsNode = getOrCreateNode(get3DNode, "portals")
  private def get2DPortalsNode = getOrCreateNode(get2DNode, "portals")

}
