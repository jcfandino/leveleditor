package com.stovokor.editor.portals

import java.util.concurrent.atomic.AtomicLong
import com.stovokor.editor.model.repository.DataExtension
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JObject
import org.json4s.{Extraction, NoTypeHints}
import org.json4s.DefaultFormats
import org.json4s.ShortTypeHints
import org.json4s.TypeHints
import com.stovokor.editor.model.Line

case class PortalLine(line: Line, option: Int) {

  def isPortal = LineOption.PORTAL == option
  def isAreaLimit = LineOption.AREA_LIMIT == option
  def isNotSet = LineOption.NONE == option

  def asPortal = PortalLine(line, LineOption.PORTAL)
  def asAreaLimit = PortalLine(line, LineOption.AREA_LIMIT)
  def cleared = PortalLine(line, LineOption.NONE)
}

object LineOption {
  val NONE = 0
  val PORTAL = 1
  val AREA_LIMIT = 2
}

object PortalsDataExtension extends DataExtension {

  implicit val formats: org.json4s.DefaultFormats = new DefaultFormats {
    override val typeHintFieldName: String = "type"
    override val typeHints: TypeHints =
      ShortTypeHints(List(classOf[PortalLine]))
  }
  override def fetch: JValue = Extraction.decompose(PortalsRepository.lines)

  def load(data: JValue): Unit = {
    data match {
      case data: JObject => {
        PortalsRepository.clear()
        data
          .extract[Map[Long, PortalLine]]
          .foreach({ case (id, l) => PortalsRepository.add(l) })
      }
      case _ =>
    }
  }

  override def clear: Unit = PortalsRepository.clear()

}

object PortalsRepository {

  val idGenerator = new AtomicLong(0)
  var lines: Map[Long, PortalLine] = Map()

  def all = lines.values

  def add(line: PortalLine) = {
    val id = idGenerator.getAndIncrement
    lines = lines.updated(id, line)
    id
  }

  def update(id: Long, line: PortalLine): Unit = {
    lines = lines.updated(id, line)
  }

  def delete(id: Long): Unit = {
    lines = lines.filterNot({ case (lid, _) => id == lid })
  }

  def clear(): Unit = {
    lines = Map()
    idGenerator.set(0)
  }

  def find(line: Line): Option[(Long, PortalLine)] = lines.find({
    case (id, pl) => pl.line.alike(line)
  })

}
