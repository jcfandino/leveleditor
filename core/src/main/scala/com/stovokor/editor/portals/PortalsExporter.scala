package com.stovokor.editor.portals

import com.jme3.asset.AssetManager
import com.simsilica.lemur.Container
import com.simsilica.lemur.Label
import java.io.File
import com.stovokor.aperture.serialization.GraphWriter
import com.stovokor.aperture.Graph
import com.stovokor.aperture.Portal
import com.jme3.math.Vector3f
import com.jme3.math.Quaternion
import com.jme3.math.Vector2f
import com.stovokor.aperture.PolygonalArea
import scala.jdk.CollectionConverters._
import com.stovokor.editor.portals.PortalGraphFactory
import com.stovokor.editor.export.MapDataExporter

class PortalsExporter extends MapDataExporter {

  def name = "Portals (*.portals)"

  def exportPanel = {
    val panel = new Container()
    panel.addChild(new Label("Portals file."))
    panel
  }

  def exportData(baseDir: String, mapName: String): Unit = {
    val file = new File(s"$baseDir/$mapName.portals")
    val graph = PortalGraphFactory.create
    println(s"Exporting .portals to ${file.getAbsolutePath}")
    var writer = new GraphWriter()
    writer.write(file, graph);
  }
}
