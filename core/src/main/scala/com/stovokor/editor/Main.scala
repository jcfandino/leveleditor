package com.stovokor.editor

import com.jme3.app.SimpleApplication
import com.jme3.input.controls.InputListener
import com.jme3.system.AppSettings
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.event.MouseAppState
import com.simsilica.lemur.style.BaseStyles
import com.stovokor.editor.input.InputFunctionsMapper
import com.stovokor.editor.state.ExportMapState
import com.stovokor.editor.state.GridState
import com.stovokor.editor.state.GuiState
import com.stovokor.editor.state.MaterialSelectionState
import com.stovokor.editor.state.SaveOpenFileState
import com.stovokor.editor.state.SectorPresenterState
import com.stovokor.editor.state.SettingsEditorState
import com.stovokor.editor.state.SettingsLoaderState
import com.stovokor.editor.state.ViewModeState
import com.simsilica.lemur.OptionPanelState
import com.stovokor.editor.state.HelpWindowState
import com.jme3.math.ColorRGBA
import com.stovokor.editor.gui.Palette
import com.jme3.app.DetailedProfilerState
import com.jme3.app.BasicProfilerState
import com.jme3.app.state.AbstractAppState
import scala.jdk.CollectionConverters._
import com.jme3.app.state.AppState
import com.stovokor.editor.state.ExtensionPanelState
import com.stovokor.editor.navmesh.NavMeshToolGuiState
import com.stovokor.editor.lighting.LightToolGuiState
import com.stovokor.editor.state.MemoryMonitorState
import com.stovokor.editor.decal.DecalToolGuiState
import com.jme3.app.StatsAppState
import com.jme3.app.FlyCamAppState
import com.jme3.app.state.ConstantVerifierState
import java.util.logging.LogManager
import java.util.logging.Logger
import java.util.logging.Level
import com.stovokor.editor.portals.PortalsToolGuiState

// Level Editor
object Main {

  def apply(extras: AppState*) = new Main(extras.toList)

  def main(args: Array[String]): Unit = {
    val app = new Main(Nil)
    val sets = new AppSettings(true)
    //    sets.setSettingsDialogImage("/Interface/logo.png")
    sets.setWidth(1024)
    sets.setHeight(768)
    sets.setGammaCorrection(false)
    sets.setTitle("M8 Editor")
    app.setSettings(sets)
    app.setDisplayFps(true)
    app.setDisplayStatView(false)

    // disable logging for now
    // LogManager.getLogManager().reset()
    Logger.getLogger("com.jme3").setLevel(Level.SEVERE)

    app.start
  }
}

class Main(extras: List[AppState])
    extends SimpleApplication(
      new StatsAppState(),
      new FlyCamAppState(),
      new ConstantVerifierState()
    ) {

  def simpleInitApp() = {
    // Init lemur
    GuiGlobals.initialize(this)
    BaseStyles.loadGlassStyle()
    GuiGlobals.getInstance().getStyles().setDefaultStyle("glass")

    // Init input
    InputFunctionsMapper.initialize(GuiGlobals.getInstance.getInputMapper)
    inputManager.removeListener(getInputListener)

    // Init states
    stateManager.attach(new GuiState)
    stateManager.attach(new GridState)
    stateManager.attach(new MouseAppState(this))
    stateManager.attach(new ViewModeState)
    stateManager.attach(new SectorPresenterState)
    stateManager.attach(new ExportMapState)
    stateManager.attach(new SettingsLoaderState)
    stateManager.attach(new MaterialSelectionState)
    stateManager.attach(new SaveOpenFileState)
    stateManager.attach(new SettingsEditorState)
    stateManager.attach(new OptionPanelState)
    stateManager.attach(new HelpWindowState)
    // stateManager.attach(new DetailedProfilerState())
    stateManager.attach(new BasicProfilerState)
    stateManager.attach(new MemoryMonitorState)
    // extensions
    stateManager.attach(new ExtensionPanelState)
    stateManager.attach(new NavMeshToolGuiState)
    stateManager.attach(new LightToolGuiState)
    stateManager.attach(new DecalToolGuiState)
    stateManager.attach(new PortalsToolGuiState)

    // extended states
    stateManager.attachAll(extras: _*)

    viewPort.setBackgroundColor(Palette.background)
  }

  def getInputListener = {
    val field = classOf[SimpleApplication].getDeclaredField("actionListener")
    field.setAccessible(true)
    field.get(this).asInstanceOf[InputListener]
  }

}
