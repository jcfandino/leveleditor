package com.stovokor.util

import com.stovokor.editor.model.Target
import com.stovokor.editor.model.SectorTarget

class TargetTracker extends EditorEventListener {

  var target: Option[Target] = None

  def init(): Unit = {
    EventBus.subscribeByType(this, classOf[PointerTargetChange])
  }

  def cleanup(): Unit = {
    EventBus.removeFromAll(this)
  }

  def onEvent(event: EditorEvent) = event match {
    case PointerTargetChange(t) => target = Some(t)
    case _                      =>
  }

  def foreach = target.foreach _
  def ifSector(a: SectorTarget => Unit) = target.foreach({
    case target: SectorTarget => a(target)
    case _                    =>
  })
}
