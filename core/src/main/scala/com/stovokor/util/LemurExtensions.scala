package com.stovokor.util

import com.jme3.scene.Spatial
import com.simsilica.lemur.event.CursorButtonEvent
import com.simsilica.lemur.event.CursorEventControl
import com.simsilica.lemur.event.DefaultCursorListener
import com.simsilica.lemur.event.CursorMotionEvent
import com.simsilica.lemur.input.InputMapper
import com.simsilica.lemur.input.FunctionId
import com.simsilica.lemur.input.InputState
import com.simsilica.lemur.input.InputMapper.Mapping
import com.simsilica.lemur.input.Axis
import com.jme3.input.KeyInput
import com.simsilica.lemur.Button
import com.simsilica.lemur.Command
import com.simsilica.lemur.ListBox
import com.simsilica.lemur.event.DefaultMouseListener
import com.jme3.input.event.MouseButtonEvent
import com.jme3.collision.CollisionResult

object LemurExtensions {

  implicit class SpatialExtension(spatial: Spatial) {

    def onCursorClick(
        action: (CursorButtonEvent, Spatial, Spatial) => Unit
    ): Unit = {
      onCursorClick((e, s1, s2, c) => action(e, s1, s2))
    }

    def onCursorClick(
        action: (
            CursorButtonEvent,
            Spatial,
            Spatial,
            Option[CollisionResult]
        ) => Unit
    ): Unit = {
      CursorEventControl.addListenersToSpatial(
        spatial,
        new DefaultCursorListener() {

          var collision: Option[CollisionResult] = None

          override def cursorButtonEvent(
              event: CursorButtonEvent,
              target: Spatial,
              capture: Spatial
          ): Unit = {
            action(event, target, capture, collision)
          }
          override def cursorMoved(
              event: CursorMotionEvent,
              target: Spatial,
              capture: Spatial
          ): Unit = {
            collision = Option(event.getCollision)
          }
        }
      )
    }
    def onCursorMove(
        action: (CursorMotionEvent, Spatial, Spatial) => Unit
    ): Unit = {
      CursorEventControl.addListenersToSpatial(
        spatial,
        new DefaultCursorListener() {
          override def cursorMoved(
              event: CursorMotionEvent,
              target: Spatial,
              capture: Spatial
          ): Unit = {
            action(event, target, capture)
          }
        }
      )
    }
  }

  implicit class InputMapperExtension(mapper: InputMapper) {
    def map(fun: FunctionId, primary: Axis, modifiers: Int*): Mapping = {
      mapper.map(fun, primary, modifiers.map(_.asInstanceOf[Object]): _*)
    }
    def map(fun: FunctionId, primary: Int, modifiers: Int*): Mapping = {
      map(fun, InputState.Positive, primary, modifiers: _*)
    }
    def map(
        fun: FunctionId,
        bias: InputState,
        primary: Int,
        modifiers: Int*
    ): Mapping = {
      mapper.map(fun, bias, primary, modifiers.map(_.asInstanceOf[Object]): _*)
    }

    /** Convenience methods to map Ctrl/Alt/Shift if any conbination of left and
      * right.
      */
    def mapWheel(
        fun: FunctionId,
        shift: Boolean = false,
        ctrl: Boolean = false,
        alt: Boolean = false
    ): Unit = {
      println(s"- Mapping wheel $fun - {s:$shift, c:$ctrl, a:$alt}")
      if (shift || ctrl || alt) {
        mods(shift, ctrl, alt).foreach(ms =>
          mapper.map(
            fun,
            Axis.MOUSE_WHEEL,
            ms.toList.map(_.asInstanceOf[Object]): _*
          )
        )
      } else mapper.map(fun, Axis.MOUSE_WHEEL)
    }
    def mapKey(
        fun: FunctionId,
        primary: Int,
        state: Boolean = true,
        shift: Boolean = false,
        ctrl: Boolean = false,
        alt: Boolean = false
    ): Unit = {
      println(
        s"- Mapping key $fun - $primary - $state - {s:$shift, c:$ctrl, a:$alt}"
      )
      if (shift || ctrl || alt) {
        mods(shift, ctrl, alt).foreach(ms =>
          mapper.map(
            fun,
            bias(state),
            primary,
            ms.toList.map(_.asInstanceOf[Object]): _*
          )
        )
      } else mapper.map(fun, bias(state), primary)
    }
    private def perm(next: List[Set[Int]], it: List[Set[Int]]): List[Set[Int]] =
      next match {
        case Nil     => it
        case s :: ls => s.toList.flatMap(c => perm(ls, it.map(m => m + c)))
      }
    private def mods(
        shift: Boolean,
        ctrl: Boolean,
        alt: Boolean
    ): Set[List[Object]] = {
      val shiftKeys: Set[Int] =
        if (shift) Set(KeyInput.KEY_LSHIFT, KeyInput.KEY_RSHIFT) else Set()
      val ctrlKeys: Set[Int] =
        if (ctrl) Set(KeyInput.KEY_LCONTROL, KeyInput.KEY_RCONTROL) else Set()
      val altKeys: Set[Int] =
        if (alt) Set(KeyInput.KEY_LMENU, KeyInput.KEY_RMENU) else Set()
      val mods = List(shiftKeys, ctrlKeys, altKeys).filterNot(_.isEmpty)
      var perms = perm(mods, mods.map(_ => Set[Int]())).toSet
      perms.map(_.toList.map(_.asInstanceOf[Object]))
    }
    private def bias(state: Boolean) =
      if (state) InputState.Positive else InputState.Negative

  }

  implicit class ButtonExtension(button: Button) {
    def onClick(action: () => Unit): Unit = {
      button.addClickCommands(
        Array[Command[Button]]((b: Button) => action()): _*
      )
    }
    def onRightClick(action: () => Unit): Unit = {
      button.addMouseListener(new DefaultMouseListener {
        override def click(
            event: MouseButtonEvent,
            target: Spatial,
            capture: Spatial
        ): Unit = {
          if (event.getButtonIndex == 1) action()
        }
      })
    }
  }
  implicit class ListBoxExtension(listBox: ListBox[_]) {
    def onClick(action: () => Unit): Unit = {
      listBox.addCommands(ListBox.ListAction.Down, (_) => action())
    }
  }
}
