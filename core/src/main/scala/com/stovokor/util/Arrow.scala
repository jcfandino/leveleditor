package com.stovokor.util

import com.jme3.scene.Mesh
import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import com.jme3.scene.VertexBuffer.Type
import com.jme3.util.BufferUtils
import scala.jdk.CollectionConverters._
import com.jme3.math.Vector2f

class Arrow(
    val length: Float,
    val shaftRadio: Float,
    val tipRadio: Float,
    val tipLength: Float,
    val radialSamples: Int
) extends Mesh {

  updateGeometry()

  def this(length: Float) = {
    this(
      length,
      shaftRadio = length / 20,
      tipRadio = length / 6,
      tipLength = length / 3,
      radialSamples = 8
    )
  }

  def updateGeometry(): Unit = {

    val verticesCount = 2 + radialSamples * 3
    val vertices = new Array[Vector3f](verticesCount * 3)
    val normals = new Array[Vector3f](verticesCount * 3)
    val coords = new Array[Vector2f](verticesCount * 2) // 0 ~ .2 ~ .6 ~ .75 ~ 1
    val indices = new Array[Int](radialSamples * 6 * 3)

    // point
    val pv = new Vector3f(0f, length, 0f)
    // origin
    val ov = new Vector3f(0f, 0f, 0f)
    val (pi, oi) = (0, 1)

    vertices(0) = pv
    vertices(1) = ov
    coords(0) = new Vector2f(1f, 1f)
    coords(1) = new Vector2f(0f, 0f)
    var vi = 2 // vertices index
    var ii = 0 // indices index

    for (step <- (0 to radialSamples - 1)) {
      val ang = if (step == 0) 0f else step * (FastMath.TWO_PI / radialSamples)
      val next =
        if (step == radialSamples - 1) -3 * (radialSamples - 1)
        else 3 // next index
      // tip
      val tv = new Vector3f(
        tipRadio * FastMath.cos(ang),
        length - tipLength,
        tipRadio * FastMath.sin(ang)
      )
      val ti = vi
      vertices(ti) = tv
      coords(ti) = new Vector2f((step.toFloat / radialSamples).toFloat, .75f)
      // joint
      val jv = new Vector3f(
        shaftRadio * FastMath.cos(ang),
        length - tipLength,
        shaftRadio * FastMath.sin(ang)
      )
      val ji = vi + 1
      vertices(ji) = jv
      coords(ji) = new Vector2f((step.toFloat / radialSamples).toFloat, 0.6f)
      // base
      val bv = new Vector3f(
        shaftRadio * FastMath.cos(ang),
        0f,
        shaftRadio * FastMath.sin(ang)
      )
      val bi = vi + 2
      vertices(bi) = bv
      coords(bi) = new Vector2f((step.toFloat / radialSamples).toFloat, 0.2f)
      vi = vi + next
      // indices
      // tip
      indices(0 + ii) = pi
      indices(1 + ii) = ti + next
      indices(2 + ii) = ti
      // joint
      indices(3 + ii) = ti
      indices(4 + ii) = ji + next
      indices(5 + ii) = ji

      indices(6 + ii) = ji + next
      indices(7 + ii) = ti
      indices(8 + ii) = ti + next
      // shaft
      indices(9 + ii) = ji
      indices(10 + ii) = bi + next
      indices(11 + ii) = bi

      indices(12 + ii) = bi + next
      indices(13 + ii) = ji
      indices(14 + ii) = ji + next
      // base
      indices(15 + ii) = bi
      indices(16 + ii) = bi + next
      indices(17 + ii) = oi
      ii = ii + 18

      normals(step * 3) = Vector3f.UNIT_Z
      normals(step * 3 + 1) = Vector3f.UNIT_Z
      normals(step * 3 + 2) = Vector3f.UNIT_Z
    }
    setBuffer(
      Type.Position,
      3,
      BufferUtils.createFloatBuffer(vertices.toArray: _*)
    )
    setBuffer(
      Type.TexCoord,
      2,
      BufferUtils.createFloatBuffer(coords.toArray: _*)
    )
    setBuffer(Type.Index, 3, BufferUtils.createIntBuffer(indices.toArray: _*))
    setBuffer(
      Type.Normal,
      3,
      BufferUtils.createFloatBuffer(normals.toArray: _*)
    )
    updateBound()
    setStatic()
  }
}
