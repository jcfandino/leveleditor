package com.stovokor.util

import com.jme3.math.Matrix3f
import com.jme3.math.Vector3f
import com.jme3.math.Vector2f
import com.jme3.math.Quaternion

object CoordinateSystem {

  // editor -> jme/opengl
  // X'=-X ; Y'=Z ; Z'=Y
  val matrix = new Matrix3f(-1, 0, 0, 0, 0, 1, 0, 1, 0)

  val inverted = matrix.invert

  def toJme(v: Vector3f): Vector3f = matrix.mult(v)
  def toJme(v: Vector2f, z: Float = 0f): Vector3f = toJme(
    new Vector3f(v.x, v.y, z)
  )
  def toJme(q: Quaternion): Quaternion =
    new Quaternion(-q.getX, q.getZ, q.getY, q.getW)
  def toEditor(v: Vector3f): Vector3f = inverted.mult(v)
}
