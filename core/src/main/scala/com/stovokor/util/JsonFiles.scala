package com.stovokor.util

import java.io.FileWriter
import java.io.BufferedWriter
import scala.io.Source
import java.io.File
import org.json4s._
import com.stovokor.editor.model.StretchMode
import com.stovokor.editor.model.StretchMode._
import com.stovokor.editor.model.SimpleMaterial
import com.stovokor.editor.model.MatDefMaterial
import com.stovokor.editor.model.MissingMaterial
import com.stovokor.editor.model.NullMaterial
import com.stovokor.editor.model.SurfaceMaterial

object JsonFiles {
  import org.json4s._
  import org.json4s.jackson.Serialization
  import org.json4s.jackson.Serialization.{read, writePretty}
  import org.json4s.DefaultFormats

  implicit val formats: org.json4s.Formats =
    DefaultFormats +
      StretchModeSerializer +
      SurfaceMaterialSerializer

  def save[T <: AnyRef](path: String, obj: T): T = {
    val json = writePretty(obj)
    val file = new File(path)
    println(s"Saving ${file.getAbsolutePath}")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(json)
    bw.close()
    obj
  }

  def load[T <: AnyRef](path: String)(implicit mf: Manifest[T]): T = {
    val file = new File(path)
    val json = Source.fromFile(file).mkString
    read(json)
  }
}

// TODO find a better place to put this
object StretchModeSerializer
    extends CustomSerializer[StretchMode](formats =>
      (
        {
          case JString("Normal")        => Normal
          case JString("FollowFloor")   => FollowFloor
          case JString("FollowCeiling") => FollowCeiling
          case JNull                    => Normal
          case JObject(_)               => Normal
        },
        { case status: StretchMode =>
          status match {
            case Normal        => JString("Normal")
            case FollowFloor   => JString("FollowFloor")
            case FollowCeiling => JString("FollowCeiling")
          }
        }
      )
    )

object SurfaceMaterialSerializer
    extends CustomSerializer[SurfaceMaterial](format =>
      (
        {
          case JObject(List(JField("simple", JString(path)))) =>
            SimpleMaterial(path)
          case JObject(List(JField("matdef", JString(path)))) =>
            MatDefMaterial(path)
          case JObject(List(JField("missing", _))) => MissingMaterial
          case JObject(List(JField("null", _)))    => NullMaterial
        },
        {
          case SimpleMaterial(path) => JObject(JField("simple", JString(path)))
          case MatDefMaterial(path) => JObject(JField("matdef", JString(path)))
          case MissingMaterial      => JObject(JField("missing", JNull))
          case NullMaterial         => JObject(JField("null", JNull))
        }
      )
    )
