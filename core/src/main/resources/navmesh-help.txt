Cell size:
    The width and depth resolution used when sampling the source geometry.
    outdoors = agentRadius/2, indoors = agentRadius/3, cellSize =
    agentRadius for very small cells.
    Constraints > 0, default=0.25

Cell height:
    The height resolution used when sampling the source geometry.
    minTraversableHeight, maxTraversableStep, and contourMaxDeviation
    will need to be greater than the value of cellHeight in order to
    function correctly. maxTraversableStep is especially susceptible to
    impact from the value of cellHeight.
    cellSize/2
    Constraints > 0, default=0.125

Min traversable height:
    Represents the minimum floor to ceiling height that will still allow
    the floor area to be considered traversable.
    minTraversableHeight should be at least two times the value of
    cellHeight in order to get good results. Max spatial height.
    Constraints > 0, default=2.0

Max traversable step:
    Represents the maximum ledge height that is considered to still be
    traversable.
    maxTraversableStep should be greater than two times cellHeight.
    Constraints >= 0, default=0.3

Max traversable slope:
    The maximum slope that is considered traversable. (In degrees.)
    Constraints >= 0, default=50

Clip ledges:
    Indicates whether ledges should be considered un-walkable.
    Constraints None, default=false

Traversable area border size:
    Represents the closest any part of a mesh can get to an obstruction in
    the source geometry.
    traversableAreaBorderSize value must be greater than the cellSize to
    have an effect. Radius of the spatial.
    Constraints >= 0, default=0.4

Smoothing threshold:
    The amount of smoothing to be performed when generating the distance
    field used for deriving regions.
    Constraints >= 0, default=1

Use conservative expansion:
    Applies extra algorithms to help prevent malformed regions from
    forming.
    Constraints None, default=true

Min unconnected region size:
    The minimum region size for unconnected (island) regions.
    Constraints > 0, default=8

Merge region size:
    Any regions smaller than this size will, if possible, be merged with
    larger regions.
    Constraints >= 0, default=20

Max edge length:
    The maximum length of polygon edges that represent the border of
    meshes. setTraversableAreaBorderSize * 8
    Constraints >= 0, default=4.0

Edge max deviation:
    The maximum distance the edges of meshes may deviate from the source
    geometry.
    1.1 to 1.5 for best results.
    Constraints >= 0 , default=1.3

Max verts per poly:
    The maximum number of vertices per polygon for polygons generated
    during the voxel to polygon conversion process.
    Constraints >= 3, default=6

Contour sample distance:
    Sets the sampling distance to use when matching the detail mesh to the
    surface of the original geometry.
    Constraints >= 0, default=10.0

Contour max deviation:
    The maximum distance the surface of the detail mesh may deviate from
    the surface of the original geometry.
    Constraints >= 0, default=5.0

Timeout:
    Time allowed before generation process times out in milliseconds.
    default=60000
