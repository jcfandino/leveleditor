package com.stovokor.editor.spec

import com.stovokor.editor.model.Point
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Polygon
import com.stovokor.util.PointClicked
import com.stovokor.util.EventBus
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.model.Line
import com.stovokor.util.PointDragged
import com.stovokor.editor.builder.SectorBuilder

trait TestSectorSupport {

  def sectorDefinedByPoints(points: Point*) = {
    val polygon = Polygon(points.toList)
    SectorRepository()
      .findByPoint(points.head)
      .exists({ case (id, sec) => polygon == sec.polygon })
  }

  def borderDefinedByPoints(points: Point*) = {
    val found = points
      .sliding(2)
      .map(ps => Line((ps(0), ps(1))))
      .map(BorderRepository().find)
      .toList
    val allLinesFound = found.forall(!_.isEmpty)
    val sectors = found
      .flatMap(l => l)
      .flatMap(b => List(b._2.sectorA, b._2.sectorB))
      .toSet
      .size
    allLinesFound && sectors > 1
  }

  def borderBetweenSectors(pointSectorA: Point, pointSectorB: Point) = {
    val resultA = SectorRepository().findInside(pointSectorA)
    val resultB = SectorRepository().findInside(pointSectorB)
    if (!resultA.isEmpty && !resultB.isEmpty) {
      val (idA, sectorA) = resultA.head
      val (idB, sectorB) = resultB.head
      BorderRepository().findFrom(idA).map(_._2).exists(_.sectorB == idB)
    } else false
  }

  def holeDefinedByPoints(points: Point*) = {
    val polygon = Polygon(points.toList)
    SectorRepository()
      .findByPoint(points.head)
      .exists({ case (id, sec) => sec.holes.contains(polygon) })
  }

  def makeClicks(points: Point*): Unit = {
    points.foreach(p => EventBus.trigger(PointClicked(p)))
  }

  def drag(from: Point, to: Point): Unit = {
    EventBus.trigger(PointDragged(from, to))
  }
}
