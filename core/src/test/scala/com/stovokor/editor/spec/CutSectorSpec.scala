package com.stovokor.editor.spec

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._
import org.mockito.Mockito._
import scala.io.Source
import com.stovokor.editor.state.DrawingState
import com.jme3.app.state.AppStateManager
import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.input.InputMapper
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.editor.model.Point
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.repository.BorderRepository

class CutSectorSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen
    with TestSectorSupport {

  val drawingState = new DrawingState
  val stateManager = mock[AppStateManager]
  val app = mock[SimpleApplication]
  val eventListener = mock[EditorEventListener]
  val sectorRepository = SectorRepository()

  override def beforeEach(): Unit = {
    GuiGlobals.setInstance(mock[GuiGlobals])
    when(GuiGlobals.getInstance.getInputMapper).thenReturn(mock[InputMapper])
    when(app.getRootNode).thenReturn(new Node)
    MaterialFactory.setInstance(mock[MaterialFactory])
    drawingState.initialize(stateManager, app)
  }

  override def afterEach(): Unit = {
    drawingState.cleanup()
    SectorRepository().removeAll
    BorderRepository().removeAll
  }

  behavior of "Cut sector (sub-divide)"

  /*
   *   D-------C----------F
   *   |       |\         |
   *   |       | \        |
   *   |       |  \       |
   *   |       |   G      |
   *   |       |  /       |
   *   |       | /        |
   *   |       |/         |
   *  øX-------B----------E
   *
   */
  val (d, c, f, g, x, b, e) = (
    Point(0, 2),
    Point(1, 2),
    Point(3, 2),
    Point(2, 1),
    Point(0, 0),
    Point(1, 0),
    Point(3, 0)
  )

  it should "be able to sub-divide a sector through an inner border" in {
    Given("A sector on the left")
    makeClicks(x, b, c, d, x)

    And("A bordering sector to the right")
    makeClicks(b, e, f, c)
    assert(sectorDefinedByPoints(x, b, c, d))

    assert(sectorDefinedByPoints(b, e, f, c))
    assert(borderDefinedByPoints(b, c))

    When("An inner path is drawn to divide the sector on the right")
    makeClicks(b, g, c, b)

    Then("The sector on the left should be preserved")
    assert(sectorDefinedByPoints(x, b, c, d))

    And("The sector on the right should be divided")
    assert(sectorDefinedByPoints(b, e, f, c, g))

    And("A new sector added between them")
    assert(sectorDefinedByPoints(b, g, c))

    And("Borders connecting them")
    assert(borderDefinedByPoints(b, c))
    assert(borderDefinedByPoints(b, g, c))
  }

}
