package com.stovokor.editor.spec

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._

import org.mockito.Mockito._
import scala.io.Source
import org.mockito.ArgumentMatchers._
import com.stovokor.editor.state.DrawingState
import com.jme3.app.state.AppStateManager
import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.input.InputMapper
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.editor.model.Point
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.state.ModifyingState
import com.stovokor.editor.builder.SectorBuilder
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.SelectionChange
import com.stovokor.editor.model.SelectionPoint
import com.stovokor.util.SplitSelection
import com.stovokor.editor.service.LineService
import com.stovokor.editor.state.EditLineState
import com.stovokor.editor.state.Parameters
import com.simsilica.lemur.style.Styles
import com.jme3.renderer.Camera
import com.jme3.material.Material
import com.jme3.math.ColorRGBA
import com.simsilica.lemur.core.GuiMaterial
import com.jme3.material.RenderState

class EditLineStateSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen
    with TestSectorSupport {

  val stateManager = mock[AppStateManager]
  val app = mock[SimpleApplication]
  val eventListener = mock[EditorEventListener]
  val sectorRepository = SectorRepository()

  val drawingState = new DrawingState
  val lineService = LineService()

  def initState(line: Line, params: Parameters) = {
    val state = new EditLineState(line, params, false)
    state.initialize(stateManager, app)
    state
  }

  override def beforeEach(): Unit = {
    GuiGlobals.setInstance(mock[GuiGlobals])
    when(GuiGlobals.getInstance.getInputMapper).thenReturn(mock[InputMapper])
    when(app.getRootNode).thenReturn(new Node)
    when(app.getStateManager).thenReturn(stateManager)
    MaterialFactory.setInstance(mock[MaterialFactory])
    drawingState.initialize(stateManager, app)
  }

  override def afterEach(): Unit = {
    drawingState.cleanup()
    SectorRepository().removeAll
    BorderRepository().removeAll
  }

  behavior of "Line edit state"

  /*
   *   F---------------G
   *   |               |
   *   |               |
   *   |               |
   *   D-------E-------C
   *   |               |
   *   |               |
   *   |               |
   *   X-------ø-------B
   *
   */
  val (f, g, d, e, c, x, b) = (
    Point(-1, 2),
    Point(1, 2),
    Point(-1, 1),
    Point(0, 1),
    Point(1, 1),
    Point(-1, 0),
    Point(1, 0)
  )

  it should "be able to split a closed wall in two segments" in {
    Given("A simple square sector")
    makeClicks(x, b, g, f, x)

    When("A line is split")
    var params = Parameters()
    val state = initState(Line(f, x), params)
    state.applyChanges()

    Then("The sector exists and it was modified")
    assert(sectorDefinedByPoints(x, b, g, f, d))
  }

  it should "be able to split a closed wall in four segments" in {
    Given("A simple square sector")
    makeClicks(x, b, g, f, x)

    When("A line is split")
    var params = Parameters()
    params.incSegments()
    params.incSegments()
    val state = initState(Line(f, x), params)
    state.applyChanges()

    Then("The sector exists and it was modified")
    val p1 = Point(x.x, 0.5f)
    val p2 = Point(x.x, 1.5f)
    assert(sectorDefinedByPoints(x, p1, d, p2, f, g, b))
  }

  it should "be able to split a closed wall in low left side" in {
    Given("A simple square sector")
    makeClicks(f, g, b, f)

    When("A line is split")
    var params = Parameters()
    params.incSegments()
    val state = initState(Line(f, b), params)
    state.applyChanges()

    Then("The sector exists and it was modified")
    val p1 = Point(-0.333, 1.333)
    val p2 = Point(0.333, 0.667)
    assert(sectorDefinedByPoints(f, p1, p2, b, g))
  }

  it should "be able to split a closed wall in low left side 2" in {
    Given("A simple square sector")
    makeClicks(f, g, b, f)

    When("A line is split")
    var params = Parameters()
    params.incSegments()
    val state = initState(Line(b, f), params)
    state.applyChanges()

    Then("The sector exists and it was modified")
    val p1 = Point(-0.333, 1.333)
    val p2 = Point(0.333, 0.667)
    assert(sectorDefinedByPoints(f, p1, p2, b, g))
  }
}
