package com.stovokor.editor.spec

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._

import org.mockito.Mockito._
import scala.io.Source
import com.stovokor.editor.state.DrawingState
import com.jme3.app.state.AppStateManager
import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.input.InputMapper
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.editor.model.Point
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.repository.BorderRepository

class DrawSectorSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen
    with TestSectorSupport {

  val drawingState = new DrawingState
  val stateManager = mock[AppStateManager]
  val app = mock[SimpleApplication]
  val eventListener = mock[EditorEventListener]
  val sectorRepository = SectorRepository()

  override def beforeEach(): Unit = {
    GuiGlobals.setInstance(mock[GuiGlobals])
    when(GuiGlobals.getInstance.getInputMapper).thenReturn(mock[InputMapper])
    when(app.getRootNode).thenReturn(new Node)
    MaterialFactory.setInstance(mock[MaterialFactory])
    drawingState.initialize(stateManager, app)
  }

  override def afterEach(): Unit = {
    drawingState.cleanup()
    SectorRepository().removeAll
    BorderRepository().removeAll
  }

  behavior of "Drawing state"

  /*
   *   J----------------------K
   *   |                      |
   *   D-------C-------F----N-I
   *   |     / |       |      |
   *   |   ø   |       |      |
   *   | /     |       |      |
   *   X-------B-------E----P-H
   *   |                      |
   *   L----------------------M
   *
   */
  val (
    j,
    k,
    d,
    c,
    f,
    n,
    i,
    x,
    b,
    e,
    p,
    h,
    l,
    m
  ) = (
    Point(-1, 2),
    Point(3, 2),
    Point(-1, 1),
    Point(1, 1),
    Point(2, 1),
    Point(2.5, 1),
    Point(3, 1),
    Point(-1, -1),
    Point(1, -1),
    Point(2, -1),
    Point(2.5, -1),
    Point(3, -1),
    Point(-1, -2),
    Point(3, -2)
  )

  it should "be able to draw a square room" in {
    Given("An empty space")

    When("Four points are drawn")
    makeClicks(x, b, c, d, x)

    Then("A new sector should be saved")
    assert(sectorDefinedByPoints(x, b, c, d))
  }

  it should "be able to divide a square room in two triangles" in {
    Given("A squared sector is drawn")
    makeClicks(x, b, c, d, x)

    When("A new line is drawn between opposed corners")
    makeClicks(x, c)

    Then("A two new sectors should be saved")
    assert(sectorDefinedByPoints(x, b, c))
    assert(sectorDefinedByPoints(c, d, x))
    And("One border should connect them")
    assert(borderDefinedByPoints(c, x))
  }

  it should "be able to extend a square room to a side (up-bottom)" in {
    Given("A squared sector is drawn")
    makeClicks(x, b, c, d, x)

    When("New lines are drawn continuing the sector to the right")
    makeClicks(c, f, e, b)

    Then("Two new sectors should be saved")
    assert(sectorDefinedByPoints(x, b, c, d))
    assert(sectorDefinedByPoints(c, f, e, b))
    And("One border should connect them")
    assert(borderDefinedByPoints(c, b))
  }

  it should "be able to extend a square room to a side (bottom-up)" in {
    Given("A squared sector is drawn")
    makeClicks(x, b, c, d, x)

    When("New lines are drawn continuing the sector to the right")
    makeClicks(b, e, f, c)

    Then("Two new sectors should be saved")
    assert(sectorDefinedByPoints(x, b, c, d))
    assert(sectorDefinedByPoints(c, f, e, b))
    And("One border should connect them")
    assert(borderDefinedByPoints(c, b))
  }

  it should "be able connect two isolated sectors" in {
    Given("Two separated squared sectors are drawn")
    makeClicks(x, b, c, d, x)
    makeClicks(e, h, i, f, e)

    When("New lines are drawn connecting both sectors")
    makeClicks(b, e, f, c)

    Then("Three new sectors should be saved")
    assert(sectorDefinedByPoints(x, b, c, d))
    assert(sectorDefinedByPoints(e, h, i, f))
    assert(sectorDefinedByPoints(b, e, f, c))
    And("Two borders should connect them")
    assert(borderDefinedByPoints(c, b))
    assert(borderDefinedByPoints(f, e))
  }

  it should "be able to draw a horseshoe with stairs" in {
    Given("a horseshoe shaped sector")
    makeClicks(j, k, i, f, c, b, e, h, m, l, j)

    And("a sector in the opening")
    makeClicks(c, f, e, b, c)

    When("another sector is adding in the opening")
    makeClicks(i, h)

    Then("three sectors should exist")
    assert(sectorDefinedByPoints(j, k, i, f, c, b, e, h, m, l))
    assert(sectorDefinedByPoints(c, f, e, b))
    assert(sectorDefinedByPoints(f, i, h, e))
    And("borders should connect them")
    assert(borderDefinedByPoints(e, b, c, f))
    assert(borderDefinedByPoints(h, e))
    assert(borderDefinedByPoints(f, i))
    assert(borderDefinedByPoints(e, f))
  }

  it should "be able to divide a sector with a hole" in {
    Given("A big sector")
    makeClicks(j, k, i, h, m, l, j)

    And("A subsector inside it")
    makeClicks(c, f, b, c)

    When("dividing the big sector without touching the subsector")
    makeClicks(i, e, h, i)

    Then("a new sector should exist")
    assert(sectorDefinedByPoints(i, e, h))
    And("the big sector is changed")
    assert(!sectorDefinedByPoints(j, k, i, h, m, l))
    assert(sectorDefinedByPoints(j, k, i, e, h, m, l))
    And("the hole in is maintained")
    assert(holeDefinedByPoints(c, f, b))
  }

  it should "be able to draw a sector connecting an empty hole and a wall" in {
    Given("A big sector")
    makeClicks(j, k, i, h, m, l, j)

    And("A column inside it")
    makeClicks(c, f, e, b, c)

    When("drawing a sector between the column and an outer wall")
    makeClicks(f, i, h, e, f)

    Then("a new sector should exist")
    assert(sectorDefinedByPoints(f, i, h, e))
    And("the big sector is changed")
    assert(!sectorDefinedByPoints(j, k, i, h, m, l))
    assert(sectorDefinedByPoints(j, k, i, f, c, b, e, h, m, l))
    And("the hole in the middle becomes a sub-sector")
    assert(sectorDefinedByPoints(c, f, e, b))
  }

  it should "be able to draw a subsector next to an empty hole" in {
    Given("A big sector")
    makeClicks(j, k, i, h, m, l, j)

    And("A column inside it")
    makeClicks(c, f, e, b, c)

    When("drawing a sub-sector next the column")
    makeClicks(f, n, p, e, f)

    Then("a new sector should exist")
    assert(sectorDefinedByPoints(f, n, p, e))
    And("a hole is added to the big sector")
    assert(sectorDefinedByPoints(f, n, p, e))
  }
}
