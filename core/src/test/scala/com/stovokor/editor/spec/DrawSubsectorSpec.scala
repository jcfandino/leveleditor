package com.stovokor.editor.spec

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._

import org.mockito.Mockito._
import scala.io.Source
import com.stovokor.editor.state.DrawingState
import com.jme3.app.state.AppStateManager
import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.input.InputMapper
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.editor.model.Point
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.util.GridSnapper
import scala.concurrent.duration.DurationInt
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.concurrent.Signaler

class DrawSubsectorSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen
    with TestSectorSupport
    with TimeLimitedTests {

  val drawingState = new DrawingState
  val stateManager = mock[AppStateManager]
  val app = mock[SimpleApplication]
  val eventListener = mock[EditorEventListener]
  val sectorRepository = SectorRepository()

  override def beforeEach(): Unit = {
    GuiGlobals.setInstance(mock[GuiGlobals])
    when(GuiGlobals.getInstance.getInputMapper).thenReturn(mock[InputMapper])
    when(app.getRootNode).thenReturn(new Node)
    MaterialFactory.setInstance(mock[MaterialFactory])
    drawingState.initialize(stateManager, app)
    drawingState.setEnabled(true)
  }

  override def afterEach(): Unit = {
    drawingState.cleanup()
    SectorRepository().removeAll
    BorderRepository().removeAll
  }

  behavior of "Draw subsectors"

  /*
   * 3  M-------N-------O-------P-------U
   *    |                       |       |
   *    |                       |       |
   *    |                       |       |
   * 2  J       K-------G-------L-------V
   *    |       | T---S | \     |       |
   *    |       | |   | |   \   |       |
   *    |       | Q---R |     \ |       |
   * 1  D       C-------F-------I-------W
   *    |                       |       |
   * 0  |   ø                   |       |
   *    |                       |       |
   * -1 X-------B-------E-------H-------Y
   *
   *   -1   0   1       2       3       4
   */
  val (m, n, o, p, u) =
    (Point(-1, 3), Point(1, 3), Point(2, 3), Point(3, 3), Point(4, 3))
  val (j, k, g, l, v) =
    (Point(-1, 2), Point(1, 2), Point(2, 2), Point(3, 2), Point(4, 2))
  val (d, c, f, i, w) =
    (Point(-1, 1), Point(1, 1), Point(2, 1), Point(3, 1), Point(4, 1))
  val (x, b, e, h, y) =
    (Point(-1, -1), Point(1, -1), Point(2, -1), Point(3, -1), Point(4, -1))
  val (q, r, s, t) =
    (Point(1.2, 1.2), Point(1.8, 1.2), Point(1.8, 1.8), Point(1.2, 1.8))

  // this is to detect infinite loops
  def timeLimit = 2.seconds

  override val defaultTestSignaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Test Timeout!")
      testThread.interrupt()
    }
  }

  it should "be able to draw a sub-sector" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    When("Another square is drawn inside it")
    makeClicks(c, f, g, k, c)

    Then("There should be one sector with a sub-sector in the middle")
    assert(sectorDefinedByPoints(x, h, p, m))
    assert(holeDefinedByPoints(c, f, g, k))
  }

  it should "Be able to draw a hole and convert to a inner sector" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    When("Another square is drawn inside it")
    makeClicks(c, f, g, k, c)

    Then("There should be one sector with a hole in the middle")
    assert(sectorDefinedByPoints(x, h, p, m))
    assert(holeDefinedByPoints(c, f, g, k))

    And("another sector filling the hole")
    assert(sectorDefinedByPoints(c, f, g, k))

    And("borders connecting both sectors")
    assert(borderDefinedByPoints(c, f, g, k))
    assert(borderBetweenSectors(corner(x), corner(c)))
  }

  it should "Be able to draw inner sectors recursively" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    And("An inner square is drawn inside it")
    makeClicks(c, f, g, k, c)

    When("A hole is drawn inside the sub-sector")
    makeClicks(q, r, s, t, q)

    Then("There should be a big sector with a sub-sector")
    assert(sectorDefinedByPoints(x, h, p, m))
    assert(holeDefinedByPoints(c, f, g, k))

    And("another sector filling the hole, also with a hole")
    assert(sectorDefinedByPoints(c, f, g, k))
    assert(borderDefinedByPoints(c, f, g, k))
    assert(holeDefinedByPoints(q, r, s, t))

    And("yet another sector filling the small hole")
    assert(sectorDefinedByPoints(q, r, s, t))

    And("borders connecting the inner sector")
    assert(borderDefinedByPoints(q, r, s, t))
  }

  it should "Be able to extend a sector with a sub-sector in it" in {
    Given("A big squred sector")
    makeClicks(c, f, g, k, c)

    And("A sub-sector is drawn inside it")
    makeClicks(q, r, s, t, q)

    When("I extend the big sector to a side")
    makeClicks(f, i, l, g)

    Then("There should be a big sector with a sub-sector")
    assert(sectorDefinedByPoints(c, f, g, k))
    assert(holeDefinedByPoints(q, r, s, t))

    And("another sector filling the hole")
    assert(sectorDefinedByPoints(q, r, s, t))
    assert(borderDefinedByPoints(q, r, s, t))

    And("another sector to the right of the big one")
    assert(sectorDefinedByPoints(f, i, l, g))
    assert(borderDefinedByPoints(f, g))
  }

  it should "Be able to draw two holes next to each other" in {
    Given("A big squared sector")
    makeClicks(x, y, u, m, x)

    And("Another square is drawn inside it")
    makeClicks(c, f, g, k, c)

    When("Another square is drawn next to the sub-sector")
    makeClicks(f, i, l, g, f)

    Then("There should be a big sector")
    assert(sectorDefinedByPoints(x, y, u, m))

    And("There should be one hole spanning both sub-sectors")
    assert(holeDefinedByPoints(c, f, i, l, g, k))
  }

  it should "Be able to draw a hole next to another and create borders" in {
    Given("A big squared sector")
    makeClicks(x, y, u, m, x)
    assert(sectorDefinedByPoints(x, y, u, m))

    And("Another square is drawn inside it")
    makeClicks(c, f, g, k, c)
    assert(holeDefinedByPoints(c, f, g, k))

    When("Another square is drawn next to the sub-sector")
    makeClicks(f, i, l, g, f)
    assert(holeDefinedByPoints(f, i, l, g, k, c))

    Then("There should be two subsectors")
    assert(sectorDefinedByPoints(c, f, g, k))
    assert(sectorDefinedByPoints(f, i, l, g))

    And("These subsectors have borders")
    assert(borderDefinedByPoints(c, f, g, k))
    assert(borderDefinedByPoints(f, i, l, g))
  }

  it should "Be able to draw two separate sub-sectors" in {
    Given("A big squared sector")
    makeClicks(x, y, u, m, x)
    assert(sectorDefinedByPoints(x, y, u, m))

    And("Another square is drawn inside it")
    makeClicks(q, r, s, t, q)

    When("Another square is drawn without any common points")
    makeClicks(f, i, l, g, f)

    Then("There should be a big sector with two holes")
    assert(sectorDefinedByPoints(x, y, u, m))
    assert(holeDefinedByPoints(q, r, s, t))
    assert(holeDefinedByPoints(f, i, l, g))

    And("Two subsectors")
    assert(sectorDefinedByPoints(q, r, s, t))
    assert(sectorDefinedByPoints(f, i, l, g))

    And("These subsectors have borders")
    assert(borderDefinedByPoints(q, r, s, t))
    assert(borderDefinedByPoints(f, i, l, g))
  }

  it should "Be able to draw three sub-sectors in a row" in {
    Given("A big squared sector")
    makeClicks(x, y, u, m, x)
    assert(sectorDefinedByPoints(x, y, u, m))

    And("The first sub-sector is drawn")
    makeClicks(c, q, t, k, c)

    And("The second sub-sector is drawn")
    makeClicks(q, r, s, t, q)

    When("The third sub-sector is drawn")
    makeClicks(r, f, g, s, r)
    assert(sectorDefinedByPoints(r, f, g, s))

    Then("There should be a big sector")
    assert(sectorDefinedByPoints(x, y, u, m))

    And("Three subsectors")
    assert(sectorDefinedByPoints(c, q, t, k))
    assert(sectorDefinedByPoints(q, r, s, t))
    assert(sectorDefinedByPoints(r, f, g, s))

    And("These subsectors have borders")
    assert(borderDefinedByPoints(c, q, t, k))
    assert(borderDefinedByPoints(q, r, s, t))
    assert(borderDefinedByPoints(r, f, g, s))
  }

  it should "Be able to divide a subsector" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    And("An inner square is drawn inside it")
    makeClicks(c, f, g, k, c)

    When("The subsector is divided")
    makeClicks(c, q, g, f, c)

    Then("There should be a big sector with a sub-sector")
    assert(sectorDefinedByPoints(x, h, p, m))
    assert(holeDefinedByPoints(c, f, g, k))

    And("two sub-sectors inside the hole")
    assert(sectorDefinedByPoints(c, q, g, f))
    assert(borderDefinedByPoints(c, k, g, q))

    And("borders connecting the inner sectors")
    assert(borderDefinedByPoints(c, q, g))
    assert(borderDefinedByPoints(c, k, g, f))
  }

  it should "Be able to divide a subsector connected to another" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    And("An inner square is drawn inside it")
    makeClicks(c, f, g, k, c)

    And("Another inner square is drawn next to it")
    makeClicks(f, i, l, g, f)

    When("The second subsector is divided")
    makeClicks(g, i, l, g)

    Then("There should be a big sector with sub-sectors")
    assert(sectorDefinedByPoints(x, h, p, m))

    And("three sub-sectors inside")
    assert(sectorDefinedByPoints(c, f, g, k))
    assert(sectorDefinedByPoints(f, i, g))
    assert(sectorDefinedByPoints(g, i, l))

    // It doesn't find all the lines, but I see them when testing manually?
    // assert(borderDefinedByPoints(c, k, g, l, i, f, c))
    assert(borderDefinedByPoints(f, c, k))
    assert(borderDefinedByPoints(g, k))
    assert(borderDefinedByPoints(f, c))

    And("borders connecting the inner sectors")
    assert(borderDefinedByPoints(g, f))
    assert(borderDefinedByPoints(g, i))
  }

  it should "Be able to connect multiple subsectors to wall" in {
    Given("A big squared sector")
    makeClicks(x, y, w, v, u, m, x)

    And("Three subsectors are drawn next to each other")
    makeClicks(q, r, s, t, q)
    makeClicks(r, f, g, s, r)
    makeClicks(f, i, l, g, f)

    When("The last subsector is connected to the wall")
    makeClicks(i, w, v, l, i)

    Then("The three sub-sectors are kept")
    assert(sectorDefinedByPoints(q, r, s, t))
    assert(sectorDefinedByPoints(r, f, g, s))
    assert(sectorDefinedByPoints(f, i, l, g))

    assert(borderDefinedByPoints(q, r, s, t))
    assert(borderDefinedByPoints(r, f, g, s))
    assert(borderDefinedByPoints(f, i, l, g))

    And("borders connecting the subsector to the side wall")
    assert(borderDefinedByPoints(l, v))
    assert(borderDefinedByPoints(i, w))
  }

  it should "Be able to combine subsector and cut sector features" in {
    Given("A big squared sector")
    makeClicks(x, h, p, m, x)

    And("Two subsectors are drawn next to each other")
    makeClicks(q, r, s, t, q)
    makeClicks(r, f, g, s, r)

    When("The big sector wall is extended to the right")
    makeClicks(h, y, u, p)

    Then("The sub-sectors are kept")
    assert(sectorDefinedByPoints(q, r, s, t))
    assert(sectorDefinedByPoints(r, f, g, s))

    assert(borderDefinedByPoints(q, r, s, t))
    assert(borderDefinedByPoints(r, f, g, s))

    And("The two big sectors are kept")
    assert(sectorDefinedByPoints(x, h, p, m))
    assert(sectorDefinedByPoints(h, y, u, p))

    And("a border connecting the big sectors")
    assert(borderDefinedByPoints(h, p))
  }

  def corner(point: Point) = point.move(.05f, .1f)

}
