package com.stovokor.editor.spec

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._

import org.mockito.Mockito._
import scala.io.Source
import com.stovokor.editor.state.DrawingState
import com.jme3.app.state.AppStateManager
import com.jme3.app.SimpleApplication
import com.jme3.scene.Node
import com.simsilica.lemur.GuiGlobals
import com.simsilica.lemur.input.InputMapper
import com.stovokor.util.EditorEventListener
import com.stovokor.util.EditorEvent
import com.stovokor.util.EventBus
import com.stovokor.util.PointClicked
import com.stovokor.editor.model.Point
import com.stovokor.util.SectorUpdated
import com.stovokor.editor.model.repository.SectorRepository
import com.stovokor.editor.model.Line
import com.stovokor.editor.factory.MaterialFactory
import com.stovokor.editor.model.repository.BorderRepository
import com.stovokor.editor.state.ModifyingState
import com.stovokor.editor.builder.SectorBuilder
import com.stovokor.editor.model.repository.Repositories
import com.stovokor.util.SelectionModeSwitch
import com.stovokor.util.EditModeSwitch
import com.stovokor.util.SelectionChange
import com.stovokor.editor.model.SelectionPoint
import com.stovokor.util.SplitSelection
import com.stovokor.editor.service.LineService

class LineServiceSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen
    with TestSectorSupport {

  val drawingState = new DrawingState
  val lineService = LineService()

  val stateManager = mock[AppStateManager]
  val app = mock[SimpleApplication]
  val eventListener = mock[EditorEventListener]
  val sectorRepository = SectorRepository()

  override def beforeEach(): Unit = {
    GuiGlobals.setInstance(mock[GuiGlobals])
    when(GuiGlobals.getInstance.getInputMapper).thenReturn(mock[InputMapper])
    when(app.getRootNode).thenReturn(new Node)
    MaterialFactory.setInstance(mock[MaterialFactory])
    drawingState.initialize(stateManager, app)
  }

  override def afterEach(): Unit = {
    drawingState.cleanup()
    SectorRepository().removeAll
    BorderRepository().removeAll
  }

  behavior of "Line service"

  /*
   *   F---------------G
   *   |               |
   *   |               |
   *   |               |
   *   D-------E-------C
   *   |               |
   *   |               |
   *   |               |
   *   X-------ø-------B
   *
   */
  val (f, g, d, e, c, x, b) = (
    Point(-1, 2),
    Point(1, 2),
    Point(-1, 1),
    Point(0, 1),
    Point(1, 1),
    Point(-1, 0),
    Point(1, 0)
  )

  it should "be able to split a closed wall in half" in {
    Given("A simple square sector")
    makeClicks(x, b, c, d, x)

    When("A line is split")
    lineService.replaceLine(Line(c, d), List(Line(c, e), Line(e, d)))

    Then("The sector exists and it was modified")
    assert(sectorDefinedByPoints(x, b, c, e, d))
  }

  it should "be able to split a border wall in half" in {
    Given("A square sector")
    makeClicks(x, b, c, d, x)

    And("and another sector next to it")
    makeClicks(c, g, f, d)
    assert(borderDefinedByPoints(c, d))
    assert(borderDefinedByPoints(d, c))

    When("A line between them is split")
    lineService.replaceLine(Line(c, d), List(Line(c, e), Line(e, d)))

    Then("The first sector exists and it was modified")
    assert(sectorDefinedByPoints(x, b, c, e, d))
    assert(!sectorDefinedByPoints(x, b, c, d))

    And("The second sector exists and it was modified")
    assert(sectorDefinedByPoints(c, g, f, d, e))
    assert(!sectorDefinedByPoints(c, g, f, d))

    And("There are borders between both sectors")
    assert(borderDefinedByPoints(d, e, c))
    assert(borderDefinedByPoints(c, e, d))

    And("The old border is removed")
    assert(!borderDefinedByPoints(c, d))
    assert(!borderDefinedByPoints(d, c))
  }
}
