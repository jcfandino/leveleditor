package com.stovokor.editor.model

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest._

import org.mockito.Mockito._
class SectorSpec
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with GivenWhenThen {

  override def beforeEach(): Unit = {}

  override def afterEach(): Unit = {}

  behavior of "Polygon"

  /*
   *   D-------C-------F------I
   *   |     / |       |      |
   *   |   ø   |       |      |
   *   | /     |       |      |
   *   X-------B-------E------H
   *
   */
  val (d, c, f, i, x, b, e, h) = (
    Point(-1, 1),
    Point(1, 1),
    Point(2, 1),
    Point(3, 1),
    Point(-1, -1),
    Point(1, -1),
    Point(2, -1),
    Point(3, -1)
  )

  it should "be able to merge two polygons with a common border" in {
    Given("A polygon")
    var polygonA = Polygon(List(x, b, c, d))

    And("Another polygon")
    var polygonB = Polygon(List(b, e, f, c))

    When("Both are merged")
    var merged = polygonA.merge(polygonB)

    Then("It should result in a single polygon")
    assert(merged.size == 1)
    assert(merged.head == Polygon(List(x, b, e, f, c, d)))
  }

  it should "be able to merge three polygons with a common border" in {
    Given("A polygon")
    var polygonA = Polygon(List(x, b, c, d))

    And("Another polygon")
    var polygonB = Polygon(List(b, e, f, c))

    And("Another polygon")
    var polygonC = Polygon(List(e, h, i, f))

    When("Both are merged")
    var mergedAB = polygonA.merge(polygonB)
    assert(mergedAB.size == 1)
    var mergedABC = mergedAB.head.merge(polygonC)

    Then("It should result in a single polygon")
    assert(mergedABC.size == 1)
    assert(mergedABC.head == Polygon(List(x, b, e, h, i, f, c, d)))
  }

  it should "not merge two polyogns without a common border" in {
    Given("A polygon")
    var polygonA = Polygon(List(x, b, c, d))

    And("Another polygon")
    var polygonB = Polygon(List(e, h, i, f))

    When("Trying to merge them")
    var merged = polygonA.merge(polygonB)

    Then("It should result in the same two polygons")
    assert(merged.size == 2)
    assert(merged.contains(polygonA))
    assert(merged.contains(polygonB))
  }
}
